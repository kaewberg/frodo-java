package se.pp.forsberg.frodo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import se.pp.forsberg.frodo.enums.ROMCall;

public class TestCpu {

	Cpu cpu;
	byte[] ram;
	static byte[] basic;
	static byte[] kernal;

	final static int PRG = 0x8000;

	@BeforeAll
	public static void init() {
		kernal = new byte[0x2000];
		basic = new byte[0x2000];
		Frodo.loadKernal(kernal);
		Frodo.loadBasic(basic);
	}

	@BeforeEach
	public void setup() {
		ram = new byte[0x10000];
		cpu = new Cpu(ram);
	}

	private void reset() {
		cpu.reset();
	}

	@Test
	public void testAdc() {
		boolean[] ft = { false, true };
		for (int a = 0; a < 256; a++) {
//				§.println(a);
			for (int d = 0; d < 256; d++) {
				for (boolean carry : ft) {
					for (boolean decimal : ft) {
						boolean c, v;
						int sum;
						if (decimal) {
							continue;
						} else {
							sum = ((byte) a) + ((byte) d) + (carry ? 1 : 0);
							v = sum < -128 || sum > 127;
							sum = a + d + (carry ? 1 : 0);
							c = sum > 255;
							sum %= 256;
						}
						reset();
						int pc = PRG;
						ram[pc++] = (byte) Instruction.ADC_IMM.ordinal();
						ram[pc++] = (byte) d;
						cpu.a = (byte) a;
						if (carry) {
							cpu.c_flag = true;
						}
						if (decimal) {
							cpu.d_flag = true;
						}
						do {
							cpu.commonOps(false);
						} while (cpu.state != 0);
						asrt(pc, getNVbsDiZC(c, v, decimal, (byte) sum), sum);
					}
				}
			}
		}
		reset();
		int pc = PRG;
		int adr = 0x8ff;
		int x, y;
		int a = 127, d = 1, unsigned = 128;

		ram[pc++] = (byte) Instruction.ADC_ZP.ordinal();
		ram[pc++] = (byte) adr;
		ram[adr & 0xff] = (byte) d;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);

		x = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ADC_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ADC_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);

		x = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ADC_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);
		y = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ADC_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);

		x = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ADC_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);

		y = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ADC_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(false, true, false, (byte) unsigned), unsigned);

	}

	@Test
	public void testAdcProblem() {
		// During init screen pointers:
//CPU [a:f0 x:06 y:84 ar:00df ar2:00cf, sp:fb, NVBsDIZC:10100100] e54f: 18         CLC                 Stack: [5d, ff, fd, fc]
//CPU [a:f0 x:06 y:84 ar:00df ar2:00cf, sp:fb, NVBsDIZC:10100100] e550: 69 28      ADC #$28            Stack: [5d, ff, fd, fc]
//CPU [a:18 x:06 y:84 ar:00df ar2:00cf, sp:fb, NVBsDIZC:00100100] e552: 90 01      BCC $e555           Stack: [5d, ff, fd, fc]
//CPU [a:18 x:06 y:84 ar:e555 ar2:00cf, sp:fb, NVBsDIZC:00100100] e555: e8         INX                 Stack: [5d, ff, fd, fc]

		// In other words:
		// a = 0xf0
		// ADC #28 = 18
		// Carry clear! Wrongo!

		// Hmm, can't reproduce in testcpu!

		MOS6510 cpu = new MOS6510(null, ram, null, null, null, null);
//		cpu.setDebug(true);
//		cpu.setTrace(true);
		do {
			cpu.EmulateCycle();
		} while (cpu.state != 0);
		int pc = PRG;
		cpu.pc = pc;
		cpu.a = (byte) 0xf0;
		ram[pc++] = (byte) Instruction.ADC_IMM.ordinal();
		ram[pc++] = 0x28;
		do {
			cpu.EmulateCycle();
		} while (cpu.state != 0);
		// Fix only made in test cpu...
		assertEquals(0x18, cpu.a & 0xff);
		assertEquals(true, cpu.c_flag);
	}

	@Test
	public void testSbc() {
		boolean[] ft = { false, true };
		for (int a = 0; a < 256; a++) {
//				System.out.println(a);
			for (int d = 0; d < 256; d++) {
				for (boolean carry : ft) {
					for (boolean decimal : ft) {
						boolean c, v;
						int sum;
						if (decimal) {
							continue;
						} else {
							sum = ((byte) a) - ((byte) d) - (carry ? 0 : 1);
							v = sum < -128 || sum > 127;
							sum = a - d - (carry ? 0 : 1) & 0xffff;
							c = sum <= 255;
							sum &= 0xff;
						}
						reset();
						int pc = PRG;
						ram[pc++] = (byte) Instruction.SBC_IMM.ordinal();
						ram[pc++] = (byte) d;
						cpu.a = (byte) a;
						if (carry) {
							cpu.c_flag = true;
						}
						if (decimal) {
							cpu.d_flag = true;
						}
						do {
							cpu.commonOps(false);
						} while (cpu.state != 0);
						asrt(pc, getNVbsDiZC(c, v, decimal, (byte) sum), sum);
					}
				}
			}
		}
		reset();
		int pc = PRG;
		int adr = 0x8ff;
		int x, y;
		int a = 127, d = 1, unsigned = 126;
		boolean c = true, v = false, df = false;

		ram[pc++] = (byte) Instruction.SBC_ZP.ordinal();
		ram[pc++] = (byte) adr;
		ram[adr & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);

		x = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SBC_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SBC_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);

		x = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SBC_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);
		y = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SBC_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);

		x = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SBC_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);

		y = 255;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SBC_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNVbsDiZC(c, v, df, (byte) unsigned), unsigned);
	}

	@Test
	public void testAnd() {
		int pc, a, d, x = 255, y = 123, r, adr = 0x8ff;
		for (a = 0; a < 256; a++) {
			for (d = 0; d < 256; d++) {
				r = a & d;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.AND_IMM.ordinal();
				ram[pc++] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZc((byte) r), r);

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.AND_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZc((byte) r), r);
			}
		}
		a = 127;
		d = 129;
		r = a & d;
		;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.AND_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.AND_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.AND_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.AND_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.AND_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.AND_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);
	}

	@Test
	public void testOr() {
		int pc, a, d, x = 255, y = 123, r, adr = 0x8ff;
		for (a = 0; a < 256; a++) {
			for (d = 0; d < 256; d++) {
				r = a | d;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.ORA_IMM.ordinal();
				ram[pc++] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZc((byte) r), r);

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.ORA_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZc((byte) r), r);
			}
		}
		a = 127;
		d = 129;
		r = a | d;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ORA_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ORA_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ORA_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ORA_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ORA_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.ORA_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);
	}

	@Test
	public void testEor() {
		int pc, a, d, x = 255, y = 123, r, adr = 0x8ff;
		for (a = 0; a < 256; a++) {
			for (d = 0; d < 256; d++) {
				r = a ^ d;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.EOR_IMM.ordinal();
				ram[pc++] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZc((byte) r), r);

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.EOR_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZc((byte) r), r);
			}
		}
		a = 127;
		d = 129;
		r = a ^ d;
		;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.EOR_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.EOR_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.EOR_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.EOR_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.EOR_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.EOR_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) r), r);
	}

	@Test
	public void testAsl() {
		for (int d = 0; d < 256; d++) {
			int r = d << 1;
			boolean c = r > 255;
			int pc;

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.ASL_A.ordinal();
			cpu.a = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZC(c, (byte) r), r);

			reset();
			pc = PRG;
			int adr = 0x8ff;
			int x;

			ram[pc++] = (byte) Instruction.ASL_ZP.ordinal();
			ram[pc++] = (byte) adr;
			ram[adr & 0xff] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr & 0xff]);

			reset();
			x = 255;
			pc = PRG;
			ram[pc++] = (byte) Instruction.ASL_ZPX.ordinal();
			ram[pc++] = (byte) adr;
			ram[(adr + x) & 0xff] = (byte) d;
			cpu.x = (byte) x;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[(adr + x) & 0xff]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.ASL_ABS.ordinal();
			ram[pc++] = (byte) adr;
			ram[pc++] = (byte) (adr >> 8);
			ram[adr] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr]);

			reset();
			x = 255;
			pc = PRG;
			ram[pc++] = (byte) Instruction.ASL_ABSX.ordinal();
			ram[pc++] = (byte) adr;
			ram[pc++] = (byte) (adr >> 8);
			ram[adr + x] = (byte) d;
			cpu.x = (byte) x;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr + x]);
		}
	}

	@Test
	public void testRol() {
		for (int d = 0; d < 256; d++) {
			for (boolean carry : new boolean[] { false, true }) {
				int r = (d << 1) | (carry ? 1 : 0);
				boolean c = r > 255;
				int pc;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROL_A.ordinal();
				cpu.a = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r);

				reset();
				pc = PRG;
				int adr = 0x8ff;
				int x;

				ram[pc++] = (byte) Instruction.ROL_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr & 0xff]);

				reset();
				x = 255;
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROL_ZPX.ordinal();
				ram[pc++] = (byte) adr;
				ram[(adr + x) & 0xff] = (byte) d;
				cpu.x = (byte) x;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[(adr + x) & 0xff]);

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROL_ABS.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr] = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr]);

				reset();
				x = 255;
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROL_ABSX.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr + x] = (byte) d;
				cpu.x = (byte) x;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr + x]);
			}
		}
	}

	@Test
	public void testRor() {
		for (int d = 0; d < 256; d++) {
			for (boolean carry : new boolean[] { false, true }) {
				int r = (d >> 1) | (carry ? 128 : 0);
				boolean c = (d & 1) != 0;
				int pc;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROR_A.ordinal();
				cpu.a = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r);

				reset();
				pc = PRG;
				int adr = 0x8ff;
				int x;

				ram[pc++] = (byte) Instruction.ROR_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr & 0xff]);

				reset();
				x = 255;
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROR_ZPX.ordinal();
				ram[pc++] = (byte) adr;
				ram[(adr + x) & 0xff] = (byte) d;
				cpu.x = (byte) x;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[(adr + x) & 0xff]);

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROR_ABS.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr] = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr]);

				reset();
				x = 255;
				pc = PRG;
				ram[pc++] = (byte) Instruction.ROR_ABSX.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr + x] = (byte) d;
				cpu.x = (byte) x;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr + x]);
			}
		}
	}

	@Test
	public void testLsr() {
		for (int d = 0; d < 256; d++) {
			for (boolean carry : new boolean[] { false, true }) {
				int r = (d >> 1); // | (carry? 128:0);
				boolean c = (d & 1) != 0;
				int pc;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.LSR_A.ordinal();
				cpu.a = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r);

				reset();
				pc = PRG;
				int adr = 0x8ff;
				int x;

				ram[pc++] = (byte) Instruction.LSR_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr & 0xff]);

				reset();
				x = 255;
				pc = PRG;
				ram[pc++] = (byte) Instruction.LSR_ZPX.ordinal();
				ram[pc++] = (byte) adr;
				ram[(adr + x) & 0xff] = (byte) d;
				cpu.x = (byte) x;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[(adr + x) & 0xff]);

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.LSR_ABS.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr] = (byte) d;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr]);

				reset();
				x = 255;
				pc = PRG;
				ram[pc++] = (byte) Instruction.LSR_ABSX.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr + x] = (byte) d;
				cpu.x = (byte) x;
				cpu.c_flag = carry;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r), r, ram[adr + x]);
			}
		}
	}

	@Test
	public void testBit() {
		for (int a = 0; a < 256; a++) {
			for (int d = 0; d < 256; d++) {
				int r = d;
				boolean v = (r & 0x40) != 0, z = (a & d) == 0;
				int pc;
				int adr = 0x8ff;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.BIT_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNVbsdiZc(v, (byte) r, z));

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.BIT_ABS.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNVbsdiZc(v, (byte) r, z));
			}
		}
	}

	@Test
	public void testJmp() {
		int pc, adr = 0x3013;

		pc = PRG;
		ram[pc++] = (byte) Instruction.JMP_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(adr, cpu.pc);

		int adr2 = 0x5555;
		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.JMP_IND.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) adr2;
		ram[adr + 1] = (byte) (adr2 >> 8);
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(adr2, cpu.pc);
	}

	@Test
	public void testJsr() {
		int pc, adr = 0x3013;

		pc = PRG;
		ram[pc++] = (byte) Instruction.JSR_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		pc = adr;
		ram[pc++] = (byte) Instruction.RTS_IMP.ordinal();

		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(adr, cpu.pc);
		assertEquals(253, cpu.sp & 0xff);
		assertEquals(PRG + 2, ((ram[511] & 0xff) << 8) | (ram[510] & 0xff));

		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(PRG + 3, cpu.pc);
		assertEquals(255, cpu.sp & 0xff);
	}

	@Test
	public void testBranch() {
		int pc;

		for (int d = 0; d < 256; d++) {
			pc = PRG;
			ram[pc++] = (byte) Instruction.BPL_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);
			reset();
			cpu.n_flag = -1;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BMI_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);
			reset();
			cpu.n_flag = -1;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BVC_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);
			reset();
			cpu.v_flag = true;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BVS_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);
			reset();
			cpu.v_flag = true;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BCC_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);
			reset();
			cpu.c_flag = true;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BCS_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);
			reset();
			cpu.c_flag = true;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BEQ_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);
			reset();
			cpu.z_flag = -1;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);

			pc = PRG;
			ram[pc++] = (byte) Instruction.BNE_REL.ordinal();
			ram[pc++] = (byte) d;
			reset();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc, cpu.pc);
			reset();
			cpu.z_flag = -1;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			assertEquals(pc + ((byte) d), cpu.pc);
		}
	}

	@Test
	public void testCmp() {
		int pc, a, d, x, y, r, adr = 0x8ff;
		boolean c;
		for (a = 0; a < 256; a++) {
			for (d = 0; d < 256; d++) {
				r = a - d;
				c = a >= d;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CMP_IMM.ordinal();
				ram[pc++] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CMP_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.a = (byte) a;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));
			}
		}
		a = 128;
		d = 127;
		r = a - d;
		c = a >= d;
		x = 255;
		y = 123;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CMP_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZC(c, (byte) r));

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CMP_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZC(c, (byte) r));

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CMP_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZC(c, (byte) r));

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CMP_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZC(c, (byte) r));

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CMP_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZC(c, (byte) r));

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CMP_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZC(c, (byte) r));
	}

	@Test
	public void testCpx() {
		int pc, d, x, r, adr = 0x8ff;
		boolean c;
		for (x = 0; x < 256; x++) {
			for (d = 0; d < 256; d++) {
				r = x - d;
				c = x >= d;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CPX_IMM.ordinal();
				ram[pc++] = (byte) d;
				cpu.x = (byte) x;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CPX_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.x = (byte) x;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CPX_ABS.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr] = (byte) d;
				cpu.x = (byte) x;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));
			}
		}
	}

	@Test
	public void testCpy() {
		int pc, d, y, r, adr = 0x8ff;
		boolean c;
		for (y = 0; y < 256; y++) {
			for (d = 0; d < 256; d++) {
				r = y - d;
				c = y >= d;

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CPY_IMM.ordinal();
				ram[pc++] = (byte) d;
				cpu.y = (byte) y;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CPY_ZP.ordinal();
				ram[pc++] = (byte) adr;
				ram[adr & 0xff] = (byte) d;
				cpu.y = (byte) y;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));

				reset();
				pc = PRG;
				ram[pc++] = (byte) Instruction.CPY_ABS.ordinal();
				ram[pc++] = (byte) adr;
				ram[pc++] = (byte) (adr >> 8);
				ram[adr] = (byte) d;
				cpu.y = (byte) y;
				do {
					cpu.commonOps(false);
				} while (cpu.state != 0);
				asrt(pc, getNvbsdiZC(c, (byte) r));
			}
		}
	}

	@Test
	public void testFlags() {
		int pc;

		pc = PRG;
		ram[pc++] = (byte) Instruction.CLC_IMP.ordinal();
		cpu.c_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00100010");

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SEC_IMP.ordinal();
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00100011");

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CLI_IMP.ordinal();
		cpu.i_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00100010");

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SEI_IMP.ordinal();
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00100110");

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CLV_IMP.ordinal();
		cpu.v_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00100010");

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.CLD_IMP.ordinal();
		cpu.d_flag = true;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00100010");

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.SED_IMP.ordinal();
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, "00101010");

	}

	@Test
	public void testDec() {
		int pc, d, x = 255, r, adr = 0x8ff;
		for (d = 0; d < 256; d++) {
			r = d - 1;

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.DEC_ZP.ordinal();
			ram[pc++] = (byte) adr;
			ram[adr & 0xff] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[adr & 0xff]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.DEC_ZPX.ordinal();
			ram[pc++] = (byte) adr;
			ram[(adr + x) & 0xff] = (byte) d;
			cpu.x = (byte) x;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[(adr + x) & 0xff]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.DEC_ABS.ordinal();
			ram[pc++] = (byte) adr;
			ram[pc++] = (byte) (adr >> 8);
			ram[adr] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[adr]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.DEC_ABSX.ordinal();
			ram[pc++] = (byte) adr;
			ram[pc++] = (byte) (adr >> 8);
			ram[adr + x] = (byte) d;
			cpu.x = (byte) x;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[adr + x]);
		}
	}

	@Test
	public void testInc() {
		int pc, d, x = 255, r, adr = 0x8ff;
		for (d = 0; d < 256; d++) {
			r = d + 1;

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.INC_ZP.ordinal();
			ram[pc++] = (byte) adr;
			ram[adr & 0xff] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[adr & 0xff]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.INC_ZPX.ordinal();
			ram[pc++] = (byte) adr;
			ram[(adr + x) & 0xff] = (byte) d;
			cpu.x = (byte) x;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[(adr + x) & 0xff]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.INC_ABS.ordinal();
			ram[pc++] = (byte) adr;
			ram[pc++] = (byte) (adr >> 8);
			ram[adr] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[adr]);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.INC_ABSX.ordinal();
			ram[pc++] = (byte) adr;
			ram[pc++] = (byte) (adr >> 8);
			ram[adr + x] = (byte) d;
			cpu.x = (byte) x;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, ram[adr + x]);
		}
	}

	@Test
	public void testLda() {
		int pc, d, adr = 0x8ff, x = 255, y = 123;

		for (d = 0; d < 256; d++) {
			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.LDA_IMM.ordinal();
			ram[pc++] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.LDA_ZP.ordinal();
			ram[pc++] = (byte) adr;
			ram[adr & 0xff] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);
		}
		d = 129;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDA_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDA_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDA_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDA_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDA_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDA_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.a);
	}

	@Test
	public void testLdx() {
		int pc, d, adr = 0x8ff, x = 255, y = 123;

		for (d = 0; d < 256; d++) {
			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.LDX_IMM.ordinal();
			ram[pc++] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) d), d, cpu.x);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.LDX_ZP.ordinal();
			ram[pc++] = (byte) adr;
			ram[adr & 0xff] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) d), d, cpu.x);
		}
		d = 129;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDX_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + y) & 0xff] = (byte) d;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.x);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDX_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.x);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDX_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + y] = (byte) d;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.x);
	}

	@Test
	public void testLdy() {
		int pc, d, adr = 0x8ff, x = 255, y = 123;

		for (d = 0; d < 256; d++) {
			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.LDY_IMM.ordinal();
			ram[pc++] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) d), d, cpu.y);

			reset();
			pc = PRG;
			ram[pc++] = (byte) Instruction.LDY_ZP.ordinal();
			ram[pc++] = (byte) adr;
			ram[adr & 0xff] = (byte) d;
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) d), d, cpu.y);
		}
		d = 129;

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDY_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		ram[(adr + x) & 0xff] = (byte) d;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.y);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDY_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr] = (byte) d;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.y);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.LDY_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		ram[adr + x] = (byte) d;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		asrt(pc, getNvbsdiZc((byte) d), d, cpu.y);
	}

	@Test
	public void testSta() {
		int pc, a = 17, adr = 0x8ff, x = 255, y = 123;

		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_ZP.ordinal();
		ram[pc++] = (byte) adr;
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[adr & 0xff]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[(adr + x) & 0xff]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[adr]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_ABSX.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[adr + x]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_ABSY.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[adr + y]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_XIND.ordinal();
		int tab = 127;
		ram[pc++] = (byte) tab;
		ram[(tab + x) & 0xff] = (byte) adr;
		ram[(tab + x + 1) & 0xff] = (byte) (adr >> 8);
		cpu.a = (byte) a;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[adr]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STA_INDY.ordinal();
		tab = 128;
		ram[pc++] = (byte) tab;
		ram[tab] = (byte) adr;
		ram[tab + 1] = (byte) (adr >> 8);
		cpu.a = (byte) a;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(a, ram[adr + y]);
	}

	@Test
	public void testStx() {
		int pc, x = 17, adr = 0x8ff, y = 123;

		pc = PRG;
		ram[pc++] = (byte) Instruction.STX_ZP.ordinal();
		ram[pc++] = (byte) adr;
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(x, ram[adr & 0xff]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STX_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		cpu.x = (byte) x;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(x, ram[(adr + y) & 0xff]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STX_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(x, ram[adr]);
	}

	@Test
	public void testSty() {
		int pc, x = 255, y = 17, adr = 0x8ff;

		pc = PRG;
		ram[pc++] = (byte) Instruction.STY_ZP.ordinal();
		ram[pc++] = (byte) adr;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(y, ram[adr & 0xff]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STY_ZPX.ordinal();
		ram[pc++] = (byte) adr;
		cpu.x = (byte) x;
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(y, ram[(adr + x) & 0xff]);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.STY_ABS.ordinal();
		ram[pc++] = (byte) adr;
		ram[pc++] = (byte) (adr >> 8);
		cpu.y = (byte) y;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(y, ram[adr]);
	}

	@Test
	public void testStack() {
		int pc, a = 123, x = 17;

		pc = PRG;
		ram[pc++] = (byte) Instruction.TXS_IMP.ordinal();
		ram[pc++] = (byte) Instruction.TSX_IMP.ordinal();
		cpu.x = (byte) x;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(x & 0xff, cpu.sp & 0xff);
		cpu.x = 0;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(x & 0xff, cpu.x & 0xff);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.PHA_IMP.ordinal();
		ram[pc++] = (byte) Instruction.PLA_IMP.ordinal();
		cpu.a = (byte) a;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(0xfe, cpu.sp & 0xff);
		assertEquals(a & 0xff, ram[0x1ff] & 0xff);
		cpu.a = 0;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(0xff, cpu.sp & 0xff);
		assertEquals(a, cpu.a & 0xff);

		reset();
		pc = PRG;
		ram[pc++] = (byte) Instruction.PHP_IMP.ordinal();
		ram[pc++] = (byte) Instruction.PLP_IMP.ordinal();
		cpu.c_flag = cpu.d_flag = cpu.i_flag = cpu.v_flag = true;
		cpu.z_flag = -1;
		cpu.n_flag = -1;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(0xfe, cpu.sp & 0xff);
		assertEquals(0xff, ram[0x1ff] & 0xff);
		cpu.c_flag = cpu.d_flag = cpu.i_flag = cpu.v_flag = false;
		cpu.z_flag = 0;
		cpu.n_flag = 0;
		do {
			cpu.commonOps(false);
		} while (cpu.state != 0);
		assertEquals(0xff, cpu.sp & 0xff);
		assertEquals(true, cpu.c_flag);
		assertEquals(true, cpu.d_flag);
		assertEquals(true, cpu.i_flag);
		assertEquals(true, cpu.v_flag);
		assertEquals(0xff, cpu.z_flag & 0xff);
		assertEquals(0xff, cpu.n_flag & 0xff);
	}

	@Test
	public void testTax() {
		for (int a = 0; a < 256; a++) {
			int r = a;

			reset();
			int pc = PRG;
			cpu.a = (byte) a;
			ram[pc++] = (byte) Instruction.TAX_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.x & 0xff);
		}
	}

	@Test
	public void testTay() {
		for (int a = 0; a < 256; a++) {
			int r = a;

			reset();
			int pc = PRG;
			cpu.a = (byte) a;
			ram[pc++] = (byte) Instruction.TAY_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.y & 0xff);
		}
	}

	@Test
	public void testTxa() {
		for (int x = 0; x < 256; x++) {
			int r = x;

			reset();
			int pc = PRG;
			cpu.x = (byte) x;
			ram[pc++] = (byte) Instruction.TXA_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.a & 0xff);
		}
	}

	@Test
	public void testTya() {
		for (int y = 0; y < 256; y++) {
			int r = y;

			reset();
			int pc = PRG;
			cpu.y = (byte) y;
			ram[pc++] = (byte) Instruction.TYA_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.a & 0xff);
		}
	}

	@Test
	public void testDex() {
		for (int x = 0; x < 256; x++) {
			int r = x - 1;

			reset();
			int pc = PRG;
			cpu.x = (byte) x;
			ram[pc++] = (byte) Instruction.DEX_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.x & 0xff);
		}
	}

	@Test
	public void testDey() {
		for (int y = 0; y < 256; y++) {
			int r = y - 1;

			reset();
			int pc = PRG;
			cpu.y = (byte) y;
			ram[pc++] = (byte) Instruction.DEY_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.y & 0xff);
		}
	}

	@Test
	public void testInx() {
		for (int x = 0; x < 256; x++) {
			int r = x + 1;

			reset();
			int pc = PRG;
			cpu.x = (byte) x;
			ram[pc++] = (byte) Instruction.INX_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.x & 0xff);
		}
	}

	@Test
	public void testIny() {
		for (int y = 0; y < 256; y++) {
			int r = y + 1;

			reset();
			int pc = PRG;
			cpu.y = (byte) y;
			ram[pc++] = (byte) Instruction.INY_IMP.ordinal();
			do {
				cpu.commonOps(false);
			} while (cpu.state != 0);
			asrt(pc, getNvbsdiZc((byte) r), r, cpu.y & 0xff);
		}
	}

	@Test
	public void testPlus() {
		cpu.mapRoms();

		int pc = 0x200;
		int pc2 = pc;

		ram[pc2++] = (byte) Instruction.JSR_ABS.ordinal();
		int adr = ROMCall.BAS_PLUS.getAddress();
		ram[pc2++] = (byte) adr;
		ram[pc2++] = (byte) (adr >> 8);

		for (int i = 1; i < 16; i++) {
			for (int j = 2; j < 16; j++) {
				cpu.setFloat(i, Cpu.FAC);
				cpu.setFloat(j, Cpu.ARG);
//				cpu.setDebug(true);
//				cpu.setTrace(true);
				cpu.z_flag = -1;
				cpu.pc = pc;
				do {
					cpu.commonOps(false);
				} while (cpu.pc != pc2);

				assertEquals(i + j, cpu.getFloat());
			}
		}
	}

	@Test
	public void testDiv() {
		cpu.mapRoms();

		int pc = 0x200;
		cpu.pc = pc;
		int pc2 = pc;

		ram[pc2++] = (byte) Instruction.JSR_ABS.ordinal();
		int adr = ROMCall.BAS_DIV.getAddress();
		ram[pc2++] = (byte) adr;
		ram[pc2++] = (byte) (adr >> 8);

		cpu.setFloat(256, Cpu.ARG);
		cpu.setFloat(1024, Cpu.FAC);
//		cpu.setDebug(true);
//		cpu.setTrace(true);
		cpu.z_flag = -1;
		do {
			cpu.commonOps(false);
		} while (cpu.pc != pc2);

		assertEquals(0.25, cpu.getFloat(Cpu.FAC));
	}

	@Test
	public void testMul() {
		cpu.mapRoms();

		int pc = 0x200;
		int pc2 = pc;

		ram[pc2++] = (byte) Instruction.JSR_ABS.ordinal();
		int adr = ROMCall.BAS_ACCU_10.getAddress();
		ram[pc2++] = (byte) adr;
		ram[pc2++] = (byte) (adr >> 8);

		for (int i = 0; i < 15; i++) {
			cpu.setFloat(i);
//			cpu.setDebug(true);
//			cpu.setTrace(true);
			cpu.z_flag = -1;
			cpu.pc = pc;
			do {
				cpu.commonOps(false);
			} while (cpu.pc != pc2);

			assertEquals(i * 10, cpu.getFloat());
		}
	}

	@Test
	public void testFtoa() {
		cpu.mapRoms();

		int pc = 0x200;
		int pc2 = pc;

		ram[pc2++] = (byte) Instruction.JSR_ABS.ordinal();
		int adr = ROMCall.BAS_ACCU2STR.getAddress();
		ram[pc2++] = (byte) adr;
		ram[pc2++] = (byte) (adr >> 8);
//		cpu.setDebug(true);
//		cpu.setTrace(true);

		for (int i = 9; i < 16; i++) {
			cpu.pc = pc;
			cpu.setFloat(i);
			cpu.z_flag = -1;
			do {
				cpu.commonOps(false);
			} while (cpu.pc != pc2);

			assertEquals(String.format(" %d", i), cpu.getString());
		}
	}

	private String getNVbsDiZC(boolean c, boolean v, boolean d, byte b) {
		return getNVbsDiZC(c, v, d, b, b == 0);
	}

	private String getNVbsDiZC(boolean c, boolean v, boolean d, byte b, boolean z) {
		return String.format("%d%d10%d0%d%d", b < 0 ? 1 : 0, v ? 1 : 0, d ? 1 : 0, z ? 1 : 0, c ? 1 : 0);
	}

	private String getNvbsdiZc(byte b) {
		return getNVbsDiZC(false, false, false, b);
	}

	private String getNvbsdiZC(boolean c, byte b) {
		return getNVbsDiZC(c, false, false, b);
	}

//		private String getNVbsdiZc(boolean v, byte b) {
//			return getNVbsDiZC(false, v, false, b);
//		}
	private String getNVbsdiZc(boolean v, byte b, boolean z) {
		return getNVbsDiZC(false, v, false, b, z);
	}

	void asrt(int pc, String flags) {
		asrt(pc, flags, cpu.a, cpu.a);
	}

	void asrt(int pc, String flags, int a) {
		asrt(pc, flags, a, cpu.a);
	}

	void asrt(int pc, String flags, int expected, int actual) {
		try {
			assertEquals(pc & 0xffff, cpu.pc & 0xffff);
			assertEquals(expected & 0xff, actual & 0xff);
			assertEquals(flags, cpu.getNVBsDIZC());
		} catch (AssertionError x) {
			throw x;
		}
	}

	static class Cpu extends CpuBase {

		byte[] ram;

		public Cpu(byte[] ram) {
			this.ram = ram;
			reset();
		}

		public String getString() {
			StringBuilder sb = new StringBuilder();
			for (int i = 0x100; ram[i] != 0; i++) {
				sb.append((char) ram[i]);
			}
			return sb.toString();
		}

		public void reset() {
			sp = (byte) 0xff;
			pc = PRG;
			ar = ar2 = a = x = y = z_flag = n_flag = 0;
			v_flag = d_flag = i_flag = c_flag = false;
		}

		@Override
		protected void write_byte(int addr, byte b) {
			ram[addr & 0xffff] = b;
		}

		@Override
		protected byte read_byte(int addr) {
			return ram[addr & 0xffff];
		}

		public void mapRoms() {
			System.arraycopy(basic, 0, ram, 0xa000, basic.length);
			System.arraycopy(kernal, 0, ram, 0xe000, kernal.length);
		}

		public void setFloat(int i) {
			setFloat(i, FAC);
		}

		public void setFloat(int i, int adr) {
			long l = 0;
			if (i != 0) {
				boolean neg = i < 0;
				l = ((long) (neg ? -i : i)) << 24;
				long exp = 32;
				while ((l & 0x0080000000000000L) == 0) {
					l <<= 1;
					exp--;
				}
				// Exponent (+ 128) goes into top byte
				l |= ((exp + 128) << 56);
				// bit 55 is implicitly set
				// l &= (~0x0080000000000000L);
				l |= 0x0080000000000000L;
				if (neg) {
					// Seperate sign byte
					l |= 0x0000000000ff0000L;
				}
				// Ex:
				// 10 -> 0x84_20000000...
				// == 0.625 * 2^4
				// -10 -> 0x84a0000000ff...
				// == -(0.625 * 2^4)
			}
			for (int b = 0; b < 8; b++) {
				ram[adr + b] = (byte) (l >> ((7 - b) * 8));
			}
		}
	}
}
