module Frodo {
	requires java.desktop;
	requires javafx.base;
	requires javafx.graphics;
	requires javafx.controls;
	requires javafx.fxml;
	requires java.base;
//	requires org.junit.jupiter.api;
	
	opens se.pp.forsberg.frodo to javafx.graphics;
		
	exports se.pp.forsberg.frodo;
	exports se.pp.forsberg.frodo.enums;
	exports se.pp.forsberg.frodo.prefs;
}