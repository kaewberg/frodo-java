package se.pp.forsberg.libc;

public class CPtr {
	char[] buf;
	int offset;
	public CPtr(char[] input) {
		buf = input;
	}
	public CPtr(CPtr p) {
		buf = p.buf;
		offset = p.offset;
	}
	public void setInc(char c) {
		buf[offset++] = c;
	}
	public char getInc() {
		return buf[offset++];
	}
	public void decSet(char c) {
		buf[--offset] = c;
	}
	public void set(int i) {
		buf[offset] = (char) i;
	}
}
