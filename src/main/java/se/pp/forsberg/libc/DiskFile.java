package se.pp.forsberg.libc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;

import static se.pp.forsberg.libc.LibC.EOF;

public class DiskFile extends StdioFile {
	
	RandomAccessFile f;

	public DiskFile(RandomAccessFile randomAccessFile) {
		f = randomAccessFile;
	}

	@Override
	public int fseek(long i, SeekWhence whence) {
		try {
			long pos = f.getFilePointer();
			switch (whence) {
			case SEEK_CUR:
				pos = f.getFilePointer() + i;
				break;
			case SEEK_END:
				pos = f.length() + i;
				break;
			case SEEK_SET:
				pos = i;
				break;
			}
			f.seek(pos);
			return 0;
		} catch (IOException x) {
			return -1;
		}
	}

	@Override
	public long ftell() {
		try {
			return f.getFilePointer();
		} catch (IOException e) {
			return -1;
		}
	}

	@Override
	public int fclose() {
		try {
			f.close();
			return 0;
		} catch (IOException e) {
			return -1;
		}
	}

	@Override
	public int fgetc() {
		try {
			return f.read();
		} catch (IOException e) {
			return EOF;
		}
	}

	@Override
	int fputc(int c) {
		try {
			f.write(c);
			return c;
		} catch (IOException e) {
			return EOF;
		}
	}

	@Override
	public long fread(BPtr buffer, long size, long nmemb) {
		try {
			int i = f.read(buffer.buf, buffer.offs, (int) (size * nmemb));
			if (i < 0) {
				return 0;
			}
			return i / size;
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public long fwrite(BPtr buffer, long size, long nmemb) {
		try {
			f.write(buffer.buf, buffer.offs, (int) (size * nmemb));
			return nmemb;
		} catch (IOException e) {
			return 0;
		}
	}

	@Override
	public boolean feof() {
		try {
			return f.getFilePointer() >= f.length();
		} catch (IOException e) {
			return true;
		}
	}

	@Override
	protected void fflush() {
		
	}

	@Override
	protected void fprintf(String pattern, Object... args) {
		throw new RuntimeException("Not implemented");
	}

	public static StdioFile fopen(File f, String mode) {
		boolean read, write, append, binary, create;
		switch (mode) {
		case "r":
			create = false; read = true;  write = true; append = false; binary = false; break;
		case "w":
			create = true;  read = false; write = true; append = false; binary = false; break;
		case "a":
			create = true;  read = false; write = true; append = true;  binary = false; break;
		case "r+":
			create = false; read = true; write = true;  append = true;  binary = false; break;
		case "w+": case "rw":
			create = true;  read = true; write = true;  append = true;  binary = false; break;
			
		case "rb":
			create = false; read = true;  write = true; append = false; binary = true; break;
		case "wb":
			create = true;  read = false; write = true; append = false; binary = true; break;
		case "ab":
			create = true;  read = false; write = true; append = true;  binary = true; break;
		case "rb+": case "r+b":
			create = false; read = true; write = true;  append = true;  binary = true; break;
		case "wb+": case "w+b": case "rwb":
			create = true;  read = true; write = true;  append = true;  binary = true; break;
		default: return null;
		}
		if (read && write) {
			mode = "rw";
		} else if (read) {
			mode = "r";
		} else {
			mode = "w";
		}
		try {
			if (create && !f.exists()) {
				f.createNewFile();
			}
			DiskFile df = new DiskFile(new RandomAccessFile(f, mode));
			if (append) {
				df.fseek(0, SeekWhence.SEEK_END);
			}
			return df;
		} catch (IOException e) {
			return null;
		}
	}

}
