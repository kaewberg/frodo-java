package se.pp.forsberg.libc;

public class BPtr {
	public byte[] buf;
	public int offs;
	
	public BPtr(byte[] buf) {
		this(buf, 0);
	}
	public BPtr(byte[] buf, int offs) {
		this.buf = buf;
		this.offs = offs;
	}
	public BPtr(BPtr bp) {
		buf = bp.buf;
		offs = bp.offs;
	}
	public BPtr(BPtr p, int offs) {
		buf = p.buf;
		this.offs = p.offs + offs;
	}
	public int get(int n) {
		return buf[offs + n] & 0xff;
	}
	public int get() {
		return buf[offs] & 0xff;
	}
	public void inc() {
		offs++;
	}
	public void inc(int i) {
		offs += i;
	}
	public byte getInc() {
		return buf[offs++];
	}
	public byte getDec() {
		return buf[offs--];
	}
	public short getIncShort() {
		return (short) (((getInc() << 8) & 0xff00) | (getInc() & 0xff));
	}
	public int getIncInt() {
		return (short) (((getInc() << 24) & 0xff000000) | ((getInc() << 16) & 0xff0000) | ((getInc() << 8) & 0xff00) | (getInc() & 0xff));
	}
	public boolean getIncBool() {
		return getInc() == 0? false:true;
	}
	public byte set(byte b) {
		buf[offs] = b;
		return b;
	}
	public byte set(int n, byte b) {
		buf[offs + n] = b;
		return b;
	}
	public byte setInc(byte b) {
		buf[offs++] = b;
		return b;
	}
	public byte setInc(boolean b) {
		return setInc((byte) (b? 255:0));
	}
	public byte setInc(short s) {
		setInc((byte) (s >> 8));
		return setInc((byte) s);
	}
	public byte setInc(char c) {
		return setInc((byte) c);
	}
	public byte setInc(int i) {
		setInc((byte) (i >> 24));
		setInc((byte) (i >> 16));
		setInc((byte) (i >> 8));
		return setInc((byte) i);
	}
	public void setInc(String s) {
		for (int i = 0; i < s.length(); i++) {
			setInc(s.charAt(i));
		}
	}
	public void memset(int n, byte c) {
		for (int i = 0; i < n; i++) {
			set(i, c);
		}
	}
	public void setOr(int i, byte b) {
		buf[offs + i] |= b;
	}
}
