package se.pp.forsberg.libc;

import java.io.File;

public class LibC {
	
	public static int EOF = -1;
	
	private LibC() {}

	public static BPtr strcpy(byte[] dest, String src) {
		return strcpy(new BPtr(dest), new BPtr(toBytes(src)));
	}
	public static BPtr strcpy(byte[] dest, byte[] src) {
		return strcpy(new BPtr(dest), new BPtr(src));
	}
	public static BPtr strcpy(BPtr dest, BPtr src) {
		dest = new BPtr(dest);
		src = new BPtr(src);
		int i = 0;
		for (; src.get(i) != 0; i++) {
			dest.set(i, (byte) src.get(i));
		}
		dest.set(i, (byte) 0);
		return dest;
	}
	public static BPtr strncpy(byte[] dest, String src, int n) {
		return strncpy(new BPtr(dest), new BPtr(toBytes(src)), n);
	}
	public static BPtr strncpy(byte[] dest, BPtr src, int n) {
		return strncpy(new BPtr(dest), src, n);
	}
	public static BPtr strncpy(BPtr dest, BPtr src, int n) {
		dest = new BPtr(dest);
		src = new BPtr(src);
		int i = 0;
		for (; src.get(i) != 0  && i < n; i++) {
			dest.set(i, (byte) src.get(i));
		}
		if (i < n) {
			dest.set(i, (byte) 0);
		}
		return dest;
	}
	public static BPtr strchr(byte[] a, char c) {
		return strchr(new BPtr(a), c);
	}
	public static BPtr strchr(BPtr p, char c) {
		p = new BPtr(p);
		for (int i = 0; p.get(i) != 0; i++) {
			if (p.get(i) == c) {
				return new BPtr(p.buf, p.offs + i);
			}
		}
		return null;
	}
	public static int strlen(BPtr p) {
		p = new BPtr(p);
		int i;
		for (i = 0; p.get() != 0; p.inc());
		return i;
	}
	
	public static BPtr memcpy(byte[] dest, BPtr src, int len) {
		return memcpy(new BPtr(dest), src, len);
	}
	public static BPtr memcpy(BPtr dest, byte[] src, int len) {
		return memcpy(dest, new BPtr(src), len);
	}
	public static BPtr memcpy(byte[] dest, byte[] src, int len) {
		return memcpy(new BPtr(dest), new BPtr(src), len);
	}
	public static BPtr memcpy(BPtr dest, BPtr src, int len) {
		dest = new BPtr(dest);
		src = new BPtr(src);
		for (int i = 0; i < len; i++) {
			dest.buf[dest.offs + i] = src.buf[src.offs + i];
		}
		return dest;
	}

	public static BPtr memset(byte[] str, int i, int n) {
		return memset(str, (byte) i, n);
	}
	public static BPtr memset(byte[] str, char c, int n) {
		return memset(str, (byte) c, n);
	}
	public static BPtr memset(byte[] str, byte c, int n) {
		return memset(new BPtr(str), c, n);
	}
	public static BPtr memset(BPtr str, byte c, int n) {
		str = new BPtr(str);
		for (int i = 0; i < n; i++) {
			str.set(i, c);
		}
		return str;
	}
	
	public static StdioFile tmpfile() { return StdioFile.tmpfile(); }
	public static StdioFile fopen(File file, String mode) { return StdioFile.fopen(file, mode); }
	public static int fseek(StdioFile f, long offset, SeekWhence whence) { return f.fseek(offset, whence); }
	public static void rewind (StdioFile stream) { stream.rewind(); }
	public static long ftell(StdioFile f) { return f.ftell(); }
	public static boolean feof(StdioFile f) { return f.feof(); }
	public static void fclose(StdioFile f) { f.fclose(); }
	public static int fgetc(StdioFile f) { return f.fgetc(); }
	public static int fputc(int c, StdioFile stream) { return stream.fputc(c); }
	public static long fread(byte[] buffer, int size, int nmemb, StdioFile f) { return f.fread(buffer, size, nmemb); }
	public static long fread(BPtr buffer, int size, int nmemb, StdioFile f) { return f.fread(buffer, size, nmemb); }
	public static int fscanf(StdioFile stream, String format, IntC out) { return stream.fscanf(format, out); }
	public static int fscanf(StdioFile stream, String format, ByteC out) { return stream.fscanf(format, out); }
	public static long fwrite(BPtr buffer, int size, int nmemb, StdioFile f) { return f.fwrite(buffer, size, nmemb); }
	public static long fwrite(byte[] buffer, int size, int nmemb, StdioFile f) { return f.fwrite(buffer, size, nmemb); }
	public static long fwrite(String s, int size, int nmemb, StdioFile f) { return f.fwrite(toBytes(s), size, nmemb); }

	public static byte[] toBytes(String s) {
		byte[] result = new byte[s.length() + 1];
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c < 1 || c > 255) {
				c = '?';
			}
			result[i] = (byte) c;
		}
		result[s.length()] = 0;
		return result;
	}
	public static String toString(byte[] s) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < ((s[s.length-1] == 0)? s.length - 1:s.length); i++) {
			sb.append((char) s[i]);
		}
		return sb.toString();
	}
	
	public static void fprintf(StdioFile f, String format, Object... args) {
		f.fprintf(format, args);
	}
	public static void fflush(StdioFile f) {
		f.fflush();
	}
	public static CPtr fgets(CPtr p, int len, StdioFile f) {
		return f.fgets(p, len);
	}
}
