package se.pp.forsberg.libc;

public enum SeekWhence {
	SEEK_SET, SEEK_CUR, SEEK_END
}