package se.pp.forsberg.libc;

import java.io.PrintStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static se.pp.forsberg.libc.LibC.EOF;;

public class OutFile extends StdioFile {

	PrintStream out;
	public OutFile(PrintStream out) {
		this.out = out;
	}

	@Override
	public int fseek(long i, SeekWhence whence) {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public long ftell() {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public int fclose() {
		out.close();
		return 0;
	}

	@Override
	public int fgetc() {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public int fputc(int c) {
		try {
			out.write(c);
			return 0;
		} catch (Exception x) {
			return EOF;
		}
	}

	@Override
	public long fread(BPtr buffer, long size, long nmemb) {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public long fwrite(BPtr buffer, long size, long nmemb) {
		buffer = new BPtr(buffer);
		for (int i = 0; i < nmemb; i++) {
			for (int j = 0; j < size; j++) {
				if (fputc(buffer.getInc() & 0xff) == EOF) {
					return i;
				}
			}
		}
		return nmemb;
	}

	@Override
	public boolean feof() {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	protected void fflush() {
		out.flush();
	}

	@Override
	protected void fprintf(String pattern, Object... args) {
		pattern = convertPattern(pattern);
		out.printf(pattern, args);
	}

	private String convertPattern(String s) {
		StringBuilder sb = new StringBuilder();
		StringBuilder p = new StringBuilder();
		
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c == '%') {
				p.setLength(0);
				p.append('%');
				for (i++; i < s.length(); i++) {
					c = s.charAt(i);
					if (c >= 'a' && c <= 'z') {
						if (c == 'l' && i < s.length() - 1) {
							// && (s.charAt(i + 1) == 'x' || s.charAt(i + 1) == 'd')) {
							i++;
							p.append(s.charAt(i));
							break;
						}
						p.append(c);
						break;
					}
					p.append(c);
				}
				sb.append(p);
				continue;
			}
			sb.append(c);
		}
		return sb.toString();
	}

}
