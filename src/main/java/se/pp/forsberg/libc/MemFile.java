package se.pp.forsberg.libc;

import java.io.File;
import java.nio.file.Files;

import static se.pp.forsberg.libc.LibC.EOF;

class MemFile extends StdioFile {
		byte[] data;
		long offs;
		boolean closed;
		@SuppressWarnings("unused")
		private boolean binary;

		public MemFile(byte[] data, boolean binary) {
			this.data = data;
			this.binary = binary;
		}


		@Override
		public int fseek(long i, SeekWhence whence) {
			if (closed) {
				return 1;
			}
			long offs = -1;
			switch (whence) {
			case SEEK_CUR: offs = this.offs + i; break;
			case SEEK_END: offs = data.length - i; break;
			case SEEK_SET: offs = i; break;
			default: break;
			}
			if (offs < 0 || offs >= data.length) {
				return 1;
			}
			return 0;
		}

		@Override
		public long ftell() {
			if (closed) {
				return -1;
			}
			return offs;
		}

		@Override
		public int fclose() {
			closed = true;
			return 0;
		}

		@Override
		public long fread(BPtr buffer, long size, long nmemb) {
			if (closed || offs + size*nmemb == data.length) {
				return -1;
			}
			for (long l = 0; l < nmemb * size; l++) {
				buffer.set((int) l, data[(int) offs++]);
				if (offs + size*nmemb >= data.length) {
					return l / size;
				}
			}
			return size;
		}

		@Override
		public long fwrite(BPtr buffer, long size, long nmemb) {
			return -1;
		}

		public static StdioFile fopen(File filepath, boolean create, boolean read, boolean write, boolean append, boolean binary) {
			if (write || append) {
				return null;
			}
			try {
				byte[] data = Files.readAllBytes(filepath.toPath());
				return new MemFile(data, binary);
			} catch (Exception x) {
				return null;
			}
		}


		@Override
		public int fgetc() {
			if (offs >= data.length) {
				return EOF;
			}
			return data[(int) offs++];
		}


		@Override
		public boolean feof() {
			return offs >= data.length;
		}


		@Override
		int fputc(int c) {
			if (offs >= data.length) {
				return c;
			}
			data[(int) offs++] = (byte) c;
			return -1;
		}


		@Override
		protected void fprintf(String pattern, Object... args) {
			throw new IllegalArgumentException("Unsupported operation");
		}


		@Override
		protected void fflush() {
		}
}