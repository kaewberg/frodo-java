package se.pp.forsberg.libc;

import static se.pp.forsberg.libc.LibC.EOF;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;

public abstract class StdioFile {
	abstract public int fseek(long i, SeekWhence whence);
	abstract public long ftell();
	abstract public int fclose();
	abstract public int fgetc();
	abstract int fputc(int c);;
	public long fread(byte[] buffer, long size, long nmemb) {
		return fread(new BPtr(buffer), size, nmemb);
	}
	abstract public long fread(BPtr buffer, long size, long nmemb);
	public long fwrite(byte[] buffer, long size, long nmemb) {
		return fwrite(new BPtr(buffer), size, nmemb);
	}
	abstract public long fwrite(BPtr buffer, long size, long nmemb);
	public abstract boolean feof();
	
	public static StdioFile fopen(File filepath, String mode) {
		return DiskFile.fopen(filepath, mode);
//		boolean read, write, append, binary, create;
//		switch (mode) {
//		case "r":
//			create = false; read = true;  write = true; append = false; binary = false; break;
//		case "w":
//			create = true;  read = false; write = true; append = false; binary = false; break;
//		case "a":
//			create = true;  read = false; write = true; append = true;  binary = false; break;
//		case "r+":
//			create = false; read = true; write = true;  append = true;  binary = false; break;
//		case "w+": case "rw":
//			create = true;  read = true; write = true;  append = true;  binary = false; break;
//			
//		case "rb":
//			create = false; read = true;  write = true; append = false; binary = true; break;
//		case "wb":
//			create = true;  read = false; write = true; append = false; binary = true; break;
//		case "ab":
//			create = true;  read = false; write = true; append = true;  binary = true; break;
//		case "rb+": case "r+b":
//			create = false; read = true; write = true;  append = true;  binary = true; break;
//		case "wb+": case "w+b": case "rwb":
//			create = true;  read = true; write = true;  append = true;  binary = true; break;
//		default: return null;
//		}
//		return MemFile.fopen(filepath, create, read, write, append, binary);
	}
	public static StdioFile fdopen(InputStream in) {
		return new InFile(in);
	}
	public static StdioFile fdopen(PrintStream out) {
		return new OutFile(out);
	}
	public static StdioFile fdopen(OutputStream out) {
		return fdopen(new PrintStream(out));
	}
	public static StdioFile fdopen(InputStream in, OutputStream out, OutputStream err) {
		return fdopen(in, new PrintStream(out), new PrintStream(err));
	}
	public static StdioFile fdopen(InputStream in, PrintStream out, PrintStream err) {
		return new JavaFile(in, out, err);
	}

	public int fscanf(String format, ByteC out) {
		if (!format.equals("%c")) {
			return 0;
		}
		char c = (char) fgetc();
		if (c < 0) {
			return 0;
		}
		out.b = (byte) c;
		return 1;
	}
	public int fscanf(String format, IntC out) {
		if (!format.equals("%d")) {
			return 0;
		}
		int result = 0;
		boolean neg = false;
		char c = (char) fgetc();
		if (c == '-') {
			neg = true;
			c = (char) fgetc();
		}
		if (c <= '0' || c >= '9') {
			return 0;
		}
		while (c >= '0' && c <= '9') {
			result = result*10 + c - '0';
			c = (char) fgetc();
		}
		if (neg) {
			result = -result;
		}
		out.i = result;
		return 1;
	}
	public void rewind() {
		fseek(0, SeekWhence.SEEK_SET);
	}
	public static StdioFile tmpfile() {
		try {
			return fopen(Files.createTempFile(null, null).toFile(), "rw");
		} catch (IOException e) {
			return null;
		}
	}
	protected abstract void fflush();
	protected CPtr fgets(CPtr p, int len) {
		int i = 0;
		p = new CPtr(p);
		CPtr result = new CPtr(p);
		while (true) {
			if (i >= len - 1) {
				break;
			}
			int c = fgetc();
			if (c == EOF || c == '\n') {
				break;
			}
			p.setInc((char) c);
			i++;
		}
		p.set('\0');
		return result;
	}
	protected abstract void fprintf(String pattern, Object... args);
}