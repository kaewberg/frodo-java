package se.pp.forsberg.libc;

import java.io.IOException;
import java.io.InputStream;

import static se.pp.forsberg.libc.LibC.EOF;

public class InFile extends StdioFile {
	
	InputStream in;
	
	public InFile(InputStream in) {
		this.in = in;
	}

	@Override
	public int fseek(long i, SeekWhence whence) {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public long ftell() {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public int fclose() {
		try {
			in.close();
			return 0;
		} catch (IOException e) {
			return EOF;
		}
	}

	@Override
	public int fgetc() {
		try {
			int i = in.read();
			return i < 0? EOF:i;
		} catch (IOException e) {
			return EOF;
		}
	}

	@Override
	int fputc(int c) {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public long fread(BPtr buffer, long size, long nmemb) {
		buffer = new BPtr(buffer);
		int read = 0;
		for (read = 0; read < size*nmemb; read++) {
			int i = fgetc();
			if (i == EOF) {
				return read / size;
			}
			buffer.setInc((byte) i);
		}
		return nmemb;
	}

	@Override
	public long fwrite(BPtr buffer, long size, long nmemb) {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	public boolean feof() {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	protected void fprintf(String pattern, Object... args) {
		throw new IllegalArgumentException("Unsupported operation");
	}

	@Override
	protected void fflush() {
	}


}
