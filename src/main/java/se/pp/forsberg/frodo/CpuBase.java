package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.CpuBase.OpCodes.*;
import static se.pp.forsberg.frodo.enums.InterruptType6510.INT_NMI;

import se.pp.forsberg.frodo.enums.ROMCall;

abstract class CpuBase {
	// States for addressing modes/operations (Frodo SC)
	enum OpCodes {
		DUMMY_00, DUMMY_01, DUMMY_02, DUMMY_03, DUMMY_04, DUMMY_05, DUMMY_06, DUMMY_07,
		DUMMY_08, DUMMY_09, DUMMY_0A, DUMMY_0B, DUMMY_0C, DUMMY_0D, DUMMY_0E, DUMMY_0F,
		DUMMY_10, DUMMY_11, DUMMY_12, DUMMY_13, DUMMY_14, DUMMY_15, DUMMY_16, DUMMY_17,
		
		// Read effective address, no extra cycles
		E_A_ZERO, // =0x18,
		E_A_ZEROX, E_A_ZEROX1,
		E_A_ZEROY, E_A_ZEROY1,
		E_A_ABS, E_A_ABS1,
		E_A_ABSX, E_A_ABSX1, E_A_ABSX2, E_A_ABSX3,
		E_A_ABSY, E_A_ABSY1, E_A_ABSY2, E_A_ABSY3,
		E_A_INDX, E_A_INDX1, E_A_INDX2, E_A_INDX3,
		E_A_INDY, E_A_INDY1, E_A_INDY2, E_A_INDY3, E_A_INDY4,

		// Read effective address, extra cycle on page crossing
		E_AE_ABSX, E_AE_ABSX1, E_AE_ABSX2,
		E_AE_ABSY, E_AE_ABSY1, E_AE_ABSY2,
		E_AE_INDY, E_AE_INDY1, E_AE_INDY2, E_AE_INDY3,

		// Read operand and write it back (for RMW instructions), no extra cycles
		E_M_ZERO,
		E_M_ZEROX, E_M_ZEROX1,
		E_M_ZEROY, E_M_ZEROY1,
		E_M_ABS, E_M_ABS1,
		E_M_ABSX, E_M_ABSX1, E_M_ABSX2, E_M_ABSX3,
		E_M_ABSY, E_M_ABSY1, E_M_ABSY2, E_M_ABSY3,
		E_M_INDX, E_M_INDX1, E_M_INDX2, E_M_INDX3,
		E_M_INDY, E_M_INDY1, E_M_INDY2, E_M_INDY3, E_M_INDY4,
		E_RMW_DO_IT, E_RMW_DO_IT1,

		// Operations (_I = Immediate/Indirect, _A = Accumulator)
		E_O_LDA, E_O_LDA_I, E_O_LDX, E_O_LDX_I, E_O_LDY, E_O_LDY_I,
		E_O_STA, E_O_STX, E_O_STY,
		E_O_TAX, E_O_TXA, E_O_TAY, E_O_TYA, E_O_TSX, E_O_TXS,
		E_O_ADC, E_O_ADC_I, E_O_SBC, E_O_SBC_I,
		E_O_INX, E_O_DEX, E_O_INY, E_O_DEY, E_O_INC, E_O_DEC,
		E_O_AND, E_O_AND_I, E_O_ORA, E_O_ORA_I, E_O_EOR, E_O_EOR_I,
		E_O_CMP, E_O_CMP_I, E_O_CPX, E_O_CPX_I, E_O_CPY, E_O_CPY_I,
		E_O_BIT,
		E_O_ASL, E_O_ASL_A, E_O_LSR, E_O_LSR_A, E_O_ROL, E_O_ROL_A, E_O_ROR, E_O_ROR_A,
		E_O_PHA, E_O_PHA1, E_O_PLA, E_O_PLA1, E_O_PLA2,
		E_O_PHP, E_O_PHP1, E_O_PLP, E_O_PLP1, E_O_PLP2,
		E_O_JMP, E_O_JMP1, E_O_JMP_I, E_O_JMP_I1,
		E_O_JSR, E_O_JSR1, E_O_JSR2, E_O_JSR3, E_O_JSR4,
		E_O_RTS, E_O_RTS1, E_O_RTS2, E_O_RTS3, E_O_RTS4,
		E_O_RTI, E_O_RTI1, E_O_RTI2, E_O_RTI3, E_O_RTI4,
		E_O_BRK, E_O_BRK1, E_O_BRK2, E_O_BRK3, E_O_BRK4, E_O_BRK5, E_O_BRK5NMI,
		E_O_BCS, E_O_BCC, E_O_BEQ, E_O_BNE, E_O_BVS, E_O_BVC, E_O_BMI, E_O_BPL,
		E_O_BRANCH_NP, E_O_BRANCH_BP, E_O_BRANCH_BP1, E_O_BRANCH_FP, E_O_BRANCH_FP1,
		E_O_SEC, E_O_CLC, E_O_SED, E_O_CLD, E_O_SEI, E_O_CLI, E_O_CLV,
		E_O_NOP,

		E_O_NOP_I, E_O_NOP_A,
		E_O_LAX, E_O_SAX,
		E_O_SLO, E_O_RLA, E_O_SRE, E_O_RRA, E_O_DCP, E_O_ISB,
		E_O_ANC_I, E_O_ASR_I, E_O_ARR_I, E_O_ANE_I, E_O_LXA_I, E_O_SBX_I,
		E_O_LAS, E_O_SHS, E_O_SHY, E_O_SHX, E_O_SHA,
		E_O_EXT
	};

	final static byte A_ZERO = (byte) E_A_ZERO.ordinal();// =0x18,
	final static byte A_ZEROX = (byte) E_A_ZEROX.ordinal();
	final static byte A_ZEROX1 = (byte) E_A_ZEROX1.ordinal();
	final static byte A_ZEROY = (byte) E_A_ZEROY.ordinal();
	final static byte A_ZEROY1 = (byte) E_A_ZEROY1.ordinal();
	final static byte A_ABS = (byte) E_A_ABS.ordinal();
	final static byte A_ABS1 = (byte) E_A_ABS1.ordinal();
	final static byte A_ABSX = (byte) E_A_ABSX.ordinal();
	final static byte A_ABSX1 = (byte) E_A_ABSX1.ordinal();
	final static byte A_ABSX2 = (byte) E_A_ABSX2.ordinal();
	final static byte A_ABSX3 = (byte) E_A_ABSX3.ordinal();
	final static byte A_ABSY = (byte) E_A_ABSY.ordinal();
	final static byte A_ABSY1 = (byte) E_A_ABSY1.ordinal();
	final static byte A_ABSY2 = (byte) E_A_ABSY2.ordinal();
	final static byte A_ABSY3 = (byte) E_A_ABSY3.ordinal();
	final static byte A_INDX = (byte) E_A_INDX.ordinal();
	final static byte A_INDX1 = (byte) E_A_INDX1.ordinal();
	final static byte A_INDX2 = (byte) E_A_INDX2.ordinal();
	final static byte A_INDX3 = (byte) E_A_INDX3.ordinal();
	final static byte A_INDY = (byte) E_A_INDY.ordinal();
	final static byte A_INDY1 = (byte) E_A_INDY1.ordinal();
	final static byte A_INDY2 = (byte) E_A_INDY2.ordinal();
	final static byte A_INDY3 = (byte) E_A_INDY3.ordinal();
	final static byte A_INDY4 = (byte) E_A_INDY4.ordinal();
	final static byte AE_ABSX = (byte) E_AE_ABSX.ordinal();
	final static byte AE_ABSX1 = (byte) E_AE_ABSX1.ordinal();
	final static byte AE_ABSX2 = (byte) E_AE_ABSX2.ordinal();
	final static byte AE_ABSY = (byte) E_AE_ABSY.ordinal();
	final static byte AE_ABSY1 = (byte) E_AE_ABSY1.ordinal();
	final static byte AE_ABSY2 = (byte) E_AE_ABSY2.ordinal();
	final static byte AE_INDY = (byte) E_AE_INDY.ordinal();
	final static byte AE_INDY1 = (byte) E_AE_INDY1.ordinal();
	final static byte AE_INDY2 = (byte) E_AE_INDY2.ordinal();
	final static byte AE_INDY3 = (byte) E_AE_INDY3.ordinal();
	final static byte M_ZERO = (byte) E_M_ZERO.ordinal();
	final static byte M_ZEROX = (byte) E_M_ZEROX.ordinal();
	final static byte M_ZEROX1 = (byte) E_M_ZEROX1.ordinal();
	final static byte M_ZEROY = (byte) E_M_ZEROY.ordinal();
	final static byte M_ZEROY1 = (byte) E_M_ZEROY1.ordinal();
	final static byte M_ABS = (byte) E_M_ABS.ordinal();
	final static byte M_ABS1 = (byte) E_M_ABS1.ordinal();
	final static byte M_ABSX = (byte) E_M_ABSX.ordinal();
	final static byte M_ABSX1 = (byte) E_M_ABSX1.ordinal();
	final static byte M_ABSX2 = (byte) E_M_ABSX2.ordinal();
	final static byte M_ABSX3 = (byte) E_M_ABSX3.ordinal();
	final static byte M_ABSY = (byte) E_M_ABSY.ordinal();
	final static byte M_ABSY1 = (byte) E_M_ABSY1.ordinal();
	final static byte M_ABSY2 = (byte) E_M_ABSY2.ordinal();
	final static byte M_ABSY3 = (byte) E_M_ABSY3.ordinal();
	final static byte M_INDX = (byte) E_M_INDX.ordinal();
	final static byte M_INDX1 = (byte) E_M_INDX1.ordinal();
	final static byte M_INDX2 = (byte) E_M_INDX2.ordinal();
	final static byte M_INDX3 = (byte) E_M_INDX3.ordinal();
	final static byte M_INDY = (byte) E_M_INDY.ordinal();
	final static byte M_INDY1 = (byte) E_M_INDY1.ordinal();
	final static byte M_INDY2 = (byte) E_M_INDY2.ordinal();
	final static byte M_INDY3 = (byte) E_M_INDY3.ordinal();
	final static byte M_INDY4 = (byte) E_M_INDY4.ordinal();
	final static byte RMW_DO_IT = (byte) E_RMW_DO_IT.ordinal();
	final static byte RMW_DO_IT1 = (byte) E_RMW_DO_IT1.ordinal();
	final static byte O_LDA = (byte) E_O_LDA.ordinal();
	final static byte O_LDA_I = (byte) E_O_LDA_I.ordinal();
	final static byte O_LDX = (byte) E_O_LDX.ordinal();
	final static byte O_LDX_I = (byte) E_O_LDX_I.ordinal();
	final static byte O_LDY = (byte) E_O_LDY.ordinal();
	final static byte O_LDY_I = (byte) E_O_LDY_I.ordinal();
	final static byte O_STA = (byte) E_O_STA.ordinal();
	final static byte O_STX = (byte) E_O_STX.ordinal();
	final static byte O_STY = (byte) E_O_STY.ordinal();
	final static byte O_TAX = (byte) E_O_TAX.ordinal();
	final static byte O_TXA = (byte) E_O_TXA.ordinal();
	final static byte O_TAY = (byte) E_O_TAY.ordinal();
	final static byte O_TYA = (byte) E_O_TYA.ordinal();
	final static byte O_TSX = (byte) E_O_TSX.ordinal();
	final static byte O_TXS = (byte) E_O_TXS.ordinal();
	final static byte O_ADC = (byte) E_O_ADC.ordinal();
	final static byte O_ADC_I = (byte) E_O_ADC_I.ordinal();
	final static byte O_SBC = (byte) E_O_SBC.ordinal();
	final static byte O_SBC_I = (byte) E_O_SBC_I.ordinal();
	final static byte O_INX = (byte) E_O_INX.ordinal();
	final static byte O_DEX = (byte) E_O_DEX.ordinal();
	final static byte O_INY = (byte) E_O_INY.ordinal();
	final static byte O_DEY = (byte) E_O_DEY.ordinal();
	final static byte O_INC = (byte) E_O_INC.ordinal();
	final static byte O_DEC = (byte) E_O_DEC.ordinal();
	final static byte O_AND = (byte) E_O_AND.ordinal();
	final static byte O_AND_I = (byte) E_O_AND_I.ordinal();
	final static byte O_ORA = (byte) E_O_ORA.ordinal();
	final static byte O_ORA_I = (byte) E_O_ORA_I.ordinal();
	final static byte O_EOR = (byte) E_O_EOR.ordinal();
	final static byte O_EOR_I = (byte) E_O_EOR_I.ordinal();
	final static byte O_CMP = (byte) E_O_CMP.ordinal();
	final static byte O_CMP_I = (byte) E_O_CMP_I.ordinal();
	final static byte O_CPX = (byte) E_O_CPX.ordinal();
	final static byte O_CPX_I = (byte) E_O_CPX_I.ordinal();
	final static byte O_CPY = (byte) E_O_CPY.ordinal();
	final static byte O_CPY_I = (byte) E_O_CPY_I.ordinal();
	final static byte O_BIT = (byte) E_O_BIT.ordinal();
	final static byte O_ASL = (byte) E_O_ASL.ordinal();
	final static byte O_ASL_A = (byte) E_O_ASL_A.ordinal();
	final static byte O_LSR = (byte) E_O_LSR.ordinal();
	final static byte O_LSR_A = (byte) E_O_LSR_A.ordinal();
	final static byte O_ROL = (byte) E_O_ROL.ordinal();
	final static byte O_ROL_A = (byte) E_O_ROL_A.ordinal();
	final static byte O_ROR = (byte) E_O_ROR.ordinal();
	final static byte O_ROR_A = (byte) E_O_ROR_A.ordinal();
	final static byte O_PHA = (byte) E_O_PHA.ordinal();
	final static byte O_PHA1 = (byte) E_O_PHA1.ordinal();
	final static byte O_PLA = (byte) E_O_PLA.ordinal();
	final static byte O_PLA1 = (byte) E_O_PLA1.ordinal();
	final static byte O_PLA2 = (byte) E_O_PLA2.ordinal();
	final static byte O_PHP = (byte) E_O_PHP.ordinal();
	final static byte O_PHP1 = (byte) E_O_PHP1.ordinal();
	final static byte O_PLP = (byte) E_O_PLP.ordinal();
	final static byte O_PLP1 = (byte) E_O_PLP1.ordinal();
	final static byte O_PLP2 = (byte) E_O_PLP2.ordinal();
	final static byte O_JMP = (byte) E_O_JMP.ordinal();
	final static byte O_JMP1 = (byte) E_O_JMP1.ordinal();
	final static byte O_JMP_I = (byte) E_O_JMP_I.ordinal();
	final static byte O_JMP_I1 = (byte) E_O_JMP_I1.ordinal();
	final static byte O_JSR = (byte) E_O_JSR.ordinal();
	final static byte O_JSR1 = (byte) E_O_JSR1.ordinal();
	final static byte O_JSR2 = (byte) E_O_JSR2.ordinal();
	final static byte O_JSR3 = (byte) E_O_JSR3.ordinal();
	final static byte O_JSR4 = (byte) E_O_JSR4.ordinal();
	final static byte O_RTS = (byte) E_O_RTS.ordinal();
	final static byte O_RTS1 = (byte) E_O_RTS1.ordinal();
	final static byte O_RTS2 = (byte) E_O_RTS2.ordinal();
	final static byte O_RTS3 = (byte) E_O_RTS3.ordinal();
	final static byte O_RTS4 = (byte) E_O_RTS4.ordinal();
	final static byte O_RTI = (byte) E_O_RTI.ordinal();
	final static byte O_RTI1 = (byte) E_O_RTI1.ordinal();
	final static byte O_RTI2 = (byte) E_O_RTI2.ordinal();
	final static byte O_RTI3 = (byte) E_O_RTI3.ordinal();
	final static byte O_RTI4 = (byte) E_O_RTI4.ordinal();
	final static byte O_BRK = (byte) E_O_BRK.ordinal();
	final static byte O_BRK1 = (byte) E_O_BRK1.ordinal();
	final static byte O_BRK2 = (byte) E_O_BRK2.ordinal();
	final static byte O_BRK3 = (byte) E_O_BRK3.ordinal();
	final static byte O_BRK4 = (byte) E_O_BRK4.ordinal();
	final static byte O_BRK5 = (byte) E_O_BRK5.ordinal();
	final static byte O_BRK5NMI = (byte) E_O_BRK5NMI.ordinal();
	final static byte O_BCS = (byte) E_O_BCS.ordinal();
	final static byte O_BCC = (byte) E_O_BCC.ordinal();
	final static byte O_BEQ = (byte) E_O_BEQ.ordinal();
	final static byte O_BNE = (byte) E_O_BNE.ordinal();
	final static byte O_BVS = (byte) E_O_BVS.ordinal();
	final static byte O_BVC = (byte) E_O_BVC.ordinal();
	final static byte O_BMI = (byte) E_O_BMI.ordinal();
	final static byte O_BPL = (byte) E_O_BPL.ordinal();
	final static byte O_BRANCH_NP = (byte) E_O_BRANCH_NP.ordinal();
	final static byte O_BRANCH_BP = (byte) E_O_BRANCH_BP.ordinal();
	final static byte O_BRANCH_BP1 = (byte) E_O_BRANCH_BP1.ordinal();
	final static byte O_BRANCH_FP = (byte) E_O_BRANCH_FP.ordinal();
	final static byte O_BRANCH_FP1 = (byte) E_O_BRANCH_FP1.ordinal();
	final static byte O_SEC = (byte) E_O_SEC.ordinal();
	final static byte O_CLC = (byte) E_O_CLC.ordinal();
	final static byte O_SED = (byte) E_O_SED.ordinal();
	final static byte O_CLD = (byte) E_O_CLD.ordinal();
	final static byte O_SEI = (byte) E_O_SEI.ordinal();
	final static byte O_CLI = (byte) E_O_CLI.ordinal();
	final static byte O_CLV = (byte) E_O_CLV.ordinal();
	final static byte O_NOP = (byte) E_O_NOP.ordinal();
	final static byte O_NOP_I = (byte) E_O_NOP_I.ordinal();
	final static byte O_NOP_A = (byte) E_O_NOP_A.ordinal();
	final static byte O_LAX = (byte) E_O_LAX.ordinal();
	final static byte O_SAX = (byte) E_O_SAX.ordinal();
	final static byte O_SLO = (byte) E_O_SLO.ordinal();
	final static byte O_RLA = (byte) E_O_RLA.ordinal();
	final static byte O_SRE = (byte) E_O_SRE.ordinal();
	final static byte O_RRA = (byte) E_O_RRA.ordinal();
	final static byte O_DCP = (byte) E_O_DCP.ordinal();
	final static byte O_ISB = (byte) E_O_ISB.ordinal();
	final static byte O_ANC_I = (byte) E_O_ANC_I.ordinal();
	final static byte O_ASR_I = (byte) E_O_ASR_I.ordinal();
	final static byte O_ARR_I = (byte) E_O_ARR_I.ordinal();
	final static byte O_ANE_I = (byte) E_O_ANE_I.ordinal();
	final static byte O_LXA_I = (byte) E_O_LXA_I.ordinal();
	final static byte O_SBX_I = (byte) E_O_SBX_I.ordinal();
	final static byte O_LAS = (byte) E_O_LAS.ordinal();
	final static byte O_SHS = (byte) E_O_SHS.ordinal();
	final static byte O_SHY = (byte) E_O_SHY.ordinal();
	final static byte O_SHX = (byte) E_O_SHX.ordinal();
	final static byte O_SHA = (byte) E_O_SHA.ordinal();
	final static byte O_EXT = (byte) E_O_EXT.ordinal();

	// Addressing mode for each opcode (first part of execution) (Frodo SC)
	final static byte[] ModeTab = {
			O_BRK,	A_INDX,	1,		M_INDX,	A_ZERO,	A_ZERO,	M_ZERO,	M_ZERO,	// 00
			O_PHP,	O_ORA_I,O_ASL_A,O_ANC_I,A_ABS,	A_ABS,	M_ABS,	M_ABS,
			O_BPL,	AE_INDY,1,		M_INDY,	A_ZEROX,A_ZEROX,M_ZEROX,M_ZEROX,// 10
			O_CLC,	AE_ABSY,O_NOP,	M_ABSY,	AE_ABSX,AE_ABSX,M_ABSX,	M_ABSX,
			O_JSR,	A_INDX,	1,		M_INDX,	A_ZERO,	A_ZERO,	M_ZERO,	M_ZERO,	// 20
			O_PLP,	O_AND_I,O_ROL_A,O_ANC_I,A_ABS,	A_ABS,	M_ABS,	M_ABS,
			O_BMI,	AE_INDY,1,		M_INDY,	A_ZEROX,A_ZEROX,M_ZEROX,M_ZEROX,// 30
			O_SEC,	AE_ABSY,O_NOP,	M_ABSY,	AE_ABSX,AE_ABSX,M_ABSX,	M_ABSX,
			O_RTI,	A_INDX,	1,		M_INDX,	A_ZERO,	A_ZERO,	M_ZERO,	M_ZERO,	// 40
			O_PHA,	O_EOR_I,O_LSR_A,O_ASR_I,O_JMP,	A_ABS,	M_ABS,	M_ABS,
			O_BVC,	AE_INDY,1,		M_INDY,	A_ZEROX,A_ZEROX,M_ZEROX,M_ZEROX,// 50
			O_CLI,	AE_ABSY,O_NOP,	M_ABSY,	AE_ABSX,AE_ABSX,M_ABSX,	M_ABSX,
			O_RTS,	A_INDX,	1,		M_INDX,	A_ZERO,	A_ZERO,	M_ZERO,	M_ZERO,	// 60
			O_PLA,	O_ADC_I,O_ROR_A,O_ARR_I,A_ABS,	A_ABS,	M_ABS,	M_ABS,
			O_BVS,	AE_INDY,1,		M_INDY,	A_ZEROX,A_ZEROX,M_ZEROX,M_ZEROX,// 70
			O_SEI,	AE_ABSY,O_NOP,	M_ABSY,	AE_ABSX,AE_ABSX,M_ABSX,	M_ABSX,
			O_NOP_I,A_INDX,	O_NOP_I,A_INDX,	A_ZERO,	A_ZERO,	A_ZERO,	A_ZERO,	// 80
			O_DEY,	O_NOP_I,O_TXA,	O_ANE_I,A_ABS,	A_ABS,	A_ABS,	A_ABS,
			O_BCC,	A_INDY,	1,		A_INDY,	A_ZEROX,A_ZEROX,A_ZEROY,A_ZEROY,// 90
			O_TYA,	A_ABSY,	O_TXS,	A_ABSY,	A_ABSX,	A_ABSX,	A_ABSY,	A_ABSY,
			O_LDY_I,A_INDX,	O_LDX_I,A_INDX,	A_ZERO,	A_ZERO,	A_ZERO,	A_ZERO,	// a0
			O_TAY,	O_LDA_I,O_TAX,	O_LXA_I,A_ABS,	A_ABS,	A_ABS,	A_ABS,
			O_BCS,	AE_INDY,1,		AE_INDY,A_ZEROX,A_ZEROX,A_ZEROY,A_ZEROY,// b0
			O_CLV,	AE_ABSY,O_TSX,	AE_ABSY,AE_ABSX,AE_ABSX,AE_ABSY,AE_ABSY,
			O_CPY_I,A_INDX,	O_NOP_I,M_INDX,	A_ZERO,	A_ZERO,	M_ZERO,	M_ZERO,	// c0
			O_INY,	O_CMP_I,O_DEX,	O_SBX_I,A_ABS,	A_ABS,	M_ABS,	M_ABS,
			O_BNE,	AE_INDY,1,		M_INDY,	A_ZEROX,A_ZEROX,M_ZEROX,M_ZEROX,// d0
			O_CLD,	AE_ABSY,O_NOP,	M_ABSY,	AE_ABSX,AE_ABSX,M_ABSX,	M_ABSX,
			O_CPX_I,A_INDX,	O_NOP_I,M_INDX,	A_ZERO,	A_ZERO,	M_ZERO,	M_ZERO,	// e0
			O_INX,	O_SBC_I,O_NOP,	O_SBC_I,A_ABS,	A_ABS,	M_ABS,	M_ABS,
			O_BEQ,	AE_INDY,O_EXT,	M_INDY,	A_ZEROX,A_ZEROX,M_ZEROX,M_ZEROX,// f0
			O_SED,	AE_ABSY,O_NOP,	M_ABSY,	AE_ABSX,AE_ABSX,M_ABSX,	M_ABSX
		};

	// Operation for each opcode (second part of execution) (Frodo SC)
	final static byte[] OpTab = {
			1,		O_ORA,	1,		O_SLO,	O_NOP_A,O_ORA,	O_ASL,	O_SLO,	// 00
			1,		1,		1,		1,		O_NOP_A,O_ORA,	O_ASL,	O_SLO,
			1,		O_ORA,	1,		O_SLO,	O_NOP_A,O_ORA,	O_ASL,	O_SLO,	// 10
			1,		O_ORA,	1,		O_SLO,	O_NOP_A,O_ORA,	O_ASL,	O_SLO,
			1,		O_AND,	1,		O_RLA,	O_BIT,	O_AND,	O_ROL,	O_RLA,	// 20
			1,		1,		1,		1,		O_BIT,	O_AND,	O_ROL,	O_RLA,
			1,		O_AND,	1,		O_RLA,	O_NOP_A,O_AND,	O_ROL,	O_RLA,	// 30
			1,		O_AND,	1,		O_RLA,	O_NOP_A,O_AND,	O_ROL,	O_RLA,
			1,		O_EOR,	1,		O_SRE,	O_NOP_A,O_EOR,	O_LSR,	O_SRE,	// 40
			1,		1,		1,		1,		1,		O_EOR,	O_LSR,	O_SRE,
			1,		O_EOR,	1,		O_SRE,	O_NOP_A,O_EOR,	O_LSR,	O_SRE,	// 50
			1,		O_EOR,	1,		O_SRE,	O_NOP_A,O_EOR,	O_LSR,	O_SRE,
			1,		O_ADC,	1,		O_RRA,	O_NOP_A,O_ADC,	O_ROR,	O_RRA,	// 60
			1,		1,		1,		1,		O_JMP_I,O_ADC,	O_ROR,	O_RRA,
			1,		O_ADC,	1,		O_RRA,	O_NOP_A,O_ADC,	O_ROR,	O_RRA,	// 70
			1,		O_ADC,	1,		O_RRA,	O_NOP_A,O_ADC,	O_ROR,	O_RRA,
			1,		O_STA,	1,		O_SAX,	O_STY,	O_STA,	O_STX,	O_SAX,	// 80
			1,		1,		1,		1,		O_STY,	O_STA,	O_STX,	O_SAX,
			1,		O_STA,	1,		O_SHA,	O_STY,	O_STA,	O_STX,	O_SAX,	// 90
			1,		O_STA,	1,		O_SHS,	O_SHY,	O_STA,	O_SHX,	O_SHA,
			1,		O_LDA,	1,		O_LAX,	O_LDY,	O_LDA,	O_LDX,	O_LAX,	// a0
			1,		1,		1,		1,		O_LDY,	O_LDA,	O_LDX,	O_LAX,
			1,		O_LDA,	1,		O_LAX,	O_LDY,	O_LDA,	O_LDX,	O_LAX,	// b0
			1,		O_LDA,	1,		O_LAS,	O_LDY,	O_LDA,	O_LDX,	O_LAX,
			1,		O_CMP,	1,		O_DCP,	O_CPY,	O_CMP,	O_DEC,	O_DCP,	// c0
			1,		1,		1,		1,		O_CPY,	O_CMP,	O_DEC,	O_DCP,
			1,		O_CMP,	1,		O_DCP,	O_NOP_A,O_CMP,	O_DEC,	O_DCP,	// d0
			1,		O_CMP,	1,		O_DCP,	O_NOP_A,O_CMP,	O_DEC,	O_DCP,
			1,		O_SBC,	1,		O_ISB,	O_CPX,	O_SBC,	O_INC,	O_ISB,	// e0
			1,		1,		1,		1,		O_CPX,	O_SBC,	O_INC,	O_ISB,
			1,		O_SBC,	1,		O_ISB,	O_NOP_A,O_SBC,	O_INC,	O_ISB,	// f0
			1,		O_SBC,	1,		O_ISB,	O_NOP_A,O_SBC,	O_INC,	O_ISB
		};

	/*
	 *  EmulCycle() function
	 */
	
	// Pop processor flags from the stack
//	#define pop_flags() \
//		read_to(sp | 0x100, data); \
//		n_flag = data; \
//		v_flag = data & 0x40; \
//		d_flag = data & 0x08; \
//		i_flag = data & 0x04; \
//		z_flag = !(data & 0x02); \
//		c_flag = data & 0x01;

	// Push processor flags onto the stack
//	#define push_flags(b_flag) \
//		data = 0x20 | (n_flag & 0x80); \
//		if (v_flag) data |= 0x40; \
//		if (b_flag) data |= 0x10; \
//		if (d_flag) data |= 0x08; \
//		if (i_flag) data |= 0x04; \
//		if (!z_flag) data |= 0x02; \
//		if (c_flag) data |= 0x01; \
//		write_byte((sp-- & 0xff) | 0x100, data);

	// Other macros

//	// Branch (cycle 1)
//	#define Branch(flag) \
//			read_to(pc++, data);  \
//			if (flag) { 
//				ar = pc + (int8)data; \
//				if ((ar >> 8) != (pc >> 8)) { \
//					if (data & 0x80) \
//						state = O_BRANCH_BP; \
//					else \
//						state = O_BRANCH_FP; \
//				} else \
//					state = O_BRANCH_NP; \
//			} else \
//				state = 0; \
//			break;	

//	// Set N and Z flags according to byte
//	#define set_nz(x) (z_flag = n_flag = (x))
	
//	// Address fetch of RMW instruction done, now read and write operand
//	#define DoRMW state = RMW_DO_IT; break;

//	// Operand fetch done, now execute opcode
//	#define Execute state = OpTab[op & 0xff]; break;

//	// Last cycle of opcode
//	#define Last state = 0; break;
	
	
// Finished macro regexes

//	pop_flags\(\);
//	\r\n// macro expansion pop_flags();\r\nread_to(sp | 0x100, data);\r\nn_flag = data;\r\nv_flag = (data & 0x40) != 0;\r\nd_flag = (data & 0x08) != 0;\r\ni_flag = (data & 0x04) != 0;\r\nz_flag = (byte) ((data & 0x02) == 0? 1:0);\r\nc_flag = (data & 0x01) != 0;\r\n//end pop_flags();\r\n

//	push_flags\((.*)\);
//	\r\n// macro expansion push_flags(\1);\r\ndata = (byte) (0x20 | (n_flag& 0x80));\r\nif (v_flag) data |= 0x40;\r\nif (\1) data |= 0x10;\r\nif (d_flag) data |= 0x08;\r\nif (i_flag) data |= 0x04;\r\nif (z_flag != 0) data |= 0x02;\r\nif (c_flag) data |= 0x01;\r\nwrite_byte((sp-- & 0xff) | 0x100, data);\r\n// end push_flags(\1);

//	Branch\((.*)\);
//	\r\n// macro expansion Branch((\1) != 0);\r\nread_to(pc++, data);\r\nif (\1) {\r\n\tar = (short) (pc + data);\r\n\tif (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {\r\n\t\tif ((data & 0x80) != 0)\r\nt\t\tstate = O_BRANCH_BP;\r\n\t\telse\r\n\t\t\tstate = O_BRANCH_FP;\r\n\t} else\r\n\t\tstate = O_BRANCH_NP;\r\n} else\r\n\tstate = 0;\r\nbreak;\r\n// end Branch(\1);
	
//	set_nz\((.*)\)
//	z_flag = n_flag = (\1)

//	DoRMW
//	state = RMW_DO_IT; break;
	
//	Execute
//	state = OpTab[op & 0xff]; break;
	
//	Last
//	state = 0; break;

//	read_to\(([^,]+,\s*(.*)\);
//	read_to\(([^,]+,\s*(.*)\);if (BALow) { result.baLowEscape = true; return result; } \2 = read_byte(\1);

//	read_idle\((.*)\);
//	if (BALow) { result.baLowEscape = true; return result; } read_byte(\1);
	
	public static enum Mode {
		ACC (1, "{opc} A"),
		IMP (1, "{opc}"),
		IMM (2, "{opc} #${p1}"),
		REL (2, "{opc} ${ea}"),
		ABS (3, "{opc} ${p2}{p1}"),
		ABSX(3, "{opc} ${p2}{p1},X"),
		ABSY(3, "{opc} ${p2}{p1},Y"),
		ZP  (2, "{opc} ${p1}"),
		ZPX (2, "{opc} ${p1},X"),
		XIND(2, "{opc} (${p1},X)"),
		INDX(2, "{opc} (${p1}),X"),
		INDY(2, "{opc} (${p1}),Y");
		
		int len;
		String format;
		private Mode(int len, String format) {
			this.len = len;
			this.format = format;
		}
	}
	class CommonResult {
		boolean baLowEscape;
		boolean opHandled;
	}
	private boolean BALow;
	private int interruptAt;
	private boolean debug = true, debugInterrupt = false, trace = false, traceStack = false;

	public void setDebug(boolean b) {
		debug = b;
	}
	public void setDebugInterrupt(boolean b) {
		debugInterrupt = b;
	}
	public void setTrace(boolean b) {
		trace = b;
	}
	
	CommonResult commonOps(boolean IS_CPU_1541) {
		byte data, tmp;
		CommonResult result = new CommonResult();
		result.opHandled = true;
		switch (OpCodes.values()[((int)state) & 0xff]) {

		// Opcode fetch (cycle 0)
		case DUMMY_00:

			if (ROMCall.exists(pc & 0xffff) && debug && !Sam.active) {
				ROMCall call = ROMCall.get(pc & 0xffff);
				if (call.print) {
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < 255 - (sp & 0xff); i++) {
						sb.append(' ');
					}
					sb.append(call);
					System.out.println(sb);
				}
				if (call.brk) {
					System.out.println(); // Breakpoint here
					trace = true;
					traceStack = true;
				}
			}
			if (read_byte(256) == 'Z') {
				trace = true;
				traceStack = true;
			}
			if (debug && trace) {
				int adr = pc & 0xffff;
				byte op, p1 = 0, p2 = 0;
				op = read_byte(pc);
				if (adr < 0xffff) {
					p1 = read_byte(adr + 1);
				}
				if (adr < 0xfffe) {
					p2 = read_byte(adr + 2);
				}
				int opi = op & 0xff;
				Instruction inst = Instruction.values()[opi];
				Mode mode = inst.mode;
				int len = 1;
				if (mode != null) {
					len = mode.len;
				}
				StringBuilder sb = new StringBuilder();
				String flags = getNVBsDIZC();
				sb.append(String.format("CPU [a:%02x x:%02x y:%02x ar:%04x ar2:%04x, sp:%02x, NVBsDIZC:%s] %04x: %02x", a, x, y, ar, ar2, sp, flags, pc, op));
				if (len > 1) {
					sb.append(String.format(" %02x", p1));
				} else {
					sb.append("   ");
				}
				if (len > 2) {
					sb.append(String.format(" %02x", p2));
				} else {
					sb.append("   ");
				}
				sb.append("   ");
				if (pc == 0xb8fe) {
					System.out.printf("Suspect %f + %f\n", getFloat(FAC), getFloat(ARG));
				}
				if (traceStack) {
					sb.append((inst.toString(adr, p1, p2) + "                              ").substring(0, 20));
				} else {
					sb.append(inst.toString(adr, p1, p2));
				}
				if (traceStack) {
					sb.append("Stack: [");
					for (int i = (sp + 1) & 0xff | 0x100; i < 0x200; i++) {
						sb.append(String.format("%02x", read_byte(i)));
						if (i != 0x1ff) {
							sb.append(", ");
						}
					}
					sb.append("]");
				}
				System.out.println(sb);
				if (inst.mode == null) {
					System.out.println("Illegal op...");
				}
				if (adr == 0xa42a) {
					System.out.println("CC problem!");
					// a42a: c4 34      CPY $34
					// Y = 8, $34 = -96
				}
			}

			if (BALow) { result.baLowEscape = true; return result; } op = read_byte(pc++);
			
			state = ModeTab[op & 0xff];
			break;


		// IRQ
		case DUMMY_08:
			interruptAt = pc;
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = 0x0009;
			break;
		case DUMMY_09:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = 0x000a;
			break;
		case DUMMY_0A:
			write_byte((sp-- & 0xff) | 0x100, (byte) (pc >> 8));
			state = 0x000b;
			break;
		case DUMMY_0B:
			write_byte((sp-- & 0xff) | 0x100, (byte) pc);
			state = 0x000c;
			break;
		case DUMMY_0C:
			
// macro expansion push_flags(false);
data = (byte) (0x20 | (n_flag& 0x80));
if (v_flag) data |= 0x40;
if (false) data |= 0x10;
if (d_flag) data |= 0x08;
if (i_flag) data |= 0x04;
if (z_flag != 0) data |= 0x02;
if (c_flag) data |= 0x01;
write_byte((sp-- & 0xff) | 0x100, data);
// end push_flags(false);
			i_flag = true;
			state = 0x000d;
			break;
		case DUMMY_0D:
			if (BALow) { result.baLowEscape = true; return result; } pc = read_byte(0xfffe) & 0xff;
			state = 0x000e;
			break;
		case DUMMY_0E:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(0xffff);
			pc |= (data << 8) & 0xff00;

			int pcl = (read_byte((((sp+3) & 0xff) | 0x100) & 0xff) | ((read_byte(((sp+2) & 0xff) | 0x100) << 8) & 0xff00)) & 0xffff;
			if (debug && debugInterrupt && !Sam.active) {
				System.out.printf("INTERRUPT %04x --> %04x    pc stacked: %04x\n", interruptAt, pc, pcl);
			}
			state = 0; break;


		// NMI
		case DUMMY_10:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = 0x0011;
			break;
		case DUMMY_11:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = 0x0012;
			break;
		case DUMMY_12:
			write_byte((sp-- & 0xff) | 0x100, (byte) (pc >> 8));
			state = 0x0013;
			break;
		case DUMMY_13:
			write_byte((sp-- & 0xff) | 0x100, (byte) pc);
			state = 0x0014;
			break;
		case DUMMY_14:
			
// macro expansion push_flags(false);
data = (byte) (0x20 | (n_flag& 0x80));
if (v_flag) data |= 0x40;
if (false) data |= 0x10;
if (d_flag) data |= 0x08;
if (i_flag) data |= 0x04;
if (z_flag != 0) data |= 0x02;
if (c_flag) data |= 0x01;
write_byte((sp-- & 0xff) | 0x100, data);
// end push_flags(false);
			i_flag = true;
			state = 0x0015;
			break;
		case DUMMY_15:
			if (BALow) { result.baLowEscape = true; return result; } pc = read_byte(0xfffa) & 0xff;
			state = 0x0016;
			break;
		case DUMMY_16:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(0xfffb);
			pc |= (data << 8) & 0xff00;
			state = 0; break;


		// Addressing modes: Fetch effective address, no extra cycles (-> ar)
		case E_A_ZERO:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = OpTab[op & 0xff]; break;

		case E_A_ZEROX:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = A_ZEROX1;
			break;
		case E_A_ZEROX1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar = (short) ((ar + (x & 0xff)) & 0xff);
			state = OpTab[op & 0xff]; break;

		case E_A_ZEROY:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = A_ZEROY1;
			break;
		case E_A_ZEROY1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar = (short) ((ar + (y & 0xff)) & 0xff);
			state = OpTab[op & 0xff]; break;

		case E_A_ABS:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = A_ABS1;
			break;
		case E_A_ABS1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			ar = (short) (ar | ((data << 8) & 0xff00));
			state = OpTab[op & 0xff]; break;

		case E_A_ABSX:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = A_ABSX1;
			break;
		case E_A_ABSX1:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);	// Note: Some undocumented opcodes rely on the value of ar2
			if (ar + (x & 0xff) < 0x100)
				state = A_ABSX2;
			else
				state = A_ABSX3;
			ar = (short) ((ar + (x & 0xff)) & 0xff | ((ar2 << 8) & 0xff00));
			break;
		case E_A_ABSX2:	// No page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = OpTab[op & 0xff]; break;
		case E_A_ABSX3:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = OpTab[op & 0xff]; break;

		case E_A_ABSY:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = A_ABSY1;
			break;
		case E_A_ABSY1:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);	// Note: Some undocumented opcodes rely on the value of ar2
			if (ar + (y & 0xff) < 0x100)
				state = A_ABSY2;
			else
				state = A_ABSY3;
			ar = (short) ((ar + (y & 0xff)) & 0xff | ((ar2 << 8) & 0xff00));
			break;
		case E_A_ABSY2:	// No page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = OpTab[op & 0xff]; break;
		case E_A_ABSY3:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = OpTab[op & 0xff]; break;

		case E_A_INDX:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);
			state = A_INDX1;
			break;
		case E_A_INDX1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar2);
			ar2 = (short) ((ar2 + (x & 0xff)) & 0xff);
			state = A_INDX2;
			break;
		case E_A_INDX2:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(ar2)) & 0xff);
			state = A_INDX3;
			break;
		case E_A_INDX3:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((ar2 + 1) & 0xff);
			ar = (short) (ar | ((data << 8) & 0xff00));
			state = OpTab[op & 0xff]; break;

		case E_A_INDY:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);
			state = A_INDY1;
			break;
		case E_A_INDY1:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(ar2)) & 0xff);
			state = A_INDY2;
			break;
		case E_A_INDY2:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = read_byte((ar2 + 1) & 0xff);	// Note: Some undocumented opcodes rely on the value of ar2
			if (ar + (y & 0xff) < 0x100)
				state = A_INDY3;
			else
				state = A_INDY4;
			ar = (short) ((ar + (y & 0xff)) & 0xff | ((ar2 << 8) & 0xff00));
			break;
		case E_A_INDY3:	// No page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = OpTab[op & 0xff]; break;
		case E_A_INDY4:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = OpTab[op & 0xff]; break;


		// Addressing modes: Fetch effective address, extra cycle on page crossing (-> ar)
		case E_AE_ABSX:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = AE_ABSX1;
			break;
		case E_AE_ABSX1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			if (ar + (x & 0xff) < 0x100) {
				ar = (short) ((ar + (x & 0xff)) & 0xff | ((data << 8) & 0xff00));
				state = OpTab[op & 0xff]; break;
			} else {
				ar = (short) ((ar + (x & 0xff)) & 0xff | ((data << 8) & 0xff00));
				state = AE_ABSX2;
			}
			break;
		case E_AE_ABSX2:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = OpTab[op & 0xff]; break;

		case E_AE_ABSY:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = AE_ABSY1;
			break;
		case E_AE_ABSY1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			if (ar + (y & 0xff) < 0x100) {
				ar = (short) ((ar + (y & 0xff)) & 0xff | ((data << 8) & 0xff00));
				state = OpTab[op & 0xff]; break;
			} else {
				ar = (short) ((ar + (y & 0xff)) & 0xff | ((data << 8) & 0xff00));
				state = AE_ABSY2;
			}
			break;
		case E_AE_ABSY2:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = OpTab[op & 0xff]; break;

		case E_AE_INDY:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);
			state = AE_INDY1;
			break;
		case E_AE_INDY1:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(ar2)) & 0xff);
			state = AE_INDY2;
			break;
		case E_AE_INDY2:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((ar2 + 1) & 0xff);
			if (ar + (y & 0xff) < 0x100) {
				ar = (short) ((ar + (y & 0xff)) & 0xff | ((data << 8) & 0xff00));
				state = OpTab[op & 0xff]; break;
			} else {
				ar = (short) ((ar + (y & 0xff)) & 0xff | ((data << 8) & 0xff00));
				state = AE_INDY3;
			}
			break;
		case E_AE_INDY3:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = OpTab[op & 0xff]; break;


		// Addressing modes: Read operand, write it back, no extra cycles (-> ar, rdbuf)
		case E_M_ZERO:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = RMW_DO_IT; break;

		case E_M_ZEROX:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = M_ZEROX1;
			break;
		case E_M_ZEROX1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar = (short) ((ar + (x & 0xff)) & 0xff);
			state = RMW_DO_IT; break;

		case E_M_ZEROY:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = M_ZEROY1;
			break;
		case E_M_ZEROY1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar = (short) ((ar + (y & 0xff)) & 0xff);
			state = RMW_DO_IT; break;

		case E_M_ABS:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = M_ABS1;
			break;
		case E_M_ABS1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			ar = (short) (ar | ((data << 8) & 0xff00));
			state = RMW_DO_IT; break;

		case E_M_ABSX:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = M_ABSX1;
			break;
		case E_M_ABSX1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			if (ar + (x & 0xff) < 0x100)
				state = M_ABSX2;
			else
				state = M_ABSX3;
			ar = (short) ((ar + (x & 0xff)) & 0xff | ((data << 8) & 0xff00));
			break;
		case E_M_ABSX2:	// No page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = RMW_DO_IT; break;
		case E_M_ABSX3:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = RMW_DO_IT; break;

		case E_M_ABSY:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = M_ABSY1;
			break;
		case E_M_ABSY1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			if (ar + (y & 0xff) < 0x100)
				state = M_ABSY2;
			else
				state = M_ABSY3;
			ar = (short) ((ar + (y & 0xff)) & 0xff | ((data << 8) & 0xff00));
			break;
		case E_M_ABSY2:	// No page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = RMW_DO_IT; break;
		case E_M_ABSY3:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = RMW_DO_IT; break;

		case E_M_INDX:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);
			state = M_INDX1;
			break;
		case E_M_INDX1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar2);
			ar2 = (short) ((ar2 + (x & 0xff)) & 0xff);
			state = M_INDX2;
			break;
		case E_M_INDX2:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(ar2)) & 0xff);
			state = M_INDX3;
			break;
		case E_M_INDX3:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((ar2 + 1) & 0xff);
			ar = (short) (ar | ((data << 8) & 0xff00));
			state = RMW_DO_IT; break;

		case E_M_INDY:
			if (BALow) { result.baLowEscape = true; return result; } ar2 = (short) ((read_byte(pc++)) & 0xff);
			state = M_INDY1;
			break;
		case E_M_INDY1:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(ar2)) & 0xff);
			state = M_INDY2;
			break;
		case E_M_INDY2:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((ar2 + 1) & 0xff);
			if (ar + (y & 0xff) < 0x100)
				state = M_INDY3;
			else
				state = M_INDY4;
			ar = (short) ((ar + (y & 0xff)) & 0xff | ((data << 8) & 0xff00));
			break;
		case E_M_INDY3:	// No page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = RMW_DO_IT; break;
		case E_M_INDY4:	// Page crossed
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			ar += 0x100;
			state = RMW_DO_IT; break;

		case E_RMW_DO_IT:
			if (BALow) { result.baLowEscape = true; return result; } rdbuf = read_byte(ar);
			state = RMW_DO_IT1;
			break;
		case E_RMW_DO_IT1:
			write_byte(ar, rdbuf);
			state = OpTab[op & 0xff]; break;


		// Load group
		case E_O_LDA:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (a = data);
			state = 0; break;
		case E_O_LDA_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a = data);
			state = 0; break;

		case E_O_LDX:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (x = data);
			state = 0; break;
		case E_O_LDX_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (x = data);
			state = 0; break;

		case E_O_LDY:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (y = data);
			state = 0; break;
		case E_O_LDY_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (y = data);
			state = 0; break;


		// Store group
		case E_O_STA:
			write_byte(ar, a);
			state = 0; break;

		case E_O_STX:
			write_byte(ar, x);
			state = 0; break;

		case E_O_STY:
			write_byte(ar, y);
			state = 0; break;


		// Transfer group
		case E_O_TAX:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (x = a);
			state = 0; break;

		case E_O_TXA:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (a = x);
			state = 0; break;

		case E_O_TAY:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (y = a);
			state = 0; break;

		case E_O_TYA:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (a = y);
			state = 0; break;

		case E_O_TSX:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (x = sp);
			state = 0; break;

		case E_O_TXS:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			sp = x;
			state = 0; break;


		// Arithmetic group
		case E_O_ADC:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			do_adc(data);
			state = 0; break;
		case E_O_ADC_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			do_adc(data);
			state = 0; break;

		case E_O_SBC:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			do_sbc(data);
			state = 0; break;
		case E_O_SBC_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			do_sbc(data);
			state = 0; break;


		// Increment/decrement group
		case E_O_INX:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (++x);
			state = 0; break;

		case E_O_DEX:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (--x);
			state = 0; break;

		case E_O_INY:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (++y);
			state = 0; break;

		case E_O_DEY:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			z_flag = n_flag = (--y);
			state = 0; break;

		case E_O_INC:
			write_byte(ar, z_flag = n_flag = (byte) (rdbuf + 1));
			state = 0; break;

		case E_O_DEC:
			write_byte(ar, z_flag = n_flag = (byte) (rdbuf - 1));
			state = 0; break;


		// Logic group
		case E_O_AND:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (a &= data);
			state = 0; break;
		case E_O_AND_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a &= data);
			state = 0; break;

		case E_O_ORA:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (a |= data);
			state = 0; break;
		case E_O_ORA_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a |= data);
			state = 0; break;

		case E_O_EOR:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (a ^= data);
			state = 0; break;
		case E_O_EOR_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a ^= data);
			state = 0; break;

		// Compare group
		case E_O_CMP:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (byte) (ar = (short) ((a & 0xff) - (data & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;
		case E_O_CMP_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (byte) (ar = (short) ((a & 0xff) - (data & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;

		case E_O_CPX:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (byte) (ar = (short) ((x & 0xff) - (data & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;
		case E_O_CPX_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (byte) (ar = (short) ((x & 0xff) - (data & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;

		case E_O_CPY:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (byte) (ar = (short) ((y & 0xff) - (data & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;
		case E_O_CPY_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (byte) (ar = (short) ((y & 0xff) - (data & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;


		// Bit-test group
		case E_O_BIT:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = (byte) (a & data);
			n_flag = data;
			v_flag = (data & 0x40) != 0;
			state = 0; break;


		// Shift/rotate group
		case E_O_ASL:
			c_flag = (rdbuf & 0x80) != 0;
			write_byte(ar, z_flag = n_flag = (byte) (rdbuf << 1));
			state = 0; break;
		case E_O_ASL_A:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			c_flag = (a & 0x80) != 0;
			z_flag = n_flag = (a <<= 1);
			state = 0; break;

		case E_O_LSR:
			c_flag = (rdbuf & 0x01) != 0;
			write_byte(ar, z_flag = n_flag = (byte) ((rdbuf & 0xff) >> 1));
			state = 0; break;
		case E_O_LSR_A:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			c_flag = (a & 0x01) != 0;
			z_flag = n_flag = a = (byte) ((a & 0xff) >> 1); // (a >>= 1);
			state = 0; break;

		case E_O_ROL:
			write_byte(ar, z_flag = n_flag = (byte) (c_flag ? (rdbuf << 1) | 0x01 : rdbuf << 1));
			c_flag = (rdbuf & 0x80) != 0;
			state = 0; break;
		case E_O_ROL_A:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			data = (byte) (a & 0x80);
			z_flag = n_flag = (a = (byte) (c_flag ? (a << 1) | 0x01 : a << 1));
			c_flag = data != 0;
			state = 0; break;

		case E_O_ROR:
			write_byte(ar, z_flag = n_flag = (byte) (c_flag ? ((rdbuf & 0xff) >> 1) | 0x80 : (rdbuf & 0xff) >> 1));
			c_flag = (rdbuf & 0x01) != 0;
			state = 0; break;
		case E_O_ROR_A:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			data = (byte) (a & 0x01);
			z_flag = n_flag = (a = (byte) (c_flag ? ((a & 0xff) >> 1) | 0x80 : (a & 0xff) >> 1));
			c_flag = data != 0;
			state = 0; break;


		// Stack group
		case E_O_PHA:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = O_PHA1;
			break;
		case E_O_PHA1:
			write_byte(((sp-- & 0xff) | 0x100) & 0xffff, a);
			state = 0; break;

		case E_O_PLA:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = O_PLA1;
			break;
		case E_O_PLA1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte((sp++ & 0xff) | 0x100);
			state = O_PLA2;
			break;
		case E_O_PLA2:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((sp & 0xff) | 0x100);
			z_flag = n_flag = (a = data);
			state = 0; break;

		case E_O_PHP:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = O_PHP1;
			break;
		case E_O_PHP1:
			
// macro expansion push_flags(true);
data = (byte) (0x20 | (n_flag& 0x80));
if (v_flag) data |= 0x40;
if (true) data |= 0x10;
if (d_flag) data |= 0x08;
if (i_flag) data |= 0x04;
if (z_flag != 0) data |= 0x02;
if (c_flag) data |= 0x01;
write_byte((sp-- & 0xff) | 0x100, data);
// end push_flags(true);
			state = 0; break;

		case E_O_PLP:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = O_PLP1;
			break;
		case E_O_PLP1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte((sp++ & 0xff) | 0x100);
			state = O_PLP2;
			break;
		case E_O_PLP2:
// macro expansion pop_flags();
if (BALow) { result.baLowEscape = true; return result; } data = read_byte((sp & 0xff) | 0x100);
n_flag = data;
v_flag = (data & 0x40) != 0;
d_flag = (data & 0x08) != 0;
i_flag = (data & 0x04) != 0;
z_flag = (byte) ((data & 0x02) != 0? -1:0);
c_flag = (data & 0x01) != 0;
//end pop_flags();
			state = 0; break;


		// Jump/branch group
		case E_O_JMP:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = O_JMP1;
			break;
		case E_O_JMP1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc);
			pc = ((data << 8) | ar) & 0xffff;
			state = 0; break;

		case E_O_JMP_I:
			if (BALow) { result.baLowEscape = true; return result; } pc = read_byte(ar) & 0xff;
			state = O_JMP_I1;
			break;
		case E_O_JMP_I1:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((ar + 1) & 0xff | ar & 0xff00);
			pc |= (data << 8) & 0xff00;
			state = 0; break;

		case E_O_JSR:
			if (BALow) { result.baLowEscape = true; return result; } ar = (short) ((read_byte(pc++)) & 0xff);
			state = O_JSR1;
			break;
		case E_O_JSR1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte((sp & 0xff) | 0x100);
			state = O_JSR2;
			break;
		case E_O_JSR2:
//			System.out.printf("%04x JSR\n", pc - 2);
			write_byte((sp-- & 0xff) | 0x100, (byte) (pc >> 8));
			state = O_JSR3;
			break;
		case E_O_JSR3:
			write_byte((sp-- & 0xff) | 0x100, (byte) pc);
			state = O_JSR4;
			break;
		case E_O_JSR4:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			pc = (ar | ((data << 8) & 0xff00)) & 0xffff;
//			System.out.printf(" -> %04x\n", pc);
			state = 0; break;

		case E_O_RTS:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = O_RTS1;
			break;
		case E_O_RTS1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte((sp++ & 0xff) | 0x100);
			state = O_RTS2;
			break;
		case E_O_RTS2:
			if (BALow) { result.baLowEscape = true; return result; } pc = read_byte((sp++ & 0xff) | 0x100) & 0xff;
			state = O_RTS3;
			break;
		case E_O_RTS3:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((sp & 0xff) | 0x100);
			pc |= (data << 8) & 0xff00;
//			System.out.printf("RTS %04x\n", pc);
			state = O_RTS4;
			break;
		case E_O_RTS4:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc++);
			state = 0; break;

		case E_O_RTI:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = O_RTI1;
			break;
		case E_O_RTI1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte((sp++ & 0xff) | 0x100);
			state = O_RTI2;
			break;
		case E_O_RTI2:
			
// macro expansion pop_flags();
if (BALow) { result.baLowEscape = true; return result; } data = read_byte((sp & 0xff) | 0x100);
n_flag = data;
v_flag = (data & 0x40) != 0;
d_flag = (data & 0x08) != 0;
i_flag = (data & 0x04) != 0;
z_flag = (byte) ((data & 0x02) != 0? -1:0);
c_flag = (data & 0x01) != 0;
//end pop_flags();

			sp++;
			state = O_RTI3;
			break;
		case E_O_RTI3:
			if (BALow) { result.baLowEscape = true; return result; } pc = read_byte((sp++ & 0xff) | 0x100) & 0xff;
			state = O_RTI4;
			break;
		case E_O_RTI4:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte((sp & 0xff) | 0x100);
			pc |= (data << 8) & 0xff00;
			if (debug && debugInterrupt && !Sam.active) {
				System.out.printf("RTI %04x\n", pc);
			}
			state = 0; break;

		case E_O_BRK:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc++);
			state = O_BRK1;
			break;
		case E_O_BRK1:
			write_byte((sp-- & 0xff) | 0x100, (byte) (pc >> 8));
			state = O_BRK2;
			break;
		case E_O_BRK2:
			write_byte((sp-- & 0xff) | 0x100, (byte) pc);
			state = O_BRK3;
			break;
		case E_O_BRK3:
			
// macro expansion push_flags(true);
data = (byte) (0x20 | (n_flag& 0x80));
if (v_flag) data |= 0x40;
if (true) data |= 0x10;
if (d_flag) data |= 0x08;
if (i_flag) data |= 0x04;
if (z_flag != 0) data |= 0x02;
if (c_flag) data |= 0x01;
write_byte((sp-- & 0xff) | 0x100, data);
// end push_flags(true);	
			i_flag = true;
if (!IS_CPU_1541) {
			if (interrupt.intr[INT_NMI.ordinal()] != 0) {			// BRK interrupted by NMI?
				interrupt.intr[INT_NMI.ordinal()] = 0;	// Simulate an edge-triggered input
				state = 0x0015;						// Jump to NMI sequence
				break;
			}
}
			state = O_BRK4;
			break;
		case E_O_BRK4:
if (!IS_CPU_1541) {
			first_nmi_cycle++;		// Delay NMI
}
			if (BALow) { result.baLowEscape = true; return result; } pc = read_byte(0xfffe) & 0xff;
			state = O_BRK5;
			break;
		case E_O_BRK5:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(0xffff);
			pc |= (data << 8) & 0xff00;
			state = 0; break;

		case E_O_BCS:
			
// macro expansion Branch(c_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (c_flag) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(c_flag);

		case E_O_BCC:
			
// macro expansion Branch(!c_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (!c_flag) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(!c_flag);

		case E_O_BEQ:
			
// macro expansion Branch(!z_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (z_flag == 0) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(!z_flag);

		case E_O_BNE:
			
// macro expansion Branch(z_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (z_flag != 0) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(z_flag);

		case E_O_BVS:
if (!IS_CPU_1541) {
			
// macro expansion Branch(v_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (v_flag) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(v_flag);
} else {
			
// macro expansion Branch((via2_pcr & 0x0e) == 0x0e ? 1 : v_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if ((((via2_pcr & 0x0e) == 0x0e) ? 1 : (v_flag? 1:0)) != 0) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch((via2_pcr & 0x0e) == 0x0e ? 1 : v_flag);	// GCR byte ready flag
}

		case E_O_BVC:
if (!IS_CPU_1541) {
			
// macro expansion Branch(!v_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (!v_flag) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(!v_flag);
} else {
			
// macro expansion Branch(!((via2_pcr & 0x0e) == 0x0e) ? 0 : v_flag);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if (!((((via2_pcr & 0x0e) == 0x0e) ? 0 : (v_flag? 1:0)) != 0)) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(!((via2_pcr & 0x0e) == 0x0e) ? 0 : v_flag);	// GCR byte ready flag
}

		case E_O_BMI:
			
// macro expansion Branch(n_flag & 0x80);
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if ((n_flag & 0x80) != 0) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(n_flag & 0x80);

		case E_O_BPL:
			
// macro expansion Branch(!(n_flag & 0x80));
if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
if ((n_flag & 0x80) == 0) {
	ar = (short) (pc + data);
	if (((ar >> 8) & 0xff) != ((pc >> 8) & 0xff)) {
		if ((data & 0x80) != 0)
			state = O_BRANCH_BP;
		else
			state = O_BRANCH_FP;
	} else
		state = O_BRANCH_NP;
} else
	state = 0;
break;
// end Branch(!(n_flag & 0x80));

		case E_O_BRANCH_NP:	// No page crossed
			first_irq_cycle++;	// Delay IRQ
if (!IS_CPU_1541) {
			first_nmi_cycle++;	// Delay NMI
}
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			pc = ar & 0xffff;
			state = 0; break;
		case E_O_BRANCH_BP:	// Page crossed, branch backwards
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			pc = ar & 0xffff;
			state = O_BRANCH_BP1;
			break;
		case E_O_BRANCH_BP1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc + 0x100);
			state = 0; break;
		case E_O_BRANCH_FP:	// Page crossed, branch forwards
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			pc = ar & 0xffff;
			state = O_BRANCH_FP1;
			break;
		case E_O_BRANCH_FP1:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc - 0x100);
			state = 0; break;


		// Flag group
		case E_O_SEC:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			c_flag = true;
			state = 0; break;

		case E_O_CLC:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			c_flag = false;
			state = 0; break;

		case E_O_SED:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			d_flag = true;
			state = 0; break;

		case E_O_CLD:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			d_flag = false;
			state = 0; break;

		case E_O_SEI:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			i_flag = true;
			state = 0; break;

		case E_O_CLI:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			i_flag = false;
			state = 0; break;

		case E_O_CLV:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			v_flag = false;
			state = 0; break;


		// NOP group
		case E_O_NOP:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc);
			state = 0; break;


/*
 * Undocumented opcodes start here
 */

		// NOP group
		case E_O_NOP_I:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(pc++);
			state = 0; break;

		case E_O_NOP_A:
			if (BALow) { result.baLowEscape = true; return result; } read_byte(ar);
			state = 0; break;


		// Load A/X group
		case E_O_LAX:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (a = x = data);
			state = 0; break;


		// Store A/X group
		case E_O_SAX:
			write_byte(ar, (byte) (a & x));
			state = 0; break;


		// ASL/ORA group
		case E_O_SLO:
			c_flag = (rdbuf & 0x80) != 0;
			rdbuf <<= 1;
			write_byte(ar, rdbuf);
			z_flag = n_flag = (a |= rdbuf);
			state = 0; break;


		// ROL/AND group
		case E_O_RLA:
			tmp = (byte) (rdbuf & 0x80);
			rdbuf = (byte) (c_flag ? (rdbuf << 1) | 0x01 : rdbuf << 1);
			c_flag = tmp != 0;
			write_byte(ar, rdbuf);
			z_flag = n_flag = (a &= rdbuf);
			state = 0; break;


		// LSR/EOR group
		case E_O_SRE:
			c_flag = (rdbuf & 0x01) != 0;
			rdbuf >>= 1;
			write_byte(ar, rdbuf);
			z_flag = n_flag = (a ^= rdbuf);
			state = 0; break;


		// ROR/ADC group
		case E_O_RRA:
			tmp = (byte) (rdbuf & 0x01);
			rdbuf = (byte) (c_flag ? (rdbuf >> 1) | 0x80 : rdbuf >> 1);
			c_flag = tmp != 0;
			write_byte(ar, rdbuf);
			do_adc(rdbuf);
			state = 0; break;


		// DEC/CMP group
		case E_O_DCP:
			write_byte(ar, --rdbuf);
			z_flag = n_flag = (byte) (ar = (short) ((a & 0xff) - (rdbuf & 0xff)));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;


		// INC/SBC group
		case E_O_ISB:
			write_byte(ar, ++rdbuf);
			do_sbc(rdbuf);
			state = 0; break;


		// Complex functions
		case E_O_ANC_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a &= data);
			c_flag = (n_flag & 0x80) != 0;
			state = 0; break;

		case E_O_ASR_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			a &= data;
			c_flag = (a & 0x01) != 0;
			z_flag = n_flag = (a >>= 1);
			state = 0; break;

		case E_O_ARR_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			data &= a;
			a = (byte) (c_flag ? (data >> 1) | 0x80 : data >> 1);
			if (!d_flag) {
				z_flag = n_flag = (a);
				c_flag = (a & 0x40) != 0;
				v_flag = ((a & 0x40) ^ ((a & 0x20) << 1)) != 0;
			} else {
				n_flag = (byte) (c_flag ? 0x80 : 0);
				z_flag = a;
				v_flag = ((data ^ a) & 0x40) != 0;
				if ((data & 0x0f) + (data & 0x01) > 5)
					a = (byte) (a & 0xf0 | (a + 6) & 0x0f);
				if (c_flag = ((data + (data & 0x10)) & 0x1f0) > 0x50)
					a += 0x60;
			}
			state = 0; break;

		case E_O_ANE_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a = (byte) ((a | 0xee) & x & data));
			state = 0; break;

		case E_O_LXA_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (a = x = (byte) ((a | 0xee) & data));
			state = 0; break;

		case E_O_SBX_I:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(pc++);
			z_flag = n_flag = (x = (byte) (ar = (short) (((x & a) & 0xff) - (data & 0xff))));
			c_flag = (ar & 0xffff) < 0x100;
			state = 0; break;

		case E_O_LAS:
			if (BALow) { result.baLowEscape = true; return result; } data = read_byte(ar);
			z_flag = n_flag = (a = x = sp = (byte) ((data & 0xff) & (sp & 0xff)));
			state = 0; break;

		case E_O_SHS:		// ar2 contains the high byte of the operand address
			write_byte(ar, (byte) ((ar2+1) & (sp = (byte) (a & x & 0xff))));
			state = 0; break;

		case E_O_SHY:		// ar2 contains the high byte of the operand address
			write_byte(ar, (byte) (y & (ar2+1)));
			state = 0; break;

		case E_O_SHX:		// ar2 contains the high byte of the operand address
			write_byte(ar, (byte) (x & (ar2+1)));
			state = 0; break;

		case E_O_SHA:		// ar2 contains the high byte of the operand address
			write_byte(ar, (byte) (a & x & (ar2+1)));
			state = 0; break;
			
			default:
			result.opHandled = false;
		}
		
		return result;
	}
	
	String getNVBsDIZC() {
		return String.format("%d%d10%d%d%d%d", ((n_flag & 0x80) != 0)? 1:0, v_flag? 1:0, d_flag? 1:0, i_flag? 1:0, (z_flag == 0)? 1:0, c_flag? 1:0);
	}
	protected void do_adc(byte b) {
		if (!d_flag) {
			int tmp;

			// Binary mode
			tmp = (a & 0xff) + (b & 0xff) + (c_flag ? 1 : 0);
			c_flag = tmp > 0xff;
			v_flag = !(((a ^ b) & 0x80) != 0) && (((a ^ tmp) & 0x80) != 0);
			z_flag = n_flag = a = (byte) tmp;

		} else {
			int al, ah;

			// Decimal mode
			al = (a & 0x0f) + (b & 0x0f) + (c_flag ? 1 : 0);		// Calculate lower nybble
			if (al > 9) al += 6;									// BCD fixup for lower nybble

			ah = ((a >> 4) & 0x0f) + ((b >> 4) & 0x0f);							// Calculate upper nybble
			if (al > 0x0f) ah++;

			z_flag = (byte) (a + b + (c_flag ? 1 : 0));					// Set flags
			n_flag = (byte) (ah << 4);	// Only highest bit used
			v_flag = ((((ah << 4) ^ a) & 0x80) != 0) && !(((a ^ b) & 0x80) != 0);

			if (ah > 9) ah += 6;									// BCD fixup for upper nybble
			c_flag = ah > 0x0f;										// Set carry flag
			a = (byte) ((ah << 4) | (al & 0x0f));							// Compose result
		}
	}
	protected void do_sbc(byte b) {
//		uint16 tmp = a - byte - (c_flag ? 0 : 1);
//
//		// Binary mode
//		c_flag = tmp < 0x100;
//		v_flag = ((a ^ tmp) & 0x80) && ((a ^ byte) & 0x80);
//		z_flag = n_flag = a = tmp;

		int tmp = ((a & 0xff) - (b & 0xff) - (c_flag ? 0 : 1)) & 0xffff;

		if (!d_flag) {

			// Binary mode
			c_flag = tmp < 0x100;
			v_flag = (((a ^ tmp) & 0x80) != 0) && (((a ^ b) & 0x80) != 0);
			z_flag = n_flag = a = (byte) tmp;

		} else {
			int al, ah;

			// Decimal mode
			al = (a & 0x0f) - (b & 0x0f) - (c_flag ? 0 : 1);		// Calculate lower nybble
			ah = (a >> 4) - ((b & 0xff) >> 4);							// Calculate upper nybble
			if ((al & 0x10) != 0) {
				al -= 6;											// BCD fixup for lower nybble
				ah--;
			}
			if ((ah & 0x10) != 0) ah -= 6;									// BCD fixup for upper nybble

			c_flag = tmp < 0x100;									// Set flags
			v_flag = (((a ^ tmp) & 0x80) != 0) && (((a ^ b) & 0x80) != 0);
			z_flag = n_flag = (byte) tmp;

			a = (byte) ((ah << 4) | (al & 0x0f));							// Compose result
		}

		/*
		int tmp = a - b - (c_flag ? 0 : 1);

		if (!d_flag) {

			// Binary mode
			c_flag = tmp < 0x100;
			v_flag = (((a ^ tmp) & 0x80) != 0) && (((a ^ b) & 0x80) != 0);
			z_flag = n_flag = a = (byte) tmp;

		} else {
			int al, ah;

			// Decimal mode
			al = (a & 0x0f) - (b & 0x0f) - (c_flag ? 0 : 1);	// Calculate lower nybble
			ah = (a >> 4) - (b >> 4);							// Calculate upper nybble
			if ((al & 0x10) != 0) {
				al -= 6;											// BCD fixup for lower nybble
				ah--;
			}
			if ((ah & 0x10) != 0) ah -= 6;									// BCD fixup for upper nybble

			c_flag = tmp < 0x100;									// Set flags
			v_flag = (((a ^ tmp) & 0x80) != 0) && (((a ^ b) & 0x80) != 0);
			z_flag = n_flag = (byte) tmp;

			a = (byte) ((ah << 4) | (al & 0x0f));							// Compose result
		}
		*/
	}
	protected abstract void write_byte(int i, byte j);

	protected abstract byte read_byte(int i);

	protected static class Interrupt {
		byte[] intr = new byte[4];
		boolean intr_any() {
			return intr[0] != 0 || intr[1] != 0 || intr[2] != 0 || intr[3] != 0;
		}
		public void clear() {
			intr[0] = intr[1] = intr[2] = intr[3] = 0;
		}
	}
	protected Interrupt interrupt = new Interrupt();

	protected byte n_flag, z_flag;
	protected boolean v_flag, d_flag, i_flag, c_flag;
	protected byte a, x, y, sp;
//		#if PC_IS_POINTER
//			byte *pc, *pc_base;
//		#else
	protected int pc;
//		#endif

//		#ifdef FRODO_SC
	protected int first_irq_cycle, first_nmi_cycle;

	protected byte state, op;		// Current state and opcode
	protected short ar, ar2;			// Address registers
	protected byte rdbuf;			// Data buffer for RMW instructions
//		#else
//			int borrowed_cycles;	// Borrowed cycles from next line
//		#endif
 
	protected byte via2_pcr;		// PCR of VIA 2

	public static int FAC = 0x61, ARG = 0x69;

	double getFloat() {
		return getFloat(FAC);
	}
	double getFloat(int adr) {
		int exp = read_byte(adr) & 0xff;
		if (exp == 0) {
			return 0;
		}
		exp -= 128;
		long mant = (((read_byte(adr+1) | 0x80) << 24) & 0xff000000L) |
		             ((read_byte(adr+2) << 16) & 0xff0000L) |
		             ((read_byte(adr+3) << 8) & 0xff00L) |
		              (read_byte(adr+4) & 0xffL);
		double fmant = ((double) mant) / 0x100000000L;
		return fmant * Math.pow(2, exp);
	}
}
