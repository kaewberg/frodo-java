package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.BPtr;

class MOS6510State {
	public static final int STATE_SIZE = 17;
	
	public byte a, x, y;
	public byte p;			// Processor flags
	public byte ddr, pr;		// Port
	public short pc, sp;
	public byte[] intr = new byte[4];		// Interrupt state
	public boolean nmi_state;	
	public byte dfff_byte;
	public boolean instruction_complete;
	
	public MOS6510State(byte[] buf) {
		BPtr bp = new BPtr(buf);
		a = bp.getInc(); x = bp.getInc(); y = bp.getInc(); p = bp.getInc(); ddr = bp.getInc(); pr = bp.getInc(); pc = bp.getIncShort();
		sp = bp.getIncShort();
		for (int i = 0; i < 4; i++) {
			intr[i] = bp.getInc();
		}
		nmi_state = bp.getIncBool(); dfff_byte = bp.getInc();
		instruction_complete = bp.getIncBool();
	}

	public MOS6510State() {
	}

	public byte[] toBytes() {
		byte[] buf = new byte[STATE_SIZE];
		BPtr bp = new BPtr(buf);
		
		bp.setInc(a); bp.setInc(x); bp.setInc(y); bp.setInc(p); bp.setInc(ddr); bp.setInc(pr); bp.setInc(pc);
		// 8
		bp.setInc(sp);
		for (int i = 0; i < 4; i++) {
			bp.setInc(intr[i]);
		}
		bp.setInc(nmi_state); bp.setInc(dfff_byte);
		// 16
		bp.setInc(instruction_complete);
		
		return buf;
	}
}
