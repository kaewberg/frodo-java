package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.InterruptType6510.*;
import static se.pp.forsberg.libc.LibC.*;

import se.pp.forsberg.frodo.enums.ROMCall;
import se.pp.forsberg.frodo.prefs.CPUPrefs;
import se.pp.forsberg.libc.ByteC;

class MOS6510 extends CpuBase {

	private final static int FRODO_VERSION = 4;
	private final static int FRODO_REVISION = 1;
	private final static byte[] VERSION_STRING = toBytes("Frodo V4.1b");
	private final static byte[] frodo_id = toBytes("FRODO\r(C) 1994-1997 CHRISTIAN BAUER");

	// Set this to 1 if the 6502 PC should be represented by a real pointer
//	#ifndef FRODO_SC
//	#ifndef PC_IS_POINTER
//	#define PC_IS_POINTER 1
//	#endif
//	#endif

	// Set this to 1 for more precise CPU cycle calculation
//	#ifndef PRECISE_CPU_CYCLES
//	#define PRECISE_CPU_CYCLES 0
//	#endif

	// Set this to 1 for instruction-aligned CIA emulation
//	#ifndef PRECISE_CIA_CYCLES
//	#define PRECISE_CIA_CYCLES 0
//	#endif


	// Interrupt types
//	enum {
//		INT_VICIRQ,
//		INT_CIAIRQ,
//		INT_NMI
//		// INT_RESET (private)
//	};


//	class MOS6569;
//	class MOS6581;
//	class MOS6526_1;
//	class MOS6526_2;
//	class REU;
//	class IEC;
//	struct MOS6510State;


	// 6510 emulation (C64)
	public MOS6510(C64 c64, byte[] Ram, byte[] Basic, byte[] Kernal, byte[] Char, byte[] Color) {
		the_c64 = c64; ram = Ram; basic_rom = Basic; kernal_rom = Kernal; char_rom = Char; color_ram = Color;
		
		a = x = y = 0;
		sp = (byte) 0xff;
		n_flag = z_flag = 0;
		v_flag = d_flag = c_flag = false;
		i_flag = true;
		dfff_byte = 0x55;
		BALow = false;
		first_irq_cycle = first_nmi_cycle = 0;
	}

//	#ifdef FRODO_SC
	void EmulateCycle() {			// Emulate one clock cycle

		// Any pending interrupts in state 0 (opcode fetch)?
		if (state == 0 && interrupt.intr_any()) {
			if (interrupt.intr[INT_RESET.ordinal()] != 0)
				Reset();
			else if ((interrupt.intr[INT_NMI.ordinal()] != 0) && (the_c64.CycleCounter-first_nmi_cycle >= 2)) {
				interrupt.intr[INT_NMI.ordinal()] = 0;	// Simulate an edge-triggered input
				state = 0x0010;
			} else if (((interrupt.intr[INT_VICIRQ.ordinal()] != 0) || (interrupt.intr[INT_CIAIRQ.ordinal()] != 0)) && (the_c64.CycleCounter-first_irq_cycle >= 2) && !i_flag)
				state = 0x0008;
		}

		CommonResult r = commonOps(false);
		if (r.baLowEscape) {
			return;
		}
		// Extension opcode
		if (!r.opHandled) {
			switch (OpCodes.values()[state & 0xff]) {
			case E_O_EXT:
					if (pc < 0xe000) {
					illegal_op((byte) 0xf2, pc-1);
					break;
				}
				switch (read_byte(pc++)) {
				
				case 0x00:
					ram[0x90] |= TheIEC.Out(ram[0x95], (ram[0xa3] & 0x80) != 0);
					c_flag = false;
					pc = 0xedac;
					state = 0; break;
				case 0x01:
					ram[0x90] |= TheIEC.OutATN(ram[0x95]);
					c_flag = false;
					pc = 0xedac;
					state = 0; break;
				case 0x02:
					ram[0x90] |= TheIEC.OutSec(ram[0x95]);
					c_flag = false;
					pc = 0xedac;
					state = 0; break;
				case 0x03:
					ByteC ac = new ByteC();
					ram[0x90] |= TheIEC.In(ac);
					a = ac.b;
					z_flag = n_flag = (a);
					c_flag = false;
					pc = 0xedac;
					state = 0; break;
				case 0x04:
					TheIEC.SetATN();
					pc = 0xedfb;
					state = 0; break;
				case 0x05:
					TheIEC.RelATN();
					pc = 0xedac;
					state = 0; break;
				case 0x06:
					TheIEC.Turnaround();
					pc = 0xedac;
					state = 0; break;
				case 0x07:
					TheIEC.Release();
					pc = 0xedac;
					state = 0; break;
				default:
					illegal_op((byte) 0xf2, pc-1);
					break;
				}
				break;
			default:
				illegal_op((byte) op, pc-1);
				break;
			}
		}
	}
//	#else
//		int EmulateLine(int cycles_left);	// Emulate until cycles_left underflows
//	#endif
	public void Reset() {
		// Delete 'CBM80' if present
		if (ram[0x8004] == 0xc3 && ram[0x8005] == 0xc2 && ram[0x8006] == 0xcd
		 && ram[0x8007] == 0x38 && ram[0x8008] == 0x30)
			ram[0x8004] = 0;

		// Initialize extra 6510 registers and memory configuration
		ddr = pr = 0;
		new_config();

		// Clear all interrupt lines
		interrupt.clear();
		nmi_state = false;

		// Read reset vector
		pc = read_word(0xfffc);
		state = 0;
	}
	public void AsyncReset() {				// Reset the CPU asynchronously
		interrupt.intr[INT_RESET.ordinal()] = 1;
	}
	public void AsyncNMI() {				// Raise NMI asynchronously (NMI pulse)
		if (!nmi_state)
			interrupt.intr[INT_NMI.ordinal()] = 1;
	}
	public MOS6510State GetState() {
		MOS6510State s = new MOS6510State();
		
		s.a = a;
		s.x = x;
		s.y = y;

		s.p = (byte) (0x20 | (n_flag & 0x80));
		if (v_flag) s.p |= 0x40;
		if (d_flag) s.p |= 0x08;
		if (i_flag) s.p |= 0x04;
		if (z_flag == 0) s.p |= 0x02;
		if (c_flag) s.p |= 0x01;
		
		s.ddr = (byte) ddr;
		s.pr = (byte) pr;

		s.pc = (short) pc;
		s.sp = (short) (sp | 0x0100);

		s.intr[INT_VICIRQ.ordinal()] = interrupt.intr[INT_VICIRQ.ordinal()];
		s.intr[INT_CIAIRQ.ordinal()] = interrupt.intr[INT_CIAIRQ.ordinal()];
		s.intr[INT_NMI.ordinal()] = interrupt.intr[INT_NMI.ordinal()];
		s.intr[INT_RESET.ordinal()] = interrupt.intr[INT_RESET.ordinal()];
		s.nmi_state = nmi_state;
		s.dfff_byte = (byte) dfff_byte;
		s.instruction_complete = (state == 0);

		return s;
	}
	public void SetState(MOS6510State s) {
		a = s.a;
		x = s.x;
		y = s.y;

		n_flag = s.p;
		v_flag = (s.p & 0x40) != 0;
		d_flag = (s.p & 0x08) != 0;
		i_flag = (s.p & 0x04) != 0;
		z_flag = (byte) ((s.p & 0x02) == 0? 255:0);
		c_flag = (s.p & 0x01) != 0;

		ddr = s.ddr;
		pr = s.pr;
		new_config();

		pc = s.pc;
		sp = (byte) (s.sp & 0xff);

		interrupt.intr[INT_VICIRQ.ordinal()] = s.intr[INT_VICIRQ.ordinal()];
		interrupt.intr[INT_CIAIRQ.ordinal()] = s.intr[INT_CIAIRQ.ordinal()];
		interrupt.intr[INT_NMI.ordinal()] = s.intr[INT_NMI.ordinal()];
		interrupt.intr[INT_RESET.ordinal()] = s.intr[INT_RESET.ordinal()];
		nmi_state = s.nmi_state;
		dfff_byte = s.dfff_byte;
		if (s.instruction_complete)
			state = 0;
	}
	public byte ExtReadByte(int adr) {
		adr &= 0xffff;
		// Save old memory configuration
		boolean bi = basic_in, ki = kernal_in, ci = char_in, ii = io_in;

		// Set new configuration
		basic_in = (ExtConfig & 3) == 3;
		kernal_in = (ExtConfig & 2) != 0;
		char_in = ((ExtConfig & 3) != 0) && ((ExtConfig & 4) == 0);
		io_in = ((ExtConfig & 3) != 0) && ((ExtConfig & 4) != 0);

		// Read byte
		byte b = read_byte(adr);

		// Restore old configuration
		basic_in = bi; kernal_in = ki; char_in = ci; io_in = ii;

		return b;
	}
	public void ExtWriteByte(int adr, byte b) {
		adr &= 0xffff;
		// Save old memory configuration
		boolean bi = basic_in, ki = kernal_in, ci = char_in, ii = io_in;

		// Set new configuration
		basic_in = (ExtConfig & 3) == 3;
		kernal_in = (ExtConfig & 2) != 0;
		char_in = ((ExtConfig & 3) != 0) && ((ExtConfig & 4) == 0);
		io_in = ((ExtConfig & 3) != 0) && ((ExtConfig & 4) != 0);

		// Write byte
		write_byte(adr, b);

		// Restore old configuration
		basic_in = bi; kernal_in = ki; char_in = ci; io_in = ii;
	}

	byte REUReadByte(int adr) {
		adr &= 0xffff;
		return read_byte(adr);
	}
	void REUWriteByte(int adr, byte b) {
		adr &= 0xffff;
		write_byte(adr, b);
	}
	
	public int ExtConfig;	// Memory configuration for ExtRead/WriteByte (0..7)

	public MOS6569 TheVIC;	// Pointer to VIC
	public MOS6581 TheSID;	// Pointer to SID
	public MOS6526_1 TheCIA1;	// Pointer to CIA 1
	public MOS6526_2 TheCIA2;	// Pointer to CIA 2
	public REU TheREU;		// Pointer to REU
	public IEC TheIEC;		// Pointer to drive array

//	#ifdef FRODO_SC
	public boolean BALow;			// BA line for Frodo SC
//	#endif

	@Override
	protected byte read_byte(int adr) {
		adr &= 0xffff;
		if (adr < 0xa000) {
			if (adr >= 2)
				return ram[adr];
			else if (adr == 0)
				return (byte) ddr;
			else
				return (byte) ((ddr & pr) | (~ddr & 0x17));
		} else
			return read_byte_io(adr);
	}
	private byte read_byte_io(int adr) {
		adr &= 0xffff;
		switch (adr >> 12) {
		case 0xa:
		case 0xb:
			if (basic_in)
				return basic_rom[adr & 0x1fff];
			else
				return ram[adr];
		case 0xc:
			return ram[adr];
		case 0xd:
			if (io_in)
				switch ((adr >> 8) & 0x0f) {
					case 0x0:	// VIC
					case 0x1:
					case 0x2:
					case 0x3:
						return TheVIC.ReadRegister(adr & 0x3f);
					case 0x4:	// SID
					case 0x5:
					case 0x6:
					case 0x7:
						return TheSID.ReadRegister(adr & 0x1f);
					case 0x8:	// Color RAM
					case 0x9:
					case 0xa:
					case 0xb:
						return (byte) (color_ram[adr & 0x03ff] & 0x0f | TheVIC.LastVICByte & 0xf0);
					case 0xc:	// CIA 1
						return TheCIA1.ReadRegister(adr & 0x0f);
					case 0xd:	// CIA 2
						return TheCIA2.ReadRegister(adr & 0x0f);
					case 0xe:	// REU/Open I/O
					case 0xf:
						if ((adr & 0xfff0) == 0xdf00)
							return TheREU.ReadRegister(adr & 0x0f);
						else if (adr < 0xdfa0)
							return TheVIC.LastVICByte;
						else
							return read_emulator_id(adr & 0x7f);
				}
			else if (char_in)
				return char_rom[adr & 0x0fff];
			else
				return ram[adr];
		case 0xe:
		case 0xf:
			if (kernal_in)
				return kernal_rom[adr & 0x1fff];
			else
				return ram[adr];
		default:	// Can't happen
			return 0;
	}
}
	private int read_word(int adr) {
		adr &= 0xffff;
		byte b1 = read_byte(adr);
		byte b2 = read_byte(adr+1);
		return (b1 & 0xff) | ((b2 << 8) & 0xff00);
	}
	@Override
	protected void write_byte(int adr, byte b) {
		adr &= 0xffff;
		if (adr < 0xd000) {
			if (adr >= 2)
				ram[adr] = b;
			else if (adr == 0) {
				ddr = b;
				ram[0] = TheVIC.LastVICByte;
				new_config();
			} else {
				pr = b;
				ram[1] = TheVIC.LastVICByte;
				new_config();
			}
		} else
			write_byte_io(adr, b);
	}
	private void write_byte_io(int adr, byte b) {
		adr &= 0xffff;
		if (adr >= 0xe000) {
			ram[adr] = b;
			if (adr == 0xff00)
				TheREU.FF00Trigger();
		} else if (io_in)
			switch ((adr >> 8) & 0x0f) {
				case 0x0:	// VIC
				case 0x1:
				case 0x2:
				case 0x3:
					TheVIC.WriteRegister(adr & 0x3f, b);
					return;
				case 0x4:	// SID
				case 0x5:
				case 0x6:
				case 0x7:
					TheSID.WriteRegister(adr & 0x1f, b);
					return;
				case 0x8:	// Color RAM
				case 0x9:
				case 0xa:
				case 0xb:
					color_ram[adr & 0x03ff] = (byte) (b & 0x0f);
					return;
				case 0xc:	// CIA 1
					TheCIA1.WriteRegister(adr & 0x0f, b);
					return;
				case 0xd:	// CIA 2
					TheCIA2.WriteRegister(adr & 0x0f, b);
					return;
				case 0xe:	// REU/Open I/O
				case 0xf:
					if ((adr & 0xfff0) == 0xdf00)
						TheREU.WriteRegister(adr & 0x0f, b);
					return;
			}
		else
			ram[adr] = b;	
	}

	private byte read_zp(int adr) {
		adr &= 0xffff;
		return ram[adr];
	}
	private int read_zp_word(int adr) {
		adr &= 0xffff;
		return ((ram[adr & 0xff] << 8) & 0xff00) | (ram[(adr+1) & 0xff] & 0xff);
	}
	private void write_zp(int adr, byte b) {
		adr &= 0xffff;
		ram[adr] = b;

		// Check if memory configuration may have changed.
		if (adr < 2)
			new_config();

	}

	private void new_config() {
		byte port = (byte) (~ddr | pr);

		basic_in = (port & 3) == 3;
		kernal_in = (port & 2) != 0;
		io_in = ((port & 3) != 0) && ((port & 4) != 0);
	}
	private void jump(int adr) {
		adr &= 0xffff;
		pc = adr;
	}
	private void illegal_op(byte op, int at) {
		Reset();
		the_c64.TheDisplay.ShowRequester(String.format("Illegal opcode %02x at %04x.", op, at),
				"Reset",
				() -> the_c64.Reset());
	}
	private void illegal_jump(int at, int to) {
		Reset();
		the_c64.TheDisplay.ShowRequester(
				String.format("Jump to I/O space at %04x to %04x.", at, to),
				"Reset",
				() -> the_c64.Reset());
	}
	
	private byte read_emulator_id(int adr) {
		adr &= 0xffff;
		switch (adr) {
			case 0x7c:	// $dffc: revision
				return FRODO_REVISION << 4;
			case 0x7d:	// $dffd: version
				return FRODO_VERSION;
			case 0x7e:	// $dffe returns 'F' (Frodo ID)
				return 'F';
			case 0x7f:	// $dfff alternates between $55 and $aa
				dfff_byte = ~dfff_byte;
				return (byte) dfff_byte;
			default:
				int adr2 = adr - 0x20;
				if (adr2 < 0 || adr2 >= frodo_id.length) {
					return 0;
				}
				return frodo_id[adr2];
		}
	}

	private C64 the_c64;		// Pointer to C64 object

	private byte[] ram;			// Pointer to main RAM
	private byte[] basic_rom, kernal_rom, char_rom, color_ram; // Pointers to ROMs and color RAM
	
	private boolean nmi_state;		// State of NMI line
//
//	private byte n_flag, z_flag;
//	private boolean v_flag, d_flag, i_flag, c_flag;
//	private byte a, x, y, sp;

//	#if PC_IS_POINTER
//		uint8 *pc, *pc_base;
//	#else
//	private int pc;
//	#endif

//	#ifdef FRODO_SC
//	private int first_irq_cycle, first_nmi_cycle;
//
//	private int state, op;		// Current state and opcode
//	private int ar, ar2;			// Address registers
//	private int rdbuf;			// Data buffer for RMW instructions
	private int ddr, pr;			// Processor port
//	#else
//		int	borrowed_cycles;	// Borrowed cycles from next line
//	#endif

	private boolean basic_in, kernal_in, char_in, io_in;
	private int dfff_byte;
	// Interrupt functions
//		#ifdef FRODO_SC
//	public void TriggerVICIRQ() {
//		if (!(interrupt.intr[INT_VICIRQ] || interrupt.intr[INT_CIAIRQ]))
//			first_irq_cycle = the_c64->CycleCounter;
//		interrupt.intr[INT_VICIRQ] = true;
//	}
//
	public void TriggerCIAIRQ() {
		if (interrupt.intr[INT_VICIRQ.ordinal()] == 0 || interrupt.intr[INT_CIAIRQ.ordinal()] != 0)
			first_irq_cycle = (int) the_c64.CycleCounter;
		interrupt.intr[INT_CIAIRQ.ordinal()] = 1;
	}

	public void TriggerVICIRQ() {
		if (interrupt.intr[INT_VICIRQ.ordinal()] == 0 || interrupt.intr[INT_CIAIRQ.ordinal()] != 0)
			first_irq_cycle = (int) the_c64.CycleCounter;
		interrupt.intr[INT_VICIRQ.ordinal()] = 1;
	}

	public void TriggerNMI() {
		if (!nmi_state) {
			nmi_state = true;
			interrupt.intr[INT_NMI.ordinal()] = 1;
		}
	}
	public void ClearVICIRQ() {
		interrupt.intr[INT_VICIRQ.ordinal()] = 0;
	}

	public void ClearCIAIRQ() {
		interrupt.intr[INT_CIAIRQ.ordinal()] = 0;
	}

	public void ClearNMI() {
		nmi_state = false;
	}

	public void NewPrefs(CPUPrefs prefs) {
		setDebug(prefs.showDebug);
		setTrace(prefs.showTrace);
		
		ROMCall.NewPrefs(prefs.prefs);
	}


}
