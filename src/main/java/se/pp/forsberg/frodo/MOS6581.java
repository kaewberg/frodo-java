package se.pp.forsberg.frodo;

import java.util.Random;

import static se.pp.forsberg.frodo.enums.SIDType.*;
import se.pp.forsberg.frodo.enums.SIDType;
import se.pp.forsberg.frodo.prefs.Prefs;

class MOS6581 {
	public MOS6581(C64 c64) {
		the_renderer = null;
		for (int i=0; i<32; i++)
			regs[i] = 0;

		// Open the renderer
		open_close_renderer(SIDTYPE_NONE, Prefs.ThePrefs.SIDType);
	}
//	MOS6581::~MOS6581()
//	{
//		// Close the renderer
//		open_close_renderer(ThePrefs.SIDType, SIDTYPE_NONE);
//	}

	public void Reset() {
		for (int i=0; i<32; i++)
			regs[i] = 0;
		last_sid_byte = 0;

		// Reset the renderer
		if (the_renderer != null)
			the_renderer.Reset();
	}
	public void NewPrefs(Prefs prefs) {
		open_close_renderer(Prefs.ThePrefs.SIDType, prefs.SIDType);
		if (the_renderer != null)
			the_renderer.NewPrefs(prefs);
	}
	public void PauseSound() {
		if (the_renderer != null)
			the_renderer.Pause();
	}
	public void ResumeSound() {
		if (the_renderer != null)
			the_renderer.Resume();
	}
	public MOS6581State GetState() {
		MOS6581State ss = new MOS6581State();
	
		ss.freq_lo_1 = regs[0]; ss.freq_hi_1 = regs[1]; ss.pw_lo_1 = regs[2]; ss.pw_hi_1 = regs[3]; ss.ctrl_1 = regs[4]; ss.AD_1 = regs[5]; ss.SR_1 = regs[6];
		ss.freq_lo_2 = regs[7]; ss.freq_hi_2 = regs[8]; ss.pw_lo_2 = regs[9]; ss.pw_hi_2 = regs[10]; ss.ctrl_2 = regs[11]; ss.AD_2 = regs[12]; ss.SR_2 = regs[13];
		ss.freq_lo_3 = regs[14]; ss.freq_hi_3 = regs[15]; ss.pw_lo_3 = regs[16]; ss.pw_hi_3 = regs[17]; ss.ctrl_3 = regs[18]; ss.AD_3 = regs[19]; ss.SR_3 = regs[20];

		ss.fc_lo = regs[21]; ss.fc_hi = regs[22]; ss.res_filt = regs[23]; ss.mode_vol = regs[24];
		ss.pot_x = (byte) 0xff; ss.pot_y = (byte) 0xff; ss.osc_3 = 0; ss.env_3 = 0;

		return ss;
	}
	public void SetState(MOS6581State ss) {
		regs[0] = ss.freq_lo_1; regs[1] = ss.freq_hi_1; regs[2] = ss.pw_lo_1; regs[3] = ss.pw_hi_1; regs[4] = ss.ctrl_1; regs[5] = ss.AD_1; regs[6] = ss.SR_1;
		regs[7] = ss.freq_lo_2; regs[8] = ss.freq_hi_2; regs[9] = ss.pw_lo_2; regs[10] = ss.pw_hi_2; regs[11] = ss.ctrl_2; regs[12] = ss.AD_2; regs[13] = ss.SR_2;
		regs[14] = ss.freq_lo_3; regs[15] = ss.freq_hi_3; regs[16] = ss.pw_lo_3; regs[17] = ss.pw_hi_3;regs[18] = ss.ctrl_3; regs[19] = ss.AD_3;  regs[20] = ss.SR_3;

		regs[21] = ss.fc_lo;
		regs[22] = ss.fc_hi;
		regs[23] = ss.res_filt;
		regs[24] = ss.mode_vol;

		// Stuff the new register values into the renderer
		if (the_renderer != null)
			for (int i=0; i<25; i++)
				the_renderer.WriteRegister(i, regs[i]);
	}

	private void open_close_renderer(SIDType old_type, SIDType new_type) {
		if (old_type == new_type)
			return;

		// Delete the old renderer
//		delete the_renderer;

		// Create new renderer
		if (new_type == SIDTYPE_DIGITAL)
//	#if defined(__BEOS__) || defined(__riscos__)
//			the_renderer = new DigitalRenderer(the_c64);
//	#else
			the_renderer = new DigitalRenderer();
//	#endif
//	#ifdef AMIGA
//		else if (new_type == SIDTYPE_SIDCARD)
//			the_renderer = new SIDCardRenderer();
//	#endif
		else
			the_renderer = null;

		// Stuff the current register values into the new renderer
		if (the_renderer != null)
			for (int i=0; i<25; i++)
				the_renderer.WriteRegister(i, regs[i]);
	}

	private C64 the_c64;				// Pointer to C64 object
	private SIDRenderer the_renderer;	// Pointer to current renderer
	private byte[] regs = new byte[32];				// Copies of the 25 write-only SID registers
	private byte last_sid_byte;		// Last value written to SID

	/*
	 * Fill buffer (for Unix sound routines), sample volume (for sampled voice)
	 */

	public void EmulateLine() {
		if (the_renderer != null)
			the_renderer.EmulateLine();
	}


	/*
	 *  Read from register
	 */

	public byte ReadRegister(int adr) {
		// A/D converters
		if (adr == 0x19 || adr == 0x1a) {
			last_sid_byte = 0;
			return (byte) 0xff;
		}

		// Voice 3 oscillator/EG readout
		if (adr == 0x1b || adr == 0x1c) {
			last_sid_byte = 0;
			return (byte) rand();
		}

		// Write-only register: Return last value written to SID
		return last_sid_byte;
	}


	/*
	 *  Write to register
	 */

	private Random random = new Random();
	private int rand() {
		return random.nextInt();
	}

	public void WriteRegister(int adr, byte b) {
		// Keep a local copy of the register values
		last_sid_byte = regs[(adr & 0xffff)] = b;

		if (the_renderer != null)
			the_renderer.WriteRegister(adr, b);
	}
	

}
