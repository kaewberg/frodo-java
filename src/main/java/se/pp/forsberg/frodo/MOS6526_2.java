package se.pp.forsberg.frodo;

class MOS6526_2 extends MOS6526 {
	public MOS6526_2(MOS6510 CPU, MOS6569 VIC, MOS6502_1541 CPU1541) {
		super(CPU);
		the_vic = VIC;
		the_cpu_1541 = CPU1541;
	}

	@Override public void Reset() {
		super.Reset();

		// VA14/15 = 0
		the_vic.ChangedVA(0);

		// IEC
		IECLines = (byte) 0xd0;
	}
	public byte ReadRegister(int adr) {
		switch (adr) {
			case 0x00:
				return (byte) ((pra | ~ddra) & 0x3f
					| IECLines & the_cpu_1541.IECLines);
			case 0x01: return (byte) (prb | ~ddrb);
			case 0x02: return ddra;
			case 0x03: return ddrb;
			case 0x04: return (byte) ta;
			case 0x05: return (byte) (ta >> 8);
			case 0x06: return (byte) tb;
			case 0x07: return (byte) (tb >> 8);
			case 0x08: tod_halt = false; return tod_10ths;
			case 0x09: return tod_sec;
			case 0x0a: return tod_min;
			case 0x0b: tod_halt = true; return tod_hr;
			case 0x0c: return sdr;
			case 0x0d: {
				byte ret = icr; // Read and clear ICR
				icr = 0;
				the_cpu.ClearNMI();
				return ret;
			}
			case 0x0e: return cra;
			case 0x0f: return crb;
		}
		return 0;	// Can't happen
	}
	public void WriteRegister(int adr, byte b) {
		switch (adr) {
			case 0x0:{
				pra = b;
				the_vic.ChangedVA(~(pra | ~ddra) & 3);
				byte old_lines = IECLines;
				IECLines = (byte) ((~b << 2) & 0x80	// DATA
					| (~b << 2) & 0x40		// CLK
					| (~b << 1) & 0x10);		// ATN
				if (((IECLines ^ old_lines) & 0x10) != 0) {	// ATN changed
					the_cpu_1541.NewATNState();
					if ((old_lines & 0x10) != 0)				// ATN 1->0
						the_cpu_1541.IECInterrupt();
				}
				break;
			}
			case 0x1: prb = b; break;
	
			case 0x2:
				ddra = b;
				the_vic.ChangedVA(~(pra | ~ddra) & 3);
				break;
			case 0x3: ddrb = b; break;
	
			case 0x4: latcha = (short) ((latcha & 0xff00) | b); break;
			case 0x5:
				latcha = (short) ((latcha & 0xff) | (b << 8));
				if ((cra & 1) == 0)	// Reload timer if stopped
					ta = latcha;
				break;
	
			case 0x6: latchb = (short) ((latchb & 0xff00) | b); break;
			case 0x7:
				latchb = (short) ((latchb & 0xff) | (b << 8));
				if ((crb & 1) == 0)	// Reload timer if stopped
					tb = latchb;
				break;
	
			case 0x8:
				if ((crb & 0x80) != 0)
					alm_10ths = (byte) (b & 0x0f);
				else
					tod_10ths = (byte) (b & 0x0f);
				break;
			case 0x9:
				if ((crb & 0x80) != 0)
					alm_sec = (byte) (b & 0x7f);
				else
					tod_sec = (byte) (b & 0x7f);
				break;
			case 0xa:
				if ((crb & 0x80) != 0)
					alm_min = (byte) (b & 0x7f);
				else
					tod_min = (byte) (b & 0x7f);
				break;
			case 0xb:
				if ((crb & 0x80) != 0)
					alm_hr = (byte) (b & 0x9f);
				else
					tod_hr = (byte) (b & 0x9f);
				break;
	
			case 0xc:
				sdr = b;
				TriggerInterrupt(8);	// Fake SDR interrupt for programs that need it
				break;
	
			case 0xd:
				if ((b & 0x80) != 0)
					int_mask |= b & 0x7f;
				else
					int_mask &= ~b;
				if ((icr & int_mask & 0x1f) != 0) { // Trigger NMI if pending
					icr |= 0x80;
					the_cpu.TriggerNMI();
				}
				break;
	
			case 0xe:
				has_new_cra = true;		// Delay write by 1 cycle
				new_cra = b;
				ta_cnt_phi2 = ((b & 0x20) == 0x00);
				break;
	
			case 0xf:
				has_new_crb = true;		// Delay write by 1 cycle
				new_crb = b;
				tb_cnt_phi2 = ((b & 0x60) == 0x00);
				tb_cnt_ta = ((b & 0x60) == 0x40);
				break;
		}
	}
	@Override public void TriggerInterrupt(int bit) {
		icr |= bit;
		if ((int_mask & bit) != 0) {
			icr |= 0x80;
			the_cpu.TriggerNMI();
		}
	}

	public byte IECLines;		// State of IEC lines (bit 7 - DATA, bit 6 - CLK, bit 4 - ATN)

	private MOS6569 the_vic;
	private MOS6502_1541 the_cpu_1541;
}
