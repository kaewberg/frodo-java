package se.pp.forsberg.frodo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Random;

import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.libc.ByteC;

class C64 {
	public final static  boolean isFrodoSC = true;
	public C64() {
		int i,j;
		byte[] p;

		// The thread is not yet running
		thread_running = false;
		quit_thyself = false;
//		have_a_break = false;

		// System-dependent things
		c64_ctor1();

		// Open display
		TheDisplay = new C64Display(this);

		// Allocate RAM/ROM memory
		RAM = new byte[0x10000];
		Basic = new byte[0x2000];
		Kernal = new byte[0x2000];
		Char = new byte[0x1000];
		Color = new byte[0x0400];
		RAM1541 = new byte[0x0800];
		ROM1541 = new byte[0x4000];

		// Create the chips
		TheCPU = new MOS6510(this, RAM, Basic, Kernal, Char, Color);

		TheJob1541 = new Job1541(RAM1541);
		TheCPU1541 = new MOS6502_1541(this, TheJob1541, TheDisplay, RAM1541, ROM1541);

		TheVIC = TheCPU.TheVIC = new MOS6569(this, TheDisplay, TheCPU, RAM, Char, Color);
		TheSID = TheCPU.TheSID = new MOS6581(this);
		TheCIA1 = TheCPU.TheCIA1 = new MOS6526_1(TheCPU, TheVIC);
		TheCIA2 = TheCPU.TheCIA2 = TheCPU1541.TheCIA2 = new MOS6526_2(TheCPU, TheVIC, TheCPU1541);
		TheIEC = TheCPU.TheIEC = new IEC(TheDisplay);
		TheREU = TheCPU.TheREU = new REU(TheCPU);

		// Initialize RAM with powerup pattern
		p = RAM;
		for (i=0/*, p=RAM*/; i<512; i++) {
			for (j=0; j<64; j++)
				p[i*128 + j] = 0;
//				*p++ = 0;
			for (j=0; j<64; j++)
				p[i*128 + 64 + j] = (byte) 0xff;
//				*p++ = 0xff;
		}

		// Initialize color RAM with random values
		p = Color;
		for (i=0/*, p=Color*/; i<1024; i++)
			p[i] = (byte) (rand() & 0x0f);
//			*p++ = rand() & 0x0f;

		// Clear 1541 RAM
//		memset(RAM1541, 0, 0x800);

		// Open joystick drivers if required
		open_close_joysticks(false, false, Prefs.ThePrefs.Joystick1On, Prefs.ThePrefs.Joystick2On);
		joykey.b = (byte) 0xff;

//	#ifdef FRODO_SC
		CycleCounter = 0;
//	#endif

		// System-dependent things
		c64_ctor2();
	}
//		~C64();

	private Random random = new Random();
	private int rand() {
		return random.nextInt();
	}

	public void Run() {
		// Reset chips
		TheCPU.Reset();
		TheSID.Reset();
		TheCIA1.Reset();
		TheCIA2.Reset();
		TheCPU1541.Reset();

		// Patch kernal IEC routines
		orig_kernal_1d84 = Kernal[0x1d84];
		orig_kernal_1d85 = Kernal[0x1d85];

		// Start timer_io
//		timer_io->tr_node.io_Command = TR_ADDREQUEST;
//		timer_io->tr_time.tv_secs = 0;
//		timer_io->tr_time.tv_micro = ThePrefs.SkipFrames * 20000;  // 20ms per frame
//		SendIO((struct IORequest *)timer_io);

		// Start the CPU thread
		thread_running = true;
		quit_thyself = false;
//		have_a_break = false;
		NewPrefs(Prefs.ThePrefs);
		
		if (Prefs.ThePrefs.saveOnExit && Frodo.autoSnapFile.exists()) {
			LoadSnapshot(Frodo.autoSnapFile);
		}
		
		thread_func();
	}
	public void Quit() {
		// Ask the thread to quit itself if it is running
		if (thread_running) {
			quit_thyself = true;
			thread_running = false;
		}
	}
	public void Pause() {
		TheSID.PauseSound();
	}
	public void Resume() {
		TheSID.ResumeSound();
	}
	public void Reset() {
		TheCPU.AsyncReset();
		TheCPU1541.AsyncReset();
		TheSID.Reset();
		TheCIA1.Reset();
		TheCIA2.Reset();
		TheIEC.Reset();
	}
	public void NMI() {
		TheCPU.AsyncNMI();
	}
	private final static int FRAME_NANO = 20_000_000;
	private long start_time = System.nanoTime();
	private int[] speed_indexes = new int[10];
	private int speed_idx = 0;
	private long t0;
	private int frame_counter = 0;
	
	public void VBlank(boolean draw_frame) {
		int speed_index;
		long end_time = System.nanoTime();

		// Poll keyboard
		TheDisplay.PollKeyboard(TheCIA1.KeyMatrix, TheCIA1.RevMatrix, joykey);
 
		// Poll joysticks
		TheCIA1.Joystick1 = poll_joystick(0);
		TheCIA1.Joystick2 = poll_joystick(1);

		if (Prefs.ThePrefs.JoystickSwap) {
			byte tmp = TheCIA1.Joystick1;
			TheCIA1.Joystick1 = TheCIA1.Joystick2;
			TheCIA1.Joystick2 = tmp;
		}

		// Joystick keyboard emulation
		if (TheDisplay.LockKey())
			TheCIA1.Joystick1 &= joykey.b;
		else
			TheCIA1.Joystick2 &= joykey.b;

		// Count TOD clocks
		TheCIA1.CountTOD();
		TheCIA2.CountTOD();

		// Update window if needed
		if (draw_frame) {
			TheDisplay.Update();

			// Calculate time between VBlanks, display speedometer
			if (frame_counter == 0) {
				t0 = System.nanoTime();
			}
			
			if (frame_counter != 0 && Prefs.ThePrefs.LimitSpeed) {
				long t = System.nanoTime();
				long t_last = t0 + (frame_counter - 1) * FRAME_NANO;
				long time_spent = t - t_last;
				long time_left = FRAME_NANO - time_spent;
				if (t < 20_000_000) {
					try {
						Thread.sleep(time_left / 1_000_000, (int) (time_left % 1_000_000));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					long t2 = System.nanoTime();
					long slept = t2 - t;
					long diff = slept - time_left;
					if (diff < 0) {
						diff = -diff;
					}
					if (diff > Integer.MAX_VALUE) {
						System.out.println("*Really* bad timers....");
					}
					int diffi = (int) diff;
					if (diffi > FRAME_NANO) {
						System.out.printf("Very bad timers - diff %d.%d ms\n", diff/1000000, diff%1000000);
					}
					end_time = System.nanoTime();
				}
			}
			
			speed_index = (int) (20_000 * 100/* * Prefs.ThePrefs.SkipFrames*/ / ((end_time - start_time)/1000 + 1));
			speed_indexes[speed_idx++ % speed_indexes.length] = speed_index;
			int speed_index_avg = (int) Arrays.stream(speed_indexes).average().orElse(100);
			TheDisplay.Speedometer(speed_index_avg);
			start_time = System.nanoTime();
			frame_counter++;
		}
		

		if (saveFile != null) {
			SaveSnapshot(saveFile);
			saveFile = null;
		}
		if (openFile != null) {
			LoadSnapshot(openFile);
			openFile = null;
		}
	}
	public void NewPrefs(Prefs prefs) {
		open_close_joysticks(Prefs.ThePrefs.Joystick1On, Prefs.ThePrefs.Joystick2On, prefs.Joystick1On, prefs.Joystick2On);
		PatchKernal(prefs.FastReset, prefs.Emul1541Proc);

		TheDisplay.NewPrefs(prefs);

//	#ifdef __riscos__
//		// Changed order of calls. If 1541 mode hasn't changed the order is insignificant.
//		if (prefs->Emul1541Proc) {
//			// New prefs have 1541 enabled ==> if old prefs had disabled free drives FIRST
//			TheIEC->NewPrefs(prefs);
//			TheJob1541->NewPrefs(prefs);
//		} else {
//			// New prefs has 1541 disabled ==> if old prefs had enabled free job FIRST
//			TheJob1541->NewPrefs(prefs);
//			TheIEC->NewPrefs(prefs);
//		}
//	#else
		TheIEC.NewPrefs(prefs);
		TheJob1541.NewPrefs(prefs);
//	#endif

		TheREU.NewPrefs(prefs);
		TheSID.NewPrefs(prefs);

		// Reset 1541 processor if turned on
		if (!Prefs.ThePrefs.Emul1541Proc && prefs.Emul1541Proc)
			TheCPU1541.AsyncReset();
		
		TheCPU.NewPrefs(prefs.cpuPrefs);
	}
	public void PatchKernal(boolean fast_reset, boolean emul_1541_proc) {
		if (fast_reset) {
			Kernal[0x1d84] = (byte) 0xa0;
			Kernal[0x1d85] = 0x00;
		} else {
			Kernal[0x1d84] = orig_kernal_1d84;
			Kernal[0x1d85] = orig_kernal_1d85;
		}

		if (emul_1541_proc) {
			Kernal[0x0d40] = 0x78;
			Kernal[0x0d41] = 0x20;
			Kernal[0x0d23] = 0x78;
			Kernal[0x0d24] = 0x20;
			Kernal[0x0d36] = 0x78;
			Kernal[0x0d37] = 0x20;
			Kernal[0x0e13] = 0x78;
			Kernal[0x0e14] = (byte) 0xa9;
			Kernal[0x0def] = 0x78;
			Kernal[0x0df0] = 0x20;
			Kernal[0x0dbe] = (byte) 0xad;
			Kernal[0x0dbf] = 0x00;
			Kernal[0x0dcc] = 0x78;
			Kernal[0x0dcd] = 0x20;
			Kernal[0x0e03] = 0x20;
			Kernal[0x0e04] = (byte) 0xbe;
		} else {
			Kernal[0x0d40] = (byte) 0xf2;	// IECOut
			Kernal[0x0d41] = 0x00;
			Kernal[0x0d23] = (byte) 0xf2;	// IECOutATN
			Kernal[0x0d24] = 0x01;
			Kernal[0x0d36] = (byte) 0xf2;	// IECOutSec
			Kernal[0x0d37] = 0x02;
			Kernal[0x0e13] = (byte) 0xf2;	// IECIn
			Kernal[0x0e14] = 0x03;
			Kernal[0x0def] = (byte) 0xf2;	// IECSetATN
			Kernal[0x0df0] = 0x04;
			Kernal[0x0dbe] = (byte) 0xf2;	// IECRelATN
			Kernal[0x0dbf] = 0x05;
			Kernal[0x0dcc] = (byte) 0xf2;	// IECTurnaround
			Kernal[0x0dcd] = 0x06;
			Kernal[0x0e03] = (byte) 0xf2;	// IECRelease
			Kernal[0x0e04] = 0x07;
		}

		// 1541
		ROM1541[0x2ae4] = (byte) 0xea;		// Don't check ROM checksum
		ROM1541[0x2ae5] = (byte) 0xea;
		ROM1541[0x2ae8] = (byte) 0xea;
		ROM1541[0x2ae9] = (byte) 0xea;
		ROM1541[0x2c9b] = (byte) 0xf2;		// DOS idle loop
		ROM1541[0x2c9c] = 0x00;
		ROM1541[0x3594] = 0x20;		// Write sector
		ROM1541[0x3595] = (byte) 0xf2;
		ROM1541[0x3596] = (byte) 0xf5;
		ROM1541[0x3597] = (byte) 0xf2;
		ROM1541[0x3598] = 0x01;
		ROM1541[0x3b0c] = (byte) 0xf2;		// Format track
		ROM1541[0x3b0d] = 0x02;
	}
	public void SaveRAM(File file) {
		try (OutputStream os = new FileOutputStream(file)) {
			os.write(RAM);
			os.write(Color);
			if (Prefs.ThePrefs.Emul1541Proc)
				os.write(RAM1541);
		} catch (Exception x) {
			TheDisplay.ShowRequester("RAM save failed.", "OK", null);
		}
	}
	public void SaveSnapshot(File file) {
		byte flags;
		byte delay;
		int stat;

		try (OutputStream os = new FileOutputStream(file)) {
			
			for (int i = 0; i < SNAPSHOT_HEADER.length(); i++) {
				char c = SNAPSHOT_HEADER.charAt(i);
				os.write(c);
			}
			os.write(10);
			os.write(0); // Version number 0
			flags = 0;
			if (Prefs.ThePrefs.Emul1541Proc)
				flags |= SNAPSHOT_1541;
			os.write(flags);
			SaveVICState(os);
			SaveSIDState(os);
			SaveCIAState(os);
	
//		#ifdef FRODO_SC
			delay = 0;
			do {
				if ((stat = SaveCPUState(os)) == -1) {	// -1 -> Instruction not finished yet
					ADVANCE_CYCLES();	// Advance everything by one cycle
					delay++;
				}
			} while (stat == -1);
			os.write(delay); // Number of cycles the saved CPUC64 lags behind the previous chips
//		#else
//			SaveCPUState(f);
//			fputc(0, f);		// No delay
//		#endif
	
			if (Prefs.ThePrefs.Emul1541Proc) {
				String f = Prefs.ThePrefs.DrivePath[0].getCanonicalPath();
				for (int i = 0; i < 256; i++) {
					os.write(i < f.length()? f.charAt(i):0);
				}
//		#ifdef FRODO_SC
				delay = 0;
				do {
					if ((stat = Save1541State(os)) == -1) {
						ADVANCE_CYCLES();
						delay++;
					}
				} while (stat == -1);
				os.write(delay);
//		#else
//				Save1541State(f);
//				fputc(0, f);	// No delay
//		#endif
				Save1541JobState(os);
			}

			
//			if (Prefs.ThePrefs.Emul1541Proc) {
//				String f = Prefs.ThePrefs.DrivePath[0].getCanonicalPath();
//				for (int i = 0; i < 256; i++) {
//					os.write(i < f.length()? f.charAt(i):0);
//				}
//			}
////		#ifdef FRODO_SC
//			delay = 0;
//			do {
//				if ((stat = Save1541State(os)) == -1) {
//					ADVANCE_CYCLES();
//					delay++;
//				}
//			} while (stat == -1);
//			os.write(delay);
////		#else
////				Save1541State(f);
////				fputc(0, f);	// No delay
////		#endif
//			Save1541JobState(os);
		} catch (Exception x) {
			TheDisplay.ShowRequester("Unable to open snapshot file", "OK", null);
		}

//	#ifdef __riscos__
//		TheWIMP->SnapshotSaved(true);
//	#endif
	}
	public boolean LoadSnapshot(File file) {
		try (InputStream is = new FileInputStream(file)) {
//			char Header[] = SNAPSHOT_HEADER;
//			char *b = Header, 
			char c = 0;
			byte delay, i;
			
			for (i = 0; i < SNAPSHOT_HEADER.length(); i++) {
				if ((c = (char) is.read()) != SNAPSHOT_HEADER.charAt(i)) {
					TheDisplay.ShowRequester("Unknown snapshot format", "OK", null);
					return false;
				}
			}

			byte flags;
			boolean error = false;
//	#ifndef FRODO_SC
//				long vicptr;	// File offset of VIC data
//	#endif

			while (c != 10)
				c = (char) is.read();	// Shouldn't be necessary
			if (is.read() != 0) {
				TheDisplay.ShowRequester("Unknown snapshot format", "OK", null);
				return false;
			}
			flags = (byte) is.read();
//	#ifndef FRODO_SC
//				vicptr = ftell(f);
//	#endif

			error |= !LoadVICState(is);
			error |= !LoadSIDState(is);
			error |= !LoadCIAState(is);
			error |= !LoadCPUState(is);

			delay = (byte) is.read();	// Number of cycles the 6510 is ahead of the previous chips
//	#ifdef FRODO_SC
			// Make the other chips "catch up" with the 6510
			for (i=0; i<delay; i++) {
				TheVIC.EmulateCycle();
				TheCIA1.EmulateCycle();
				TheCIA2.EmulateCycle();
			}
//	#endif
			if ((flags & SNAPSHOT_1541) != 0) {
				Prefs prefs = new Prefs(Prefs.ThePrefs);
	
				// First switch on emulation
				StringBuilder sb = new StringBuilder();
				for (i = 0; i < 256; i++) {
					c = (char) is.read();
					if (c > 0) {
						sb.append(c);
					}
				}
				prefs.DrivePath[0] = new File(sb.toString());
				prefs.Emul1541Proc = true;
				NewPrefs(prefs);
				Prefs.ThePrefs = prefs;
		
				// Then read the context
				error |= !Load1541State(is);
	
				delay = (byte) is.read();	// Number of cycles the 6502 is ahead of the previous chips
//	#ifdef FRODO_SC
				// Make the other chips "catch up" with the 6502
				for (i=0; i<delay; i++) {
					TheVIC.EmulateCycle();
					TheCIA1.EmulateCycle();
					TheCIA2.EmulateCycle();
					TheCPU.EmulateCycle();
				}
//	#endif
				Load1541JobState(is);
//	#ifdef __riscos__
//					TheWIMP->ThePrefsToWindow();
//	#endif
			} else if (Prefs.ThePrefs.Emul1541Proc) {	// No emulation in snapshot, but currently active?
				Prefs prefs = new Prefs(Prefs.ThePrefs);
				prefs.Emul1541Proc = false;
				NewPrefs(prefs);
				Prefs.ThePrefs = prefs;
//	#ifdef __riscos__
//					TheWIMP->ThePrefsToWindow();
//	#endif
			}

//	#ifndef FRODO_SC
//				fseek(f, vicptr, SEEK_SET);
//				LoadVICState(f);	// Load VIC data twice in SL (is REALLY necessary sometimes!)
//	#endif
			
			if (error) {
				TheDisplay.ShowRequester("Error reading snapshot file", "OK", null);
				Reset();
				return false;
			} else
				return true;
		} catch (Exception x) {
			TheDisplay.ShowRequester("Can't open snapshot file", "OK", null);
			return false;
		}
	}
	public int SaveCPUState(OutputStream os) {
		MOS6510State state = TheCPU.GetState();

		if (!state.instruction_complete)
			return -1;

		try {
			os.write(RAM);
			os.write(Color);
			os.write(state.toBytes());
		} catch (Exception x) {
			return 0;
		}
		return 1;
	}
	public int Save1541State(OutputStream os) {
		MOS6502State state = TheCPU1541.GetState();

		if (!state.idle && !state.instruction_complete)
			return -1;

		try {
			os.write(RAM1541);
			os.write(state.toBytes());
		} catch (Exception x) {
			x.printStackTrace();
			return -1;
		}
		return 1;
	}
	public boolean Save1541JobState(OutputStream os) {
		try {
			os.write(TheJob1541.GetState().toBytes());
			return true;
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
	}
	public boolean SaveVICState(OutputStream os) {
		try {
			os.write(TheVIC.GetState().toBytes());
			return true;
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
	}
	public boolean SaveSIDState(OutputStream os) {
		try {
			os.write(TheSID.GetState().toBytes());
			return true;
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
	}
	public boolean SaveCIAState(OutputStream os) {
		try {
			os.write(TheCIA1.GetState().toBytes());
			os.write(TheCIA2.GetState().toBytes());
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean LoadCPUState(InputStream is) {
		byte[] buf = new byte[MOS6510State.STATE_SIZE];

		try {
			read(is, RAM);
			read(is, Color);
			read(is, buf);
			
			TheCPU.SetState(new MOS6510State(buf));
			return true;
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
	}
	public boolean Load1541State(InputStream is) {
		byte buf[] = new byte[MOS6502State.STATE_SIZE];

		try {
			read(is, RAM1541);
			read(is, buf);
			TheCPU1541.SetState(new MOS6502State(buf));
			return true;
		} catch (Exception x) {
			return false;
		}
	}
	public boolean Load1541JobState(InputStream is) {
		byte[] buf = new byte[Job1541State.STATE_SIZE];
		try {
			read(is, buf);
			TheJob1541.SetState(new Job1541State(buf));
			return true;
		} catch (Exception x) {
			return false;
		}
	}

	public boolean LoadVICState(InputStream is) {
		byte[] buf = new byte[MOS6569State.STATE_SIZE];

		try {
			read(is, buf);
			TheVIC.SetState(new MOS6569State(buf));
			return true;
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
	}
	public boolean LoadSIDState(InputStream is) {
		byte[] buf = new byte[MOS6581State.STATE_SIZE];

		try {
			read(is, buf);
			TheSID.SetState(new MOS6581State(buf));
			return true;
		} catch (Exception x) {
			x.printStackTrace();
			return false;
		}
	}
	public boolean LoadCIAState(InputStream is) {

		try {
			byte[] buf = new byte[MOS6526State.STATE_SIZE];
			read(is, buf);
			TheCIA1.SetState(new MOS6526State(buf));
			read(is, buf);
			TheCIA2.SetState(new MOS6526State(buf));
			return true;
		} catch (Exception x) {
			return false;
		}
	}

	private void read(InputStream is, byte[] buf) throws IOException {
		int totRead = 0;
		do {
			int read = is.read(buf, totRead, buf.length - totRead);
			totRead += read;
		} while (totRead < buf.length);
	}

	public byte[] RAM, Basic, Kernal,
			  Char, Color;		// C64
	public byte[] RAM1541, ROM1541;	// 1541

	public C64Display TheDisplay;

	public MOS6510 TheCPU;			// C64
	public MOS6569 TheVIC;
	public MOS6581 TheSID;
	public MOS6526_1 TheCIA1;
	public MOS6526_2 TheCIA2;
	public IEC TheIEC;
	public REU TheREU;

	public MOS6502_1541 TheCPU1541;	// 1541
	public Job1541 TheJob1541;

//	#ifdef FRODO_SC
	public long CycleCounter;
//	#endif


	private final static String SNAPSHOT_HEADER = "FrodoSnapshot";
	private final int SNAPSHOT_1541 = 1;

	private void c64_ctor1() {
//		// Open game_io
//		game_port = CreateMsgPort();
//		game_io = (struct IOStdReq *)CreateIORequest(game_port, sizeof(IOStdReq));
//		game_io->io_Message.mn_Node.ln_Type = NT_UNKNOWN;
//		game_open = port_allocated = false;
//		if (!OpenDevice("gameport.device", 1, (struct IORequest *)game_io, 0))
//			game_open = true;
	}
	private void c64_ctor2() {
//		// Initialize joystick variables
//		joy_state = 0xff;
//
//		// Open timer_io
//		timer_port = CreateMsgPort();
//		timer_io = (struct timerequest *)CreateIORequest(timer_port, sizeof(struct timerequest));
//		OpenDevice(TIMERNAME, UNIT_MICROHZ, (struct IORequest *)timer_io, 0);
//
//		// Get timer base
//		TimerBase = timer_io->tr_node.io_Device;
//
//		// Preset speedometer start time
//		GetSysTime(&start_time);
		start_time = System.nanoTime();
	}
//	private void c64_dtor() {
//		// Stop and delete timer_io
//		if (timer_io != NULL) {
//			if (!CheckIO((struct IORequest *)timer_io))
//				WaitIO((struct IORequest *)timer_io);
//			CloseDevice((struct IORequest *)timer_io);
//			DeleteIORequest((struct IORequest *)timer_io);
//		}
//
//		if (timer_port != NULL)
//			DeleteMsgPort(timer_port);
//
//		if (game_open) {
//			if (!CheckIO((struct IORequest *)game_io)) {
//				AbortIO((struct IORequest *)game_io);
//				WaitIO((struct IORequest *)game_io);
//			}
//			CloseDevice((struct IORequest *)game_io);
//		}
//
//		if (game_io != NULL)
//			DeleteIORequest((struct IORequest *)game_io);
//
//		if (game_port != NULL)
//			DeleteMsgPort(game_port);

//	}
	private void open_close_joysticks(boolean oldjoy1, boolean oldjoy2, boolean newjoy1, boolean newjoy2) {
//		if (game_open && (oldjoy2 != newjoy2))
//
//			if (newjoy2) {	// Open joystick
//				joy_state = 0xff;
//				port_allocated = false;
//
//				// Allocate game port
//				BYTE ctype;
//				Forbid();
//				game_io->io_Command = GPD_ASKCTYPE;
//				game_io->io_Data = &ctype;
//				game_io->io_Length = 1;
//				DoIO((struct IORequest *)game_io);
//
//				if (ctype != GPCT_NOCONTROLLER)
//					Permit();
//				else {
//					ctype = GPCT_ABSJOYSTICK;
//					game_io->io_Command = GPD_SETCTYPE;
//					game_io->io_Data = &ctype;
//					game_io->io_Length = 1;
//					DoIO((struct IORequest *)game_io);
//					Permit();
//
//					port_allocated = true;
//
//					// Set trigger conditions
//					game_trigger.gpt_Keys = GPTF_UPKEYS | GPTF_DOWNKEYS;
//					game_trigger.gpt_Timeout = 65535;
//					game_trigger.gpt_XDelta = 1;
//					game_trigger.gpt_YDelta = 1;
//					game_io->io_Command = GPD_SETTRIGGER;
//					game_io->io_Data = &game_trigger;
//					game_io->io_Length = sizeof(struct GamePortTrigger);
//					DoIO((struct IORequest *)game_io);
//
//					// Flush device buffer
//					game_io->io_Command = CMD_CLEAR;
//					DoIO((struct IORequest *)game_io);
//
//					// Start reading joystick events
//					game_io->io_Command = GPD_READEVENT;
//					game_io->io_Data = &game_event;
//					game_io->io_Length = sizeof(struct InputEvent);
//					SendIO((struct IORequest *)game_io);
//				}
//
//			} else {	// Close joystick
//
//				// Abort game_io
//				if (!CheckIO((struct IORequest *)game_io)) {
//					AbortIO((struct IORequest *)game_io);
//					WaitIO((struct IORequest *)game_io);
//				}
//
//				// Free game port
//				if (port_allocated) {
//					BYTE ctype = GPCT_NOCONTROLLER;
//					game_io->io_Command = GPD_SETCTYPE;
//					game_io->io_Data = &ctype;
//					game_io->io_Length = 1;
//					DoIO((struct IORequest *)game_io);
//
//					port_allocated = false;
//				}
//			}
	}
	byte poll_joystick(int port) {
//		if (port == 0)
//			return 0xff;
//
//		if (game_open && port_allocated) {
//
//			// Joystick event arrived?
//			while (GetMsg(game_port) != NULL) {
//
//				// Yes, analyze event
//				switch (game_event.ie_Code) {
//					case IECODE_LBUTTON:					// Button pressed
//						joy_state &= 0xef;
//						break;
//
//					case IECODE_LBUTTON | IECODE_UP_PREFIX:	// Button released
//						joy_state |= 0x10;
//						break;
//
//					case IECODE_NOBUTTON:					// Joystick moved
//						if (game_event.ie_X == 1)
//							joy_state &= 0xf7;				// Right
//						if (game_event.ie_X == -1)
//							joy_state &= 0xfb;				// Left
//						if (game_event.ie_X == 0)
//							joy_state |= 0x0c;
//						if (game_event.ie_Y == 1)
//							joy_state &= 0xfd;				// Down
//						if (game_event.ie_Y == -1)
//							joy_state &= 0xfe;				// Up
//						if (game_event.ie_Y == 0)
//							joy_state |= 0x03;
//						break;
//				}
//
//				// Start reading the next event
//				game_io->io_Command = GPD_READEVENT;
//				game_io->io_Data = &game_event;
//				game_io->io_Length = sizeof(struct InputEvent);
//				SendIO((struct IORequest *)game_io);
//			}
//			return joy_state;
//
//		} else
			return (byte) 0xff;
	}
	private void thread_func() {
		try {
			while (!quit_thyself) {
		
		//			#ifdef FRODO_SC
						// The order of calls is important here
						if (TheVIC.EmulateCycle())
							TheSID.EmulateLine();
						TheCIA1.CheckIRQs();
						TheCIA2.CheckIRQs();
						TheCIA1.EmulateCycle();
						TheCIA2.EmulateCycle();
						TheCPU.EmulateCycle();
		
						if (Prefs.ThePrefs.Emul1541Proc) {
							TheCPU1541.CountVIATimers(1);
							if (!TheCPU1541.Idle)
								TheCPU1541.EmulateCycle();
						}
						CycleCounter++;
						
		//			#else
		//					// The order of calls is important here
		//					int cycles = TheVIC.EmulateLine();
		//					TheSID.EmulateLine();
		//			#if !PRECISE_CIA_CYCLES
		//					TheCIA1.EmulateLine(ThePrefs.CIACycles);
		//					TheCIA2.EmulateLine(ThePrefs.CIACycles);
		//			#endif
		//
		//					if (ThePrefs.Emul1541Proc) {
		//						int cycles_1541 = ThePrefs.FloppyCycles;
		//						TheCPU1541.CountVIATimers(cycles_1541);
		//
		//						if (!TheCPU1541.Idle) {
		//							// 1541 processor active, alternately execute
		//							//  6502 and 6510 instructions until both have
		//							//  used up their cycles
		//							while (cycles >= 0 || cycles_1541 >= 0)
		//								if (cycles > cycles_1541)
		//									cycles -= TheCPU.EmulateLine(1);
		//								else
		//									cycles_1541 -= TheCPU1541.EmulateLine(1);
		//						} else
		//							TheCPU.EmulateLine(cycles);
		//					} else
		//						// 1541 processor disabled, only emulate 6510
		//						TheCPU.EmulateLine(cycles);
		//			#endif
					}
		} catch (Exception x) {
			System.err.println("Exiting emulator due to exception in emulation thread");
			x.printStackTrace();
			System.exit(1);
		}
	}
	
	File saveFile, openFile;

	private boolean thread_running;	// Emulation thread is running
	private boolean quit_thyself;		// Emulation thread shall quit
//	private boolean have_a_break;		// Emulation thread shall pause

//	private int joy_minx, joy_maxx, joy_miny, joy_maxy; // For dynamic joystick calibration
	private ByteC joykey = new ByteC();			// Joystick keyboard emulation mask value

	private byte orig_kernal_1d84,	// Original contents of kernal locations $1d84 and $1d85
			  orig_kernal_1d85;	// (for undoing the Fast Reset patch)

	private void ADVANCE_CYCLES() {
		TheVIC.EmulateCycle();
		TheCIA1.EmulateCycle();
		TheCIA2.EmulateCycle();
		TheCPU.EmulateCycle();
		if (Prefs.ThePrefs.Emul1541Proc) {
			TheCPU1541.CountVIATimers(1);
			if (!TheCPU1541.Idle)
				TheCPU1541.EmulateCycle();
		}
	}
//	#ifdef __BEOS__
//	public:
//	public void SoundSync();
//
//	private:
//		static long thread_invoc(void *obj);
//
//		BJoystick *joy[2];		// Joystick objects
//		thread_id the_thread;
//		sem_id pause_sem;
//		sem_id sound_sync_sem;
//		double start_time;
//	#endif
//
//	#ifdef AMIGA
//		struct MsgPort *timer_port;		// For speed limiter
//		struct timerequest *timer_io;
//		struct timeval start_time;
//		struct MsgPort *game_port;		// For joystick
//		struct IOStdReq *game_io;
//		struct GamePortTrigger game_trigger;
//		struct InputEvent game_event;
//		UBYTE joy_state;				// Current state of joystick
//	public boolean game_open, port_allocated;	// Flags: gameport.device opened, game port allocated
//	#endif
//
//	#ifdef __unix
//	public int joyfd[2];			// File descriptors for joysticks
//		double speed_index;
//	public:
//		CmdPipe *gui;
//	#endif
//
//	#ifdef WIN32
//	private:
//	public void CheckTimerChange();
//	public void StartTimer();
//	public void StopTimer();
//		static void CALLBACK StaticTimeProc(UINT uID, UINT uMsg, DWORD dwUser, DWORD dw1, DWORD dw2);
//	public void TimeProc(UINT id);
//	#ifdef FRODO_SC
//	public void EmulateCyclesWith1541();
//	public void EmulateCyclesWithout1541();
//	#endif
//
//		DWORD ref_time;				// when frame count was reset
//	public int skipped_frames;			// number of skipped frames
//	public int timer_every;			// frequency of timer in frames
//		HANDLE timer_semaphore;		// Timer semaphore for synch
//		MMRESULT timer_id;			// Timer identifier
//	public int frame;					// current frame number
//		uint8 joy_state;			// Current state of joystick
//	public boolean state_change;
//	#endif

//	#ifdef __riscos__
//	public:
//	public void RequestSnapshot();
//	public boolean LoadOldSnapshot(File f);
//	public void LoadSystemConfig(const char *filename);	// loads timing vals and keyboard joys
//	public void SaveSystemConfig(const char *filename);	// saves timing vals and keyboard joys
//	public void ReadTimings(int *poll_after, int *speed_after, int *sound_after);
//	public void WriteTimings(int poll_after, int speed_after, int sound_after);
//
//		WIMP TheWIMP;
//	public int PollAfter;		// centiseconds before polling
//	public int SpeedAfter;		// centiseconds before updating speedometer
//	public int PollSoundAfter;	// *rasterlines* after which DigitalRenderer is polled
//	public int HostVolume;		// sound volume of host machine
//
//	private:
//	public boolean make_a_snapshot;
//
//		uint8 joykey2;			// two keyboard joysticks possible here
//
//		uint8 joystate[2];		// Joystick state
//	public boolean Poll;			// TRUE if polling should take place
//	public int LastPoll, LastFrame, LastSpeed;	// time of last poll / last frame / speedom (cs)
//	public int FramesSince;
//	public int laststate;			// last keyboard state (-> scroll lock)
//	public int lastptr;			// last mouse pointer shape
//	public boolean SingleTasking;
//	#endif

	
}
