package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.BPtr;

class MOS6581State {
	public static final int STATE_SIZE = 32;
	
	public byte freq_lo_1;
	public byte freq_hi_1;
	public byte pw_lo_1;
	public byte pw_hi_1;
	public byte ctrl_1;
	public byte AD_1;
	public byte SR_1;

	public byte freq_lo_2;
	public byte freq_hi_2;
	public byte pw_lo_2;
	public byte pw_hi_2;
	public byte ctrl_2;
	public byte AD_2;
	public byte SR_2;

	public byte freq_lo_3;
	public byte freq_hi_3;
	public byte pw_lo_3;
	public byte pw_hi_3;
	public byte ctrl_3;
	public byte AD_3;
	public byte SR_3;

	public byte fc_lo;
	public byte fc_hi;
	public byte res_filt;
	public byte mode_vol;

	public byte pot_x;
	public byte pot_y;
	public byte osc_3;
	public byte env_3;
	
	public MOS6581State(byte[] buf) {
		BPtr bp = new BPtr(buf);
		
		freq_lo_1 = bp.getInc(); freq_hi_1 = bp.getInc(); pw_lo_1 = bp.getInc(); pw_hi_1 = bp.getInc(); ctrl_1 = bp.getInc(); AD_1 = bp.getInc(); SR_1 = bp.getInc();
		freq_lo_2 = bp.getInc(); freq_hi_2 = bp.getInc(); pw_lo_2 = bp.getInc(); pw_hi_2 = bp.getInc(); ctrl_2 = bp.getInc(); AD_2 = bp.getInc(); SR_2 = bp.getInc();
		freq_lo_3 = bp.getInc(); freq_hi_3 = bp.getInc(); pw_lo_3 = bp.getInc(); pw_hi_3 = bp.getInc(); ctrl_3 = bp.getInc(); AD_3 = bp.getInc(); SR_3 = bp.getInc();
		
		fc_lo = bp.getInc(); fc_hi = bp.getInc(); res_filt = bp.getInc(); mode_vol = bp.getInc();
		pot_x = bp.getInc(); pot_y = bp.getInc(); osc_3 = bp.getInc(); env_3 = bp.getInc();
	}

	public MOS6581State() {
	}

	public byte[] toBytes() {
		byte[] buf = new byte[STATE_SIZE];
		BPtr bp = new BPtr(buf);
		
		bp.setInc(freq_lo_1); bp.setInc(freq_hi_1); bp.setInc(pw_lo_1); bp.setInc(pw_hi_1); bp.setInc(ctrl_1); bp.setInc(AD_1); bp.setInc(SR_1);
		bp.setInc(freq_lo_2); bp.setInc(freq_hi_2); bp.setInc(pw_lo_2); bp.setInc(pw_hi_2); bp.setInc(ctrl_2); bp.setInc(AD_2); bp.setInc(SR_2);
		bp.setInc(freq_lo_3); bp.setInc(freq_hi_3); bp.setInc(pw_lo_3); bp.setInc(pw_hi_3); bp.setInc(ctrl_3); bp.setInc(AD_3); bp.setInc(SR_3);
		bp.setInc(fc_lo); bp.setInc(fc_hi); bp.setInc(res_filt); bp.setInc(mode_vol);
		bp.setInc(pot_x); bp.setInc(pot_y); bp.setInc(osc_3); bp.setInc(env_3);
		
		return buf;
	}
}
