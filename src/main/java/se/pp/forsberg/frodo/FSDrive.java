package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.AccessMode.*;
import static se.pp.forsberg.frodo.enums.ErrorCode1541.*;
import static se.pp.forsberg.frodo.enums.FileType.*;
import static se.pp.forsberg.libc.LibC.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.libc.BPtr;
import se.pp.forsberg.libc.BoolC;
import se.pp.forsberg.libc.ByteC;
import se.pp.forsberg.libc.StdioFile;

class FSDrive extends Drive {
	public FSDrive(IEC iec, File path) {
		super(iec);
		
		orig_dir_path = path;
		dir_path = null;

		if (change_dir(orig_dir_path)) {
			for (int i=0; i<16; i++)
				file[i] = null;

			Reset();

			Ready = true;
		}
	}
//	FSDrive::~FSDrive()
//	{
//		if (Ready) {
//			close_all_channels();
//			Ready = false;
//		}
//	}
	@Override
	public byte Open(int channel, byte[] filename) {
		set_error(ERR_OK);

		// Channel 15: Execute file name as command
		if (channel == 15) {
			execute_command(filename);
			return ST_OK;
		}

		// Close previous file if still open
		if (file[channel] != null) {
			fclose(file[channel]);
			file[channel] = null;
		}

		if (filename[0] == '$')
			return open_directory(channel, new BPtr(filename, 1));

		if (filename[0] == '#') {
			set_error(ERR_NOCHANNEL);
			return ST_OK;
		}

		return open_file(channel, filename);
	}
	@Override
	public byte Close(int channel) {
		if (channel == 15) {
			close_all_channels();
			return ST_OK;
		}

		if (file[channel] != null) {
			fclose(file[channel]);
			file[channel] = null;
		}

		return ST_OK;
	}
	@Override
	public byte Read(int channel, ByteC b) {
		int c;

		// Channel 15: Error channel
		if (channel == 15) {
			b.b = error_ptr.getInc();

			if (b.b != '\r')
				return ST_OK;
			else {	// End of message
				set_error(ERR_OK);
				return ST_EOF;
			}
		}

		if (file[channel] == null) return ST_READ_TIMEOUT;

		// Read one byte
		b.b = read_char[channel];
		c = fgetc(file[channel]);
		if (c < 0)
			return ST_EOF;
		else {
			read_char[channel] = (byte) c;
			return ST_OK;
		}
	}
	@Override
	public byte Write(int channel, byte b, boolean eoi) {
		// Channel 15: Collect chars and execute command on EOI
		if (channel == 15) {
			if (cmd_len >= 40)
				return ST_TIMEOUT;
			
			cmd_buffer[cmd_len++] = b;

			if (eoi) {
				cmd_buffer[cmd_len] = 0;
				cmd_len = 0;
				execute_command(cmd_buffer);
			}
			return ST_OK;
		}

		if (file[channel] == null) {
			set_error(ERR_FILENOTOPEN);
			return ST_TIMEOUT;
		}

		if (fputc(b, file[channel]) < 0) {
			set_error(ERR_WRITEERROR);
			return ST_TIMEOUT;
		}

		return ST_OK;
	}
	@Override
	public void Reset() {
		close_all_channels();
		cmd_len = 0;	
		set_error(ERR_STARTUP);
	}

	private boolean change_dir(File dirpath) {
//	#ifndef __riscos__
		if (dirpath.isDirectory()) {
			dir_path = dirpath;
			strncpy(dir_title, dir_path.getName(), 16);
			return true;
		} else
			return false;
//	#else
//		int Info[4];
//
//		if ((ReadCatalogueInfo(dirpath,Info) & 2) != 0)	// Directory or image file
//		{
//		  strcpy(dir_path, dirpath);
//		  strncpy(dir_title, dir_path, 16);
//		  return true;
//		}
//		else
//		{
//		  return false;
//		}
//	#endif
	}
	private byte open_file(int channel, byte[] filename) {
		byte[] plainname = new byte[NAMEBUF_LENGTH];
		ModeType mt = new ModeType();
		mt.filemode = FMODE_READ;
		mt.filetype = FTYPE_PRG;
		BoolC wildflag = new BoolC();
		String mode = "rb";

		convert_filename(new BPtr(filename), new BPtr(plainname), mt, wildflag);

		// Channel 0 is READ PRG, channel 1 is WRITE PRG
		if (channel == 0) {
			mt.filemode = FMODE_READ;
			mt.filetype = FTYPE_PRG;
		}
		if (channel == 1) {
			mt.filemode = FMODE_WRITE;
			mt.filetype = FTYPE_PRG;
		}

		// Wildcards are only allowed on reading
		if (wildflag.b) {
			if (mt.filemode != FMODE_READ) {
				set_error(ERR_SYNTAX33);
				return ST_OK;
			}
			find_first_file(plainname);
		}

		// Select fopen() mode according to file mode
		switch (mt.filemode) {
			case FMODE_READ:
				mode = "rb";
				break;
			case FMODE_WRITE:
				mode = "wb";
				break;
			case FMODE_APPEND:
				mode = "ab";
				break;
		}

		// Open file
//	#ifndef __riscos__
//		if (chdir(dir_path))
//			set_error(ERR_NOTREADY);
//		else if ((file[channel] = fopen(plainname, mode)) != NULL) {
		file[channel] = fopen(new File(dir_path, toString(plainname)), mode);
		if (file[channel] != null) {
			if (mt.filemode == FMODE_READ)	// Read and buffer first byte
				read_char[channel] = (byte) fgetc(file[channel]);
		} else
			set_error(ERR_FILENOTFOUND);
//		chdir(AppDirPath);
//	#else
//		{
//		  char fullname[NAMEBUF_LENGTH];
//
//	  	  // On RISC OS make a full filename
//		  sprintf(fullname,"%s.%s",dir_path,plainname);
//		  if ((file[channel] = fopen(fullname, mode)) != NULL)
//		  {
//		    if (filemode == FMODE_READ)
//		    {
//		      read_char[channel] = fgetc(file[channel]);
//		    }
//		  }
//		  else
//		  {
//		    set_error(ERR_FILENOTFOUND);
//		  }
//		}
//	#endif

		return ST_OK;
	}
	private byte open_directory(int channel, BPtr filename) {
		filename = new BPtr(filename);
		byte[] buf = toBytes("\001\004\001\001\0\0\022\042                \042 00 2A");
		byte[] str = new byte[NAMEBUF_LENGTH];
		byte[] pattern = new byte[NAMEBUF_LENGTH];
		BPtr p, q;
		int i;
		ModeType mt = new ModeType();
		BoolC wildflag = new BoolC();

//	#ifndef __riscos__
//		DIR *dir;
//		struct dirent *de;
//		struct stat statbuf;

		// Special treatment for "$0"
		if (filename.get() == '0' && filename.get(1) == 0)
			filename.inc();

		// Convert filename ('$' already stripped), filemode/type are ignored
		convert_filename(filename, new BPtr(pattern), mt, wildflag);

		// Open directory for reading and skip '.' and '..'
//		if ((dir = opendir(dir_path)) == null) {
//			set_error(ERR_NOTREADY);
//			return ST_OK;
//		}
//		de = readdir(dir);
//		while (de && (0 == strcmp(".", de->d_name) || 0 == strcmp("..", de->d_name))) 
//			de = readdir(dir);
//
//		// Create temporary file
//		if ((file[channel] = tmpfile()) == NULL) {
//			closedir(dir);
//			return ST_OK;
//		}

		// Create directory title
		p = new BPtr(buf, 8);
		for (i=0; i<16 && dir_title[i] != 0; i++)
			p.setInc(conv_to_64(dir_title[i], false));
		fwrite(buf, 1, 32, file[channel]);

		// Create and write one line for every directory entry
		for (File f: dir_path.listFiles()) {
			String name = f.getName();
			if (name.equals(".") || name.equals("..")) {
				continue;
			}
			// Include only files matching the pattern
			if (match(new BPtr(pattern), new BPtr(toBytes(name)))) {

				try {
					i = (int) ((Files.size(f.toPath()) + 254) / 254);
				} catch (IOException x) {
					continue;
				}
				
				// Get file statistics
//				chdir(dir_path);
//				stat(de->d_name, &statbuf);
//				chdir(AppDirPath);

				// Clear line with spaces and terminate with null byte
				memset(buf, ' ', 31);
				buf[31] = 0;

				p = new BPtr(buf);
				p.setInc((byte) 0x01);	// Dummy line link
				p.setInc((byte) 0x01);

				// C8alculate size in blocks (254 bytes each)
//				i = (statbuf.st_size + 254) / 254;
				p.setInc((byte) (i & 0xff));
				p.setInc((byte) ((i >> 8) & 0xff));

				p.inc();
				if (i < 10) p.inc();	// Less than 10: add one space
				if (i < 100) p.inc();	// Less than 100: add another space

				// Convert and insert file name
				strcpy(str, name);
				p.setInc('\"');
				q = new BPtr(p);
				for (i=0; i<16 && str[i] != 0; i++)
					q.setInc(conv_to_64(str[i], true));
				q.setInc('\"');
				p.inc(18);

				// File type
				if (f.isDirectory()) {
					p.setInc("DIR");
				} else {
					p.setInc("PRG");
				}

				// Write line
				fwrite(buf, 1, 32, file[channel]);
			}
		}
//	#else
//		dir_full_info di;
//		dir_env de;
//
//		// Much of this is very similar to the original
//		if ((filename[0] == '0') && (filename[1] == 0)) {filename++;}
//		// Concatenate dir_path and pattern in buffer pattern ==> read subdirs!
//		strcpy(pattern,dir_path);
//		convert_filename(filename, pattern + strlen(pattern), &filemode, &filetype, &wildflag);
//
//		// We don't use tmpfile() -- problems involved!
//		DeleteFile(RO_TEMPFILE);	// first delete it, if it exists
//		if ((file[channel] = fopen(RO_TEMPFILE,"wb+")) == NULL)
//		{
//		  return(ST_OK);
//		}
//		de.offset = 0; de.buffsize = NAMEBUF_LENGTH; de.match = filename;
//
//		// Create directory title - copied from above
//		p = &buf[8];
//		for (i=0; i<16 && dir_title[i]; i++)
//			*p++ = conv_to_64(dir_title[i], false);
//		fwrite(buf, 1, 32, file[channel]);
//
//		do
//		{
//		  de.readno = 1;
//		  if (ReadDirNameInfo(pattern,&di,&de) != NULL) {de.offset = -1;}
//		  else if (de.offset != -1)	// don't have to check for match here
//		  {
//		    memset(buf,' ',31); buf[31] = 0;	// most of this: see above
//		    p = buf; *p++ = 0x01; *p++ = 0x01;
//		    i = (di.length + 254) / 254; *p++ = i & 0xff; *p++ = (i>>8) & 0xff;
//		    p++;
//		    if (i < 10)  {*p++ = ' ';}
//		    if (i < 100) {*p++ = ' ';}
//		    strcpy(str, di.name);
//		    *p++ = '\"'; q = p;
//		    for (i=0; (i<16 && str[i]); i++)
//		    {
//		      *q++ = conv_to_64(str[i], true);
//		    }
//		    *q++ = '\"'; p += 18;
//		    if ((di.otype & 2) == 0)
//		    {
//		      *p++ = 'P'; *p++ = 'R'; *p++ = 'G';
//		    }
//		    else
//		    {
//		      *p++ = 'D'; *p++ = 'I'; *p++ = 'R';
//		    }
//		    fwrite(buf, 1, 32, file[channel]);
//		  }
//		}
//		while (de.offset != -1);
//	#endif

		// Final line
		fwrite("\001\001\0\0BLOCKS FREE.             \0\0", 1, 32, file[channel]);

		// Rewind file for reading and read first byte
		rewind(file[channel]);
		read_char[channel] = (byte) fgetc(file[channel]);

//	#ifndef __riscos
		// Close directory
//		closedir(dir);
//	#endif

		return ST_OK;
	}
	private void convert_filename(BPtr srcname, BPtr destname, ModeType mt, BoolC wildflag) {
		BPtr p, q;
		int i;

		// Search for ':', p points to first character after ':'
		if ((p = strchr(srcname, ':')) != null)
			p.inc();
		else
			p = new BPtr(srcname);

		// Convert char set of the remaining string -> destname
		q = new BPtr(destname);
		for (i=0; i<NAMEBUF_LENGTH && (q.setInc(conv_from_64(p.getInc(), true))) != 0; i++) ;

		// Look for mode parameters seperated by ','
		p = destname;
		while ((p = strchr(p, ',')) != null) {

			// Cut string after the first ','
			p.setInc((byte) 0);

			switch ((char) (p.get() & 0xff)) {
				case 'p':
					mt.filetype = FTYPE_PRG;
					break;
				case 's':
					mt.filetype = FTYPE_SEQ;
					break;
				case 'r':
					mt.filemode = FMODE_READ;
					break;
				case 'w':
					mt.filemode = FMODE_WRITE;
					break;
				case 'a':
					mt.filemode = FMODE_APPEND;
					break;
			}
		}

		// Search for wildcards
		wildflag.b = (strchr(destname, '?') != null) || (strchr(destname, '*') != null);
	}
	/*
	 *  Find first file matching wildcard pattern and get its real name
	 */

	// Return true if name 'n' matches pattern 'p'
	private static boolean match(BPtr p, BPtr n) {
		p = new BPtr(p);
		n = new BPtr(n);
		if (p.get() == 0)		// Null pattern matches everything
			return true;

		do {
			if (p.get() == '*')	// Wildcard '*' matches all following characters
				return true;
			if ((p.get() != n.get()) && (p.get() != '?'))	// Wildcard '?' matches single character
				return false;
			p.inc(); n.inc();
		} while (p.get() != 0);

		return n.get() != 0;
	}
	private void find_first_file(byte[] name) {
//	#ifndef __riscos__
//		DIR *dir;
//		struct dirent *de;

		// Open directory for reading and skip '.' and '..'
//		if ((dir = opendir(dir_path)) == NULL)
//			return;
//		de = readdir(dir);
//		while (de && (0 == strcmp(".", de->d_name) || 0 == strcmp("..", de->d_name))) 
//			de = readdir(dir);
//
//		while (de) {
		for (File f: dir_path.listFiles()) {
			String fname = f.getName();
			if (fname.equals(".") || fname.equals("..")) {
				continue;
			}
			// Match found? Then copy real file name
			if (match(new BPtr(name), new BPtr(toBytes(fname)))) {
				strncpy(name, fname, NAMEBUF_LENGTH);
				return;
			}
		}
//	#else
//		dir_env de;
//		char Buffer[NAMEBUF_LENGTH];
//
//		de.offset = 0; de.buffsize = NAMEBUF_LENGTH; de.match = name;
//		do
//		{
//		  de.readno = 1;
//		  if (ReadDirName(dir_path,Buffer,&de) != NULL) {de.offset = -1;}
//		  else if (de.offset != -1)
//		  {
//		    if (match(name,Buffer))
//		    {
//		      strncpy(name, Buffer, NAMEBUF_LENGTH);
//		      return;
//		    }
//		  }
//		}
//		while (de.offset != -1);
//	#endif
	}
	private void close_all_channels() {
		for (int i=0; i<15; i++)
			Close(i);

		cmd_len = 0;
	}
	private void execute_command(byte[] command) {
	switch (command[0]) {
		case 'I':
			close_all_channels();
			set_error(ERR_OK);
			break;

		case 'U':
			if ((command[1] & 0x0f) == 0x0a) {
				Reset();
			} else
				set_error(ERR_SYNTAX30);
			break;

		case 'G':
			if (command[1] != ':')
				set_error(ERR_SYNTAX30);
			else
				chdir_cmd(new BPtr(command, 2));
			break;

		default:
			set_error(ERR_SYNTAX30);
	}
}
	private void chdir_cmd(BPtr dirpath) {
		dirpath = new BPtr(dirpath);
		byte[] str = new byte[NAMEBUF_LENGTH];
		BPtr p = new BPtr(str);

		close_all_channels();

		// G:. resets the directory path to its original setting
		if (dirpath.get() == '.' && dirpath.get(1) == 0) {
			change_dir(orig_dir_path);
		} else {

			// Convert directory name
			for (int i=0; i<NAMEBUF_LENGTH && (p.setInc(conv_from_64(dirpath.getInc(), false)) != 0); i++) ;

			if (!change_dir(new File(dir_path, toString(str))))
				set_error(ERR_NOTREADY);
		}
	}
	private byte conv_from_64(byte c, boolean map_slash) {
		if ((c >= 'A') && (c <= 'Z') || (c >= 'a') && (c <= 'z'))
			return (byte) (c ^ 0x20);
		if ((c >= 0xc1) && (c <= 0xda))
			return (byte) (c ^ 0x80);
		if ((c == '/') && map_slash && Prefs.ThePrefs.MapSlash)
//	#ifdef __riscos__
//			return '.';	// directory separator is '.' in RO
//		if (c == '.') {return('_');}	// convert dot to underscore
//	#else
			return '\\';
//	#endif
		return c;
	}
	private byte conv_to_64(byte c, boolean map_slash) {
		if ((c >= 'A') && (c <= 'Z') || (c >= 'a') && (c <= 'z'))
			return (byte) (c ^ 0x20);
//	#ifdef __riscos__
//		if ((c == '.') && map_slash && ThePrefs.MapSlash)
//	#else
		if ((c == '\\') && map_slash && Prefs.ThePrefs.MapSlash)
//	#endif
			return '/';
//	#ifdef __riscos__
//		if (c == '_') {return('.');}	// convert underscore to dot
//	#endif
		return c;
	}

	private File dir_path;		// Path to directory
	private File orig_dir_path; // Original directory path
	private byte[] dir_title = new byte[16];		// Directory title
	private StdioFile[] file = new StdioFile[16];			// File pointers for each of the 16 channels

	private byte[] cmd_buffer = new byte[44];	// Buffer for incoming command strings
	private int cmd_len;			// Length of received command

	private byte[] read_char = new byte[16];	// Buffers for one-byte read-ahead
}
