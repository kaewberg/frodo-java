package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.CpuBase.Mode.ABS;
import static se.pp.forsberg.frodo.CpuBase.Mode.ABSX;
import static se.pp.forsberg.frodo.CpuBase.Mode.ABSY;
import static se.pp.forsberg.frodo.CpuBase.Mode.ACC;
import static se.pp.forsberg.frodo.CpuBase.Mode.IMM;
import static se.pp.forsberg.frodo.CpuBase.Mode.IMP;
import static se.pp.forsberg.frodo.CpuBase.Mode.INDY;
import static se.pp.forsberg.frodo.CpuBase.Mode.REL;
import static se.pp.forsberg.frodo.CpuBase.Mode.XIND;
import static se.pp.forsberg.frodo.CpuBase.Mode.ZP;
import static se.pp.forsberg.frodo.CpuBase.Mode.ZPX;

import se.pp.forsberg.frodo.CpuBase.Mode;

enum Instruction {
	BRK_IMP("BRK", 0x00, IMP),  ORA_XIND("ORA", 0x01, XIND), ILL_02 ("ILL", 0x02, null), ILL_03("ILL", 0x03, null), ILL_04 ("ILL", 0x04, null), ORA_ZP ("ORA", 0x05, ZP),  ASL_ZP ("ASL", 0x06, ZP),  ILL_07("ILL", 0x07, null), PHP_IMP("PHP", 0x08, IMP), ORA_IMM ("ORA", 0x09, IMM),  ASL_A  ("ASL", 0x0a, ACC),  ILL_0B("ILL", 0x0b, null), ILL_0C  ("ILL", 0x0c, null), ORA_ABS ("ORA", 0x0d, ABS),  ASL_ABS ("ASL", 0x0e, ABS),  ILL0F("ILL", 0x0f, null),
	BPL_REL("BPL", 0x10, REL),  ORA_INDY("ORA", 0x11, INDY), ILL_12 ("ILL", 0x12, null), ILL_13("ILL", 0x13, null), ILL_14 ("ILL", 0x14, null), ORA_ZPX("ORA", 0x15, ZPX), ASL_ZPX("ASL", 0x16, ZPX), ILL_17("ILL", 0x17, null), CLC_IMP("CLC", 0x18, IMP), ORA_ABSY("ORA", 0x19, ABSY), ILL_1A ("ILL", 0x1a, null), ILL_1B("ILL", 0x1b, null), ILL_1C  ("ILL", 0x1c, null), ORA_ABSX("ORA", 0x1d, ABSX), ASL_ABSX("ASL", 0x1e, ABSX), ILL1F("ILL", 0x1f, null),
	JSR_ABS("JSR", 0x20, ABS),  AND_XIND("AND", 0x21, XIND), ILL_22 ("ILL", 0x22, null), ILL_23("ILL", 0x23, null), BIT_ZP ("BIT", 0x24, ZP),   AND_ZP ("AND", 0x25, ZP),  ROL_ZP ("ROL", 0x26, ZP),  ILL_27("ILL", 0x27, null), PLP_IMP("PLP", 0x28, IMP), AND_IMM ("AND", 0x29, IMM),  ROL_A  ("ROL", 0x2a, ACC),  ILL_2B("ILL", 0x2b, null), BIT_ABS ("BIT", 0x2c, ABS),  AND_ABS ("AND", 0x2d, ABS),  ROL_ABS ("ROL", 0x2e, ABS),  ILL2F("ILL", 0x2f, null),
	BMI_REL("BMI", 0x30, REL),  AND_INDY("AND", 0x31, INDY), ILL_32 ("ILL", 0x32, null), ILL_33("ILL", 0x33, null), ILL_34 ("ILL", 0x34, null), AND_ZPX("AND", 0x35, ZPX), ROL_ZPX("ROL", 0x36, ZPX), ILL_37("ILL", 0x37, null), SEC_IMP("SEC", 0x38, IMP), AND_ABSY("AND", 0x39, ABSY), ILL_3A ("ILL", 0x3a, null), ILL_3B("ILL", 0x3b, null), ILL_3C  ("ILL", 0x3c, null), AND_ABSX("AND", 0x3d, ABSX), ROL_ABSX("ROL", 0x3e, ABSX), ILL3F("ILL", 0x3f, null),
	RTI_IMP("RTI", 0x40, IMP),  EOR_XIND("EOR", 0x41, XIND), ILL_42 ("ILL", 0x42, null), ILL_43("ILL", 0x43, null), ILL_44 ("ILL", 0x44, null), EOR_ZP ("EOR", 0x45, ZP),  LSR_ZP ("LSR", 0x46, ZP),  ILL_47("ILL", 0x47, null), PHA_IMP("PHA", 0x48, IMP), EOR_IMM ("EOR", 0x49, IMM),  LSR_A  ("LSR", 0x4a, ACC),  ILL_4B("ILL", 0x4b, null), JMP_ABS ("JMP", 0x4c, ABS),  EOR_ABS ("EOR", 0x4d, ABS),  LSR_ABS ("LSR", 0x4e, ABS),  ILL4F("ILL", 0x4f, null),
	BVC_REL("BVC", 0x50, REL),  EOR_INDY("EOR", 0x51, INDY), ILL_52 ("ILL", 0x52, null), ILL_53("ILL", 0x53, null), ILL_54 ("ILL", 0x54, null), EOR_ZPX("EOR", 0x55, ZPX), LSR_ZPX("LSR", 0x56, ZPX), ILL_57("ILL", 0x57, null), CLI_IMP("CLI", 0x58, IMP), EOR_ABSY("EOR", 0x59, ABSY), ILL_5A ("ILL", 0x5a, null), ILL_5B("ILL", 0x5b, null), ILL_5C  ("ILL", 0x5c, null), EOR_ABSX("EOR", 0x5d, ABSX), LSR_ABSX("LSR", 0x5e, ABSX), ILL5F("ILL", 0x5f, null),
	RTS_IMP("RTS", 0x60, IMP),  ADC_XIND("ADC", 0x61, XIND), ILL_62 ("ILL", 0x62, null), ILL_63("ILL", 0x63, null), ILL_64 ("ILL", 0x64, null), ADC_ZP ("ADC", 0x65, ZP),  ROR_ZP ("ROR", 0x66, ZP),  ILL_67("ILL", 0x67, null), PLA_IMP("PLA", 0x68, IMP), ADC_IMM ("ADC", 0x69, IMM),  ROR_A  ("ROR", 0x6a, ACC),  ILL_6B("ILL", 0x6b, null), JMP_IND ("JMP", 0x6c, ABS),  ADC_ABS ("ADC", 0x6d, ABS),  ROR_ABS ("ROR", 0x6e, ABS),  ILL6F("ILL", 0x6f, null),
	BVS_REL("BVS", 0x70, REL),  ADC_INDY("ADC", 0x71, INDY), ILL_72 ("ILL", 0x72, null), ILL_73("ILL", 0x73, null), ILL_74("ILL",  0x74, null), ADC_ZPX("ADC", 0x75, ZPX), ROR_ZPX("ROR", 0x76, ZPX), ILL_77("ILL", 0x77, null), SEI_IMP("SEI", 0x78, IMP), ADC_ABSY("ADC", 0x79, ABSY), ILL_7A ("ILL", 0x7a, null), ILL_7B("ILL", 0x7b, null), ILL_7C  ("ILL", 0x7c, null), ADC_ABSX("ADC", 0x7d, ABSX), ROR_ABSX("ROR", 0x7e, ABSX), ILL7F("ILL", 0x7f, null),
	ILL_80 ("ILL", 0x80, null), STA_XIND("STA", 0x81, XIND), ILL_82 ("ILL", 0x82, null), ILL_83("ILL", 0x83, null), STY_ZP ("STY", 0x84, ZP),   STA_ZP ("STA", 0x85, ZP),  STX_ZP ("STX", 0x86, ZP),  ILL_87("ILL", 0x87, null), DEY_IMP("DEY", 0x88, IMP), ILL_89  ("ILL", 0x89, null), TXA_IMP("TXA", 0x8a, IMP),  ILL_8B("ILL", 0x8b, null), STY_ABS ("STY", 0x8c, ABS),  STA_ABS ("STA", 0x8d, ABS),  STX_ABS ("STX", 0x8e, ABS),  ILL8F("ILL", 0x8f, null),
	BCC_REL("BCC", 0x90, REL),  STA_INDY("STA", 0x91, INDY), ILL_92 ("ILL", 0x92, null), ILL_93("ILL", 0x93, null), STY_ZPX("STY", 0x94, ZPX),  STA_ZPX("STA", 0x95, ZPX), STX_ZPX("STX", 0x96, ZPX), ILL_97("ILL", 0x97, null), TYA_IMP("TYA", 0x98, IMP), STA_ABSY("STA", 0x99, ABSY), TXS_IMP("TXS", 0x9a, IMP),  ILL_9B("ILL", 0x9b, null), ILL_9C  ("ILL", 0x9c, null), STA_ABSX("STA", 0x9d, ABSX), ILL_8F  ("ILL", 0x9e, null), ILL9F("ILL", 0x9f, null),
	LDY_IMM("LDY", 0xa0, IMM),  LDA_XIND("LDA", 0xa1, XIND), LDX_IMM("LDX", 0xa2, IMM),  ILL_A3("ILL", 0xa3, null), LDY_ZP ("LDY", 0xa4, ZP),   LDA_ZP ("LDA", 0xa5, ZP),  LDX_ZP ("LDX", 0xa6, ZP),  ILL_A7("ILL", 0xa7, null), TAY_IMP("TAY", 0xa8, IMP), LDA_IMM ("LDA", 0xa9, IMM),  TAX_IMP("TAX", 0xaa, IMP),  ILL_AB("ILL", 0xab, null), LDY_ABS ("LDY", 0xac, ABS),  LDA_ABS ("LDA", 0xad, ABS),  LDX_ABS ("LDX", 0xae, ABS),  ILLAF("ILL", 0xaf, null),
	BCS_REL("BCS", 0xb0, REL),  LDA_INDY("LDA", 0xb1, INDY), ILL_B2 ("ILL", 0xb2, null), ILL_B3("ILL", 0xb3, null), LDY_ZPX("LDY", 0xb4, ZPX),  LDA_ZPX("LDA", 0xb5, ZPX), LDX_ZPX("LDX", 0xb6, ZPX), ILL_B7("ILL", 0xb7, null), CLV_IMP("CLV", 0xb8, IMP), LDA_ABSY("LDA", 0xb9, ABSY), TSX_IMP("TSX", 0xba, IMP),  ILL_BB("ILL", 0xbb, null), LDY_ABSX("LDY", 0xbc, ABS),  LDA_ABSX("LDA", 0xbd, ABSX), LDX_ABSY("LDX", 0xbe, ABSX), ILLBF("ILL", 0xbf, null),
	CPY_IMM("CPY", 0xc0, IMM),  CMP_XIND("CMP", 0xc1, XIND), ILL_C2 ("ILL", 0xc2, null), ILL_C3("ILL", 0xc3, null), CPY_ZP ("CPY", 0xc4, ZP),   CMP_ZP ("CMP", 0xc5, ZP),  DEC_ZP ("DEC", 0xc6, ZP),  ILL_C7("ILL", 0xc7, null), INY_IMP("INY", 0xc8, IMP), CMP_IMM ("CMP", 0xc9, IMM),  DEX_IMP("DEX", 0xca, IMP),  ILL_CB("ILL", 0xcb, null), CPY_ABS ("CPY", 0xcc, ABS),  CMP_ABS ("CMP", 0xcd, ABS),  DEC_ABS ("DEC", 0xce, ABS),  ILLCF("ILL", 0xcf, null),
	BNE_REL("BNE", 0xd0, REL),  CMP_INDY("CMP", 0xd1, INDY), ILL_D2 ("ILL", 0xd2, null), ILL_D3("ILL", 0xd3, null), ILL_D4 ("ILL", 0xd4, null), CMP_ZPX("CMP", 0xd5, ZPX), DEC_ZPX("DEC", 0xd6, ZPX), ILL_D7("ILL", 0xd7, null), CLD_IMP("CLD", 0xd8, IMP), CMP_ABSY("CMP", 0xd9, ABSY), ILL_DA ("ILL", 0xda, null), ILL_DB("ILL", 0xdb, null), ILL_CC  ("ILL", 0xdc, null), CMP_ABSX("CMP", 0xdd, ABSX), DEC_ABSX("DEC", 0xde, ABSX), ILLDF("ILL", 0xdf, null),
	CPX_IMM("CPX", 0xe0, IMM),  SBC_XIND("SBC", 0xe1, XIND), ILL_E2 ("ILL", 0xe2, null), ILL_E3("ILL", 0xe3, null), CPX_ZP ("CPX", 0xe4, ZP),   SBC_ZP ("SBC", 0xe5, ZP),  INC_ZP ("INC", 0xe6, ZP),  ILL_E7("ILL", 0xe7, null), INX_IMP("INX", 0xe8, IMP), SBC_IMM ("SBC", 0xe9, IMM),  NOP_IMP("NOP", 0xea, IMP),  ILL_EB("ILL", 0xeb, null), CPX_ABS ("CPX", 0xec, ABS),  SBC_ABS ("SBC", 0xed, ABS),  INC_ABS ("INC", 0xee, ABS),  ILLEF("ILL", 0xef, null),
	BEQ_REL("BEQ", 0xf0, REL),  SBC_INDY("SBC", 0xf1, INDY), EXT_IMM("EXT", 0xf2, IMM), ILL_F3("ILL", 0xf3, null), ILL_F4 ("ILL", 0xf4, null), SBC_ZPX("SBC", 0xf5, ZPX), INC_ZPX("INC", 0xf6, ZPX), ILL_F7("ILL", 0xf7, null), SED_IMP("SED", 0xf8, IMP), SBC_ABSY("SBC", 0xf9, ABSY), ILL_FA ("ILL", 0xfa, null), ILL_FB("ILL", 0xfb, null), ILL_FC  ("ILL", 0xfc, null), SBC_ABSX("SBC", 0xfd, ABSX), INC_ABSX("INC", 0xfe, ABSX), ILLFF("ILL", 0xff, null);

	String opcode;
	byte op;
	Mode mode;
	private Instruction(String opcode, int op, Mode mode) {
		this.opcode = opcode;
		this.op = (byte) op;
		this.mode = mode;
	}
	public String toString(int adr, byte p1, byte p2) {
		if (mode == null) {
			return "<ill op>";
		}
		int ea = adr + mode.len + p1;
		String fmt = mode.format;
		fmt = fmt.replaceAll("\\{opc\\}", opcode);
		fmt = fmt.replaceAll("\\{p1\\}", String.format("%02x", p1));
		fmt = fmt.replaceAll("\\{p2\\}", String.format("%02x", p2));
		fmt = fmt.replaceAll("\\{ea\\}", String.format("%04x", ea));
		
		return fmt;
	}
}