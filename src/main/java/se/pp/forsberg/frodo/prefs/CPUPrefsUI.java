package se.pp.forsberg.frodo.prefs;

import java.util.List;
import java.util.stream.Collectors;

import javafx.collections.ObservableList;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import se.pp.forsberg.frodo.prefs.CPUPrefs.CallPrefs;

public class CPUPrefsUI extends Dialog<CPUPrefs> {

	CPUPrefs prefs;
	
	private CPUPrefsUI(CPUPrefs prefs) {
		this.prefs = new CPUPrefs(prefs);
	}

	public static CPUPrefs edit(CPUPrefs prefs) {
		return ui(prefs)
				.showAndWait()
				.orElse(null);
	}
	private static CPUPrefsUI ui(CPUPrefs prefs) {
		return new CPUPrefsUI(prefs).build();
	}

	private CPUPrefsUI build() {
		VBox vbox = new VBox();
		HBox hbox = new HBox();
		CheckBox cb;
		
		cb = new CheckBox("Show CPU debug output");
		cb.setSelected(prefs.showDebug);
		cb.selectedProperty().addListener((observable, oldValue, newValue) -> prefs.showDebug = newValue);
		hbox.getChildren().add(cb);

		Region spacer = new Region();
		spacer.setPrefWidth(40);
		hbox.getChildren().add(spacer);
		
		cb = new CheckBox("Show CPU instruction trace");
		cb.setSelected(prefs.showTrace);
		cb.selectedProperty().addListener((observable, oldValue, newValue) -> prefs.showTrace = newValue);
		hbox.getChildren().add(cb);
		
		vbox.getChildren().add(hbox);
		
		GridPane headers = new GridPane();
		headers.add(new Label("Address"), 0, 0);
		headers.add(new Label("Print"), 1, 0);
		headers.add(new Label("Break"), 2, 0);
		headers.add(new Label("Description"), 3, 0);
		
		GridPane grid = new GridPane();
		int y = 0;
		List<CallPrefs> cps = prefs.prefs.values().stream()
				.sorted((p1, p2) -> p1.call.getAddress() - p2.call.getAddress())
				.collect(Collectors.toList());
		for (CallPrefs cp: cps) {
			int x = 0;
			grid.add(new Label(String.format("$%04x", cp.call.getAddress())), x++, y);
		
			cb = new CheckBox();
			cb.setSelected(cp.print);
			cb.selectedProperty().addListener((observable, oldValue, newValue) -> cp.print = newValue);
			grid.add(cb, x++, y);
			
			cb = new CheckBox();
			cb.setSelected(cp.brk);
			cb.selectedProperty().addListener((observable, oldValue, newValue) -> cp.brk = newValue);
			grid.add(cb, x++, y);
			
			grid.add(new Label(cp.call.getDescription()), x++, y);
			
			y++;
		}
		ObservableList<ColumnConstraints> hcc = headers.getColumnConstraints();
		ObservableList<ColumnConstraints> gcc = grid.getColumnConstraints();
		ColumnConstraints cc;
		cc = new ColumnConstraints(75);
		hcc.add(cc);
		gcc.add(cc);
		cc = new ColumnConstraints(50);
		hcc.add(cc);
		gcc.add(cc);
		hcc.add(cc);
		gcc.add(cc);
		cc = new ColumnConstraints();
		cc.setFillWidth(true);
		cc.setMinWidth(300);
		hcc.add(cc);
		gcc.add(cc);
		
		vbox.getChildren().add(headers);

		ScrollPane scroll = new ScrollPane(grid);
		scroll.setMaxHeight(400);
		vbox.getChildren().add(scroll);
		
		getDialogPane().getButtonTypes().add(ButtonType.OK);
		getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
//		getDialogPane().setMinSize(600,  400);
		setResizable(true);
		getDialogPane().setContent(vbox);
		setResultConverter(bt -> {
			if (bt == ButtonType.OK) {
				return prefs;
			}
			return null;
		});
		return this;
	}
}
