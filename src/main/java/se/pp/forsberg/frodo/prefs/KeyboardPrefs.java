package se.pp.forsberg.frodo.prefs;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.scene.input.KeyCode;
import se.pp.forsberg.frodo.prefs.KeyMapping.RC;

public class KeyboardPrefs {

	KeyMapping map;
	
	public KeyboardPrefs(KeyboardPrefs prefs) {
		this(prefs.map);
	}

	public KeyboardPrefs(KeyMapping map) {
		this.map = new KeyMapping(map);
	}

	public void save(Properties props) {
		for (KeyCode code: map.getMultiKeys()) {
			KeyCode[] expansion = map.getMultiKey(code);
			StringBuilder sb = new StringBuilder();
			for (KeyCode kc: expansion) {
				if (sb.length() > 0) {
					sb.append(' ');
				}
				sb.append(kc);
			}
			props.setProperty(String.format("Key.Multi.%s", code), sb.toString());
		}
		for (KeyCode code: map.getKeyCodes()) {
			RC rc = map.getKey(code);
			props.setProperty(String.format("Key.Code.%s", code), String.format("%d %d", rc.r, rc.c));
		}
		for (String s: map.getKeyStrings()) {
			RC rc = map.getStringKey(s);
			props.setProperty(String.format("Key.String.%s", s), String.format("%d %d", rc.r, rc.c));
		}
	}

	public void load(Properties props) {
		Pattern mp = Pattern.compile("Key\\.Multi\\.(.*");
		Pattern kp = Pattern.compile("Key\\.Code\\.(.*");
		Pattern sp = Pattern.compile("Key\\.String\\.(.*");
		
		Map<KeyCode, KeyCode[]> multiKeys = new HashMap<>();
		Map<KeyCode, RC> keys = new HashMap<>();
		Map<String, RC> strings = new HashMap<>();
		
		for (Object key: props.keySet()) {
			String s = key.toString();
			Matcher match;
			match = mp.matcher(s);
			if (match.matches()) {
				String[] ss = props.getProperty(s).split(" ");
				KeyCode[] expansion = new KeyCode[ss.length];
				for (int i = 0; i < ss.length; i++) {
					expansion[i] = KeyCode.valueOf(ss[i]);
				}
				multiKeys.put(KeyCode.valueOf(match.group(1)), expansion);
				continue;
			}
			match = kp.matcher(s);
			if (match.matches()) {
				keys.put(KeyCode.valueOf(match.group(1)), new RC(props.getProperty(s)));
				continue;
			}
			match = sp.matcher(s);
			if (match.matches()) {
				strings.put(match.group(1), new RC(props.getProperty(s)));
				continue;
			}
			throw new IllegalArgumentException("Bad prefs");
		}
		map = new KeyMapping(multiKeys, keys, strings);
	}
}
