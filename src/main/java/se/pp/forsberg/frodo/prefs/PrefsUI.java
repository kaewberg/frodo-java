package se.pp.forsberg.frodo.prefs;

import java.io.File;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import javafx.collections.FXCollections;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import se.pp.forsberg.frodo.enums.DisplayType;
import se.pp.forsberg.frodo.enums.DriveType;
import se.pp.forsberg.frodo.enums.REUSize;
import se.pp.forsberg.frodo.enums.SIDType;

public class PrefsUI extends Dialog<Prefs> {
	Prefs prefs;
	GridPane grid;
	int x, y;

	private PrefsUI(Prefs prefs) {
		this.prefs = new Prefs(prefs);
	}

	public static Prefs edit(Prefs prefs) {
		return ui(prefs)
				.showAndWait()
				.orElse(null);
	}
	private static PrefsUI ui(Prefs prefs) {
		return new PrefsUI(prefs).build();
	}

	private void wrap() {
		x = 0;
		y++;
	}
	private PrefsUI build() {
		grid = new GridPane();
		intPrefs("Normal cycles", () -> prefs.NormalCycles, i -> prefs.NormalCycles = i);
		intPrefs("Bad line cycles", () -> prefs.BadLineCycles, i -> prefs.BadLineCycles = i);
		intPrefs("CIA cycles", () -> prefs.CIACycles, i -> prefs.CIACycles = i);
		intPrefs("Floppy cycles", () -> prefs.FloppyCycles, i -> prefs.FloppyCycles = i);
//		SkipFrames = 2;
//		intPrefs("Latency min", () -> prefs.LatencyMin, i -> prefs.LatencyMin = i);
//		intPrefs("Latency max", () -> prefs.LatencyMax, i -> prefs.LatencyMax = i);
//		intPrefs("Latency avg", () -> prefs.LatencyAvg, i -> prefs.LatencyAvg = i);

//		ScalingNumerator = 2;
//		ScalingDenominator = 2;
//
		wrap();
		for (int i=0; i<4; i++) {
			int theI = i;
 			enumPrefs("Drive " + (i + 8), DriveType.class, () -> prefs.DriveType[theI],  t -> prefs.DriveType[theI] = t);
			stringPrefs("Dir", () -> prefs.DrivePath[theI], p -> prefs.DrivePath[theI] = new File(p));
			wrap();
		}

//		ViewPort = "Default";
//		DisplayMode = "Default";
		
		enumPrefs("SID type", SIDType.class, () -> prefs.SIDType, t -> prefs.SIDType = t);
		enumPrefs("REU size", REUSize.class, () -> prefs.REUSize, t -> prefs.REUSize = t);
//		enumPrefs("Display type", DisplayType.class, () -> prefs.DisplayType, t -> prefs.DisplayType = t);

		boolPrefs("Sprites on", () -> prefs.SpritesOn, b -> prefs.SpritesOn = b);
		boolPrefs("Sprite collisions", () -> prefs.SpriteCollisions, b -> prefs.SpriteCollisions = b);
		boolPrefs("Joystick 1", () -> prefs.Joystick1On, b -> prefs.Joystick1On = b);
		boolPrefs("Joystick 2", () -> prefs.Joystick2On, b -> prefs.Joystick2On = b);
		boolPrefs("Swap joysticks", () -> prefs.JoystickSwap, b -> prefs.JoystickSwap = b);
		boolPrefs("Limit speed", () -> prefs.LimitSpeed, b -> prefs.LimitSpeed = b);
		boolPrefs("Fast reset", () -> prefs.FastReset, b -> prefs.FastReset = b);
		boolPrefs("CIA IRQ hack", () -> prefs.CIAIRQHack, b -> prefs.CIAIRQHack= b);
		boolPrefs("Map slash", () -> prefs.MapSlash, b -> prefs.MapSlash = b);
		boolPrefs("Emulate 1541 cpu", () -> prefs.Emul1541Proc, b -> prefs.Emul1541Proc = b);
		boolPrefs("SID filters", () -> prefs.SIDFilters, b -> prefs.SIDFilters = b);
		
		boolPrefs("Save on exit", () -> prefs.saveOnExit, b -> prefs.saveOnExit = b);
		
//		boolPrefs("Double scan", () -> prefs.DoubleScan, b -> prefs.DoubleScan = b);
//		boolPrefs("Hide cursor", () -> prefs.HideCursor, b -> prefs.HideCursor = b);
//		boolPrefs("Direct sound", () -> prefs.DirectSound, b -> prefs.DirectSound = b);
//		boolPrefs("Exclusive sound", () -> prefs.ExclusiveSound, b -> prefs.ExclusiveSound = b);
//		boolPrefs("Auto pause", () -> prefs.AutoPause, b -> prefs.AutoPause = b);
//		boolPrefs("Prefs at startup", () -> prefs.PrefsAtStartup, b -> prefs.PrefsAtStartup = b);
//		boolPrefs("System memory", () -> prefs.SystemMemory, b -> prefs.SystemMemory = b);
//		boolPrefs("Always copy", () -> prefs.AlwaysCopy, b -> prefs.AlwaysCopy = b);
//		boolPrefs("System keys", () -> prefs.SystemKeys, b -> prefs.SystemKeys = b);
//		boolPrefs("Show LEDs", () -> prefs.ShowLEDs, b -> prefs.ShowLEDs = b);

//		scene = new Scene(grid);
		grid.autosize();
		
		getDialogPane().getButtonTypes().add(ButtonType.OK);
		getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
		getDialogPane().setMinSize(600,  400);
		grid.setMinSize(600, 380);
		setResizable(true);
		getDialogPane().setContent(grid);
		setResultConverter(bt -> {
			if (bt == ButtonType.OK) {
				return prefs;
			}
			return null;
		});
		return this;
	}

	private void next() {
		x += 2;
		if (x > 5) {
			x = 0;
			y++;
		}
	}
	private void intPrefs(String label, IntSupplier get, IntConsumer set) {
		grid.add(new Label(label), x, y);
		TextField text = new TextField(Integer.toString(get.getAsInt()));
		// force the field to be numeric only
		text.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				text.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});
		text.textProperty().addListener((observable, oldValue, newValue) -> set.accept(Integer.parseInt(newValue)));
		grid.add(text, x + 1, y);
		next();
	}
	private <T extends Enum<T>> void enumPrefs(String label, Class<T> clazz, Supplier<T> get, Consumer<T> set) {
		grid.add(new Label(label), x, y);
		ComboBox<T> cb = new ComboBox<>(FXCollections.observableArrayList(clazz.getEnumConstants()));
		cb.setValue(get.get()); 
		cb.valueProperty().addListener((observable, oldValue, newValue) -> set.accept(newValue));
		grid.add(cb, x + 1, y);
		next();
	}
	private <T> void stringPrefs(String label, Supplier<T> get, Consumer<String> set) {
		grid.add(new Label(label), x, y);
		TextField text = new TextField(get.get().toString());
		text.textProperty().addListener((observable, oldValue, newValue) -> set.accept(newValue));
		grid.add(text, x + 1, y);
		next();
	}
	private void boolPrefs(String label, BooleanSupplier get, Consumer<Boolean> set) {
		CheckBox cb = new CheckBox(label);
		cb.setSelected(get.getAsBoolean());
		cb.selectedProperty().addListener((observable, oldValue, newValue) -> set.accept(newValue));
		grid.add(cb, x, y, 2, 1);
		next();
	}
}
