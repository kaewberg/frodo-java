package se.pp.forsberg.frodo.prefs;

import static se.pp.forsberg.frodo.enums.DriveType.DRVTYPE_D64;
import static se.pp.forsberg.frodo.enums.DriveType.DRVTYPE_DIR;
import static se.pp.forsberg.frodo.enums.DriveType.DRVTYPE_T64;
import static se.pp.forsberg.frodo.enums.REUSize.REU_128K;
import static se.pp.forsberg.frodo.enums.REUSize.REU_256K;
import static se.pp.forsberg.frodo.enums.REUSize.REU_512K;
import static se.pp.forsberg.frodo.enums.REUSize.REU_NONE;
import static se.pp.forsberg.frodo.enums.SIDType.SIDTYPE_DIGITAL;
import static se.pp.forsberg.frodo.enums.SIDType.SIDTYPE_NONE;
import static se.pp.forsberg.frodo.enums.SIDType.SIDTYPE_SIDCARD;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.Properties;

import se.pp.forsberg.frodo.Frodo;
import se.pp.forsberg.frodo.enums.DriveType;
import se.pp.forsberg.frodo.enums.REUSize;
import se.pp.forsberg.frodo.enums.SIDType;

public class Prefs {

	static public Prefs ThePrefs = new Prefs(), ThePrefsOnDisk = new Prefs();

	public Prefs() {
		NormalCycles = 63;
		BadLineCycles = 23;
		CIACycles = 63;
		FloppyCycles = 64;
//		SkipFrames = 2;
//		LatencyMin = 80;
//		LatencyMax = 120;
//		LatencyAvg = 280;
//		ScalingNumerator = 2;
//		ScalingDenominator = 2;

		for (int i = 0; i < 4; i++)
			DriveType[i] = DRVTYPE_DIR;

		DrivePath[0] = new File("64prgs");

		ViewPort = "Default";
		DisplayMode = "Default";

		SIDType = SIDTYPE_NONE;
		REUSize = REU_NONE;
//		DisplayType = DISPTYPE_WINDOW;

		SpritesOn = true;
		SpriteCollisions = true;
		Joystick1On = false;
		Joystick2On = false;
		JoystickSwap = false;
		LimitSpeed = true;
		FastReset = false;
		CIAIRQHack = false;
		MapSlash = true;
		Emul1541Proc = false;
		SIDFilters = true;
		
		saveOnExit = true;
//		DoubleScan = true;
//		HideCursor = false;
//		DirectSound = true;
//		ExclusiveSound = false;
//		AutoPause = false;
//		PrefsAtStartup = false;
//		SystemMemory = false;
//		AlwaysCopy = false;
//		SystemKeys = true;
//		ShowLEDs = true;

	}

	public Prefs(Prefs p) {
		copy(p);
	}
	
	private void copy(Prefs p) {
		NormalCycles = p.NormalCycles;
		BadLineCycles = p.BadLineCycles;
		CIACycles = p.CIACycles;
		FloppyCycles = p.FloppyCycles;
//		SkipFrames = p.SkipFrames;
		for (int i = 0; i < DriveType.length; i++) {
			DriveType[i] = p.DriveType[i];
		}
		for (int i = 0; i < DrivePath.length; i++) {
			DrivePath[i] = p.DrivePath[i];
		}
		ViewPort = p.ViewPort;
		DisplayMode = p.DisplayMode;
		SIDType = p.SIDType;
		REUSize = p.REUSize;
//		DisplayType = p.DisplayType;
//		LatencyMin = p.LatencyMin;
//		LatencyMax = p.LatencyMax;
//		LatencyAvg = p.LatencyAvg;
//		ScalingNumerator = p.ScalingNumerator;
//		ScalingDenominator = p.ScalingDenominator; // Window scaling denominator (Win32)

		SpritesOn = p.SpritesOn;
		SpriteCollisions = p.SpriteCollisions;
		Joystick1On = p.Joystick1On;
		Joystick2On = p.Joystick2On;
		JoystickSwap = p.JoystickSwap;
		LimitSpeed = p.LimitSpeed;
		FastReset = p.FastReset;
		CIAIRQHack = p.CIAIRQHack;
		MapSlash = p.MapSlash;
		Emul1541Proc = p.Emul1541Proc;
		SIDFilters = p.SIDFilters;
		
		saveOnExit = p.saveOnExit;
//		DoubleScan = p.DoubleScan;
//		HideCursor = p.HideCursor;
//		DirectSound = p.DirectSound;
//		ExclusiveSound = p.ExclusiveSound;
//		AutoPause = p.AutoPause;
//		PrefsAtStartup = p.PrefsAtStartup;
//		SystemMemory = p.SystemMemory;
//		AlwaysCopy = p.AlwaysCopy;
//		SystemKeys = p.SystemKeys;
//		ShowLEDs = p.ShowLEDs;
	}

//	public boolean ShowEditor(boolean startup, File prefs) {
//		return false;
//		// TODO
//	}

	public void Check() {
//		if (SkipFrames <= 0) SkipFrames = 1;

		if (SIDType == null)
			SIDType = SIDTYPE_NONE;

		if (REUSize == null)
			REUSize = REU_NONE;

//		if (DisplayType == null)
//			DisplayType = DISPTYPE_WINDOW;

		for (int i = 0; i < 4; i++)
			if (DriveType[i] == null)
				DriveType[i] = DRVTYPE_DIR;
	}

	public void Load(File file) {
		try {
//			loadXML(file);
			loadProps(file);
		} catch (Exception x) {
			// x.printStackTrace();
			Save(file);
		}
	}

	public boolean Save(File file) {
//		return saveXML(file);
		return saveProps(file);
	}

//	private void loadXML(File file) throws JAXBException {
//		JAXBContext context = JAXBContext.newInstance(getClass());
//		Unmarshaller u = context.createUnmarshaller();
//		Prefs prefs = (Prefs) u.unmarshal(file);
//	}

//	private boolean saveXML(File file) {
//		try {
//			Writer writer = new FileWriter(file);
//			JAXBContext context = JAXBContext.newInstance(getClass());
//			Marshaller m = context.createMarshaller();
//			m.marshal(this, writer);
//			return true;
//		} catch (Exception x) {
//			x.printStackTrace();
//			return false;
//		}
//	}

	private boolean saveProps(File file) {
		if (file == null) {
			file = Frodo.prefsFile;
		}
		Check();
		try {
			Files.createDirectories(file.getCanonicalFile().getParentFile().toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (Writer out = new FileWriter(file)) {
			Properties props = new Properties();
			props.setProperty("NormalCycles", Integer.toString(NormalCycles));
			props.setProperty("BadLineCycles", Integer.toString(BadLineCycles));
			props.setProperty("CIACycles", Integer.toString(CIACycles));
			props.setProperty("FloppyCycles", Integer.toString(FloppyCycles));
//			props.setProperty("SkipFrames", Integer.toString(SkipFrames));
//			props.setProperty("LatencyMin", Integer.toString(LatencyMin));
//			props.setProperty("LatencyMax", Integer.toString(LatencyMax));
//			props.setProperty("LatencyAvg", Integer.toString(LatencyAvg));
//			props.setProperty("ScalingNumerator", Integer.toString(ScalingNumerator));
//			props.setProperty("ScalingDenominator", Integer.toString(ScalingDenominator));
			for (int i = 0; i < 4; i++) {
				String s = "DriveType" + (i + 8);
				switch (DriveType[i]) {
				case DRVTYPE_DIR:
					props.setProperty(s, "DIR");
					break;
				case DRVTYPE_D64:
					props.setProperty(s, "D64");
					break;
				case DRVTYPE_T64:
					props.setProperty(s, "T64");
					break;
				}
				props.setProperty("DrivePath" + (i + 8), DrivePath[i].getCanonicalPath());
			}
			props.setProperty("ViewPort", ViewPort);
			props.setProperty("DisplayMode", DisplayMode);
			switch (SIDType) {
			case SIDTYPE_NONE:
				props.setProperty("SIDType", "NONE");
				break;
			case SIDTYPE_DIGITAL:
				props.setProperty("SIDType", "DIGITAL");
				break;
			case SIDTYPE_SIDCARD:
				props.setProperty("SIDType", "SIDCARD");
				break;
			}
			switch (REUSize) {
			case REU_NONE:
				props.setProperty("REUSize", "NONE");
				break;
			case REU_128K:
				props.setProperty("REUSize", "128K");
				break;
			case REU_256K:
				props.setProperty("REUSize", "256K");
				break;
			case REU_512K:
				props.setProperty("REUSize", "512K");
				break;
			}
//			props.setProperty("DisplayType", DisplayType == DISPTYPE_WINDOW ? "WINDOW" : "SCREEN");
			props.setProperty("SpritesOn", SpritesOn ? "TRUE" : "FALSE");
			props.setProperty("SpriteCollisions", SpriteCollisions ? "TRUE" : "FALSE");
			props.setProperty("Joystick1On", Joystick1On ? "TRUE" : "FALSE");
			props.setProperty("Joystick2On", Joystick2On ? "TRUE" : "FALSE");
			props.setProperty("JoystickSwap", JoystickSwap ? "TRUE" : "FALSE");
			props.setProperty("LimitSpeed", LimitSpeed ? "TRUE" : "FALSE");
			props.setProperty("FastReset", FastReset ? "TRUE" : "FALSE");
			props.setProperty("CIAIRQHack", CIAIRQHack ? "TRUE" : "FALSE");
			props.setProperty("MapSlash", MapSlash ? "TRUE" : "FALSE");
			props.setProperty("Emul1541Proc", Emul1541Proc ? "TRUE" : "FALSE");
			props.setProperty("SIDFilters", SIDFilters ? "TRUE" : "FALSE");

			props.setProperty("saveOnExit", saveOnExit ? "TRUE" : "FALSE");
			
//			props.setProperty("DoubleScan", DoubleScan ? "TRUE" : "FALSE");
//			props.setProperty("HideCursor", HideCursor ? "TRUE" : "FALSE");
//			props.setProperty("DirectSound", DirectSound ? "TRUE" : "FALSE");
//			props.setProperty("ExclusiveSound", ExclusiveSound ? "TRUE" : "FALSE");
//			props.setProperty("AutoPause", AutoPause ? "TRUE" : "FALSE");
//			props.setProperty("PrefsAtStartup", PrefsAtStartup ? "TRUE" : "FALSE");
//			props.setProperty("SystemMemory", SystemMemory ? "TRUE" : "FALSE");
//			props.setProperty("AlwaysCopy", AlwaysCopy ? "TRUE" : "FALSE");
//			props.setProperty("SystemKeys", SystemKeys ? "TRUE" : "FALSE");

			cpuPrefs.save(props);
			keyboardPrefs.save(props);

			props.store(out, null);
			ThePrefsOnDisk = this;
			return true;
		} catch (Exception x) {
			x.printStackTrace();
		}
		return false;
	}
	private void loadProps(File file) throws FileNotFoundException, IOException {
		Properties props = new Properties();
		props.load(new FileReader(file));
		for (Object key : props.keySet()) {
			String keyword = key.toString();
			String value = props.getProperty(keyword);
			switch (keyword) {
			case "NormalCycles":
				NormalCycles = atoi(value);
				break;
			case "BadLineCycles":
				BadLineCycles = atoi(value);
				break;
			case "CIACycles":
				CIACycles = atoi(value);
				break;
			case "FloppyCycles":
				FloppyCycles = atoi(value);
				break;
//				case "SkipFrames": SkipFrames = atoi(value); break;
//			case "LatencyMin":
//				LatencyMin = atoi(value);
//				break;
//			case "LatencyMax":
//				LatencyMax = atoi(value);
//				break;
//			case "LatencyAvg":
//				LatencyAvg = atoi(value);
//				break;
//			case "ScalingNumerator":
//				ScalingNumerator = atoi(value);
//				break;
//			case "ScalingDenominator":
//				ScalingDenominator = atoi(value);
//				break;
			case "DriveType8":
				switch (value) {
				case "DIR":
					DriveType[0] = DRVTYPE_DIR;
					break;
				case "D64":
					DriveType[0] = DRVTYPE_D64;
					break;
				default:
					DriveType[0] = DRVTYPE_T64;
				}
				break;
			case "DriveType9":
				switch (value) {
				case "DIR":
					DriveType[1] = DRVTYPE_DIR;
					break;
				case "D64":
					DriveType[1] = DRVTYPE_D64;
					break;
				default:
					DriveType[1] = DRVTYPE_T64;
				}
				break;
			case "DriveType10":
				switch (value) {
				case "DIR":
					DriveType[2] = DRVTYPE_DIR;
					break;
				case "D64":
					DriveType[2] = DRVTYPE_D64;
					break;
				default:
					DriveType[2] = DRVTYPE_T64;
				}
				break;
			case "DriveType11":
				switch (value) {
				case "DIR":
					DriveType[3] = DRVTYPE_DIR;
					break;
				case "D64":
					DriveType[3] = DRVTYPE_D64;
					break;
				default:
					DriveType[3] = DRVTYPE_T64;
				}
				break;
			case "DrivePath8":
				DrivePath[0] = new File(value);
				break;
			case "DrivePath9":
				DrivePath[1] = new File(value);
				break;
			case "DrivePath10":
				DrivePath[2] = new File(value);
				break;
			case "DrivePath11":
				DrivePath[3] = new File(value);
				break;
			case "ViewPort":
				ViewPort = value;
				break;
			case "DisplayMode":
				DisplayMode = value;
				break;
			case "SIDType":
				switch (value) {
				case "DIGITAL":
					SIDType = SIDTYPE_DIGITAL;
					break;
				case "SIDCARD":
					SIDType = SIDTYPE_SIDCARD;
					break;
				default:
					SIDType = SIDTYPE_NONE;
				}
				break;
			case "REUSize":
				switch (value) {
				case "128K":
					REUSize = REU_128K;
					break;
				case "256K":
					REUSize = REU_256K;
					break;
				case "512K":
					REUSize = REU_512K;
					break;
				default:
					REUSize = REU_NONE;
				}
				break;
//			case "DisplayType":
//				DisplayType = value.equals("SCREEN") ? DISPTYPE_SCREEN : DISPTYPE_WINDOW;
//				break;
			case "SpritesOn":
				SpritesOn = value.equals("TRUE");
				break;
			case "SpriteCollisions":
				SpriteCollisions = value.equals("TRUE");
				break;
			case "Joystick1On":
				Joystick1On = value.equals("TRUE");
				break;
			case "Joystick2On":
				Joystick2On = value.equals("TRUE");
				break;
			case "JoystickSwap":
				JoystickSwap = value.equals("TRUE");
				break;
			case "LimitSpeed":
				LimitSpeed = value.equals("TRUE");
				break;
			case "FastReset":
				FastReset = value.equals("TRUE");
				break;
			case "CIAIRQHack":
				CIAIRQHack = value.equals("TRUE");
				break;
			case "MapSlash":
				MapSlash = value.equals("TRUE");
				break;
			case "Emul1541Proc":
				Emul1541Proc = value.equals("TRUE");
				break;
			case "SIDFilters":
				SIDFilters = value.equals("TRUE");
				break;
				
			case "saveOnExit":
				saveOnExit = value.equals("TRUE");
				break;
//			case "DoubleScan":
//				DoubleScan = value.equals("TRUE");
//				break;
//			case "HideCursor":
//				HideCursor = value.equals("TRUE");
//				break;
//			case "DirectSound":
//				DirectSound = value.equals("TRUE");
//				break;
//			case "ExclusiveSound":
//				ExclusiveSound = value.equals("TRUE");
//				break;
//			case "AutoPause":
//				AutoPause = value.equals("TRUE");
//				break;
//			case "PrefsAtStartup":
//				PrefsAtStartup = value.equals("TRUE");
//				break;
//			case "SystemMemory":
//				SystemMemory = value.equals("TRUE");
//				break;
//			case "AlwaysCopy":
//				AlwaysCopy = value.equals("TRUE");
//				break;
//			case "SystemKeys":
//				SystemKeys = value.equals("TRUE");
//				break;
//			case "ShowLEDs":
//				ShowLEDs = value.equals("TRUE");
//				break;
			}
		}
		cpuPrefs.load(props);
		keyboardPrefs.load(props);
		Check();
		ThePrefsOnDisk = this;
	}

	private int atoi(String value) {
		return Integer.parseInt(value);
	}


	@Override
	public boolean equals(Object obj) {
		Prefs rhs = (Prefs) obj;
		return (true && NormalCycles == rhs.NormalCycles && BadLineCycles == rhs.BadLineCycles
				&& CIACycles == rhs.CIACycles && FloppyCycles == rhs.FloppyCycles
//				&& SkipFrames == rhs.SkipFrames
//				&& LatencyMin == rhs.LatencyMin && LatencyMax == rhs.LatencyMax && LatencyAvg == rhs.LatencyAvg
//				&& ScalingNumerator == rhs.ScalingNumerator && ScalingDenominator == rhs.ScalingNumerator
				&& DriveType[0] == rhs.DriveType[0] && DriveType[1] == rhs.DriveType[1]
				&& DriveType[2] == rhs.DriveType[2] && DriveType[3] == rhs.DriveType[3]
				&& equals(DrivePath[0], rhs.DrivePath[0]) && equals(DrivePath[1], rhs.DrivePath[1])
				&& equals(DrivePath[2], rhs.DrivePath[2]) && equals(DrivePath[3], rhs.DrivePath[3])
				&& equals(ViewPort, rhs.ViewPort) && equals(DisplayMode, rhs.DisplayMode) && SIDType == rhs.SIDType
				&& REUSize == rhs.REUSize 
//				&& DisplayType == rhs.DisplayType
				&& SpritesOn == rhs.SpritesOn
				&& SpriteCollisions == rhs.SpriteCollisions && Joystick1On == rhs.Joystick1On
				&& Joystick2On == rhs.Joystick2On && JoystickSwap == rhs.JoystickSwap && LimitSpeed == rhs.LimitSpeed
				&& FastReset == rhs.FastReset && CIAIRQHack == rhs.CIAIRQHack && MapSlash == rhs.MapSlash
				&& Emul1541Proc == rhs.Emul1541Proc && SIDFilters == rhs.SIDFilters
				
				&& saveOnExit == rhs.saveOnExit
//				&& DoubleScan == rhs.DoubleScan
//				&& HideCursor == rhs.HideCursor && DirectSound == rhs.DirectSound
//				&& ExclusiveSound == rhs.ExclusiveSound && AutoPause == rhs.AutoPause
//				&& PrefsAtStartup == rhs.PrefsAtStartup && SystemMemory == rhs.SystemMemory
//				&& AlwaysCopy == rhs.AlwaysCopy && SystemKeys == rhs.SystemKeys && ShowLEDs == rhs.ShowLEDs
				);
	}

	private <T> boolean equals(T t1, T t2) {
		if (t1 == null && t2 == null) {
			return true;
		}
		if (t1 == null || t2 == null) {
			return false;
		}
		return t1.equals(t2);
	}

	public int NormalCycles; // Available CPU cycles in normal raster lines
	public int BadLineCycles; // Available CPU cycles in Bad Lines
	public int CIACycles; // CIA timer ticks per raster line
	public int FloppyCycles; // Available 1541 CPU cycles per line
//	public int SkipFrames;			// Draw every n-th frame

	public DriveType[] DriveType = new DriveType[4]; // Type of drive 8..11

	public File[] DrivePath = { new File(""), new File(""), new File(""), new File("") }; // Path for drive 8..11

	public String ViewPort; // Size of the C64 screen to display (Win32)
	public String DisplayMode; // Video mode to use for full screen (Win32)

	public SIDType SIDType; // SID emulation type
	public REUSize REUSize; // Size of REU
//	public DisplayType DisplayType; // Display type (BeOS)
//	public int LatencyMin; // Min msecs ahead of sound buffer (Win32)
//	public int LatencyMax; // Max msecs ahead of sound buffer (Win32)
//	public int LatencyAvg; // Averaging interval in msecs (Win32)
//	public int ScalingNumerator; // Window scaling numerator (Win32)
//	public int ScalingDenominator; // Window scaling denominator (Win32)

	public boolean SpritesOn; // Sprite display is on
	public boolean SpriteCollisions; // Sprite collision detection is on
	public boolean Joystick1On; // Joystick connected to port 1 of host
	public boolean Joystick2On; // Joystick connected to port 2 of host
	public boolean JoystickSwap; // Swap joysticks 1<->2
	public boolean LimitSpeed; // Limit speed to 100%
	public boolean FastReset; // Skip RAM test on reset
	public boolean CIAIRQHack; // Write to CIA ICR clears IRQ
	public boolean MapSlash; // Map '/' in C64 filenames
	public boolean Emul1541Proc; // Enable processor-level 1541 emulation
	public boolean SIDFilters; // Emulate SID filters
	
	public boolean saveOnExit = true;
	
//	public boolean DoubleScan; // Double scan lines (BeOS, if DisplayType == DISPTYPE_SCREEN)
//	public boolean HideCursor; // Hide mouse cursor when visible (Win32)
//	public boolean DirectSound; // Use direct sound (instead of wav) (Win32)
//	public boolean ExclusiveSound; // Use exclusive mode with direct sound (Win32)
//	public boolean AutoPause; // Auto pause when not foreground app (Win32)
//	public boolean PrefsAtStartup; // Show prefs dialog at startup (Win32)
//	public boolean SystemMemory; // Put view work surface in system mem (Win32)
//	public boolean AlwaysCopy; // Always use a work surface (Win32)
//	public boolean SystemKeys; // Enable system keys and menu keys (Win32)
//	public boolean ShowLEDs; // Show LEDs (Win32)
	public CPUPrefs cpuPrefs = new CPUPrefs();
	public KeyboardPrefs keyboardPrefs = new KeyboardPrefs(KeyMapping.keyMapSvLaptop);

//	#ifdef __mac__
//		void ChangeDisks(void);
//	#endif

//	#ifdef WIN32
//	private:
//		static BOOL CALLBACK StandardDialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
//		static BOOL CALLBACK WIN32DialogProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
//		BOOL DialogProc(int page, HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
//		void SetupControls(int page);
//		void SetValues(int page);
//		void GetValues(int page);
//		void BrowseForDevice(int id);
//
//		static Prefs *edit_prefs;
//		static char *edit_prefs_name;
//		static HWND hDlg;
//	#endif
}
