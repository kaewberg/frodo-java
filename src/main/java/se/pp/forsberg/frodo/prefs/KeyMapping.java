package se.pp.forsberg.frodo.prefs;

import static javafx.scene.input.KeyCode.A;
import static javafx.scene.input.KeyCode.B;
import static javafx.scene.input.KeyCode.BACK_SPACE;
import static javafx.scene.input.KeyCode.C;
import static javafx.scene.input.KeyCode.COMMA;
import static javafx.scene.input.KeyCode.CONTROL;
import static javafx.scene.input.KeyCode.D;
import static javafx.scene.input.KeyCode.DEAD_DIAERESIS;
import static javafx.scene.input.KeyCode.DELETE;
import static javafx.scene.input.KeyCode.DIGIT0;
import static javafx.scene.input.KeyCode.DIGIT1;
import static javafx.scene.input.KeyCode.DIGIT2;
import static javafx.scene.input.KeyCode.DIGIT3;
import static javafx.scene.input.KeyCode.DIGIT4;
import static javafx.scene.input.KeyCode.DIGIT5;
import static javafx.scene.input.KeyCode.DIGIT6;
import static javafx.scene.input.KeyCode.DIGIT7;
import static javafx.scene.input.KeyCode.DIGIT8;
import static javafx.scene.input.KeyCode.DIGIT9;
import static javafx.scene.input.KeyCode.DOWN;
import static javafx.scene.input.KeyCode.E;
import static javafx.scene.input.KeyCode.END;
import static javafx.scene.input.KeyCode.ENTER;
import static javafx.scene.input.KeyCode.ESCAPE;
import static javafx.scene.input.KeyCode.F;
import static javafx.scene.input.KeyCode.F1;
import static javafx.scene.input.KeyCode.F2;
import static javafx.scene.input.KeyCode.F3;
import static javafx.scene.input.KeyCode.F4;
import static javafx.scene.input.KeyCode.F5;
import static javafx.scene.input.KeyCode.F6;
import static javafx.scene.input.KeyCode.F7;
import static javafx.scene.input.KeyCode.F8;
import static javafx.scene.input.KeyCode.G;
import static javafx.scene.input.KeyCode.H;
import static javafx.scene.input.KeyCode.HOME;
import static javafx.scene.input.KeyCode.I;
import static javafx.scene.input.KeyCode.INSERT;
import static javafx.scene.input.KeyCode.J;
import static javafx.scene.input.KeyCode.K;
import static javafx.scene.input.KeyCode.L;
import static javafx.scene.input.KeyCode.LEFT;
import static javafx.scene.input.KeyCode.M;
import static javafx.scene.input.KeyCode.MINUS;
import static javafx.scene.input.KeyCode.N;
import static javafx.scene.input.KeyCode.O;
import static javafx.scene.input.KeyCode.P;
import static javafx.scene.input.KeyCode.PAGE_DOWN;
import static javafx.scene.input.KeyCode.PERIOD;
import static javafx.scene.input.KeyCode.PLUS;
import static javafx.scene.input.KeyCode.Q;
import static javafx.scene.input.KeyCode.QUOTE;
import static javafx.scene.input.KeyCode.R;
import static javafx.scene.input.KeyCode.RIGHT;
import static javafx.scene.input.KeyCode.S;
import static javafx.scene.input.KeyCode.SHIFT;
import static javafx.scene.input.KeyCode.SPACE;
import static javafx.scene.input.KeyCode.T;
import static javafx.scene.input.KeyCode.TAB;
import static javafx.scene.input.KeyCode.U;
import static javafx.scene.input.KeyCode.UP;
import static javafx.scene.input.KeyCode.V;
import static javafx.scene.input.KeyCode.W;
import static javafx.scene.input.KeyCode.X;
import static javafx.scene.input.KeyCode.Y;
import static javafx.scene.input.KeyCode.Z;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.scene.input.KeyCode;

public class KeyMapping {
	private KeyCode[][] keyCodes;
	private String[][] stringKeys;
	private MultiKey[] multiKeys;
	private Map<KeyCode, KeyCode[]> multiKeyMap = new HashMap<>();
	private Map<String, RC> stringKeyMap = new HashMap<>();
	private Map<KeyCode, RC> keyCodeMap = new HashMap<>();
	
	public KeyCode[] getMultiKey(KeyCode code) {
		return multiKeyMap.get(code);
	}
	public RC getStringKey(String s) {
		return stringKeyMap.get(s);
	}
	public RC getKey(KeyCode code) {
		return keyCodeMap.get(code);
	}
	
	public static class MultiKey {
		KeyCode keyCode;
		KeyCode[] mapsTo;
		public MultiKey(KeyCode keyCode, KeyCode... mapsTo) {
			this.keyCode = keyCode;
			this.mapsTo = mapsTo;
		}
	}
	public static class RC {
		public int r, c;
		public RC(int r, int c) {
			this.r = r;
			this.c = c;
		}
		public RC(String s) {
			String[] ss = s.split(" ");
			r = Integer.parseInt(ss[0]);
			c = Integer.parseInt(ss[1]);
		}
		@Override
		public String toString() {
			return String.format("[r=%d, c=%d]", r, c);
		}
	}
	public KeyMapping(KeyCode[][] keyCodes, String[][] stringKeys, MultiKey... multiKeys) {
		this.keyCodes = copy(keyCodes);
		this.stringKeys = copy(stringKeys);
		this.multiKeys = copy(multiKeys);
		for (MultiKey mk: multiKeys) {
			multiKeyMap.put(mk.keyCode, mk.mapsTo);
		}
		for (int r = 0; r < 8; r++) {
			for (int c = 0; c < 8; c++) {
				RC rc = new RC(r, c);
				String s = stringKeys[r][7-c];
				KeyCode kc = keyCodes[r][7-c];
				if (s == null && kc == null) {
					throw new IllegalArgumentException("Unmapped key " + rc);
				}
				if (s != null) {
					stringKeyMap.put(s, rc);
				} else {
					keyCodeMap.put(kc, rc);
				}
			}
		}
	}
	private static <T> T[][] copy(T[][] tm) {
		@SuppressWarnings("unchecked")
		T[][] result = (T[][]) Array.newInstance(tm.getClass().getComponentType(), tm.length);
		for (int i = 0; i < tm.length; i++) {
			result[i] = copy(tm[i]);
		}
		return result;
	}
	private static <T> T[] copy(T[] ta) {
		return Arrays.copyOf(ta, ta.length);
	}
	public KeyMapping(KeyMapping map) {
		this(map.keyCodes, map.stringKeys, map.multiKeys);
	}
	
	public KeyMapping(Map<KeyCode, KeyCode[]> multi, Map<KeyCode, RC> keys, Map<String, RC> strings) {
		multiKeyMap = multi;
		keyCodeMap = keys;
		stringKeyMap = strings;
		
		keyCodes = new KeyCode[8][8];
		stringKeys = new String[8][8];
		multiKeys = new MultiKey[multiKeyMap.size()];
		
		for (KeyCode code: keyCodeMap.keySet()) {
			RC rc = keyCodeMap.get(code);
			keyCodes[rc.r][7-rc.c] = code;
		}
		for (String s: stringKeyMap.keySet()) {
			RC rc = stringKeyMap.get(s);
			stringKeys[rc.r][7-rc.c] = s;
		}
		int i = 0;
		for (KeyCode code: multiKeyMap.keySet()) {
			KeyCode[] expansion = multiKeyMap.get(code);
			multiKeys[i++] = new MultiKey(code, expansion);
		}
	}

	public Collection<RC> set(int r, int c, KeyCode code, String character) {
		RC rc = new RC(r, c);
		List<RC> inUse = new ArrayList<>();
		if (code != null && code != KeyCode.UNDEFINED) {
			keyCodes[r][7-c] = code;
			inUse.add(keyCodeMap.put(code, rc));
		} else if (character != null) {
			stringKeys[r][7-c] = character;
			inUse.add(stringKeyMap.put(character, rc));
		}
		return inUse;
	}
	public Collection<KeyCode> getMultiKeys() {
		return multiKeyMap.keySet();
	}
	public Collection<KeyCode> getKeyCodes() {
		return keyCodeMap.keySet();
	}
	public Collection<String> getKeyStrings() {
		return stringKeyMap.keySet();
	}

	/*
	  C64 keyboard matrix:
Restore key???
	    Bit 7   6   5   4   3   2   1   0
	  0    CUD  F5  F3  F1  F7 CLR RET DEL
	  1    SHL  E   S   Z   4   A   W   3
	  2     X   T   F   C   6   D   R   5
	  3     V   U   H   B   8   G   Y   7
	  4     N   O   K   M   0   J   I   9
	  5     ,   @   :   .   -   L   P   +
	  6     /   🠕   =  SHR HOM  ;   *   £
	  7    R/S  Q   C= SPC  2  CTL  🠔   1
	*/
	// Utility class to express a keyboard mapping concisely,
	// so we can define instances prettily like so:
	public static KeyMapping keyMapSvLaptop = new KeyMapping(
			// Keycode to C=64 h/v line
			new KeyCode[][]{
				{  DOWN,   F5,   F3,   F1,        F7,    RIGHT,  ENTER,      DELETE },
				{  SHIFT,  E,    S,    Z,        DIGIT4,   A,    W,          DIGIT3 },
				{      X,  T,    F,    C,        DIGIT6,   D,    R,          DIGIT5 },
				{      V,  U,    H,    B,        DIGIT8,   G,    Y,          DIGIT7 },
				{      N,  O,    K,    M,        DIGIT0,   J,    I,          DIGIT9 },
				{  COMMA, null, null,  PERIOD,    HOME,    L,    P,          PLUS },
				{  MINUS, null, QUOTE, PAGE_DOWN, INSERT, null, DEAD_DIAERESIS, END },
				{ ESCAPE,  Q, CONTROL, SPACE,   DIGIT2,   TAB,  BACK_SPACE,  DIGIT1 }
			},
			// String key (for unmapped keycodes like swedish "åäö") to C=64 h/v line
			new String[][]{
				{ null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null },
				{ null, "å",  "ö",  null, null, null, null, null },
				{ null, "§",  null, null, null, "ä",  null, null },
				{ null, null, null, null, null, null, null, null }
			},
			// Keycodes that map to multiple C=64 keys
			new MultiKey(F2,   SHIFT, F1),
			new MultiKey(F4,   SHIFT, F3),
			new MultiKey(F6,   SHIFT, F5),
			new MultiKey(F8,   SHIFT, F7),
			new MultiKey(UP,   SHIFT, DOWN),
			new MultiKey(LEFT, SHIFT, RIGHT)
		);

	public String toString(int r, int c) {
		
		KeyCode code = keyCodes[r][7-c];
		if (code != null) {
			String s =  code.toString();
			if (s.startsWith("DIGIT")) {
				s = s.substring(5);
			}
			return s;
		}
		String s = stringKeys[r][7-c];
		return s == null? "":s;
	}

}