package se.pp.forsberg.frodo.prefs;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.pp.forsberg.frodo.enums.ROMCall;

public class CPUPrefs {
	public static class CallPrefs {
		ROMCall call;
		boolean print, brk;
		private CallPrefs(ROMCall call, boolean print, boolean brk) {
			this.call = call;
			this.print = print;
			this.brk = brk;
		}
		public CallPrefs(CallPrefs cp) {
			call = cp.call;
			print = cp.print;
			brk = cp.brk;
		}
		public ROMCall getCall() {
			return call;
		}
		public boolean isPrint() {
			return print;
		}
		public boolean isBrk() {
			return brk;
		}
	}
	public boolean showDebug = true, showTrace;
	public Map<ROMCall, CallPrefs> prefs = new HashMap<>();
	
	public CPUPrefs() {
		for (ROMCall call: ROMCall.values()) {
			CallPrefs prefs = new CallPrefs(call, call.isPrint(), call.isBrk());
			this.prefs.put(call,  prefs);
		}
	}

	public CPUPrefs(CPUPrefs prefs) {
		showDebug = prefs.showDebug;
		showTrace = prefs.showTrace;
		for (CallPrefs cp: prefs.prefs.values()) {
			this.prefs.put(cp.call, new CallPrefs(cp));
		}
	}

	public void save(Properties props) {
		props.setProperty("CPU.Debug", showDebug? "TRUE":"FALSE");
		props.setProperty("CPU.Trace", showTrace? "TRUE":"FALSE");
		for (CallPrefs cp: prefs.values()) {
			ROMCall call = cp.call;
			props.setProperty(String.format("CPU.ROMCall.%04x", call.getAddress()),
					String.format("%s %s", cp.print? "TRUE":"FALSE", cp.brk? "TRUE":"FALSE"));
		}
	}

	public void load(Properties props) {
		Pattern pat = Pattern.compile("CPU\\.ROMCall\\.(\\w+)");
		for (Object key: props.keySet()) {
			String prop = key.toString();
			Matcher match = pat.matcher(prop);
			if (!match.matches()) {
				continue;
			}
			int adr = Integer.parseInt(match.group(1), 16);
			String val = props.getProperty(prop);
			ROMCall call = ROMCall.get(adr);
			if (call == null) {
				continue;
			}
			String[] v = val.split(" ");
			call.setPrint(v[0].equals("TRUE"));
			call.setBrk(v[1].equals("TRUE"));
		}
	}
}
