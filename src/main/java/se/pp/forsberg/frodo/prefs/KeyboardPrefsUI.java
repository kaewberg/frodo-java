package se.pp.forsberg.frodo.prefs;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import se.pp.forsberg.frodo.Frodo;
import se.pp.forsberg.frodo.prefs.KeyMapping.RC;

public class KeyboardPrefsUI extends Dialog<KeyboardPrefs> {

	KeyboardPrefs prefs;
	
	private KeyboardPrefsUI(KeyboardPrefs prefs) {
		this.prefs = new KeyboardPrefs(prefs);
	}

	public static KeyboardPrefs edit(KeyboardPrefs prefs) {
		return ui(prefs)
				.showAndWait()
				.orElse(null);
	}
	private static KeyboardPrefsUI ui(KeyboardPrefs prefs) {
		return new KeyboardPrefsUI(prefs).build();
	}

	GridPane grid = new GridPane();
	int x, y, w;
	
	private KeyboardPrefsUI build() {
		
		space(2);
		key(6, "🠔", 7, 1);
		key(6, "1", 7, 0);
		key(6, "2", 7, 3);
		key(6, "3", 1, 0);
		key(6, "4", 1, 3);
		key(6, "5", 2, 0);
		key(6, "6", 2, 3);
		key(6, "7", 3, 0);
		key(6, "8", 3, 3);
		key(6, "9", 4, 0);
		key(6, "0", 4, 3);
		key(6, "+", 5, 0);
		key(6, "-", 5, 3);
		key(6, "£", 6, 0);
		key(6, "CLR\nHOME", 6, 3);
		key(6, "INST\nDEL", 0, 0);
		space(2);
		key(9, "F 1", 0, 4);
		
		nextRow();
		space(2);
		key(9, "CTRL", 7, 2);
		key(6, "Q", 7, 6);
		key(6, "W", 1, 1);
		key(6, "E", 1, 6);
		key(6, "R", 2, 1);
		key(6, "T", 2, 6);
		key(6, "Y", 3, 1);
		key(6, "U", 3, 6);
		key(6, "I", 4, 1);
		key(6, "O", 4, 6);
		key(6, "P", 5, 1);
		key(6, "@", 5, 6);
		key(6, "*", 6, 1);
		key(6, "🠕", 6, 6);
		key(9, "RESTORE", -1, -1);
		space(2);
		key(9, "F 3", 0, 5);

		nextRow();
		key(6, "RUN\nSTOP", 7, 7);
		key(6, "SHIFT\nLOCK", -1, -1);
		key(6, "A", 1, 2);
		key(6, "S", 1, 5);
		key(6, "D", 2, 2);
		key(6, "F", 2, 5);
		key(6, "G", 3, 2);
		key(6, "H", 3, 5);
		key(6, "J", 4, 2);
		key(6, "K", 4, 5);
		key(6, "L", 5, 2);
		key(6, "[\n:", 5, 5);
		key(6, "]\n;", 6, 2);
		key(6, "=", 6, 5);
		key(12, "RETURN", 0, 1);
		space(4);
		key(9, "F 5", 0, 6);

		nextRow();
		key(6, "C=", 1, 2);
		key(9, "SHIFT", 1, 7);
		key(6, "Z", 1, 4);
		key(6, "X", 2, 7);
		key(6, "C", 2, 4);
		key(6, "V", 3, 7);
		key(6, "B", 3, 4);
		key(6, "N", 4, 7);
		key(6, "M", 4, 4);
		key(6, "<\n,", 5, 7);
		key(6, ">\n.", 5, 4);
		key(6, "?\n/", 6, 7);
		key(9, "SHIFT", 6, 4);
		key(6, "⮁", 0, 7);
		key(6, "⮀", 0, 2);
		space(4);
		key(9, "F 7", 0, 3);
		
		nextRow();
		space(17);
		key(65, "", 7, 4);

		ObservableList<ColumnConstraints> ccs = grid.getColumnConstraints();
		ColumnConstraints cc = new ColumnConstraints();
		cc.setPercentWidth(100.0/w);
		for (int i = 0; i < w; i++) {
			ccs.add(cc);
		}
		
		getDialogPane().getButtonTypes().add(ButtonType.OK);
		getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
		setResizable(true);
		
		BorderPane bp = new BorderPane(grid);
		ImageView image = new ImageView(new Image(Frodo.class.getResourceAsStream("C=64 keyboard.jpg")));
		image.setPreserveRatio(true);
		image.setFitWidth(600);
		bp.setTop(image);
		BorderPane.setAlignment(image, Pos.BOTTOM_CENTER);
		
		getDialogPane().setContent(bp);
		getDialogPane().setMaxWidth(600);
		setResultConverter(bt -> {
			if (bt == ButtonType.OK) {
				return prefs;
			}
			return null;
		});
		setTitle("Keyboard settings");
		return this;
	}

	TextField[][] textFields = new TextField[8][8];
	private void key(int w, String glyph, int r, int c) {
		VBox key = new VBox();
		key.setAlignment(Pos.CENTER);
		
		Label label = new Label(glyph);
		label.setTextAlignment(TextAlignment.CENTER);
		label.setAlignment(Pos.CENTER);
		if (glyph.indexOf('\n') >= 0) {
			Font font = label.getFont();
			font = Font.font(font.getName(), FontWeight.EXTRA_BOLD, font.getSize()/2);
			label.setFont(font);
			label.setLineSpacing(-4);
		}
		key.getChildren().add(label);
		
		if (r >= 0) {
			TextField tf = new TextField(prefs.map.toString(r, c));
			textFields[r][c] = tf;
			tf.setMaxWidth(w*7);
			tf.setTooltip(new Tooltip(tf.getText()));
			tf.addEventFilter(KeyEvent.ANY, e -> {
				if (e.getEventType() == KeyEvent.KEY_RELEASED) {
					keyEvent(tf, r, c, e.getCode(), e.getCharacter());
				}
				e.consume();	
			});
//			tf.setOnKeyPressed(e -> {
//				e.consume();
//			});
//			tf.setOnKeyTyped(e -> {
//				e.consume();
//			});
//			tf.setOnKeyReleased(e -> {
//				e.consume();
//			});
			key.getChildren().add(tf);
		}	
		grid.add(key, x, y, w, 1);
		x += w;
		this.w = Integer.max(x, this.w);
	}

	private void keyEvent(TextField tf, int r, int c, KeyCode code, String character) {
		for (RC inUse: prefs.map.set(r, c, code, character)) {
			textFields[inUse.r][inUse.c].setText("");
		}
		tf.setText(prefs.map.toString(r, c));
		tf.setTooltip(new Tooltip(tf.getText()));
	}

	private void nextRow() {
		x = 0;
		y++;
	}
	private void space(int i) {
		x += i;
	}
}
