package se.pp.forsberg.frodo;

class MOS6501State {
	byte a, x, y;
	byte p;			// Processor flags
	byte ddr, pr;		// Port
	short pc, sp;
	byte[] intr = new byte[4];		// Interrupt state
	boolean nmi_state;	
	byte dfff_byte;
	boolean instruction_complete;
}
