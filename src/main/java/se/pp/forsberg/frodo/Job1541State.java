package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.BPtr;

class Job1541State {
	public static final int STATE_SIZE = 10;
	
	public int current_halftrack;
	public int gcr_ptr;
	public boolean write_protected;
	public boolean disk_changed;

	public Job1541State(byte[] buf) {
		BPtr bp = new BPtr(buf);
		
		bp.setInc(current_halftrack);
		bp.setInc(gcr_ptr);
		bp.setInc(write_protected);
		bp.setInc(disk_changed);
	}

	public Job1541State() {
	}

	public byte[] toBytes() {
		byte[] buf = new byte[STATE_SIZE];
		BPtr bp = new BPtr(buf);
		
		bp.setInc(current_halftrack);
		bp.setInc(gcr_ptr);
		bp.setInc(write_protected);
		bp.setInc(disk_changed);
		
		return buf;
	}
}
