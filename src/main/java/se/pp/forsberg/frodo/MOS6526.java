package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.TimerState.*;
import se.pp.forsberg.frodo.enums.TimerState;

abstract class MOS6526 {
	public MOS6526(MOS6510 CPU) {
		the_cpu = CPU;
	}

	public void Reset() {
		pra = prb = ddra = ddrb = 0;

		ta = tb = (short) 0xffff;
		latcha = latchb = 1;

		tod_10ths = tod_sec = tod_min = tod_hr = 0;
		alm_10ths = alm_sec = alm_min = alm_hr = 0;

		sdr = icr = cra = crb = int_mask = 0;

		tod_halt = false;
		tod_divider = 0;

		ta_cnt_phi2 = tb_cnt_phi2 = tb_cnt_ta = false;

		ta_irq_next_cycle = tb_irq_next_cycle = false;
		ta_state = tb_state = T_STOP;
	}
	public MOS6526State GetState() {
		MOS6526State cs = new MOS6526State();

		cs.pra = pra;
		cs.prb = prb;
		cs.ddra = ddra;
		cs.ddrb = ddrb;

		cs.ta_lo = (byte) (ta & 0xff);
		cs.ta_hi = (byte) (ta >> 8);
		cs.tb_lo = (byte) (tb & 0xff);
		cs.tb_hi = (byte) (tb >> 8);
		cs.latcha = latcha;
		cs.latchb = latchb;
		cs.cra = cra;
		cs.crb = crb;

		cs.tod_10ths = tod_10ths;
		cs.tod_sec = tod_sec;
		cs.tod_min = tod_min;
		cs.tod_hr = tod_hr;
		cs.alm_10ths = alm_10ths;
		cs.alm_sec = alm_sec;
		cs.alm_min = alm_min;
		cs.alm_hr = alm_hr;

		cs.sdr = sdr;

		cs.int_data = icr;
		cs.int_mask = int_mask;

		return cs;
	}
	public void SetState(MOS6526State cs) {
		
		pra = cs.pra;
		prb = cs.prb;
		ddra = cs.ddra;
		ddrb = cs.ddrb;

		ta = (short) ((cs.ta_hi << 8) | cs.ta_lo);
		tb = (short) ((cs.tb_hi << 8) | cs.tb_lo);
		latcha = cs.latcha;
		latchb = cs.latchb;
		cra = cs.cra;
		crb = cs.crb;

		tod_10ths = cs.tod_10ths;
		tod_sec = cs.tod_sec;
		tod_min = cs.tod_min;
		tod_hr = cs.tod_hr;
		alm_10ths = cs.alm_10ths;
		alm_sec = cs.alm_sec;
		alm_min = cs.alm_min;
		alm_hr = cs.alm_hr;

		sdr = cs.sdr;

		icr = cs.int_data;
		int_mask = cs.int_mask;

		tod_halt = false;
		ta_cnt_phi2 = ((cra & 0x20) == 0x00);
		tb_cnt_phi2 = ((crb & 0x60) == 0x00);
		tb_cnt_ta = ((crb & 0x60) == 0x40);

		ta_state = ((cra & 1) != 0) ? T_COUNT : T_STOP;
		tb_state = ((crb & 1) != 0) ? T_COUNT : T_STOP;

	}
//	#ifdef FRODO_SC
	public void CheckIRQs() {
		// Trigger pending interrupts
		if (ta_irq_next_cycle) {
			ta_irq_next_cycle = false;
			TriggerInterrupt(1);
		}
		if (tb_irq_next_cycle) {
			tb_irq_next_cycle = false;
			TriggerInterrupt(2);
		}
	}
	public void EmulateCycle() {
		boolean ta_underflow = false;
		boolean ta_interrupt = false;
ta_idle:
		do {
ta_count:
			do {
				ta_interrupt = false;
				// Timer A state machine
				switch (ta_state) {
					case T_WAIT_THEN_COUNT:
						ta_state = T_COUNT;		// fall through
					case T_STOP:
						break ta_idle;
					case T_LOAD_THEN_STOP:
						ta_state = T_STOP;
						ta = latcha;			// Reload timer
						break ta_idle;
					case T_LOAD_THEN_COUNT:
						ta_state = T_COUNT;
						ta = latcha;			// Reload timer
						break ta_idle;
					case T_LOAD_THEN_WAIT_THEN_COUNT:
						ta_state = T_WAIT_THEN_COUNT;
						if (ta == 1) {
							ta_interrupt = true;
							break ta_count;	// Interrupt if timer == 1
						}else {
							ta = latcha;		// Reload timer
							break ta_idle;
						}
					case T_COUNT:
						break ta_count;
					case T_COUNT_THEN_STOP:
						ta_state = T_STOP;
						break ta_count;
				}
		
			} while (false); // ta_count:
				// Count timer A

				if (ta_cnt_phi2 || ta_interrupt)
					if (ta == 0 || --ta == 0 || ta_interrupt) {				// Decrement timer, underflow?
						if (ta_state != T_STOP || ta_interrupt) {
//			ta_interrupt:
						ta = latcha;			// Reload timer
						ta_irq_next_cycle = true; // Trigger interrupt in next cycle
						icr |= 1;				// But set ICR bit now
	
						if ((cra & 8) != 0) {			// One-shot?
							cra &= 0xfe;		// Yes, stop timer
							new_cra &= 0xfe;
							ta_state = T_LOAD_THEN_STOP;	// Reload in next cycle
						} else
							ta_state = T_LOAD_THEN_COUNT;	// No, delay one cycle (and reload)
					}
					ta_underflow = true;
				}
	
			// Delayed write to CRA?
		} while (false); // ta_idle:
		
		boolean ta_idle2_restart = false;	
ta_idle2:
		do {
			ta_idle2_restart = false;
			if (has_new_cra) {
				switch (ta_state) {
					case T_STOP:
					case T_LOAD_THEN_STOP:
						if ((new_cra & 1) != 0) {		// Timer started, wasn't running
							if ((new_cra & 0x10) != 0)	// Force load
								ta_state = T_LOAD_THEN_WAIT_THEN_COUNT;
							else				// No force load
								ta_state = T_WAIT_THEN_COUNT;
						} else {				// Timer stopped, was already stopped
							if ((new_cra & 0x10) != 0)	// Force load
								ta_state = T_LOAD_THEN_STOP;
						}
						break;
					case T_COUNT:
						if ((new_cra & 1) != 0) {		// Timer started, was already running
							if ((new_cra & 0x10) != 0)	// Force load
								ta_state = T_LOAD_THEN_WAIT_THEN_COUNT;
						} else {				// Timer stopped, was running
							if ((new_cra & 0x10) != 0)	// Force load
								ta_state = T_LOAD_THEN_STOP;
							else				// No force load
								ta_state = T_COUNT_THEN_STOP;
						}
						break;
					case T_LOAD_THEN_COUNT:
					case T_WAIT_THEN_COUNT:
						if ((new_cra & 1) != 0) {
							if ((new_cra & 8) != 0) {		// One-shot?
								new_cra &= 0xfe;	// Yes, stop timer
								ta_state = T_STOP;
							} else if ((new_cra & 0x10) != 0)	// Force load
								ta_state = T_LOAD_THEN_WAIT_THEN_COUNT;
						} else {
							ta_state = T_STOP;
						}
						break;
				}
				cra = (byte) (new_cra & 0xef);
				has_new_cra = false; 
			}

			boolean tb_interrupt = false;
			// Timer B state machine
	tb_idle:
			do {
	tb_count:
				do {
					switch (tb_state) {
						case T_WAIT_THEN_COUNT:
							tb_state = T_COUNT;		// fall through
						case T_STOP:
							break tb_idle;
						case T_LOAD_THEN_STOP:
							tb_state = T_STOP;
							tb = latchb;			// Reload timer
							break tb_idle;
						case T_LOAD_THEN_COUNT:
							tb_state = T_COUNT;
							tb = latchb;			// Reload timer
							ta_idle2_restart = true;
							continue ta_idle2;
						case T_LOAD_THEN_WAIT_THEN_COUNT:
							tb_state = T_WAIT_THEN_COUNT;
							if (tb == 1) {
								tb_interrupt = true;	// Interrupt if timer == 1
								break tb_count;
							}else {
								tb = latchb;		// Reload timer
								break tb_idle;
							}
						case T_COUNT:
							break tb_count;
						case T_COUNT_THEN_STOP:
							tb_state = T_STOP;
							break tb_count;
					}
				} while (false); // tb_count:
					// Count timer B
					if (tb_cnt_phi2 || (tb_cnt_ta && ta_underflow) || tb_interrupt)
						if (tb == 0 || --tb == 0 || tb_interrupt) {				// Decrement timer, underflow?
							if (tb_state != T_STOP || tb_interrupt) {
								tb = latchb;			// Reload timer
								tb_irq_next_cycle = true; // Trigger interrupt in next cycle
								icr |= 2;				// But set ICR bit now
			
								if ((crb & 8) != 0) {			// One-shot?
									crb &= 0xfe;		// Yes, stop timer
									new_crb &= 0xfe;
									tb_state = T_LOAD_THEN_STOP;	// Reload in next cycle
								} else
									tb_state = T_LOAD_THEN_COUNT;	// No, delay one cycle (and reload)
							}
						}
			
					// Delayed write to CRB?
			} while (false); // tb_idle:
			if (has_new_crb) {
				switch (tb_state) {
					case T_STOP:
					case T_LOAD_THEN_STOP:
						if ((new_crb & 1) != 0) {		// Timer started, wasn't running
							if ((new_crb & 0x10) != 0)	// Force load
								tb_state = T_LOAD_THEN_WAIT_THEN_COUNT;
							else				// No force load
								tb_state = T_WAIT_THEN_COUNT;
						} else {				// Timer stopped, was already stopped
							if ((new_crb & 0x10) != 0)	// Force load
								tb_state = T_LOAD_THEN_STOP;
						}
						break;
					case T_COUNT:
						if ((new_crb & 1) != 0) {		// Timer started, was already running
							if ((new_crb & 0x10) != 0)	// Force load
								tb_state = T_LOAD_THEN_WAIT_THEN_COUNT;
						} else {				// Timer stopped, was running
							if ((new_crb & 0x10) != 0)	// Force load
								tb_state = T_LOAD_THEN_STOP;
							else				// No force load
								tb_state = T_COUNT_THEN_STOP;
						}
						break;
					case T_LOAD_THEN_COUNT:
					case T_WAIT_THEN_COUNT:
						if ((new_crb & 1) != 0) {
							if ((new_crb & 8) != 0) {		// One-shot?
								new_crb &= 0xfe;	// Yes, stop timer
								tb_state = T_STOP;
							} else if ((new_crb & 0x10) != 0)	// Force load
								tb_state = T_LOAD_THEN_WAIT_THEN_COUNT;
						} else {
							tb_state = T_STOP;
						}
						break;
				}
				crb = (byte) (new_crb & 0xef);
				has_new_crb = false;
			}
		} while (ta_idle2_restart); // ta_idle(2)
	}
//	#else
//		void EmulateLine(int cycles);
//	#endif
	public void CountTOD() {
		byte lo, hi;

		// Decrement frequency divider
		if (tod_divider != 0)
			tod_divider--;
		else {

			// Reload divider according to 50/60 Hz flag
			if ((cra & 0x80) != 0)
				tod_divider = 4;
			else
				tod_divider = 5;

			// 1/10 seconds
			tod_10ths++;
			if (tod_10ths > 9) {
				tod_10ths = 0;

				// Seconds
				lo = (byte) ((tod_sec & 0x0f) + 1);
				hi = (byte) (tod_sec >> 4);
				if (lo > 9) {
					lo = 0;
					hi++;
				}
				if (hi > 5) {
					tod_sec = 0;

					// Minutes
					lo = (byte) ((tod_min & 0x0f) + 1);
					hi = (byte) (tod_min >> 4);
					if (lo > 9) {
						lo = 0;
						hi++;
					}
					if (hi > 5) {
						tod_min = 0;

						// Hours
						lo = (byte) ((tod_hr & 0x0f) + 1);
						hi = (byte) ((tod_hr >> 4) & 1);
						tod_hr &= 0x80;		// Keep AM/PM flag
						if (lo > 9) {
							lo = 0;
							hi++;
						}
						tod_hr |= (hi << 4) | lo;
						if ((tod_hr & 0x1f) > 0x11)
							tod_hr = (byte) (tod_hr & 0x80 ^ 0x80);
					} else
						tod_min = (byte) ((hi << 4) | lo);
				} else
					tod_sec = (byte) ((hi << 4) | lo);
			}

			// Alarm time reached? Trigger interrupt if enabled
			if (tod_10ths == alm_10ths && tod_sec == alm_sec &&
				tod_min == alm_min && tod_hr == alm_hr)
				TriggerInterrupt(4);
		}
	}
	abstract void TriggerInterrupt(int bit);

	MOS6510 the_cpu;	// Pointer to 6510

	byte pra, prb, ddra, ddrb;
	short ta, tb, latcha, latchb;

	byte tod_10ths, tod_sec, tod_min, tod_hr;
	byte alm_10ths, alm_sec, alm_min, alm_hr;

	byte sdr, icr, cra, crb;
	byte int_mask;

	int tod_divider;	// TOD frequency divider

	boolean tod_halt,		// Flag: TOD halted
			ta_cnt_phi2,	// Flag: Timer A is counting Phi 2
			tb_cnt_phi2,	// Flag: Timer B is counting Phi 2
			tb_cnt_ta;		// Flag: Timer B is counting underflows of Timer A

//	#ifdef FRODO_SC
	boolean ta_irq_next_cycle,		// Flag: Trigger TA IRQ in next cycle
			 tb_irq_next_cycle,		// Flag: Trigger TB IRQ in next cycle
			 has_new_cra,			// Flag: New value for CRA pending
			 has_new_crb;			// Flag: New value for CRB pending
	TimerState ta_state, tb_state;	// Timer A/B states
	byte new_cra, new_crb;		// New val
}
