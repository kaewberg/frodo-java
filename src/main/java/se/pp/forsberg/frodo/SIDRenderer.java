package se.pp.forsberg.frodo;

import se.pp.forsberg.frodo.prefs.Prefs;

interface SIDRenderer {
	void Reset();
	void EmulateLine();
	void WriteRegister(int adr, byte b);
	void NewPrefs(Prefs prefs);
	void Pause();
	void Resume();
}
