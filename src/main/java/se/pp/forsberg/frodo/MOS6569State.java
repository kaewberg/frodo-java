package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.BPtr;

class MOS6569State {
	public static final int STATE_SIZE = 115;
	
	public byte m0x;				// Sprite coordinates
	public byte m0y;
	public byte m1x;
	public byte m1y;
	public byte m2x;
	public byte m2y;
	public byte m3x;
	public byte m3y;
	// 8
	public byte m4x;
	public byte m4y;
	public byte m5x;
	public byte m5y;
	public byte m6x;
	public byte m6y;
	public byte m7x;
	public byte m7y;
	// 16
	public byte mx8;

	public byte ctrl1;			// Control registers
	public byte raster;
	public byte lpx;
	public byte lpy;
	public byte me;
	public byte ctrl2;
	public byte mye;
	// 24
	public byte vbase;
	public byte irq_flag;
	public byte irq_mask;
	public byte mdp;
	public byte mmc;
	public byte mxe;
	public byte mm;
	public byte md;
	// 32
	
	public byte ec;				// Color registers
	public byte b0c;
	public byte b1c;
	public byte b2c;
	public byte b3c;
	public byte mm0;
	public byte mm1;
	public byte m0c;
	// 40
	public byte m1c;
	public byte m2c;
	public byte m3c;
	public byte m4c;
	public byte m5c;
	public byte m6c;
	public byte m7c;
							// Additional registers
	public byte pad0;
	// 48
	public short irq_raster;		// IRQ raster line
	public short vc;				// Video counter
	public short vc_base;			// Video counter base
	public byte rc;				// Row counter
	public byte spr_dma;			// 8 Flags: Sprite DMA active
	// 56
	public byte spr_disp;			// 8 Flags: Sprite display active
	public byte[] mc = new byte[8];			// Sprite data counters
	public byte[] mc_base = new byte[8];		// Sprite data counter bases
	public boolean display_state;		// true: Display state, false: Idle state
	public boolean bad_line;			// Flag: Bad Line state
	public boolean bad_line_enable;	// Flag: Bad Lines enabled for this frame
	public boolean lp_triggered;		// Flag: Lightpen was triggered in this frame
	public boolean border_on;			// Flag: Upper/lower border on (Frodo SC: Main border flipflop)

	public short bank_base;		// VIC bank base address
	// 80
	public short matrix_base;		// Video matrix base
	public short char_base;		// Character generator base
	public short bitmap_base;		// Bitmap base
	public short[] sprite_base = new short[8];	// Sprite bases
	// 106
							// Frodo SC:
	int cycle;				// Current cycle in line (1..63)
	public short raster_x;		// Current raster x position
	int ml_index;			// Index in matrix/color_line[]
	// 116
	public byte ref_cnt;			// Refresh counter
	public byte last_vic_byte;	// Last byte read by VIC
	public boolean ud_border_on;		// Flag: Upper/lower border on
	
	public MOS6569State(byte[] buf) {
		BPtr bp = new BPtr(buf);
		m0x = bp.getInc(); m0y = bp.getInc(); m1x = bp.getInc(); m1y = bp.getInc(); m2x = bp.getInc(); m2y = bp.getInc(); m3x = bp.getInc(); m3y = bp.getInc();
		m4x = bp.getInc(); m4y = bp.getInc(); m5x = bp.getInc(); m5y = bp.getInc(); m6x = bp.getInc(); m6y = bp.getInc(); m7x = bp.getInc(); m7y = bp.getInc();
		mx8 = bp.getInc(); ctrl1 = bp.getInc(); raster = bp.getInc(); lpx = bp.getInc(); lpy = bp.getInc(); me = bp.getInc(); ctrl2 = bp.getInc(); mye = bp.getInc();
		vbase = bp.getInc(); irq_flag = bp.getInc(); irq_mask = bp.getInc(); mdp = bp.getInc(); mmc = bp.getInc(); mxe = bp.getInc(); mm = bp.getInc(); md = bp.getInc();
		
		ec = bp.getInc(); b0c = bp.getInc(); b1c = bp.getInc(); b2c = bp.getInc(); b3c = bp.getInc(); mm0 = bp.getInc(); mm1 = bp.getInc(); m0c = bp.getInc();
		m1c = bp.getInc(); m2c = bp.getInc(); m3c = bp.getInc(); m4c = bp.getInc(); m5c = bp.getInc(); m6c = bp.getInc(); m7c = bp.getInc(); pad0 = bp.getInc();
		irq_raster = bp.getIncShort(); vc = bp.getIncShort(); vc_base = bp.getIncShort(); rc = bp.getInc(); spr_dma = bp.getInc();
		spr_disp = bp.getInc();
		
		for (int i = 0; i < 8; i++) {
			mc[i] = bp.getInc();
		}
		for (int i = 0; i < 8; i++) {
			mc_base[i] = bp.getInc();
		}
		
		display_state = bp.getIncBool(); bad_line = bp.getIncBool(); bad_line_enable = bp.getIncBool(); lp_triggered = bp.getIncBool(); border_on = bp.getIncBool();
		bank_base = bp.getIncShort(); matrix_base = bp.getIncShort();
		
		char_base = bp.getIncShort(); bitmap_base = bp.getIncShort();
		for (int i = 0; i < 8; i++) {
			sprite_base[i] = bp.getIncShort();
		}
		
		cycle = bp.getIncShort(); raster_x = bp.getIncShort(); ml_index = bp.getIncInt(); ref_cnt = bp.getInc(); last_vic_byte = bp.getInc(); ud_border_on = bp.getIncBool();
	}

	public MOS6569State() {
	}

	public byte[] toBytes() {
		byte[] buf = new byte[STATE_SIZE];
		BPtr bp = new BPtr(buf);
		
		bp.setInc(m0x); bp.setInc(m0y); bp.setInc(m1x); bp.setInc(m1y); bp.setInc(m2x); bp.setInc(m2y); bp.setInc(m3x); bp.setInc(m3y);
		bp.setInc(m4x); bp.setInc(m4y); bp.setInc(m5x); bp.setInc(m5y); bp.setInc(m6x); bp.setInc(m6y); bp.setInc(m7x); bp.setInc(m7y);
		bp.setInc(mx8); bp.setInc(ctrl1); bp.setInc(raster); bp.setInc(lpx); bp.setInc(lpy); bp.setInc(me); bp.setInc(ctrl2); bp.setInc(mye);
		bp.setInc(vbase); bp.setInc(irq_flag); bp.setInc(irq_mask); bp.setInc(mdp); bp.setInc(mmc); bp.setInc(mxe); bp.setInc(mm); bp.setInc(md);	
		// 32
		bp.setInc(ec); bp.setInc(b0c); bp.setInc(b1c); bp.setInc(b2c); bp.setInc(b3c); bp.setInc(mm0); bp.setInc(mm1); bp.setInc(m0c);
		bp.setInc(m1c); bp.setInc(m2c); bp.setInc(m3c); bp.setInc(m4c); bp.setInc(m5c); bp.setInc(m6c); bp.setInc(m7c); bp.setInc(pad0);
		// 48
		bp.setInc(irq_raster); bp.setInc(vc); bp.setInc(vc_base); bp.setInc(rc); bp.setInc(spr_dma);
		bp.setInc(spr_disp);
		// 57
		for (int i = 0; i < 8; i++) {
			bp.setInc(mc[i]);
		}
		for (int i = 0; i < 8; i++) {
			bp.setInc(mc_base[i]);
		}
		// 73
		bp.setInc(display_state); bp.setInc(bad_line); bp.setInc(bad_line_enable); bp.setInc(lp_triggered); bp.setInc(border_on);
		bp.setInc(bank_base); bp.setInc(matrix_base);
		bp.setInc(char_base); bp.setInc(bitmap_base);
		// 86
		for (int i = 0; i < 8; i++) {
			bp.setInc(sprite_base[i]);
		}
		// 102
		bp.setInc(cycle); bp.setInc(raster_x); bp.setInc(ml_index); bp.setInc(ref_cnt); bp.setInc(last_vic_byte); bp.setInc(ud_border_on);
		// 115
		
		return buf;
	}
}
