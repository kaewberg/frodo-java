package se.pp.forsberg.frodo;

class MOS6526_1 extends MOS6526 {
	public MOS6526_1(MOS6510 CPU, MOS6569 VIC) {
		super(CPU);
		the_vic = VIC;
	}

	@Override public void Reset() {
		super.Reset();

		// Clear keyboard matrix and joystick states
		for (int i=0; i<8; i++)
			KeyMatrix[i] = RevMatrix[i] = (byte) 0xff;

		Joystick1 = Joystick2 = (byte) 0xff;
		prev_lp = 0x10;
	}
	public byte ReadRegister(int adr) {
	switch (adr) {
		case 0x00: {
			byte ret = (byte) (pra | ~ddra), tst = (byte) ((prb | ~ddrb) & Joystick1);
			if ((tst & 0x01) == 0) ret &= RevMatrix[0];	// AND all active columns
			if ((tst & 0x02) == 0) ret &= RevMatrix[1];
			if ((tst & 0x04) == 0) ret &= RevMatrix[2];
			if ((tst & 0x08) == 0) ret &= RevMatrix[3];
			if ((tst & 0x10) == 0) ret &= RevMatrix[4];
			if ((tst & 0x20) == 0) ret &= RevMatrix[5];
			if ((tst & 0x40) == 0) ret &= RevMatrix[6];
			if ((tst & 0x80) == 0) ret &= RevMatrix[7];
			byte joyKey = (byte) (ret & Joystick2);
			if (joyKey != -1) {
				System.out.println("JoyKey " + joyKey);
			}
			return joyKey;
		}
		case 0x01: {
			byte ret = (byte) ~ddrb, tst = (byte) ((pra | ~ddra) & Joystick2);
			if ((tst & 0x01) == 0) ret &= KeyMatrix[0];	// AND all active rows
			if ((tst & 0x02) == 0) ret &= KeyMatrix[1];
			if ((tst & 0x04) == 0) ret &= KeyMatrix[2];
			if ((tst & 0x08) == 0) ret &= KeyMatrix[3];
			if ((tst & 0x10) == 0) ret &= KeyMatrix[4];
			if ((tst & 0x20) == 0) ret &= KeyMatrix[5];
			if ((tst & 0x40) == 0) ret &= KeyMatrix[6];
			if ((tst & 0x80) == 0) ret &= KeyMatrix[7];
			byte joyKey = (byte) ((ret | (prb & ddrb)) & Joystick1);
//			if (joyKey != -1) {
//				System.out.println("JoyKey " + joyKey);
//			}
			return joyKey;
		}
		case 0x02: return ddra;
		case 0x03: return ddrb;
		case 0x04: return (byte) ta;
		case 0x05: return (byte) (ta >> 8);
		case 0x06: return (byte) tb;
		case 0x07: return (byte) (tb >> 8);
		case 0x08: tod_halt = false; return tod_10ths;
		case 0x09: return tod_sec;
		case 0x0a: return tod_min;
		case 0x0b: tod_halt = true; return tod_hr;
		case 0x0c: return sdr;
		case 0x0d: {
			byte ret = icr;			// Read and clear ICR
			icr = 0;
			the_cpu.ClearCIAIRQ();		// Clear IRQ
			return ret;
		}
		case 0x0e: return cra;
		case 0x0f: return crb;
	}
	return 0;	// Can't happen
}
	public void WriteRegister(int adr, byte b) {
	switch (adr) {
		case 0x0: pra = b; break;
		case 0x1:
			prb = b;
			check_lp();
			break;
		case 0x2: ddra = b; break;
		case 0x3:
			ddrb = b;
			check_lp();
			break;

		case 0x4: latcha = (short) ((latcha & 0xff00) | b); break;
		case 0x5:
			latcha = (short) ((latcha & 0xff) | (b << 8));
			if ((cra & 1) == 0)	// Reload timer if stopped
				ta = latcha;
			break;

		case 0x6: latchb = (short) ((latchb & 0xff00) | b); break;
		case 0x7:
			latchb = (short) ((latchb & 0xff) | (b << 8));
			if ((crb & 1) == 0)	// Reload timer if stopped
				tb = latchb;
			break;

		case 0x8:
			if ((crb & 0x80) != 0)
				alm_10ths = (byte) (b & 0x0f);
			else
				tod_10ths = (byte) (b & 0x0f);
			break;
		case 0x9:
			if ((crb & 0x80) != 0)
				alm_sec = (byte) (b & 0x7f);
			else
				tod_sec = (byte) (b & 0x7f);
			break;
		case 0xa:
			if ((crb & 0x80) != 0)
				alm_min = (byte) (b & 0x7f);
			else
				tod_min = (byte) (b & 0x7f);
			break;
		case 0xb:
			if ((crb & 0x80) != 0)
				alm_hr = (byte) (b & 0x9f);
			else
				tod_hr = (byte) (b & 0x9f);
			break;

		case 0xc:
			sdr = b;
			TriggerInterrupt(8);	// Fake SDR interrupt for programs that need it
			break;

		case 0xd:
			if ((b & 0x80) != 0)
				int_mask |= b & 0x7f;
			else
				int_mask &= ~b;
			if ((icr & int_mask & 0x1f) != 0) { // Trigger IRQ if pending
				icr |= 0x80;
				the_cpu.TriggerCIAIRQ();
			}
			break;

		case 0xe:
			has_new_cra = true;		// Delay write by 1 cycle
			new_cra = b;
			ta_cnt_phi2 = ((b & 0x20) == 0x00);
			break;

		case 0xf:
			has_new_crb = true;		// Delay write by 1 cycle
			new_crb = b;
			tb_cnt_phi2 = ((b & 0x60) == 0x00);
			tb_cnt_ta = ((b & 0x60) == 0x40);
			break;
	}
}
	@Override void TriggerInterrupt(int bit) {
		icr |= bit;
		if ((int_mask & bit) != 0) {
			icr |= 0x80;
			the_cpu.TriggerCIAIRQ();
		}
	}

	public byte[] KeyMatrix = new byte[8];	// C64 keyboard matrix, 1 bit/key (0: key down, 1: key up)
	public byte[] RevMatrix = new byte[8];	// Reversed keyboard matrix

	public byte Joystick1;	// Joystick 1 AND value
	public byte Joystick2;	// Joystick 2 AND value

	private void check_lp() {
		if (((byte) ((prb | ~ddrb) & 0x10)) != prev_lp)
			the_vic.TriggerLightpen();
		prev_lp = (byte) ((prb | ~ddrb) & 0x10);
	}


	private MOS6569 the_vic;

	private byte prev_lp;		// Previous state of LP line (bit 4)

}
