package se.pp.forsberg.frodo.enums;

public enum InterruptType6502 {
	INT_VIA1IRQ,
	INT_VIA2IRQ,
	INT_IECIRQ,
	INT_RESET
}
