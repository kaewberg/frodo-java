package se.pp.forsberg.frodo.enums;

public enum REUSize {
	REU_NONE,		// No REU
	REU_128K,		// 128K
	REU_256K,		// 256K
	REU_512K		// 512K
}
