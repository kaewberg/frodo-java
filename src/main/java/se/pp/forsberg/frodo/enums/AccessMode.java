package se.pp.forsberg.frodo.enums;

public enum AccessMode {
	FMODE_READ, FMODE_WRITE, FMODE_APPEND
}