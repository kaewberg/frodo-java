package se.pp.forsberg.frodo.enums;

public enum InterruptType6510 {
	INT_VICIRQ,
	INT_CIAIRQ,
	INT_NMI,
	INT_RESET
}
