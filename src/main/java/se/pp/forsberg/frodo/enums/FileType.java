package se.pp.forsberg.frodo.enums;

public enum FileType {
	FTYPE_PRG, FTYPE_SEQ, FTYPE_USR, FTYPE_REL
}
