package se.pp.forsberg.frodo.enums;

// LED states
public enum DriveLEDState {
	DRVLED_OFF,		// LED off
	DRVLED_ON,			// LED on (green)
	DRVLED_ERROR,	// LED blinking red
}