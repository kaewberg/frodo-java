package se.pp.forsberg.frodo.enums;

import java.util.HashMap;
import java.util.Map;

import se.pp.forsberg.frodo.prefs.CPUPrefs;

public enum ROMCall {

	// ********* Commodore 64 BASIC ROM dissasembly *********
	// *********      Version 1.0 (June 1994)       *********

	// 0xa000 Data RESET address
	// 0xa002 Data Qarm Start address
	// 0xa004 "CBMBASIC"

	// address table for commands
	// (address minus 1 used)

	// 0xa00c END
	// 0xa00e FOR
	// 0xa010 NEXT
	// 0xa012 DATA
	// 0xa014 INPUT#
	// 0xa016 INPUT
	// 0xa018 DIM
	// 0xa01a READ
	// 0xa01c LET
	// 0xa01e GOTO
	// 0xa020 RUN
	// 0xa022 IF
	// 0xa024 RESTORE
	// 0xa026 GOSUB
	// 0xa028 RETURN
	// 0xa02a REM
	// 0xa02c STOP
	// 0xa02e ON
	// 0xa030 WAIT
	// 0xa032 LOAD
	// 0xa034 SAVE
	// 0xa036 VERIFY
	// 0xa038 DEF
	// 0xa03a POKE
	// 0xa03c PRINT#
	// 0xa03e PRINT
	// 0xa040 CONT
	// 0xa042 LIST
	// 0xa044 CLR
	// 0xa046 CMD
	// 0xa048 SYS
	// 0xa04a OPEN
	// 0xa04c CLOSE
	// 0xa04e GET
	// 0xa050 NEW

	// address table for functions

	// 0xa052 SGN
	// 0xa054 INT
	// 0xa056 ABS
	// 0xa058 USR
	// 0xa05a FRE
	// 0xa05c POS
	// 0xa05e SQR
	// 0xa060 RND
	// 0xa062 LOG
	// 0xa064 EXP
	// 0xa066 COS
	// 0xa068 SIN
	// 0xa06a TAN
	// 0xa06c ATN
	// 0xa07e PEEK
	// 0xa070 LEN
	// 0xa072 STR$
	// 0xa074 VAL
	// 0xa076 ASC
	// 0xa078 CHR$
	// 0xa07a LEFT$
	// 0xa07c RIGHT$
	// 0xa07e MID$

	// priority and address table for operators
	// (address minus 1 used)

	// 1 byte pri, 2 byte address

	// 0xa080 +
	// 0xa083 -
	// 0xa086 *
	// 0xa089 /
	// 0xa08c 🠕
	// 0xa08f AND
	// 0xa092 OR
	// 0xa095 - (unary)
	// 0xa098 NOT
	// 0xa09b < = >

	// table of commands
	// each ended with a +$80 (8th bit set

	// 0xa09e "END"
	// 0xa0a1 "FOR"
	// 0xa0a4 "NEXT"
	// 0xa0a8 "DATA"
	// 0xa0ac "INPUT#"
	// 0xa0b2 "INPUT"
	// 0xa0b7 "DIM"
	// 0xa0ba "READ"
	// 0xa0be "LET"
	// 0xa0c1 "GOTO"
	// 0xa0c5 "RUN"
	// 0xa0c8 "IF"
	// 0xa0ca "RESTORE"
	// 0xa0d1 "GOSUB"
	// 0xa0d6 "RETURN"
	// 0xa0dc "REM"
	// 0xa0df "STOP"
	// 0xa0e3 "ON"
	// 0xa0e5 "WAIT"
	// 0xa0e9 "LOAD"
	// 0xa0ed "SAVE"
	// 0xa0f1 "VERIFY"
	// 0xa0f7 "DEF"
	// 0xa0fa "POKE"
	// 0xa0fe "PRINT#"
	// 0xa104 "PRINT"
	// 0xa109 "CONT"
	// 0xa10d "LIST"
	// 0xa111 "CLR"
	// 0xa114 "CMD"
	// 0xa117 "SYS"
	// 0xa11a "OPEN"
	// 0xa11e "CLOSE"
	// 0xa123 "GET"
	// 0xa126 "NEW"

	// table of functions
	// each ended with a +$80 (8th bit set

	// 0xa129 "TAB("
	// 0xa12d "TO"
	// 0xa12f "FN"
	// 0xa131 "SPC("
	// 0xa135 "THEN"
	// 0xa139 "NOT"
	// 0xa13c "STEP"
	// 0xa140 "+"
	// 0xa141 "-"
	// 0xa142 "*"
	// 0xa143 "/"
	// 0xa144 "🠕"
	// 0xa145 "AND"
	// 0xa148 "ON"
	// 0xa14a ">"
	// 0xa14b "="
	// 0xa14c "<"
	// 0xa14d "SGN"
	// 0xa150 "INT"
	// 0xa153 "ABS"
	// 0xa156 "USR"
	// 0xa159 "FRE"
	// 0xa15c "POS"
	// 0xa15f "SQR"
	// 0xa162 "RND"
	// 0xa165 "LOG"
	// 0xa168 "EXP"
	// 0xa16b "COS"
	// 0xa164 "SIN"
	// 0xa171 "TAN"
	// 0xa174 "ATN"
	// 0xa177 "PEEK"
	// 0xa17b "LEN"
	// 0xa17e "STR$"
	// 0xa182 "VAL"
	// 0xa185 "ASC"
	// 0xa188 "CHR$"
	// 0xa18c "LEFT$"
	// 0xa191 "RIGHT$"
	// 0xa197 "MID$"

	// other commands

	// 0xa19b "go"
	// 0x19d 0

	// table of errors messages
	// each ended with a +$80 (8th bit set)

	// 0xa19e "TOO MANY FILES"
	// 0x1a1c "FILE OPEN"
	// 0xa1b5 "FILE NOT OPEN"
	// 0xa1c2 "FILE NOT FOUND"
	// 0xa1d0 "DEVICE NOT PRESENT"
	// 0xa1e2 "NOT INPUT FILE"
	// 0xa1f0 "NOT OUTPUT FILE"
	// 0xa1ff "MISSING FILE NAME"
	// 0xa210 "ILLEGAL DEVICE NUMBER"
	// 0xa225 "NEXT WITHOUT FOR"
	// 0xa235 "SYNTAX"
	// 0xa23b "RETURN WITHOUT GOSUB"
	// 0xa24f "OUT OF DATA"
	// 0xa25a "ILLEGAL QUANTITY"
	// 0xa26a "OVERFLOW"
	// 0xa272 "OUT OF MEMORY"
	// 0xa27f "UNDEF'D STATEMENT"
	// 0xa290 "BAD SUBSCRIPT"
	// 0xa29d "REDIM'D ARRAY"
	// 0xa2ba "ILLEGAL DIRECT"
	// 0xa2c8 "TYPE MISMATCH"
	// 0xa2d5 "STRING TOO LONG"
	// 0xa2e4 "FILE DATA"
	// 0xa2ed "FORMULA TOO COMPLEX"
	// 0xa300 "CAN'T CONTINUE"
	// 0xa30e "UNDEF'D FUNCTION"
	// 0xa31e "VERIFY"
	// 0xa324 "LOAD"

	// error message address locations

//		A328   .WD $A19E   ; 01 too many files
//		A32A   .WD $A1AC   ; 02 file open
//		A32C   .WD $A1B5   ; 03 file not open
//		A32E   .WD $A1C2   ; 04 file not found
//		A330   .WD $A1D0   ; 05 device not present
//		A332   .WD $A1E2   ; 06 not input file
//		A334   .WD $A1F0   ; 07 not output file
//		A336   .WD $A1FF   ; 08 missing file name
//		A338   .WD $A210   ; 09 illegal device number
//		A33A   .WD $A225   ; 0A next without for
//		A33C   .WD $A235   ; 0B syntax
//		A33E   .WD $A23B   ; 0C return without gosub
//		A340   .WD $A24F   ; 0D out of data
//		A342   .WD $A25A   ; 0E illegal quantity
//		A344   .WD $A26A   ; 0F overflow
//		A346   .WD $A272   ; 10 out of memory
//		A348   .WD $A27F   ; 11 undef'd statment
//		A34A   .WD $A290   ; 12 bad subscript
//		A34C   .WD $A29D   ; 13 redim'd array
//		A34E   .WD $A2AA   ; 14 devision by zero
//		A350   .WD $A2BA   ; 15 illegal direct
//		A352   .WD $A2C8   ; 16 type mismatch
//		A354   .WD $A2D5   ; 17 string too long
//		A356   .WD $A2E4   ; 18 file data
//		A358   .WD $A2ED   ; 19 formula too complex
//		A35A   .WD $A300   ; 1A can't continue
//		A35C   .WD $A30E   ; 1B undef'd function
//		A35E   .WD $A31E   ; 1C verify
//		A360   .WD $A324   ; 1D load
//		A362   .WD $A383   ; 1E break

	// other messages

	// 0xa364 "OK"
	// 0xa369 "ERROR"
	// 0xa371 "IN"
	// 0xa376 "READY"
	// 0xa381 "\r\nBREAK

	// 0xa388 00 a0

	BAS_SEARCH_FOR(0xa38a, "BAS search for 'for' blocks", "Search for 'for' blocks on the stack", true, false),
	BAS_MOVE_BYTES_CS(0xa3b8, "BAS move bytes check space", "Move bytes after check for space", true, false),
	BAS_MOVE_BYTES(0xa3bf, "BAS move bytes", "Move bytes", true, false),
	BAS_CHECK_STACK(0xa3fb, "BAS check stack space", "Check for 2 * A bytes free on stack", true, false),
	BAS_CHECK_ARROF(0xa408, "BAS array overflow check", "Array area overflow check", true, false),
	BAS_OUT_OF_MEM(0xa435, "BAS out of memory", "Out of memory error", true, false),
	BAS_HANDLE_ERROR(0xa437, "BAS handle error", "Handle error messages", true, false),
	BAS_ERROR_HANDLE(0xa43a, "BAS error handler", "Standard error emssage handler", true, false),
	BAS_HANDLE_WARM_START(0xa480, "BAS warm start", "Warm start", true, false),
	BAS_WARM_START_HANDLE(0xa483, "BAS warm start handler", "Standard warm start routine", true, false),
	BAS_INS_DEL_LINE(0xa49c, "BAS ins/del line", "Insert/delete bsic lines", true, false),
	BAS_DEL_LINE(0xa4a9, "BAS del line", "Delete old basic line", true, false),
	BAS_INS_LINE(0xa4ed, "BAS ins line", "Insert new basic line", true, false),
	BAS_RELINK(0xa533, "BAS relink", "Relink basic program", true, false),
	BAS_GET_STMT(0xa560, "BAS get statment", "Get statement into buffer", true, false),
	BAS_HANDLE_CRUNCH(0xa579, "BAS crunch tokens", "Crunch tokens", true, false),
	BAS_CRUNCH_HANDLE(0xa57c, "BAS crunch tokens handler", "Standard token cruncher", true, false),
	BAS_SEARCH_LINE(0xa613, "BAS search line", "Search for a line in a program", true, false),
	BAS_NEW(0xa642, "BAS NEW", "NEW command", true, false), BAS_CLR(0xa65e, "BAS CLR", "CLR command", true, false),
	BAS_RESET_STACK(0xa67a, "BAS reset stack", "Reset stack and program pointers", true, false),
	BAS_RESET_CHARP(0xa68e, "BAS reset charp", "Set current character pointer to start of basic - 1", true, false),
	BAS_LIST(0xa69c, "BAS LIST", "LIST command", true, false),
	BAS_LIST_LINES(0xa6c9, "BAS LIST lines", "List lines from $5f/$60 to $14/$15", true, false),
	BAS_HANDLE_PRINT_TOKENS(0xa717, "BAS print tokens", "Print tokens routine", true, false),
	BAS_PRINT_TOKENS_HANDLE(0xa71a, "BAS print tokens handler", "Standard token printer", true, false),
	BAS_PRINT_KEYWORD(0xa737, "BAS print keyword", "Print keyword", true, false),
	BAS_FOR(0xa742, "BAS FOR", "FOR command", true, false),
	BAS_NEXT_STMT(0xa742, "BAS next statement", "Execute next statement", true, false),
	BAS_EXEC_STMT(0xa7e4, "BAS execute statement", "Execute a statement", true, false),
	BAS_EXEC_CMD(0xa7ed, "BAS execute command", "Execute command in A", true, false),
	BAS_RESTORE(0xa81d, "BAS RESTORE", "RESTORE command", true, false),
	BAS_STOP(0xa82f, "BAS STOP", "STOP command", true, false),
	BAS_END(0xa831, "BAS END", "END command", true, false),

	// PASTE

	BAS_CHK_C(0xAD90, "BAS Check C", "check value according to C flag", true, false),
	BAS_EVAL(0xAD9E, "BAS Eval", "evaluate expression", true, false),
	BAS_EVAL_REC(0xAE20, "BAS Eval rec", "recursive entry for evaluation of expressions", true, false),
	BAS_ROUND(0xAE33, "BAS Round", "save rounded value of left operand", true, false),
	BAS_APPLY_OP(0xAE58, "BAS Apply op", "apply operator", true, false),
	BAS_HANDLE_ARTHM(0xAE83, "BAS Handle arithmetic ", "get arithmetic element routine", true, false),
	BAS_ARTHM_HANDLE(0xAE86, "BAS Artithmetic handler ", "standard arithmetic element", true, false),
	BAS_NOT(0xAED4, "BAS NOT ", "NOT operator", true, false),
	BAS_GET(0xAEE3, "BAS GET", "GET operand", true, false),
	BAS_SKIP(0xAEF7, "BAS Skip chars", "check and skip characters", true, false),
	BAS_GET_REC(0xAF0D, "BAS Get rec ", "recursive get value", true, false),
	BAS_CHK_VAR_PTR(0xAF14, "BAS Check var ptr", "check variable pointer range", true, false),
	BAS_GET_VAR(0xAF28, "BAS Get var", "get value of variable", true, false),
	BAS_TIME(0xAF84, "BAS Get time", "get time in float accu", true, false),
	BAS_GET_VAR_2(0xAF92, "BAS Get var (2)", "continue of get value of variable", true, false),
	BAS_APPLY_FN(0xAFA7, "BAS Apply function", "apply function", true, false),
	BAS_OR(0xAFE6, "BAS OR", "OR operator", true, false),
	BAS_AND(0xAFE9, "BAS AND", "AND operator", true, false),
	BAS_CMP(0xB016, "BAS <=>", "greater/equal/less operator", true, false),
	BAS_DIM(0xB07E, "BAS DIM", "DIM command", true, false),
	BAS_GET_PTR(0xB08B, "BAS Get ptr", "get name and pointer to a variable", true, false),
	BAS_ALPHA(0xB113, "BAS Is alpha", "check character in A, C=1 if alphabetic, C=0 if not", true, false),
	BAS_VAR_NOT_FOUND(0xB11D, "BAS Var not found", "variable not found", true, false),
	BAS_VAR_FOUND(0xB185, "BAS Var found", "variable found", true, false),
	BAS_ARR_PTR(0xB194, "BAS Array ptr", "compute pointer to array body", true, false),
//	B1A5   float number for conversion to integer (5 bytes)
	BAS_FLOT2FIX(0xB1AA, "BAS Float to fixpoint", "routine to convert float to fixed point", true, false),
	BAS_TO_INT(0xB1B2, "BAS Cast to int", "convert value from statement to integer", true, false),
	BAS_FLOAT2INT(0xB1BF, "BAS Round to int", "convert float number to integer", true, false),
	BAS_DIM_PTR(0xB1D1, "BAS Dim ptr", "get pointer to dimensioned variable", true, false),
	BAS_ARR_ALLOC(0xB261, "BAS Allocate array", "allocate array", true, false),
	BAS_ARR_REF(0xB2EA, "BAS Array ref", "compute reference to array element", true, false),
	BAS_ARR_LEN(0xB34C, "BAS Array length", "XY = XA = length * limit from array data", true, false),
	BAS_FRE(0xB37D, "BAS FRE", "FRE function", true, false),
	BAS_INT2FLOAT(0xB391, "BAS Int to float", "routine to convert integer to float", true, false),
	BAS_POS(0xB39E, "BAS POS", "POS function", true, false),
	BAS_CHK_NONDIR(0xB3A6, "BAS Check nondirect", "check for non-direct mode", true, false),
	BAS_DEF(0xB3B3, "BAS DEF", "DEF command", true, false),
	BAS_FN_NAME(0xB3E1, "BAS Function name", "get function name", true, false),
	BAS_EXPAND_FN(0xB3F4, "BAS Expand funtion", "expand FN call", true, false),
	BAS_STR(0xB465, "BAS STR$", "STR$ function", true, false),
	BAS_ALLOCA(0xB47D, "BAS Allocate A", "allocate area according to A", true, false),
	BAS_STR_DESC(0xB487, "BAS String description", "get description of string into float accu", true, false),
	BAS_STACK_DESC(0xB4CA, "BAS Stack descriptor", "save descriptor from $61-$63 on stack", true, false),
	BAS_ALLOC(0xB4F4, "BAS Alloc", "allocate number of bytes in A", true, false),
	BAS_GBAS_STRING(0xB526, "BAS Garbage collect string", "string garbage clean up", true, false),
	BAS_CHK_STRINGS(0xB5BD, "BAS Check string area", "check string area", true, false),
	BAS_CHK_STRINGS2(0xB5C7, "BAS Check strings 2", "check string area", true, false),
	BAS_GBAS_2(0xB606, "BAS Garbage collect 2", "continuation of garbage clean up", true, false),
	BAS_CONCAT(0xB63D, "BAS Concat", "joining strings", true, false),
	BAS_STR_MOVE(0xB67A, "BAS Move string", "move string", true, false),
	BAS_STR_MOVE2(0xB688, "BAS Move string 2", "move string with length A, pointer in XY", true, false),
	BAS_FREE_TMP_STR(0xB6A3, "BAS Free temp string", "de-allocate temporary string", true, false),
	BAS_CHK_DESC(0xB6DB, "BAS Check descriptors", "check descriptor stack", true, false),
	BAS_CHR(0xB6EC, "BAS CHR$", "CHR$ function", true, false),
	BAS_LEFT(0xB700, "BAS LEFT$", "LEFT$ function", true, false),
	BAS_RIGHT(0xB72C, "BAS RIGHT$", "RIGHT$ function", true, false),
	BAS_MID(0xB737, "BAS MID$", "MID$ function", true, false),
	BAS_LEN(0xB77C, "BAS LEN", "LEN function", true, false),
	BAS_ASC(0xB78B, "BAS ASC", "ASC function", true, false),
	BAS_GET_INT(0xB79B, "BAS Get int", "fetch integer value in X and check range", true, false),
	BAS_VAL(0xB7AD, "BAS VAL", "VAL function", true, false),
	BAS_GET_ADR(0xB7EB, "BAS Get address", "get address into $14/$15 and integer in X", true, false),
	BAS_FLOAT2INT_14(0xB7F7, "BAS Float to int $14", "convert float ti integer in $14/$15", true, false),
	BAS_PEEK(0xB80D, "BAS PEEK", "PEEK function", true, false),
	BAS_POKE(0xB824, "BAS POKE", "POKE command", true, false),
	BAS_WAIT(0xB82D, "BAS WAIT", "WAIT command", true, false),
	BAS_ADD_HALF(0xB849, "BAS + 0.5", "add 0.5 to float accu (rounding)", true, false),
	BAS_UNARY_MINUS(0xB850, "BAS -x", "unary minus operator", true, false),
	BAS_ADD_FLOAT(0xB867, "BAS ", "add float indexed by AY to float accu", true, false),
	BAS_PLUS(0xB86A, "BAS +", "plus operator", true, false),
	BAS_PLUS_FRAC(0xB8FE, "BAS Plus fractions", "add fractions", true, false),
	BAS_POSTSHIFT(0xB91D, "BAS Postshift", "postshift", true, false),
	BAS_NEG(0xB947, "BAS Negate", "negate float accu", true, false),
	BAS_INC_FRAC(0xB96F, "BAS Increment fraction", "increment fraction", true, false),
	BAS_PRESHIFT(0xB983, "BAS Preshift", "preshift", true, false),
//	B9BC	float "1" (5 bytes)
//	B9C1	LOG polynomial table
//	B9D6	float SQR(1/2)
//	B9DB	float SQR(2)
//	B9E0	float -0.5
//	B9E5	float LOG(2)
	BAS_LOG(0xB9EA, "BAS LOG", "LOG function", true, false),
	BAS_TIMES(0xBA2B, "BAS *", "times operator", true, false),
	BAS_FLOAT_IDX(0xBA8C, "BAS Float indexed", "move float indexed by AY into second float accu", true, false),
	BAS_ADD_EXP(0xBAB7, "BAS Add exponents", "add exponents", true, false),
	BAS_ACCU_10(0xBAE2, "BAS Float accu *10", "multiply float accu by 10", true, false),
//	BAF9	float constant 10 for division
	BAS_DIV10(0xBAFE, "BAS Divide by 10", "divide float by 10", true, false),
	BAS_DIV_IDX(0xBB0F, "BAS Divide indexed", "divide number indexed by AY by float accu", true, false),
	BAS_DIV(0xBB12, "BAS /", "divide operator", true, false),
	BAS_FLOAT_ACCU_5C(0xBBC7, "BAS Float accu 5c", "store float accu at $5C-$60", true, false),
	BAS_FLOAT_ACCU_57(0xBBCA, "BAS Float accu 57", "store float accu at $57-$5B", true, false),
	BAS_FLOAT_ACCU_49(0xBBD0, "BAS Float accu 49", "store float accu in index at $49/$4A", true, false),
	BAS_FLOAT_ACCU_XY(0xBBD4, "BAS Float accu XY", "store float accu in index XY", true, false),
	BAS_CPY_FLOAT_ACCU(0xBBFC, "BAS Copy float accu", "move second float accu into first", true, false),
	BAS_CPY_ROUND_FLOAT_ACCU(0xBC0C, "BAS Copy rounded float accu", "move rounded float accu into second", true, false),
	BAS_FLOAT_ACCU_GUARD(0xBC1B, "BAS Round according to guard", "round float accu according to guard bit", true, false),
	BAS_ACCU_SGN(0xBC2B, "BAS Accu sgn", "get sign of float accu in A", true, false),
	BAS_SGN(0xBC39, "BAS SGN", "SGN function", true, false),
	BAS_A2FLOAT(0xBC3C, "BAS Move A to float", "move signed number from A into float accu", true, false),
	BAS_ABS(0xBC58, "BAS ABS", "ABS function", true, false),
	BAS_CMP_ACCU(0xBC5B, "BAS Compare accu", "compare float accu to float indexed by XY", true, false),
	BAS_FLOAT2LONG(0xBC9B, "BAS Float to long", "convert float to a 4 byte signed integer", true, false),
	BAS_INT(0xBCCC, "BAS INT", "INT function", true, false),
	BAS_CLEAR_ACCU(0xBCE9, "BAS ", "clear float accu", true, false),
	BAS_STR2FLOAT(0xBCF3, "BAS String to float", "convert string to float in float accu", true, false),
	BAS_INT_PLUS_FLOAT(0xBD7E, "BAS Add int to float", "add signed integer from A to float accu", true, false),
	BAS_STR_EXP(0xBD91, "BAS String exponent", "get exponent of number from string", true, false),
//	BDB3	constants for float to string conversion
	BAS_PRINT_IN(0xBDC2, "BAS Print 'IN ...'", "print IN followed by line number", true, false),
	BAS_PRINT_AX(0xBDCD, "BAS Print AX", "print number from AX", true, false),
	BAS_ACCU2STR(0xBDDD, "BAS ftoa", "convert number in float accu to string", true, false),
	BAS_ACCU2STR2(0xBE35, "ftoa2", "Check range", true, false),
	BAS_ACCU2STR3(0xBE93, "ftoa3", "Digit 1", true, false),

//	BF16	divisors for decimal conversion
//	BF3A	divisors for time conversion
//	BF52	unused, is this some version id?
//	BF53	unused
	BAS_SQR(0xBF71, "BAS SQR", "SQR function", true, false),
	BAS_POW(0xBF7B, "BAS 🠕", "power operator (🠕)", true, false),
	BAS_MINUS(0xBFB4, "BAS -", "minus operator", true, false),
//	 floating point constands for EXP

//	BFBF	1/LOG(2)
//	BFC4	EXP polynomial table
	BAS_EXP(0xBFED, "BAS EXP", "EXP command", true, false),

// ********* Commodore 64 KERNAL ($03) ROM dissasembly *********
// *********          Version 1.0 (June 1994)          *********

	KRN_EXP_2(0xE000, "KRN Exp (2)", "continuation of EXP function", true, false),
	KRN_ODD_DEG_TAB(0xE043, "KRN Odd degrees for SIN ", "compute odd degrees for SIN and ATN", true, false),
	KRN_COMP_POLY(0xE059, "KRN ", "compute polynomials according to table indexed by AY", true, false),
//	E08D	float numbers for RND
	KRN_(0xE097, "KRN ", "RND function", true, false),
	KRN_IO_ERR(0xE0F9, "KRN IO error", "handle errors for direct I/O calls from basic", true, false),
	KRN_SYS(0xE12A, "KRN SYS", "SYS command", true, false),
	KRN_SAVE(0xE156, "KRN SAVE", "SAVE command", true, false),
	KRN_VERIFY(0xE165, "KRN VERIFY", "VERIFY command", true, false),
	KRN_LOAD(0xE168, "KRN LOAD", "LOAD command", true, false),
	KRN_OPEN(0xE1BE, "KRN OPEN", "OPEN command", true, false),
	KRN_CLOSE(0xE1C7, "KRN CLOSE", "CLOSE command", true, false),
	KRN_IO_PARAM(0xE1D4, "KRN Set IO parameters", "set parameters for load/verify/save", true, false),
	KRN_COMMA_X(0xE200, "KRN Get ,X", "skip comma and get integer in X", true, false),
	KRN_GETC_EOL(0xE206, "KRN Get char + eol", "get character and check for end of line", true, false),
	KRN_SKIP_COMMA(0xE20E, "KRN Skip comma", "check for comma and skip it", true, false),
	KRN_OPEN_PARAM(0xE219, "KRN Get open/close parameters", "get open/close parameters", true, false),
	KRN_COS(0xE264, "KRN COS", "COS function", true, false),
	KRN_SIN(0xE26B, "KRN SIN", "SIN function", true, false),
	KRN_TAN(0xE2B4, "KRN TAN", "TAN function", true, false),
//	float numbers for SIN, COS and TAN

//	E2E0	0.5 * PI
//	E2E5	2 * PI
//	E2EA	0,25
//	E2EF	polynomial table
	KRN_ATN(0xE30E, "KRN ATN", "ATN function", true, false),
//	float numbers for ATN
//	E33E	polynomial table
	KRN_WAR_START(0xE37B, "KRN Warm start", "warm start entry", true, false),
	KRN_ERR(0xE38B, "KRN Error message", "handle error messages", true, false),
	KRN_RESET(0xE394, "KRN Reset", "RESET routine", true, false),
	KRN_CHR_73(0xE3A2, "KRN Char 73 ", "character fetch code for zero page $0073-$008F", true, false),
//	E3BA	first RND seed value
	KRN_INIT_BASIC(0xE3BF, "KRN Init BASIC", "initialisation of basic", true, false),
	KRN_BASIC_START_MSG(0xE422, "KRN BASIC start message", "print BASIC start up messages", true, false),
//	DBG_BYTES_FREE(0xe430, "DBG Basic bytes free", "Basic bytes free", true, true),
//	E447	vectors for $0300-$030B
	KRN_INIT_VECTOR(0xE453, "KRN Initialize vectors", "initialise vectors", true, false),
//	E45F	startup messages
//	E460	"basic bytes free"
//	E473	"**** commodore 64 basic v2 ****"
//	E49A	"64k ram system"
	KRN_SET_DEV_OUT(0xE4AD, "KRN Set output device", "set output device", true, false),
//	E4B7	unused
	KRN_CLEAR_COLOR(0xE4DA, "KRN Clear color RAM", "clear byte in color ram", true, false),
	KRN_CASETTE_FOUND(0xE4E0, "KRN Casette file found", "pause after finding a file on casette", true, false),
//	E4EC	baud rate factor table
	KRN_IO_BASE(0xE500, "KRN IO device base", "read base address of I/O device into XY", true, false),
	KRN_SCR_SIZE(0xE505, "KRN Read scfeen size", "read screen size", true, false),
	KRN_CURSOR_POS(0xE50A, "KRN Cusror pos", "read/set XY cursor position", true, false),
	KRN_INIT_KEYSCR(0xE518, "KRN Initialize screen and keyboard", "initialise screen and keyboard", true, false),
	KRN_CLEARSCR(0xE544, "KRN Clear screen", "Set up line pointers and clear screen", true, false),
	KRN_SET_LINE_ADR(0xE56C, "KRN Set current line", "set address of curent screen line", true, false), 
//	0xE59A	this code is unused by kernel since no other part of the rom jumps to this location!", true, false),
	KRN_INIT_VIC(0xE5A0, "KRN Initialize VIC", "initialise vic chip", true, false),
	KRN_GETC_KEYB(0xE5B4, "KRN Get char", "get character from keyboard buffer", true, false),
	KRN_WAIT_LINE(0xE5CA, "KRN Wait for return key", "wait for return for keyboard", true, false),
	KRN_GETC03(0xE632, "KRN Get char from 0/3", "get character from device 0 or 3", true, false),
	KRN_GETC_SCR(0xE63A, "KRN Get char from screen", "get character from current screen line", true, false),
	KRN_IS_QUOTE(0xE684, "KRN Check for quote", "check for quote mark and set flag", true, false),
	KRN_FILL_SCR(0xE691, "KRN Fill screen", "fill screen at current position", true, false),
	KRN_OUTPUT_RET(0xE6A8, "KRN Return from output", "return from output to the screen", true, false),
	KRN_NEWLINE(0xE6B6, "KRN New line", "get/insert new line", true, false),
	KRN_PREVLINE(0xE701, "KRN Previous line", "move backwards over a line boundary", true, false),
	KRN_PUTC_SCR(0xE716, "KRN Put char", "put a character to screen", true, false),
	KRN_PUTC_SHIFT(0xE7D4, "KRN Put char shifted", "put shifted chars to screen", true, false),
	KRN_SET_NEXT_LINE_NO(0xE87C, "KRN Set next line number", "set next line number", true, false),
	KRN_RETURN(0xE891, "KRN Action for return", "action for return", true, false), 
	KRN_LEFT_PREVLINE(0xE8A1, "KRN Left to previous line", "move cursor to previous line if at start of line", true, false),
	KRN_RIGHT_NEXTLINE(0xE8B3, "KRN Right to next line", "move cursor to next line if at end of line", true, false),
	KRN_IS_COLOUR_CHG(0xE8CB, "KRN Check for colour change code", "check for colour change codes", true, false),
//	E8DA	colour key codes
	KRN_SCROLL(0xE8EA, "KRN Scroll screen", "scroll screen", true, false),
	KRN_BLANK_LINE(0xE965, "KRN Blank line", "insert blank line in screen", true, false),
	KRN_MOVE_LINE(0xE9C8, "KRN Move line", "move one screen line", true, false),
	KRN_SET_COLSCR_ADR(0xE9E0, "KRN Set colour and screen addresses", "set colour and screen addresses", true, false),
	KRN_GET_COLSCR_ADR(0xE9F0, "KRN Get colour and screen addresses", "fetch screen addresses", true, false),
	KRN_CLR_LINE(0xE9FF, "KRN Clear line", "clear one screen line", true, false),
	KRN_SET_BLINK_COL(0xEA13, "KRN Set cusror blink and colour adr", "set cursor flash timing and colour memory addresses", true, false),
	KRN_PRINTC(0xEA1C, "KRN Print char", "put a char on the screen", false, false),
	KRN_SET_COL_PAR(0xEA24, "KRN Set col par", "set colour memory adress parallel to screen", false, false),
	KRN_IRQ_HANDLER(0xEA31, "KRN IRQ handler", "normal IRQ interrupt", false, false),
	KRN_SCAN_KEYB(0xEA87, "KRN Scan keyboard", "scan keyboard", false, false),
	KRN_SELECT_KEYB(0xEB64, "KRN Select keyboard table", "select keyboard table", true, false),
//	EB79	table addresses
//	EB79   .WD $EB81   ; standard
//	EB7B   .WD $EBC2   ; shift
//	EB7D   .WD $EC03   ; commodore key
//	EB7F   .WD $EC78   ; control
//	EB81	standard keyboard table
//	EBC2	shift keyboard table
//	EC03	commodore key keyboard table
	KRN_SPECIAL_CHAR(0xEC44, "KRN Check for special petscii", "check for special petscii codes", true, false),
	KRN_SHIFT_CMD(0xEC5E, "KRN Shift + commodore", "shift + commodore key check", true, false),
//	EC78	control keyboard table
//	ECB9	default values for VIC chip
//	ECE7	"load"
//	ECEC	"run"
//	ECF0	low bytes of screen line addresses
	KRN_TALK(0xED09, "KRN Send talk", "send talk on serial bus", true, false),
	KRN_LISTEN(0xED0C, "KRN Send listen", "send listen on serial bus", true, false),
	KRN_SEND_95(0xED40, "KRN Send byte $95", "send byte from $95 on serial bus", true, false),
	KRN_SEND_ADR_LISTEN(0xEDB9, "KRN Send adr", "send secondary address (listen) on serial bus", true, false),
	KRN_SEND_ADR_TALK(0xEDC7, "KRN Send adr talk", "send secondary address (talk) on serial bus", true, false),
	KRN_SEND(0xEDDD, "KRN Send byte", "output byte on serial bus", true, false),
	KRN_UNTALK(0xEDEF, "KRN Send untalk", "send untalk on serial bus", true, false),
	KRN_UNLISTEN(0xEDFE, "KRN Unlisten", "send unlisten on serial bus", true, false),
	KRN_RECV(0xEE13, "KRN Recieve", "input byte on serial bus", true, false),
	KRN_CLOCK_LOW(0xEE85, "KRN Clock low", "set serial clock line low", true, false),
	KRN_CLOCK_HIGH(0xEE8E, "KRN Clock high", "set serial clock line high", true, false),
	KRN_DATA_LOW(0xEE97, "KRN Data low", "set serial data line low", true, false),
	KRN_DATA_HIGH(0xEEA0, "KRN Data high", "set serial data line high", true, false),
	KRN_MS_SLEEP(0xEEB3, "KRN Sleep 1ms", "delay 1 millisecond", true, false),
	KRN_SET_SEND_BIT(0xEEBB, "KRN Set bit to send", "set next bit to transmit on RS-232", true, false),
	KRN_SER_ERROR(0xEF2E, "KRN Serial error", "handle RS-232 errors", true, false),
	KRN_CHECK_CONTROL(0xEF4A, "KRN Check control registers", "check control register", true, false),
	KRN_ADD_BIT(0xEF59, "KRN Add input bit", "add bit input on RS-232 bus to word being input", true, false),
	KRN_END_WORD(0xEF6E, "KRN End word", "handle end of word for RS-232 input", true, false),
	KRN_RECV_BYTES(0xEF7E, "KRN Enable byte reception", "enable byte reception", true, false),
	KRN_RCV_START(0xEF90, "KRN Receiver start", "receiver start bit test", true, false),
	KRN_PUT_RECV(0xEF97, "KRN Put received data", "put received data into RS-232 buffer", true, false),
	KRN_SER_OUTPUT(0xEFE1, "KRN Get serial", "output of RS-232 device", true, false),
	KRN_PUTC_SER(0xF014, "KRN Buffer char to send", "buffer char to output on RS-232", true, false),
	KRN_INIT_SER(0xF04D, "KRN Initialize serial", "initalise RS-232 input", true, false),
	KRN_GETC_SER(0xF086, "KRN Get char from serial", "get next character from RS-232 input buffer", true, false),
	KRN_PAUSE_NMI(0xF0A4, "KRN Pause NMI", "protect serial/casette routine from RS-232 NMI's", true, false),
//	kernal I/O messages
//	F0BD	"I/O error"
//	F0C9	"searching for"
//	F0D8	"pre       ss play on tape"
//	F0EB	"press record and play on tape"
//	F106	"loading"
//	F10E	"saving"
//	F116	"verifying"
//	F120	"found"
//	F127	"ok"
	KRN_MSG(0xF12B, "KRN Print kernal message", "print kernal message indexed by Y", true, false),
	KRN_GETC(0xF13E, "KRN Get char", "get a character", true, false),
	KRN_INPC(0xF157, "KRN Input char", "input a character", true, false),
	KRN_GET_CASS(0xF199, "KRN Read a byte from cassette", "read a byte from cassette buffer", true, false),
	KRN_GET_SER(0xF1B5, "KRN Read a byte from serial", "read a byte from serial bus", true, false),
	KRN_GET_RS232(0xF1B8, "KRN Read a byte from RS-232", "read a byte from RS-232 bus", true, false),
	KRN_PUTC(0xF1CA, "KRN Put character", "output a character", true, false),
	KRN_SET_IN(0xF20E, "KRN Set input device", "set input device", true, false),
	KRN_SET_SER_IN(0xF237, "KRN Set serial input device", "set serial bus input device", true, false),
	KRN_SET_OUT(0xF250, "KRN Set output device", "set output device", true, false),
	KRN_SET_SER_OUT(0xF279, "KRN Set serial output device", "set serial bus output device", true, false),
	KRN_FCLOSE(0xF291, "KRN Close", "close a file", true, false),
	KRN_CLOSE_CASS(0xF2C8, "KRN Close cassette", "close cassette device", true, false),
	KRN_CLOSE_SER(0xF2EE, "KRN Close serial", "close serial bus device", true, false),
	KRN_REORG_DILE(0xF2F2, "KRN Reorganize file tables", "reorganise file tables", true, false),
	KRN_CHK_FILE(0xF30F, "KRN Check file table", "check X against logical file table", true, false),
	KRN_SET_FILE_PARAM(0xF31F, "KRN Set file parameters", "set file parameters depending on X", true, false),
	KRN_CLOSE_ALL(0xF32F, "KRN Close all", "close all files", true, false),
	KRN_IO_DEFAULT(0xF333, "KRN Restore I/O to default devices", "restore I/O to default devices", true, false),
	KRN_FOPEN(0xF34A, "KRN Open", "open a file", true, false),
	KRN_OPEN_CASS(0xF38B, "KRN Open cassette", "open for cassette device", true, false),
	KRN_OPEN_CASS_RD(0xF3B8, "KRN Open cassette for input", "open cassette for input", true, false),
	KRN_OPEN_SER(0xF3D5, "KRN Open serial", "open for serial bus devices", true, false),
	KRN_OPEN_RS232(0xF409, "KRN Open RS-232", "open RS-232 device", true, false),
	KRN_INIT_CIA2(0xF483, "KRN Initialize CIA2", "initialise CIA2", true, false),
	KRN_LOAD_RAM(0xF49E, "KRN Load RAM", "load ram from a device", true, false),
	KRN_LOAD_RAM_ENTRY(0xF4A5, "KRN Load RAM entry", "standard load ram entry", true, false),
	KRN_LOAD_MSG(0xF5AF, "KRN Load message", "handle messages for loading", true, false),
	KRN_LOAD_VERIFY_MSG(0xF5D2, "KRN Load/verify message", "do load/verify message", true, false),
	KRN_SAVE_RAM(0xF5DD, "KRN Save RAM", "save ram to a device", true, false),
	KRN_SAVE_RAM_ENTRY(0xF5ED, "KRN Save RAM entry", "standard save ram entry", true, false),
	KRN_CLOSE_SER2(0xF642, "KRN Close serial bus", "close serial bus device", true, false),
	KRN_SAVE_RAM_CAS(0xF65F, "KRN Save RAM to cassette", "save ram to cassette", true, false),
	KRN_SAVE_MSG(0xF68F, "KRN Save message", "do saving message and filename", true, false),
	KRN_INC_CLOCK(0xF69B, "KRN Increment clock", "increment real time clock", false, false),
	KRN_GET_CLOCK(0xF6DD, "KRN Get clock", "read real time clock", true, false),
	KRN_SET_CLOCK(0xF6E4, "KRN Set clock", "set real time clock", true, false),
	KRN_IS_STOP(0xF6ED, "KRN Check for STOP key", "test STOP key", true, false),

//	F6FB	handle I/O errors

//F6FB   A9 01      LDA #$01   ; too many files
//F6FD   .BY $2C
//F6FE   A9 02      LDA #$02   ; file open
//F700   .BY $2C
//F701   A9 03      LDA #$03   ; file not open
//F703   .BY $2C
//F704   A9 04      LDA #$04   ; file not found
//F706   .BY $2C
//F707   A9 05      LDA #$05   ; device not present
//F709   .BY $2C
//F70A   A9 06      LDA #$06   ; not input file
//F70C   .BY $2C
//F70D   A9 07      LDA #$07   ; not output file
//F70F   .BY $2C
//F710   A9 08      LDA #$08   ; file name missing
//F712   .BY $2C
//F713   A9 09      LDA #$09   ; illegal device no.
	KRN_NEXT_FILE(0xF72C, "KRN Next file", "get next file header from cassette", true, false),
	KRN_SPEC_CAS(0xF76A, "KRN Write special block", "write a special block to cassette with code in A", true, false),
	KRN_SET_TAPE_PTR(0xF7D0, "KRN ", "set tape buffer pointer in XY", true, false),
	KRN_SET_CAS_IO(0xF7D7, "KRN Set cassette buffer to I/O", "set cassette buffer to I/O area", true, false),
	KRN_SEARCH_TAPE(0xF7EA, "KRN Search tape", "search tape for a file name", true, false),
	KRN_INC_TAPE(0xF80D, "KRN Add 1 to tape index", "add 1 to tape index and test for overflow", true, false),
	KRN_CAS_BTN(0xF817, "KRN Cassette buttons", "handle messages and test cassette buttons for read", true, false),
	KRN_CAS_BTN_DN(0xF82E, "KRN Cassette button down", "test sense line for a button depressed on cassette", true, false),
	KRN_CAS_INPUT(0xF838, "KRN Cassette input", "set messages and test cassette line for input", true, false),
	KRN_READ_BLOCK(0xF841, "KRN Read block", "read a block from cassette", true, false),
	KRN_WRITE_BLOCK(0xF864, "KRN Write block", "write a block from cassette", true, false),
	KRN_CAS_RW(0xF875, "KRN Common code for cassette read/write", "common code for cassette read and write", true, false),
	KRN_CAS_STOP(0xF8D0, "KRN Cassette stop key", "handle stop key during cassette operations", true, false),
	KRN_SET_TIMER_A(0xF8E2, "KRN Set timer A", "schedule CIA1 timer A depending on X", true, false),
	KRN_CASSETTE_READ_IRQ(0xF92C, "KRN Cassette read IRQ", "cassette read IRQ routine", true, false),
	KRN_CAS_RECV(0xFA60, "KRN Recevie next byte from cassette", "receive next byte from cassette", true, false),
	KRN_IO_ADR_AC(0xFB8E, "KRN Move save/load address into $AC/$AD", "move save/load address into $AC/$AD", true, false),
	KRN_INIT_CAS(0xFB97, "KRN Initalise cassette read/write variables", "initalise cassette read/write variables", true, false),
	KRN_SET_TIMER_B(0xFBA6, "KRN Set timer B", "schedule CIA1 timer B and invert casette write line", true, false),
	KRN_CASSETTE_WRITE_IRQ(0xFBC8, "KRN Cassette write IRQ", "IRQ routine for cassette write B", true, false),
	KRN_CASSETE_WRITE_A(0xFC6A, "KRN Cassette write A", "IRQ routine for cassette write A", true, false),
	KRN_RESET_CAS_IRQ(0xFC93, "KRN Reset cassette IRQ", "switch from cassette IRQ to default IRQ", true, false),
	KRN_STOP_CAS_IO(0xFCB8, "KRN Terminate cassette I/O", "terminate cassette I/O", true, false),
	KRN_SET_IRQ(0xFCBD, "KRN Set IRQ", "set IRQ vector depending on X", true, false),
	KRN_STOP_MOTOR(0xFCCA, "KRN Stop motor", "stop cassette motor", true, false),
	KRN_CMP_AC_AE(0xFCD1, "KRN Compare $AC/$AD with $AE/$AF", "compare $AC/$AD with $AE/$AF", true, false),
	KRN_INC_AC(0xFCDB, "KRN Increment AC", "increment $AC/$AD", true, false),
	KRN_RESET2(0xFCE2, "KRN Reset", "RESET routine", true, false),
	KRN_CHK_CART(0xFD02, "KRN Check for cartridge", "check for a cartridge", true, false),
//	FD10	"CBM80"
	KRN_RESTORE_IO(0xFD15, "KRN Restore I/O vectors", "restore I/O vectors", true, false),
	KRN_SET_IO(0xFD1A, "KRN Set I/O vectors", "set I/O vectors depending on XY", true, false),

//	 FD30	vectors for OS at $0314-$0333

//FD30   .WD $EA31   ; IRQ
//FD32   .WD $FE66   ; BRK
//FD34   .WD $FE47   ; NMI
//FD36   .WD $F34A   ; open
//FD38   .WD $F291   ; close
//FD3A   .WD $F20E   ; set input dev
//FD3C   .WD $F250   ; set output dev
//FD3E   .WD $F333   ; restore I/O
//FD40   .WD $F157   ; input
//FD42   .WD $F1CA   ; output
//FD44   .WD $F6ED   ; test stop key
//FD46   .WD $F13E   ; get
//FD48   .WD $F32F   ; abort I/O
//FD4A   .WD $FE66   ; unused (BRK)
//FD4C   .WD $F4A5   ; load ram
//FD4E   .WD $F5ED   ; save ram


	KRN_INIT_MEMP(0xFD50, "KRN Initialize memory pointers", "initalise memory pointers", true, false),
//	FD9B	IRQ vectors
//FD9B   .WD $FC6A   ; cassette write A
//FD9D   .WD $FBCD   ; cassette write B
//FD9F   .WD $EA31   ; standard IRQ
//FDA1   .WD $F92C   ; cassette read
	KRN_INIT_IO(0xFDA3, "KRN Init I/O", "initaliase I/O devices", true, false),
	KRN_INIT_TA(0xFDDD, "KRN Initialize TA", "initalise TAL1/TAH1 fpr 1/60 of a second", true, false),
	KRN_INIT_FNAMP(0xFDF9, "KRN Initialize file name parameters", "initalise file name parameters", true, false),
	KRN_INIT_FILEP(0xFE00, "KRN Initialize file parameters", "inatalise file parameters", true, false),
	KRN_IO_STATUS(0xFE07, "KRN Read I/O status word", "read I/O status word", true, false),
	
	KRN_CTRL_MSG(0xFE18, "KRN Control kernel messages", "control kernel messages", true, false),
	KRN_READ_ST(0xFE1A, "KRN Read ST", "read ST", true, false),
	KRN_ADD_ST(0xFE1C, "KRN Add a to STS", "add A to ST", true, false),
	KRN_SET_SER_TIMEOUT(0xFE21, "KRN Set timeout on serial bus", "set timeout on serail bus", true, false),
	KRN_MEM_TOP(0xFE25, "KRN Read/set top of memory", "read/set top of memory", true, false),
	KRN_MEM_BOT(0xFE34, "KRN Read/set bottom of memoty", "read/set bottom of memory", true, false),
	KRN_NMI_ENTRY(0xFE43, "KRN NMI entry", "NMI entry", true, false),
	KRN_NMI(0xFE47, "KRN NMI", "standard NMI routine", true, false),
	KRN_BRK(0xFE66, "KRN BRK", "BRK routine", true, false),
	KRN_NMI_INTERNAL(0xFE72, "KRN NMI internal", "internal NMI", true, false),
//	FEC2	baud rate tables
	KRN_INPUT_BIT(0xFED6, "KRN Input bit", "input next bit on RS-232 and schedule TB2", true, false),
	KRN_SCHED_TB2(0xFF07, "KRN Schedule TB2", "schedule TB2 using baud rate factor", true, false),
	KRN_BAUD_2(0xFF2E, "KRN Boud rate 2", "continuation of baud rate calculation", true, false),
	KRN_IRQ(0xFF48, "KRN IRQ", "IRQ entry point", false, false),
	KRN_IO_INIT_2(0xFF5B, "KRN IO init 2", "addition to I/O device initalisation", true, false),
	KRN_SCHED_TA_2(0xFF6E, "KRN Schedule TA 2", "end of scheduling TA for 1/60 second IRQ's", true, false),
//	FF80	kernal version number

	//	FF81	kernal vectors
//FF81   4C 5B FF   JMP $FF5B   ; initalise screen and keyboard
//FF84   4C A3 FD   JMP $FDA3   ; initalise I/O devices
//FF87   4C 50 FD   JMP $FD50   ; initalise memory pointers
//FF8A   4C 15 FD   JMP $FD15   ; restore I/O vectors
//FF8D   4C 1A FD   JMP $FD1A   ; set I/O vectors from XY
//FF90   4C 18 FE   JMP $FE18   ; control kernal messages
//FF93   4C B9 ED   JMP $EDB9   ; read secondary address after listen
//FF96   4C C7 ED   JMP $EDC7   ; read secondary address after talk
//FF99   4C 25 FE   JMP $FE25   ; read/set top of memory
//FF9C   4C 34 FE   JMP $FE34   ; read/set bottom of memory
//FF9F   4C 87 EA   JMP $EA87   ; scan keyboard
//FFA2   4C 21 FE   JMP $FE21   ; set timout for serial bus
//FFA5   4C 13 EE   JMP $EE13   ; input on serial bus
//FFA8   4C DD ED   JMP $EDDD   ; output byte on serial bus
//FFAB   4C EF ED   JMP $EDEF   ; send untalk on serial bus
//FFAE   4C FE ED   JMP $EDFE   ; send unlisten on serial bus
//FFB1   4C 0C ED   JMP $ED0C   ; send listen on serial bus
//FFB4   4C 09 ED   JMP $ED09   ; send talk on serial 
//FFB7   4C 07 FE   JMP $FE07   ; read I/O status word
//FFBA   4C 00 FE   JMP $FE00   ; set file 
//FFBD   4C F9 FD   JMP $FDF9   ; set filename parameters
//FFC0   6C 1A 03   JMP ($031A)   ; (F34A) open a 
//FFC3   6C 1C 03   JMP ($031C)   ; (F291) close a file
//FFC6   6C 1E 03   JMP ($031E)   ; (F20E) set input device
//FFC9   6C 20 03   JMP ($0320)   ; (F250) set output device
//FFCC   6C 22 03   JMP ($0322)   ; (F333) restore I/O devices to default
//FFCF   6C 24 03   JMP ($0324)   ; (F157) input char on current device
//FFD2   6C 26 03   JMP ($0326)   ; (F1CA) output char on current device
//FFD5   4C 9E F4   JMP $F49E   ; load ram from device
//FFD8   4C DD F5   JMP $F5DD   ; save ram to device
//FFDB   4C E4 F6   JMP $F6E4   ; set real time clock
//FFDE   4C DD F6   JMP $F6DD   ; read real time clock
//FFE1   6C 28 03   JMP ($0328)   ; (F6ED) check stop key
//FFE4   6C 2A 03   JMP ($032A)   ; (F13E) get a character
//FFE7   6C 2C 03   JMP ($032C)   ; (F32F) close all channels and files
//FFEA   4C 9B F6   JMP $F69B   ; increment real time clock
//FFED   4C 05 E5   JMP $E505   ; read organisation of screen into XY
//FFF0   4C 0A E5   JMP $E50A   ; read/set XY cursor position
//FFF3   4C 00 E5   JMP $E500   ; read base address of I/O devices

//	FFF6	unused
//	FFFA	NMI vector
//	FFFC	RESET vector
//	FFFE	IRQ/BRK vector

	// Interrupt/reset
	NMI(0xfffa, "NMI", "NMI vector", true, false), RESET(0xfffc, "RESET", "RESET vector", true, false),
	IRQ_BRK(0xfffe, "IRQ/BRK", "IRQ/BRK vector", true, false),

	// Kernel jump table
	KRN_INIT_SCR_V(0xff81, "KRN init screen", "Kernel vector initialise screen and keyboard", true, false),
	KRN_INIT_IO_V(0xff84, "KRN init IO", "Kernel vector initialise I/O devices", true, false),
	KRN_INIT_MEM_V(0xff87, "KRN init mem", "Kernel vector initialise memory pointers", true, false),
	KRN_RST_IO_V(0xff8a, "KRN restore IO", "Kernel vector restore I/O vectors", true, false),
	KRN_SET_IO_V(0xff8d, "KRN set IO", "Kernel vector set I/O vectors from XY", true, false),
	KRN_CTRL_MSG_V(0xff90, "KRN ctrl msg", "Kernel vector control kernel messages", true, false),
	KRN_RD2_LISTEN_V(0xff93, "KRN read 2 listen", "Kernel vector read secondary address after listen ", true, false),
	KRN_RD_TALK_V(0xff96, "KRN read 2 talk", "Kernel vector read secondary address after talk", true, false),
	KRN_RS_TOP_V(0xff99, "KRN read/set top", "Kernel vector read/set top of memory", true, false),
	KRN_RS_BOTTOM_V(0xff9c, "KRN read/set bottom", "Kernel vector read/set bottom of memory", true, false),
	KRN_SCAN_KEY_V(0xff9f, "KRN  scan key", "Kernel vector scan keyboard", true, false),
	KRN_SERIAL_TO_V(0xffa2, "KRN serial timeout", "Kernel vector set timeout for serial bus", true, false),
	KRN_SERIAL_IN_V(0xffa5, "KRN serial in", "Kernel vector input on serial bus", true, false),
	KRN_SERIAL_OUT_V(0xffa8, "KRN serial out", "Kernel vector output on serial bus", true, false),
	KRN_UNTALK_V(0xffab, "KRN untalk", "Kernel vector send untalk on serial bus", true, false),
	KRN_UNLISTEN_V(0xffae, "KRN unlisten", "Kernel vector send unlisten on serial bus", true, false),
	KRN_LISTEN_V(0xffb1, "KRN listen", "Kernel vector send listen on serial bus", true, false),
	KRN_TALK_V(0xffb4, "KRN talk", "Kernel vector send talk on serial bus", true, false),
	KRN_IO_STATUS_V(0xffb7, "KRN IO status", "Kernel vector read I/O status word", true, false),
	KRN_FILE_PARAM_V(0xffba, "KRN set file param", "Kernel vector set file parameters", true, false),
	KRN_FNAM_PARAM_V(0xffbd, "KRN set filename param", "Kernel vector set filename parameters", true, false),
	KRN_FOPEN_V(0xffc0, "KRN open file", "Kernel vector open a file", true, false),
	KRN_FCLOSE_V(0xffc3, "KRN close file", "Kernel vector close a file", true, false),
	KRN_SET_IN_V(0xffc6, "KRN set in", "Kernel vector set input device", true, false),
	KRN_SET_OUT_V(0xffc9, "KRN set out", "Kernel vector set output device", true, false),
	KRN_IO_DEF_V(0xffcc, "KRN default IO", "Kernel vector restore I/O devices to default", true, false),
	KRN_CHAR_IN_V(0xffcf, "KRN char in", "Kernel vector input char on current device", true, false),
	KRN_CHAR_OUT_V(0xffd2, "KRN char out", "Kernel vector output char on current device", true, false),
	KRN_LOAD_RAM_V(0xffd5, "KRN load ram", "Kernel vector load ram from device", true, false),
	KRN_SAVE_RAM_V(0xffd8, "KRN save ram", "Kernel vector save ram to device", true, false),
	KRN_SET_CLK_V(0xffdb, "KRN set clock", "Kernel vector set real time clock", true, false),
	KRN_READ_CLK_V(0xffde, "KRN read clock", "Kernel vector read real time clock", true, false),
	KRN_CHK_STOP_V(0xffe1, "KRN stop key", "Kernel vector check stop key", true, false),
	KRN_GET_CHAR_V(0xffe4, "KRN get char ", "Kernel vector get a character", true, false),
	KRN_CLOSE_ALL_V(0xffe7, "KRN close all", "Kernel vector close all channels and files", true, false),
	KRN_INC_CLK_V(0xffea, "KRN increment clock", "Kernel vector increment real time clock", false, false),
	KRN_READ_SCREEN_ORG_V(0xffed, "KRN read screen organisation", "Kernel vector read organisation of screen into XY",
			true, false),
	KRN_READSET_CURSOR_V(0xfff0, "KRN read/set cursor", "Kernel vector read/set XY cursor position", true, false),
	KRN_IO_BASE_V(0xfff3, "KRN read I/O base", "Kernel vector read base address of I/O devices", true, false);

	int address;
	String name, description;
	public boolean print;
	public boolean brk;

	private ROMCall(int address, String name, String description, boolean print, boolean brk) {
		this.address = address;
		this.name = name;
		this.description = description;
		this.print = print;
		this.brk = brk;
	}

	@Override
	public String toString() {
		return String.format("%04x: %s", address, name);
	}
	
	private final static Map<Integer, ROMCall> calls = new HashMap<>();
	static {
		for (ROMCall call: ROMCall.values()) {
			calls.put(call.address, call);
		}
	}
	public static boolean exists(int adr) {
		return calls.containsKey(adr);
	}
	public static ROMCall get(int adr) {
		return calls.get(adr);
	}

	public int getAddress() {
		return address;
	}
	public boolean isPrint() {
		return print;
	}
	public boolean isBrk() {
		return brk;
	}
	public String getName() {
		return name;
	}
	public String getDescription() {
		return description;
	}
	public void setPrint(boolean print) {
		this.print = print;
	}
	public void setBrk(boolean brk) {
		this.brk = brk;
	}

	public static void NewPrefs(Map<ROMCall, CPUPrefs.CallPrefs> prefs) {
		for (CPUPrefs.CallPrefs cp: prefs.values()) {
			ROMCall call = cp.getCall();
			call.setPrint(cp.isPrint());
			call.setBrk(cp.isBrk());
		}
	}
}