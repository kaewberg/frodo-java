package se.pp.forsberg.frodo.enums;

public enum DriveType {
	DRVTYPE_DIR("File system"),	// 1541 emulation in host file system
	DRVTYPE_D64(".d64"),		// 1541 emulation in .d64 file
	DRVTYPE_T64(".t64");		// 1541 emulation in .t64 file
	private String label;
	private DriveType(String label) {
		this.label = label;
	}
	public String getLabel() {
		return label;
	}
	@Override
	public String toString() {
		return label;
	}
}
