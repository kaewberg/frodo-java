package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.MOS6569.DISPLAY_X;
import static se.pp.forsberg.frodo.MOS6569.DISPLAY_Y;
import static se.pp.forsberg.frodo.enums.DriveLEDState.DRVLED_OFF;
import static se.pp.forsberg.frodo.enums.DriveLEDState.DRVLED_ON;

import java.awt.Toolkit;
import java.io.File;
import java.nio.IntBuffer;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Optional;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.image.WritablePixelFormat;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Duration;
import se.pp.forsberg.frodo.enums.DriveLEDState;
import se.pp.forsberg.frodo.prefs.CPUPrefs;
import se.pp.forsberg.frodo.prefs.CPUPrefsUI;
import se.pp.forsberg.frodo.prefs.KeyMapping;
import se.pp.forsberg.frodo.prefs.KeyMapping.RC;
import se.pp.forsberg.frodo.prefs.KeyboardPrefs;
import se.pp.forsberg.frodo.prefs.KeyboardPrefsUI;
import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.frodo.prefs.PrefsUI;
import se.pp.forsberg.libc.ByteC;

class C64Display {

	// C64 color palette (more realistic looking colors)
	final static byte[] palette_red = toByte(new int[] {
		0x00, 0xff, 0x99, 0x00, 0xcc, 0x44, 0x11, 0xff, 0xaa, 0x66, 0xff, 0x40, 0x80, 0x66, 0x77, 0xc0
	});

	final static byte[] palette_green = toByte(new int[] {
		0x00, 0xff, 0x00, 0xff, 0x00, 0xcc, 0x00, 0xff, 0x55, 0x33, 0x66, 0x40, 0x80, 0xff, 0x77, 0xc0
	});

	final static byte[] palette_blue = toByte(new int[] {
		0x00, 0xff, 0x00, 0xcc, 0xcc, 0x44, 0x99, 0x00, 0x00, 0x00, 0x66, 0x40, 0x80, 0x66, 0xff, 0xc0
	});


	/*
	 *  Update drive LED display (deferred until Update())
	 */


	public C64Display(C64 the_c64) {
		TheC64 = the_c64;		
		int i;

		// LEDs off
		for (i=0; i<4; i++)
			led_state[i] = DRVLED_OFF;

		// Allocate chunky buffer to draw into
		display_buffer = new byte[DISPLAY_X * DISPLAY_Y];

		// Open fonts
//		led_font = OpenDiskFont(&led_font_attr);
//		speedo_font = OpenDiskFont(&speedo_font_attr);

		// Open window on default pubscreen
//		the_window = OpenWindowTags(NULL,
//			WA_Left, 0,
//			WA_Top, 0,
//			WA_InnerWidth, DISPLAY_X,
//			WA_InnerHeight, DISPLAY_Y + 16,
//			WA_Title, (ULONG)"Frodo",
//			WA_ScreenTitle, (ULONG)"Frodo C64 Emulator",
//			WA_IDCMP, IDCMP_CLOSEWINDOW | IDCMP_RAWKEY | IDCMP_MENUPICK | IDCMP_REFRESHWINDOW,
//			WA_DragBar, TRUE,
//			WA_DepthGadget, TRUE,
//			WA_CloseGadget, TRUE,
//			WA_SimpleRefresh, TRUE,
//			WA_Activate, TRUE,
//			WA_NewLookMenus, TRUE,
//			TAG_DONE);
//		the_screen = the_window->WScreen;
//		the_rp = the_window->RPort;
//		xo = the_window->BorderLeft;
//		yo = the_window->BorderTop;

		// Create menus
//		the_visual_info = GetVisualInfo(the_screen, NULL);
//		the_menus = CreateMenus(new_menus, GTMN_FullMenu, TRUE, TAG_DONE);
//		LayoutMenus(the_menus, the_visual_info, GTMN_NewLookMenus, TRUE, TAG_DONE);
//		SetMenuStrip(the_window, the_menus);

		// Obtain 16 pens for the C64 colors
//		for (i=0; i<16; i++)
//			pens[i] = ObtainBestPen(the_screen->ViewPort.ColorMap,
//				palette_red[i] * 0x01010101, palette_green[i] * 0x01010101, palette_blue[i] * 0x01010101);

		// Allocate temporary RastPort for WritePixelArra8()
//		temp_bm = AllocBitMap(DISPLAY_X, 1, 8, 0, NULL);
//		InitRastPort(&temp_rp);
//		temp_rp.BitMap = temp_bm;

		// Draw LED bar
//		draw_led_bar();

		// Allocate file requesters
//		open_req = (struct FileRequester *)AllocAslRequestTags(ASL_FileRequest,
//			ASLFR_Window, (ULONG)the_window,
//			ASLFR_SleepWindow, TRUE,
//			ASLFR_TitleText, (ULONG)"Frodo: Load snapshot...",
//			ASLFR_RejectIcons, TRUE,
//			TAG_DONE);
//		save_req = (struct FileRequester *)AllocAslRequestTags(ASL_FileRequest,
//			ASLFR_Window, (ULONG)the_window,
//			ASLFR_SleepWindow, TRUE,
//			ASLFR_TitleText, (ULONG)"Frodo: Save snapshot...",
//			ASLFR_DoSaveMode, TRUE,
//			ASLFR_RejectIcons, TRUE,
//			TAG_DONE);
	}

//		~C64Display();

	private final static boolean EXPAND = true;

	@SuppressWarnings("unused")
	private Stage stage;
	private Scene scene;
	private ImageView imageView;
	private PixelWriter img;
	private Rectangle leds[] = new Rectangle[4];
	private Label speed;
	private int imgW, imgH;

	private Stage keyboardCheat;
	private FileChooser fileChooserSnap;
	
	public void JavaFXInit(Stage stage) {
		this.stage = stage;
		BorderPane pane = new BorderPane();
		Node center;
		if (EXPAND) {
			imgW = DISPLAY_X*2 - 1;
			imgH = DISPLAY_Y*2 - 1;
		} else {
			imgW = DISPLAY_X;
			imgH = DISPLAY_Y;
		}
		WritableImage image = new WritableImage(imgW, imgH);
		img = image.getPixelWriter();
		imageView = new ImageView(image);
		if (!EXPAND) {
			imageView.setFitWidth(DISPLAY_X*2);
			imageView.setFitHeight(DISPLAY_Y*2);
			WritableImage scanline = new WritableImage(DISPLAY_X*2, DISPLAY_Y*2);
			PixelWriter pw = scanline.getPixelWriter();
			Color black = new Color(0, 0, 0, 0.5);
			for (int y = 0; y < DISPLAY_Y*2; y += 2) {
				for (int x = 0; x < DISPLAY_X * 2; x++) {
					pw.setColor(x, y, black);
				}
			}
			StackPane stack = new StackPane();
			stack.getChildren().add(imageView);
			stack.getChildren().add(new ImageView(scanline));
			pane.setCenter(stack);
			center = stack;
		} else {
			pane.setCenter(imageView);
			center = imageView;
		}
		center.setOnKeyPressed(this::onKeyPressed);
		center.setOnKeyReleased(this::onKeyReleased);
		center.setFocusTraversable(true);
		center.requestFocus();
		
//		right = new SwingNode();
//		JPanel j = new JPanel();
//		j.setPreferredSize(new Dimension(100, 20));
//		j.setSize(100,  20);
//		SwingUtilities.invokeLater(() -> right.setContent(j));
//		KeyAdapter kl = new KeyAdapter() {
//			public void keyPressed(java.awt.event.KeyEvent e) {
//				System.out.println(e.getKeyLocation());
//			};
//		};
//		j.addKeyListener(kl );
//		pane.setRight(right);
		
		HBox statusbar = new HBox();
		for (int i = 0; i < 4; i++) {
			HBox drive = new HBox();
			drive.getStyleClass().add("drive-box");
			drive.getChildren().add(new Label("Drive " + (8 + i)));
			Rectangle led = new Rectangle(20, 8);
			leds[i] = led;
			drive.getChildren().add(led);
			statusbar.getChildren().add(drive);
		}
		speed = new Label("100%");
		statusbar.getChildren().add(speed);
		pane.setBottom(statusbar);

		pane.setTop(createMenuBar());
		
		scene = new Scene(pane); // 384, 280);
		stage.setTitle("Frodo C=64");
		stage.setScene(scene);
		
//		EventDispatcher sceneEventDispatcher = scene.getEventDispatcher();
//        EventDispatcher borderPaneEventDispatcher = borderPane.getEventDispatcher();
//        EventDispatcher scrollPaneEventDispatcher = scrollPane.getEventDispatcher();
//
//        scene.setEventDispatcher((event, tail) -> {
//            if (KeyEvent.ANY.equals(event.getEventType().getSuperType())) {
//                System.out.println("DISPATCH\tScene\t\tevent=" + event.getEventType());
//            }
//            return sceneEventDispatcher.dispatchEvent(event, tail);
//        });
//
//        stackPane.setEventDispatcher((event, tail) -> {
//            if (KeyEvent.ANY.equals(event.getEventType().getSuperType())) {
//                System.out.println("DISPATCH\tStackPane\tevent=" + event.getEventType());
//            }
//            return stackPaneEventDispatcher.dispatchEvent(event, tail);
//        });
//
//        scrollPane.setEventDispatcher((event, tail) -> {
//            if (KeyEvent.ANY.equals(event.getEventType().getSuperType())) {
//                System.out.println("DISPATCH\tScrollPane\tevent=" + event.getEventType());
//            }
//            Event eventToDispatch = scrollPaneEventDispatcher.dispatchEvent(event, tail);
//            if (KeyEvent.KEY_PRESSED.equals(event.getEventType())) {
//                if (KeyCode.LEFT.equals(((KeyEvent) event).getCode()) || KeyCode.RIGHT.equals(((KeyEvent) event).getCode())) {
//                    if (eventToDispatch == null) {
//                        return event;
//                    }
//                }
//            }
//            return eventToDispatch;
//        });

		Timeline timeline = new Timeline(new KeyFrame(
		        Duration.millis(400),
		        this::blinkenLeds));
		timeline.setCycleCount(Animation.INDEFINITE);
		timeline.play();
		stage.show();
		
		fileChooserSnap = new FileChooser();
		fileChooserSnap.setInitialDirectory(Frodo.frodoDir);
		fileChooserSnap.getExtensionFilters().addAll(
				new ExtensionFilter("Frodo snapshot", "*.snp")
		);
	}

	private Node createMenuBar() {
		MenuBar menuBar = new MenuBar();
		Menu menu;
		MenuItem item;
		CheckMenuItem check;
		
		menu = new Menu("File");
		{
			item = new MenuItem("Open...");
			item.setOnAction(this::onOpen);
			menu.getItems().add(item);

			item = new MenuItem("Save...");
			item.setOnAction(this::onSave);
			menu.getItems().add(item);
			
			item = new MenuItem("Preferences...");
			item.setOnAction(this::onPreferences);
			menu.getItems().add(item);
		
			item = new MenuItem("  Keyboard...");
			item.setOnAction(this::onKeyboardPreferences);
			menu.getItems().add(item);
		
			item = new MenuItem("Quit");
			item.setOnAction(this::onQuit);
			item.setAccelerator(KeyCombination.keyCombination("F10"));
			menu.getItems().add(item);
			
			menuBar.getMenus().add(menu);
		}
		menu = new Menu("Tools");
		{
			item = new MenuItem("Run/Stop Restore (NMI)");
			item.setAccelerator(KeyCombination.keyCombination("F11"));
			item.setOnAction(e -> TheC64.NMI());
			menu.getItems().add(item);
		
			item = new MenuItem("Reset");
			item.setAccelerator(KeyCombination.keyCombination("F12"));
			item.setOnAction(e -> TheC64.Reset());
			menu.getItems().add(item);
			
			item = new MenuItem("Keyboard cheat-sheet...");
			item.setOnAction(this::onKeyboardCheat);
			menu.getItems().add(item);
			
			item = new MenuItem("SAM");
			item.setAccelerator(KeyCombination.keyCombination("F9"));
			item.setOnAction(e -> sam());
			menu.getItems().add(item);
	
			item = new CheckMenuItem("Show console");
			item.setDisable(true);
			menu.getItems().add(item);
		
			check = new CheckMenuItem("Cpu debug output");
			check.setSelected(true);
			check.setOnAction(this::onCpuDebug);
			menu.getItems().add(check);
		
			check = new CheckMenuItem("  Show interrupts");
			check.setOnAction(this::onCpuDebugInterrupt);
			menu.getItems().add(check);
		
			item = new MenuItem("  Debug settings...");
			item.setOnAction(this::onCPUPrefs);
			menu.getItems().add(item);

			menuBar.getMenus().add(menu);
		}
		return menuBar;
	}
	private void onCpuDebug(ActionEvent e) {
		CheckMenuItem check = (CheckMenuItem) e.getSource();
		TheC64.TheCPU.setDebug(check.isSelected());
	}

	private void onCpuDebugInterrupt(ActionEvent e) {
		CheckMenuItem check = (CheckMenuItem) e.getSource();
		TheC64.TheCPU.setDebugInterrupt(check.isSelected());
	}
	private void onOpen(ActionEvent e) {
		TheC64.openFile = fileChooserSnap.showOpenDialog(stage);
	}
	private void onSave(ActionEvent e) {
		TheC64.saveFile = fileChooserSnap.showSaveDialog(stage);
	}
	private void onPreferences(ActionEvent e) {
		Prefs prefs = PrefsUI.edit(Prefs.ThePrefs);
		if (prefs != null) {
			Prefs.ThePrefs = prefs;
			Prefs.ThePrefs.Save(null);
			TheC64.NewPrefs(prefs);
		}
	}
	private void onKeyboardPreferences(ActionEvent e) {
		KeyboardPrefs prefs = KeyboardPrefsUI.edit(Prefs.ThePrefs.keyboardPrefs);
		if (prefs != null) {
			Prefs.ThePrefs.keyboardPrefs = prefs;
			Prefs.ThePrefs.Save(null);
			TheC64.NewPrefs(Prefs.ThePrefs);
		}
	}
	private void onCPUPrefs(ActionEvent e) {
		CPUPrefs prefs = CPUPrefsUI.edit(Prefs.ThePrefs.cpuPrefs);
		if (prefs != null) {
			Prefs.ThePrefs.cpuPrefs = prefs;
			Prefs.ThePrefs.Save(null);
			TheC64.NewPrefs(Prefs.ThePrefs);
		}
	}
	private void onQuit(ActionEvent e) {
		quit();
	}
	void quit() {
		if (Prefs.ThePrefs.saveOnExit) {
			TheC64.saveFile = Frodo.autoSnapFile;
			while (TheC64.saveFile != null) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e1) {
				}
			}
		}
		Platform.exit();
		System.exit(0);
	}
	private void onKeyboardCheat(ActionEvent e) {
		if (keyboardCheat == null) {
			Image keyboard = new Image(getClass().getResourceAsStream("C=64 keyboard.jpg"));
			ImageView image = new ImageView(keyboard);
			image.setPreserveRatio(true);
			image.setFitWidth(500);
			keyboardCheat = new Stage();
			keyboardCheat.setScene(new Scene(new BorderPane(image)));
			keyboardCheat.setTitle("Keyboard");
			keyboardCheat.setAlwaysOnTop(true);
		}
		keyboardCheat.show();
	}

	private void exit() {
		Platform.exit();
		System.exit(0);
	}

	private void sam() {
		if (Sam.active) {
			return;
		}
		Platform.runLater(() -> Sam.sam(TheC64));
	}

	
	public void UpdateLEDs(DriveLEDState l0, DriveLEDState l1, DriveLEDState l2, DriveLEDState l3) {
		DriveLEDState[] newState = {l0, l1, l2, l3};
		Platform.runLater(() -> {
			for (int i = 0; i < 4; i++) {
				if (newState[i] == led_state[i]) {
					continue;
				}
				String cls;
				if (newState[i] == DRVLED_OFF) {
					cls = "drive-led-off";
				} else if (newState[i] == DRVLED_ON) {
					cls = "drive-led-on";
				} else {
					cls = "drive-led-error-on";
				}
//				switch (newState[i]) {
//				case DRVLED_OFF:
//					cls = "drive-led-off";
//					break;
//				case DRVLED_ON:
//					cls = "drive-led-on";
//					break;
//				case DRVLED_ERROR:
//					cls = "drive-led-error-on";
//					break;
//				}
				leds[i].getStyleClass().clear();
				leds[i].getStyleClass().add(cls);
			}
		});
	}
	private void blinkenLeds(ActionEvent e) {
		for (int i = 0; i < 4; i++) {
			if (led_state[i] != DriveLEDState.DRVLED_ERROR) {
				continue;
			}
			ObservableList<String> styles = leds[i].getStyleClass();
			String cls;
			if (styles.contains("drive-led-error-on")) {
				cls = "drive-led-error-off";
			} else {
				cls = "drive-led-error-on";
			}
			styles.clear();
			styles.add(cls);
		}
	}

	private static byte[] toByte(int[] a) {
		byte[] result = new byte[a.length];
		for (int i = 0; i < a.length; i++) {
			result[i] = (byte) a[i];
		}
		return result;
	}

	public void Update_old() {
		Platform.runLater(() -> {
			for (int y = 0; y < DISPLAY_Y; y++) {
				for (int x = 0; x < DISPLAY_X; x++) {
					byte c = (byte) (display_buffer[y * DISPLAY_X + x] & 0xf);
					int cc = 0xff000000 | ((palette_red[c] << 16) & 0xff0000) | ((palette_green[c] << 8 ) & 0xff00) | (palette_blue[c] & 0xff);
					byte n, w, nw;
					int nc = 0, wc = 0, nwc;
					
					if (EXPAND) {
						img.setArgb(x*2, y*2, cc);
						if (y > 0) {
							n = (byte) (display_buffer[(y - 1) * DISPLAY_X + x] & 0xf);
							nc = 0xff000000 | ((palette_red[n] << 16) & 0xff0000) | ((palette_green[n] << 8 ) & 0xff00) | (palette_blue[n] & 0xff);
							img.setArgb(x*2, y*2 - 1, mix(mix(cc, nc), 0));
						}
						if (x > 0) {
							w = (byte) (display_buffer[y * DISPLAY_X + (x - 1)] & 0xf);
							wc = 0xff000000 | ((palette_red[w] << 16) & 0xff0000) | ((palette_green[w] << 8 ) & 0xff00) | (palette_blue[w] & 0xff);
							img.setArgb(x*2 - 1, y*2, mix(cc, wc));
						}
						if (y > 0 && x > 0) {
							nw = (byte) (display_buffer[(y - 1) * DISPLAY_X + (x - 1)] & 0xf);
							nwc = 0xff000000 | ((palette_red[nw] << 16) & 0xff0000) | ((palette_green[nw] << 8 ) & 0xff00) | (palette_blue[nw] & 0xff);
							img.setArgb(x*2 - 1, y*2 - 1, mix(mix(mix(cc, wc), mix(nc, nwc)), 0));
						}
					} else {
						img.setArgb(x, y, cc);
					}
				}
			}//			imageView.setImage(imageView.getImage());
		});
	}
	int[] buf;
	public void Update() {
		WritablePixelFormat<IntBuffer> pf = PixelFormat.getIntArgbInstance();
		if (buf == null || buf.length != imgW*imgH) {
			buf = new int[imgW * imgH];
		}
		for (int y = 0; y < DISPLAY_Y; y++) {
			for (int x = 0; x < DISPLAY_X; x++) {
				byte c = (byte) (display_buffer[y * DISPLAY_X + x] & 0xf);
				int cc = 0xff000000 | ((palette_red[c] << 16) & 0xff0000) | ((palette_green[c] << 8 ) & 0xff00) | (palette_blue[c] & 0xff);
//				buf[y * DISPLAY_X + x] = cc;
				byte n, w, nw;
				int nc = 0, wc = 0, nwc;
				
				if (EXPAND) {
					buf[y*2*imgW + x*2] = cc;
					if (y > 0) {
						n = (byte) (display_buffer[(y - 1) * DISPLAY_X + x] & 0xf);
						nc = 0xff000000 | ((palette_red[n] << 16) & 0xff0000) | ((palette_green[n] << 8 ) & 0xff00) | (palette_blue[n] & 0xff);
						buf[(y*2 - 1)*imgW + x*2] = mix(mix(cc, nc), 0);
					}
					if (x > 0) {
						w = (byte) (display_buffer[y * DISPLAY_X + (x - 1)] & 0xf);
						wc = 0xff000000 | ((palette_red[w] << 16) & 0xff0000) | ((palette_green[w] << 8 ) & 0xff00) | (palette_blue[w] & 0xff);
						buf[y*2*imgW + x*2 - 1] = mix(cc, wc);
					}
					if (y > 0 && x > 0) {
						nw = (byte) (display_buffer[(y - 1) * DISPLAY_X + (x - 1)] & 0xf);
						nwc = 0xff000000 | ((palette_red[nw] << 16) & 0xff0000) | ((palette_green[nw] << 8 ) & 0xff00) | (palette_blue[nw] & 0xff);
						buf[(y*2 - 1)*imgW + x*2 - 1] = mix(mix(mix(cc, wc), mix(nc, nwc)), 0);
					}
				} else {
					buf[y*imgW + x] = cc;
				}
			}
		}
		Platform.runLater(() -> {
			img.setPixels(0, 0, imgW, imgH, pf, buf , 0, imgW);
		});
	}

	private int mix(int c1, int c2) {
		return ((c1 >> 1) & 0x7f7f7f7f) + ((c2 >> 1) & 0x7f7f7f7f);
	}
	public void Speedometer(int speed) {
		Platform.runLater(() -> this.speed.setText(String.format("%d%%", speed)));
	}
	public byte[] BitmapBase() {
		return display_buffer;
	}
	public int BitmapXMod() {
		return DISPLAY_X;
	}
	
	KeyMapping keyMap = KeyMapping.keyMapSvLaptop;
//	private int MATRIX(int a, int b) {
//		return ((byte) ((((a) << 3) | (b)))) & 0xff;
//	}
	private void translate_key(KeyEvent event, boolean key_up, byte[] key_matrix, byte[] rev_matrix, ByteC joystick) {
		KeyCode kc = event.getCode();
		KeyCode[] expansion = keyMap.getMultiKey(kc);
		if (expansion != null) {
			for (KeyCode kc2: expansion) {
				translate_key(kc2, key_up, key_matrix, rev_matrix, joystick);
			}
		} else if (kc == KeyCode.UNDEFINED) {
			RC rc = keyMap.getStringKey(event.getText());
			translate_key(rc, key_up, key_matrix, rev_matrix, joystick);
		} else {
			translate_key(kc, key_up, key_matrix, rev_matrix, joystick);
		}
	}
	private void translate_key(KeyCode kc, boolean key_up, byte[] key_matrix, byte[] rev_matrix, ByteC joystick) {
		translate_key(keyMap.getKey(kc), key_up, key_matrix, rev_matrix, joystick);
	}
	private void translate_key(RC rc, boolean key_up, byte[] key_matrix, byte[] rev_matrix, ByteC joystick) {
		if (rc == null) {
			return;
		}
		if (key_up) {
			key_matrix[rc.r] |= (1 << rc.c);
			rev_matrix[rc.c] |= (1 << rc.r);
		} else {
			key_matrix[rc.r] &= ~(1 << rc.c);
			rev_matrix[rc.c] &= ~(1 << rc.r);
		}
	}
/*
	private void translate_key_old(KeyEvent event, boolean key_up, byte[] key_matrix, byte[] rev_matrix, ByteC joystick) {
		int c64_key = -1;
		boolean handled = true;
		
		switch (event.getCode()) {
			case A: c64_key = MATRIX(1,2); break;
			case B: c64_key = MATRIX(3,4); break;
			case C: c64_key = MATRIX(2,4); break;
			case D: c64_key = MATRIX(2,2); break;
			case E: c64_key = MATRIX(1,6); break;
			case F: c64_key = MATRIX(2,5); break;
			case G: c64_key = MATRIX(3,2); break;
			case H: c64_key = MATRIX(3,5); break;
			case I: c64_key = MATRIX(4,1); break;
			case J: c64_key = MATRIX(4,2); break;
			case K: c64_key = MATRIX(4,5); break;
			case L: c64_key = MATRIX(5,2); break;
			case M: c64_key = MATRIX(4,4); break;
			case N: c64_key = MATRIX(4,7); break;
			case O: c64_key = MATRIX(4,6); break;
			case P: c64_key = MATRIX(5,1); break;
			case Q: c64_key = MATRIX(7,6); break;
			case R: c64_key = MATRIX(2,1); break;
			case S: c64_key = MATRIX(1,5); break;
			case T: c64_key = MATRIX(2,6); break;
			case U: c64_key = MATRIX(3,6); break;
			case V: c64_key = MATRIX(3,7); break;
			case W: c64_key = MATRIX(1,1); break;
			case X: c64_key = MATRIX(2,7); break;
			case Y: c64_key = MATRIX(3,1); break;
			case Z: c64_key = MATRIX(1,4); break;
	
			case DIGIT0 : c64_key = MATRIX(4,3); break;
			case DIGIT1: c64_key = MATRIX(7,0); break;
			case DIGIT2: c64_key = MATRIX(7,3); break;
			case DIGIT3: c64_key = MATRIX(1,0); break;
			case DIGIT4: c64_key = MATRIX(1,3); break;
			case DIGIT5: c64_key = MATRIX(2,0); break;
			case DIGIT6: c64_key = MATRIX(2,3); break;
			case DIGIT7: c64_key = MATRIX(3,0); break;
			case DIGIT8: c64_key = MATRIX(3,3); break;
			case DIGIT9: c64_key = MATRIX(4,0); break;
	
			case SPACE: c64_key = MATRIX(7,4); break;
			case BACK_QUOTE: c64_key = MATRIX(7,1); break;
			case BACK_SLASH: c64_key = MATRIX(6,6); break;
			case COMMA: c64_key = MATRIX(5,7); break;
			case PERIOD: c64_key = MATRIX(5,4); break;
			case MINUS: c64_key = MATRIX(5,0); break;
			case EQUALS: c64_key = MATRIX(5,3); break;
			case BRACELEFT: c64_key = MATRIX(5,6); break;
			case BRACERIGHT: c64_key = MATRIX(6,1); break;
			case SEMICOLON: c64_key = MATRIX(5,5); break;
			case QUOTE: c64_key = MATRIX(6,2); break;
			case SLASH: c64_key = MATRIX(6,7); break;
	
			case ESCAPE: c64_key = MATRIX(7,7); break;
			case ENTER: c64_key = MATRIX(0,1); break;
			case BACK_SPACE: case DELETE: c64_key = MATRIX(0,0); break;
			case INSERT: c64_key = MATRIX(6,3); break;
			case HOME: c64_key = MATRIX(6,3); break;
			case END: c64_key = MATRIX(6,0); break;
			case PAGE_UP: c64_key = MATRIX(6,0); break;
			case PAGE_DOWN: c64_key = MATRIX(6,5); break;
	
			case TAB: c64_key = MATRIX(7,2); break;
			case CONTROL: c64_key = MATRIX(7,5); break;
			case SHIFT: c64_key = MATRIX(1,7); break;
//			case SDLK_RSHIFT: c64_key = MATRIX(6,4); break;
			case ALT: c64_key = MATRIX(7,5); break;
			case ALT_GRAPH: c64_key = MATRIX(7,5); break;
	
			case UP: c64_key = (byte) (MATRIX(0,7) | 0x80); break;
			case DOWN: c64_key = MATRIX(0,7); break;
			case LEFT: c64_key = (byte) (MATRIX(0,2) | 0x80); break;
			case RIGHT: c64_key = MATRIX(0,2); break;
	
			case F1: c64_key = MATRIX(0,4); break;
			case F2: c64_key = (byte) (MATRIX(0,4) | 0x80); break;
			case F3: c64_key = MATRIX(0,5); break;
			case F4: c64_key = (byte) (MATRIX(0,5) | 0x80); break;
			case F5: c64_key = MATRIX(0,6); break;
			case F6: c64_key = (byte) (MATRIX(0,6) | 0x80); break;
			case F7: c64_key = MATRIX(0,3); break;
			case F8: c64_key = (byte) (MATRIX(0,3) | 0x80); break;

			case NUMPAD0: case NUMPAD5: c64_key = 0x10 | 0x40; break;
			case NUMPAD1: c64_key = 0x06 | 0x40; break;
			case NUMPAD2: c64_key = 0x02 | 0x40; break;
			case NUMPAD3: c64_key = 0x0a | 0x40; break;
			case NUMPAD4: c64_key = 0x04 | 0x40; break;
			case NUMPAD6: c64_key = 0x08 | 0x40; break;
			case NUMPAD7: c64_key = 0x05 | 0x40; break;
			case NUMPAD8: c64_key = 0x01 | 0x40; break;
			case NUMPAD9: c64_key = 0x09 | 0x40; break;

			case DIVIDE: c64_key = MATRIX(6,7); break;
			
			default: handled = false;
		}
	
		if (!handled)
			return;
		c64_key &= 0xff;
	
		// Handle joystick emulation
		if ((c64_key & 0x40) != 0) {
			c64_key &= 0x1f;
			if (key_up)
				joystick.b |= c64_key;
			else
				joystick.b &= ~c64_key;
			return;
		}
	
		// Handle other keys
		boolean shifted = (c64_key & 0x80) != 0;
		int c64_byte = (c64_key >> 3) & 7;
		int c64_bit = c64_key & 7;
		if (key_up) {
			if (shifted) {
				key_matrix[6] |= 0x10;
				rev_matrix[4] |= 0x40;
			}
			key_matrix[c64_byte] |= (1 << c64_bit);
			rev_matrix[c64_bit] |= (1 << c64_byte);
		} else {
			if (shifted) {
				key_matrix[6] &= 0xef;
				rev_matrix[4] &= 0xbf;
			}
			key_matrix[c64_byte] &= ~(1 << c64_bit);
			rev_matrix[c64_bit] &= ~(1 << c64_byte);
		}
	}
*/
	private enum KeyState {
		PRESSED, RELEASED;
	}
	private class ExtendedKeyEvent {
		KeyEvent event;
		KeyState type;
		public ExtendedKeyEvent(KeyEvent event, KeyState type) {
			this.event = event;
			this.type = type;;
		}
	}
	private Deque<ExtendedKeyEvent> events = new ArrayDeque<>();
	private void onKeyPressed(KeyEvent e) {
		events.addFirst(new ExtendedKeyEvent(e, KeyState.PRESSED));
	}
	private void onKeyReleased(KeyEvent e) {
		events.addFirst(new ExtendedKeyEvent(e, KeyState.RELEASED));
	}
	public void PollKeyboard(byte[] keyMatrix, byte[] revMatrix, ByteC joystick) {

		while (!events.isEmpty()) {
			ExtendedKeyEvent xevent = events.removeLast();
			translate_key(xevent.event, xevent.type == KeyState.RELEASED, keyMatrix, revMatrix, joystick);
		}
	}

	public boolean LockKey() {
//		return Toolkit.getDefaultToolkit().getLockingKeyState(java.awt.event.KeyEvent.VK_NUM_LOCK);
		return Toolkit.getDefaultToolkit().getLockingKeyState(java.awt.event.KeyEvent.VK_CAPS_LOCK);
	}
	public void InitColors(byte[] colors) {
		// Not needed since we don't need palette remapping???
		System.out.println(Arrays.toString(colors));
		// YES needed, this is where we are supposed to set the mapping from c=64 to system colors!
		// (even though no-one uses indexed color any more...)
		for (int i = 0; i < colors.length; i++) {
			colors[i] = (byte) (i & 0xf);
		}
	}
	public void NewPrefs(Prefs prefs) {
		
	}

	public boolean ShowRequester(String message, String option1, Runnable done) {
		Platform.runLater(() -> {
			ButtonType btn = new ButtonType(option1, ButtonBar.ButtonData.OK_DONE);
			Alert dlg = new Alert(AlertType.INFORMATION, message, btn);
//			Optional<ButtonType> dlgResult = 
					dlg.showAndWait();
			if (done != null) {
				done.run();
			}
		});
		return true;
	}
	public boolean ShowRequester(String message, String option1, String option2, Runnable option1Selected, Runnable option2Selected) {
		Platform.runLater(() -> {
			if (option2 == null) {
				throw new IllegalArgumentException("Use other signature");
			} else {
				ButtonType btn1 = new ButtonType(option1, ButtonBar.ButtonData.OK_DONE);
				ButtonType btn2 = new ButtonType(option2, ButtonBar.ButtonData.OK_DONE);
				Alert dlg = new Alert(AlertType.INFORMATION, message, btn1, btn2);
				Optional<ButtonType> dlgResult = dlg.showAndWait();
				if (dlgResult.isEmpty() || dlgResult.get() == btn1) {
					option1Selected.run();
				} else {
					option2Selected.run();
				}
			}
		});
		return true;
	}

	public C64 TheC64;
	
	// byte per pixel, lower nybble is C=64 color 0..15
	private byte[] display_buffer;

	
//	#ifdef __BEOS__
//	public void Pause(void);
//	public void Resume(void);
//	#endif
//
//	#ifdef __riscos__
//	public void ModeChange(void);
//		unsigned int *GetColourTable(void);	// returns pointer to mode_cols
//		bool CheckForUnpause(bool CheckLastState);
//
//		ROScreen *screen;
//		Joy_Keys JoystickKeys[2];		// it's easier making the joystick keys public
//	#endif
//
//	#ifdef __unix
//		bool quit_requested;
//	#endif

	private DriveLEDState[] led_state = {
			DRVLED_OFF,
			DRVLED_OFF,
			DRVLED_OFF,
			DRVLED_OFF
	};
	
//	#ifdef __BEOS__
//		C64Window *the_window;	// One of these is NULL
//		C64Screen *the_screen;
//		bool using_screen;		// Flag: Using the_screen
//		key_info old_key_info;
//		int draw_bitmap;		// Number of bitmap for the VIC to draw into
//	#endif
//
//	#ifdef AMIGA
//	public void draw_led_bar(void);	// Draw LED bar at the bottom of the window
//	public void draw_led(int num, int state);	// Draw one LED
//
//		struct Window *the_window;	// Pointer to C64 display window
//		struct Screen *the_screen;	// The window's screen
//		struct RastPort *the_rp;	// The window's RastPort
//		struct VisualInfo *the_visual_info;
//		struct Menu *the_menus;
//		struct TextFont *led_font;
//		struct TextFont *speedo_font;
//		struct RastPort temp_rp;	// For WritePixelArray8()
//		struct BitMap *temp_bm;
//		uint8 *chunky_buf;			// Chunky buffer for drawing into
//		LONG pens[16];				// Pens for C64 colors
//		int xo, yo;					// Window X/Y border size
//		struct FileRequester *open_req, *save_req;	// File requesters for load/save snapshot
//	#endif
//
//	#ifdef HAVE_SDL
//		char speedometer_string[16];		// Speedometer text
//	public void draw_string(SDL_Surface *s, int x, int y, const char *str, uint8 front_color, uint8 back_color);
//	#endif
//
//	#ifdef __unix
//	public void draw_led(int num, int state);	// Draw one LED
//		static void pulse_handler(...);		// LED error blinking
//	#endif
//
//	#ifdef WIN32
//	public:
//		long ShowRequester(const char *str, const char *button1, const char *button2 = NULL);
//	public void WaitUntilActive();
//	public void NewPrefs();
//	public void Pause();
//	public void Resume();
//	public void Quit();
//
//		struct DisplayMode {
//			int x;
//			int y;
//			int depth;
//			BOOL modex;
//		};
//		int GetNumDisplayModes() const;
//		const DisplayMode *GetDisplayModes() const;
//
//	private:
//		// Window members.
//	private void ResetKeyboardState();
//		BOOL MakeWindow();
//		static LRESULT CALLBACK StaticWindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
//		long WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
//		static int VirtKey2C64(int virtkey, DWORD keydata);
//		BOOL CalcViewPort();
//		BOOL SetupWindow();
//		BOOL SetupWindowMode(BOOL full_screen);
//		BOOL RestoreWindow();
//		BOOL ResizeWindow(int side, RECT *pRect);
//	private void WindowTitle();
//	private void CreateObjects();
//	private void DeleteObjects();
//
//		// DirectDraw management members.
//		BOOL StartDirectDraw();
//		BOOL ResumeDirectDraw();
//		BOOL ResetDirectDraw();
//		BOOL StopDirectDraw();
//		static HRESULT CALLBACK EnumModesCallback(LPDDSURFACEDESC pDDSD, LPVOID lpContext);
//		HRESULT EnumModesCallback(LPDDSURFACEDESC pDDSD);
//		static int CompareModes(const void *e1, const void *e2);
//		BOOL Fail(const char *message);
//
//		// DirectDraw worker members.
//		BOOL SetPalettes();
//		BOOL BuildColorTable();
//		BOOL CopySurface(RECT &rcWork);
//		BOOL FlipSurfaces();
//		BOOL EraseSurfaces();
//		BOOL RestoreSurfaces();
//
//	private void draw_led_bar(void);		// Draw LED bar on the window
//	private void draw_leds(BOOL force = false);	// Draw LEDs if force or changed
//	private void led_rect(int n, RECT &rc, RECT &led); // Compute LED rectangle
//	private void InsertNextDisk();			// should be a common func
//		BOOL FileNameDialog(char *prefs_path, BOOL save = false);
//	private void OfferSave();			// Offer chance to save changes
//
//		UBYTE *chunky_buf;			// Chunky buffer for drawing
//		BOOL active;				// is application active?
//		BOOL paused;				// is application paused?
//		BOOL waiting;				// is application waiting?
//		DWORD windowed_style;			// style of windowed window
//		DWORD fullscreen_style;			// style of fullscreen window
//		char failure_message[128];		// what when wrong
//		int speed_index;			// look ma, no hands
//		BOOL show_leds;				// cached prefs option
//		BOOL full_screen;			// cached prefs option
//		BOOL in_constructor;			// if we are being contructed 
//		BOOL in_destructor;			// if we are being destroyed
//
//		LPDIRECTDRAW pDD;			// DirectDraw object
//		LPDIRECTDRAWSURFACE pPrimary;		// DirectDraw primary surface
//		LPDIRECTDRAWSURFACE pBack;		// DirectDraw back surface
//		LPDIRECTDRAWSURFACE pWork;		// DirectDraw working surface
//		LPDIRECTDRAWCLIPPER pClipper;		// DirectDraw clipper
//		LPDIRECTDRAWPALETTE pPalette;		// DirectDraw palette
//
//		DWORD colors[256];			// our palette colors
//		int colors_depth;			// depth of the colors table
//	#endif
//
//	#ifdef __riscos__
//		unsigned int mode_cols[256];	// Colours in the current mode corresponding to C64's
//		uint8 *bitmap;
//		uint32 lastkeys[8];		// bitfield describing keys pressed last time.
//	#endif

}
