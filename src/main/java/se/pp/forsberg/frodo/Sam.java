package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.CPtr;
import se.pp.forsberg.libc.StdioFile;

import static se.pp.forsberg.libc.LibC.*;

import java.io.File;

import static se.pp.forsberg.frodo.Sam.AddrMode.*;
import static se.pp.forsberg.frodo.Sam.Mnemonic.*;
import static se.pp.forsberg.frodo.Sam.Token.*;

class Sam {
	/*
	 *  SAM.h - Simple Assembler and Monitor With Integrated System Explorer
	 *
	 *  Frodo (C) 1994-1997,2002 Christian Bauer
	 */
	
	// Pointers to chips
	static MOS6510 TheCPU;
	static MOS6502_1541 TheCPU1541;
	static MOS6569 TheVIC;
	static MOS6581 TheSID;
	static MOS6526_1 TheCIA1;
	static MOS6526_2 TheCIA2;

	// 6510/6502 registers
	static MOS6510State R64;
	static MOS6502State R1541;

	static boolean access_1541;	// false: accessing C64, true: accessing 1541

	// Access to 6510/6502 address space
	static byte SAMReadByte(int adr) {
		if (access_1541)
			return TheCPU1541.ExtReadByte(adr);
		else
			return TheCPU.ExtReadByte(adr);
	}

	static void SAMWriteByte(int adr, byte b) {
		if (access_1541)
			TheCPU1541.ExtWriteByte(adr, b);
		else
			TheCPU.ExtWriteByte(adr, b);
	}


	// Streams for input, output and error messages
	private static StdioFile fin, fout, ferr;

	// Input line
	final static int INPUT_LENGTH = 80;
	static char[] input = new char[INPUT_LENGTH];
	static CPtr in_ptr;

	static int address, end_address;


	// Input tokens
	enum Token {
		T_NULL,		// Invalid token
		T_END,		// End of line
		T_NUMBER,	// Hexadecimal number
		T_STRING,	// String enclosed in ""
		T_LPAREN,	// '('
		T_RPAREN,	// ')'
		T_ADD,		// '+'
		T_SUB,		// '-'
		T_MUL,		// '*'
		T_DIV,		// '/'
		T_COMMA,	// ','
		T_IMMED,	// '#'
		T_X,		// 'x'
		T_Y,		// 'y'
		T_PC,		// 'pc'
		T_SP,		// 'sp'

		T_A,		// 'a'	(get_reg_token() only)
		T_DR,		// 'dr'	(get_reg_token() only)
		T_PR		// 'pr'	(get_reg_token() only)
	};

	static Token the_token;			// Last token read
	static int the_number;				// Contains the number if the_token==T_NUMBER
	static char[] the_string = new char[INPUT_LENGTH];	// Contains the string if the_token==T_STRING


	// Addressing modes
	enum AddrMode {
		A_IMPL,
		A_ACCU,		// A
		A_IMM,		// #zz
		A_REL,		// Branches
		A_ZERO,		// zz
		A_ZEROX,	// zz,x
		A_ZEROY,	// zz,y
		A_ABS,		// zzzz
		A_ABSX,		// zzzz,x
		A_ABSY,		// zzzz,y
		A_IND,		// (zzzz)
		A_INDX,		// (zz,x)
		A_INDY		// (zz),y
	};

	// Mnemonics
	enum Mnemonic {
		M_ADC, M_AND, M_ASL, M_BCC, M_BCS, M_BEQ, M_BIT, M_BMI, M_BNE, M_BPL,
		M_BRK, M_BVC, M_BVS, M_CLC, M_CLD, M_CLI, M_CLV, M_CMP, M_CPX, M_CPY,
		M_DEC, M_DEX, M_DEY, M_EOR, M_INC, M_INX, M_INY, M_JMP, M_JSR, M_LDA,
		M_LDX, M_LDY, M_LSR, M_NOP, M_ORA, M_PHA, M_PHP, M_PLA, M_PLP, M_ROL,
		M_ROR, M_RTI, M_RTS, M_SBC, M_SEC, M_SED, M_SEI, M_STA, M_STX, M_STY,
		M_TAX, M_TAY, M_TSX, M_TXA, M_TXS, M_TYA,

		M_ILLEGAL,  // Undocumented opcodes start here

		M_IANC, M_IANE, M_IARR, M_IASR, M_IDCP, M_IISB, M_IJAM, M_INOP, M_ILAS,
		M_ILAX, M_ILXA, M_IRLA, M_IRRA, M_ISAX, M_ISBC, M_ISBX, M_ISHA, M_ISHS,
		M_ISHX, M_ISHY, M_ISLO, M_ISRE,

		M_MAXIMUM  // Highest element
	};

	// Mnemonic for each opcode
	static Mnemonic[] mnemonic = {
		M_BRK , M_ORA , M_IJAM, M_ISLO, M_INOP, M_ORA, M_ASL , M_ISLO,	// 00
		M_PHP , M_ORA , M_ASL , M_IANC, M_INOP, M_ORA, M_ASL , M_ISLO,
		M_BPL , M_ORA , M_IJAM, M_ISLO, M_INOP, M_ORA, M_ASL , M_ISLO,	// 10
		M_CLC , M_ORA , M_INOP, M_ISLO, M_INOP, M_ORA, M_ASL , M_ISLO,
		M_JSR , M_AND , M_IJAM, M_IRLA, M_BIT , M_AND, M_ROL , M_IRLA,	// 20
		M_PLP , M_AND , M_ROL , M_IANC, M_BIT , M_AND, M_ROL , M_IRLA,
		M_BMI , M_AND , M_IJAM, M_IRLA, M_INOP, M_AND, M_ROL , M_IRLA,	// 30
		M_SEC , M_AND , M_INOP, M_IRLA, M_INOP, M_AND, M_ROL , M_IRLA,
		M_RTI , M_EOR , M_IJAM, M_ISRE, M_INOP, M_EOR, M_LSR , M_ISRE,	// 40
		M_PHA , M_EOR , M_LSR , M_IASR, M_JMP , M_EOR, M_LSR , M_ISRE,
		M_BVC , M_EOR , M_IJAM, M_ISRE, M_INOP, M_EOR, M_LSR , M_ISRE,	// 50
		M_CLI , M_EOR , M_INOP, M_ISRE, M_INOP, M_EOR, M_LSR , M_ISRE,
		M_RTS , M_ADC , M_IJAM, M_IRRA, M_INOP, M_ADC, M_ROR , M_IRRA,	// 60
		M_PLA , M_ADC , M_ROR , M_IARR, M_JMP , M_ADC, M_ROR , M_IRRA,
		M_BVS , M_ADC , M_IJAM, M_IRRA, M_INOP, M_ADC, M_ROR , M_IRRA,	// 70
		M_SEI , M_ADC , M_INOP, M_IRRA, M_INOP, M_ADC, M_ROR , M_IRRA,
		M_INOP, M_STA , M_INOP, M_ISAX, M_STY , M_STA, M_STX , M_ISAX,	// 80
		M_DEY , M_INOP, M_TXA , M_IANE, M_STY , M_STA, M_STX , M_ISAX,
		M_BCC , M_STA , M_IJAM, M_ISHA, M_STY , M_STA, M_STX , M_ISAX,	// 90
		M_TYA , M_STA , M_TXS , M_ISHS, M_ISHY, M_STA, M_ISHX, M_ISHA,
		M_LDY , M_LDA , M_LDX , M_ILAX, M_LDY , M_LDA, M_LDX , M_ILAX,	// a0
		M_TAY , M_LDA , M_TAX , M_ILXA, M_LDY , M_LDA, M_LDX , M_ILAX,
		M_BCS , M_LDA , M_IJAM, M_ILAX, M_LDY , M_LDA, M_LDX , M_ILAX,	// b0
		M_CLV , M_LDA , M_TSX , M_ILAS, M_LDY , M_LDA, M_LDX , M_ILAX,
		M_CPY , M_CMP , M_INOP, M_IDCP, M_CPY , M_CMP, M_DEC , M_IDCP,	// c0
		M_INY , M_CMP , M_DEX , M_ISBX, M_CPY , M_CMP, M_DEC , M_IDCP,
		M_BNE , M_CMP , M_IJAM, M_IDCP, M_INOP, M_CMP, M_DEC , M_IDCP,	// d0
		M_CLD , M_CMP , M_INOP, M_IDCP, M_INOP, M_CMP, M_DEC , M_IDCP,
		M_CPX , M_SBC , M_INOP, M_IISB, M_CPX , M_SBC, M_INC , M_IISB,	// e0
		M_INX , M_SBC , M_NOP , M_ISBC, M_CPX , M_SBC, M_INC , M_IISB,
		M_BEQ , M_SBC , M_IJAM, M_IISB, M_INOP, M_SBC, M_INC , M_IISB,	// f0
		M_SED , M_SBC , M_INOP, M_IISB, M_INOP, M_SBC, M_INC , M_IISB
	};

	// Addressing mode for each opcode
	static AddrMode[] adr_mode = {
		A_IMPL, A_INDX, A_IMPL, A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// 00
		A_IMPL, A_IMM , A_ACCU, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROX, A_ZEROX,	// 10
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSX , A_ABSX,
		A_ABS , A_INDX, A_IMPL, A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// 20
		A_IMPL, A_IMM , A_ACCU, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROX, A_ZEROX,	// 30
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSX , A_ABSX,
		A_IMPL, A_INDX, A_IMPL, A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// 40
		A_IMPL, A_IMM , A_ACCU, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROX, A_ZEROX,	// 50
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSX , A_ABSX,
		A_IMPL, A_INDX, A_IMPL, A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// 60
		A_IMPL, A_IMM , A_ACCU, A_IMM , A_IND  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROX, A_ZEROX,	// 70
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSX , A_ABSX,
		A_IMM , A_INDX, A_IMM , A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// 80
		A_IMPL, A_IMM , A_IMPL, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROY, A_ZEROY,	// 90
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSY , A_ABSY,
		A_IMM , A_INDX, A_IMM , A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// a0
		A_IMPL, A_IMM , A_IMPL, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROY, A_ZEROY,	// b0
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSY , A_ABSY,
		A_IMM , A_INDX, A_IMM , A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// c0
		A_IMPL, A_IMM , A_IMPL, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROX, A_ZEROX,	// d0
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSX , A_ABSX,
		A_IMM , A_INDX, A_IMM , A_INDX, A_ZERO , A_ZERO , A_ZERO , A_ZERO,	// e0
		A_IMPL, A_IMM , A_IMPL, A_IMM , A_ABS  , A_ABS  , A_ABS  , A_ABS,
		A_REL , A_INDY, A_IMPL, A_INDY, A_ZEROX, A_ZEROX, A_ZEROX, A_ZEROX,	// f0
		A_IMPL, A_ABSY, A_IMPL, A_ABSY, A_ABSX , A_ABSX , A_ABSX , A_ABSX
	};

	// Chars for each mnemonic
	static char[] mnem_1 = "aaabbbbbbbbbbcccccccdddeiiijjllllnopppprrrrssssssstttttt?aaaadijnlllrrsssssssss".toCharArray();
	static char[] mnem_2 = "dnscceimnprvvllllmppeeeonnnmsdddsorhhlloottbeeetttaasxxy?nnrscsaoaaxlrabbhhhhlr".toCharArray();
	static char[] mnem_3 = "cdlcsqtielkcscdivpxycxyrcxypraxyrpaapaplrisccdiaxyxyxasa?cerrpbmpsxaaaxcxasxyoe".toCharArray();

	// Instruction length for each addressing mode
	static int[] adr_length = {1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 2, 2};

	/*
	 *  Open and handle SAM
	 */

	static public void sam(C64 the_c64) {
		new Sam(the_c64);
	}
	private Sam(C64 the_c64) {
		active = true;
		boolean done = false;
		char c;

		TheCPU = the_c64.TheCPU;
		TheCPU1541 = the_c64.TheCPU1541;
		TheVIC = the_c64.TheVIC;
		TheSID = the_c64.TheSID;
		TheCIA1 = the_c64.TheCIA1;
		TheCIA2 = the_c64.TheCIA2;

		// Get CPU registers and current memory configuration
		R64 = TheCPU.GetState();
		TheCPU.ExtConfig = (~R64.ddr | R64.pr) & 7;
		R1541 = TheCPU1541.GetState();

//	#ifdef __riscos__
//		Wimp_CommandWindow((int)"SAM");
//	#endif

//	#ifdef AMIGA
//		if (!(fin = fout = ferr = fopen("CON:0/0/640/480/SAM", "w+")))
//			return;
//	#else
//		fin = stdin;
//		fout = stdout;
//		ferr = stdout;
//	#endif
		
		fin = StdioFile.fdopen(System.in);
		fout = StdioFile.fdopen(System.out);
		ferr = StdioFile.fdopen(System.err);

		access_1541 = false;
		address = R64.pc;

		fprintf(ferr, "\n *** SAM - Simple Assembler and Monitor ***\n ***         Press 'h' for help         ***\n\n");
		init_abort();
		display_registers();

		while (!done) {
			if (access_1541)
				fprintf(ferr, "1541> ");
			else
				fprintf(ferr, "C64> ");
			fflush(ferr);
			read_line();
			while ((c = get_char()) == ' ') ;

			switch (c) {
				case 'a':		// Assemble
					get_token();
					assemble();
					break;

				case 'b':		// Binary dump
					get_token();
					binary_dump();
					break;

				case 'c':		// Compare
					get_token();
					compare();
					break;

				case 'd':		// Disassemble
					get_token();
					disassemble();
					break;

				case 'e':       // Interrupt vectors
					int_vectors();
					break;

				case 'f':		// Fill
					get_token();
					fill();
					break;

				case 'h':		// Help
					help();
					break;

				case 'i':		// ASCII dump
					get_token();
					ascii_dump();
					break;

				case 'k':		// Memory configuration
					get_token();
					mem_config();
					break;

				case 'l':		// Load data
					get_token();
					load_data();
					break;

				case 'm':		// Memory dump
					get_token();
					memory_dump();
					break;

				case 'n':		// Screen code dump
					get_token();
					screen_dump();
					break;

				case 'o':		// Redirect output
					get_token();
					redir_output();
					break;

				case 'p':		// Sprite dump
					get_token();
					sprite_dump();
					break;

				case 'r':		// Registers
					get_reg_token();
					registers();
					break;

				case 's':		// Save data
					get_token();
					save_data();
					break;

				case 't':		// Transfer
					get_token();
					transfer();
					break;

				case 'v':		// View machine state
					view_state();
					break;

				case 'x':		// Exit
					done = true;
					break;

				case ':':		// Change memory
					get_token();
					modify();
					break;

				case '1':		// Switch to 1541 mode
					access_1541 = true;
					break;

				case '6':		// Switch to C64 mode
					access_1541 = false;
					break;

				case '?':		// Compute expression
					get_token();
					print_expr();
					break;

				case '\n':		// Blank line
					break;

				default:		// Unknown command
					error("Unknown command");
					break;
			}
		}

		exit_abort();

//	#ifdef AMIGA
//		fclose(fin);
//	#endif
//		if (fout != ferr)
//			fclose(fout);
//
//	#ifdef __riscos__
//		Wimp_CommandWindow(-1);
//	#endif

		// Set CPU registers
		TheCPU.SetState(R64);
		TheCPU1541.SetState(R1541);
		active = false;
	}


	/*
	 *  Print error message
	 */

	static void error(String s) {
		fprintf(ferr, "*** %s\n", s);
	}

	/*
	 *  CTRL-C pressed?
	 */

	static boolean WasAborted;
	public static boolean active;

//	#ifdef HAVE_SIGACTION
//	struct sigaction my_sa;
//	#endif

	static void handle_abort() {
		WasAborted = true;
//	#if !defined(HAVE_SIGACTION) && defined(HAVE_SIGNAL)
//	#ifdef __riscos__
//		signal(SIGINT, (Handler*) handle_abort);
//	#else
//		signal(SIGINT, (sighandler_t) handle_abort);
//	#endif
//	#endif
		// TODO
	}

	static void init_abort() {
		WasAborted = false;
//	#if defined(HAVE_SIGACTION)
//		my_sa.sa_handler = (void (*)(int))handle_abort;
//		my_sa.sa_flags = 0;
//		sigemptyset(&my_sa.sa_mask);
//		sigaction(SIGINT, &my_sa, NULL);
//	#elif defined(HAVE_SIGNAL)
//	#ifdef __riscos__
//		signal(SIGINT, (Handler*) handle_abort);
//	#else
//		signal(SIGINT, (sighandler_t) handle_abort);
//	#endif
//	#endi
		// TODOf
	}

	static void exit_abort() {
//	#if defined(HAVE_SIGACTION)
//		my_sa.sa_handler = SIG_DFL;
//		sigaction(SIGINT, &my_sa, NULL);
//	#elif defined(HAVE_SIGNAL)
//		signal(SIGINT, SIG_DFL);
//	#endif
	}

	static boolean aborted() {
		boolean ret = WasAborted;

		WasAborted = false;
		return ret;
	}


	/*
	 *  Read a line from the keyboard
	 */

	static void read_line() {
//	#ifdef __riscos__
//		OS_ReadLine(in_ptr = input, INPUT_LENGTH, 0, 255, 0);
//	#else
		fgets(in_ptr = new CPtr(input), INPUT_LENGTH, fin);
//	#endif
	}


	/*
	 *  Read a character from the input line
	 */


	static char get_char() {
		return in_ptr.getInc();
	}


	/*
	 *  Stuff back a character into the input line
	 */

	static void put_back(char c) {
		in_ptr.decSet(c);
	}


	/*
	 *  Scanner: Get a token from the input line
	 */

	static Token get_token() {
		char c;

		// Skip spaces
		while ((c = get_char()) == ' ') ;

		switch (c) {
			case '\r':
				get_char();
				return the_token = T_END;
			case '\n':
				return the_token = T_END;
			case '(':
				return the_token = T_LPAREN;
			case ')':
				return the_token = T_RPAREN;
			case '+':
				return the_token = T_ADD;
			case '-':
				return the_token = T_SUB;
			case '*':
				return the_token = T_MUL;
			case '/':
				return the_token = T_DIV;
			case ',':
				return the_token = T_COMMA;
			case '#':
				return the_token = T_IMMED;
			case 'x':
				return the_token = T_X;
			case 'y':
				return the_token = T_Y;
			case 'p':
				if (get_char() == 'c')
					return the_token = T_PC;
				else {
					error("Unrecognized token");
					return the_token = T_NULL;
				}
			case 's':
				if (get_char() == 'p')
					return the_token = T_SP;
				else {
					error("Unrecognized token");
					return the_token = T_NULL;
				}
			case '0': case '1': case '2': case '3': case '4':
			case '5': case '6': case '7': case '8': case '9':
			case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
				put_back(c);
				the_number = get_number();
				return the_token = T_NUMBER;
			case '"':
				return the_token = get_string(new CPtr(the_string));
			default:
				error("Unrecognized token");
				return the_token = T_NULL;
		}
	}

	static Token get_reg_token() {
		char c;

		// Skip spaces
		while ((c = get_char()) == ' ') ;

		switch (c) {
			case '\n':
				return the_token = T_END;
			case 'a':
				return the_token = T_A;
			case 'd':
				if (get_char() == 'r')
					return the_token = T_DR;
				else {
					error("Unrecognized token");
					return the_token = T_NULL;
				}
			case 'p':
				if ((c = get_char()) == 'c')
					return the_token = T_PC;
				else if (c == 'r')
					return the_token = T_PR;
				else {
					error("Unrecognized token");
					return the_token = T_NULL;
				}
			case 's':
				if (get_char() == 'p')
					return the_token = T_SP;
				else {
					error("Unrecognized token");
					return the_token = T_NULL;
				}
			case 'x':
				return the_token = T_X;
			case 'y':
				return the_token = T_Y;
			default:
				error("Unrecognized token");
				return the_token = T_NULL;
		}
	}

	static int get_number() {
		char c;
		int i = 0;

		while (((c = get_char()) >= '0') && (c <= '9') || (c >= 'a') && (c <= 'f'))
			if (c < 'a')
				i = (i << 4) + (c - '0');
			else
				i = (i << 4) + (c - 'a' + 10);

		put_back(c);
		return i;
	}

	static Token get_string(CPtr str) {
		char c;

		while ((c = get_char()) != '\n') {
			if (c == '"') {
				str.set(0);
				return T_STRING;
			}
			str.setInc(c);
		}

		error("Unterminated string");
		return T_NULL;
	}


	/*
	 *  expression = term {(ADD | SUB) term}
	 *  true: OK, false: Error
	 */

	static Integer expression() {
		Integer accu, trm;

		if ((accu = term()) == null)
			return null;

		for (;;)
			switch (the_token) {
				case T_ADD:
					get_token();
					if ((trm = term()) == null)
						return null;
					accu += trm;
					break;

				case T_SUB:
					get_token();
					if ((trm = term()) == null)
						return null;
					accu -= trm;
					break;

				default:
					return accu;
			}
	}


	/*
	 *  term = factor {(MUL | DIV) factor}
	 *  true: OK, false: Error
	 */

	static Integer term() {
		Integer accu, fact;

		if ((accu = factor()) == null)
			return null;

		for (;;)
			switch (the_token) {
				case T_MUL:
					get_token();
					if ((fact = factor()) == null)
						return null;
					accu *= fact;
					break;

				case T_DIV:
					get_token();
					if ((fact = factor()) == null)
						return null;
					if (fact == 0) {
						error("Division by 0");
						return null;
					}
					accu /= fact;
					break;

				default:
					return accu;
			}
	}


	/*
	 *  factor = NUMBER | PC | SP | LPAREN expression RPAREN
	 *  true: OK, false: Error
	 */

	static Integer factor() {
		switch (the_token) {
			case T_NUMBER:
				Integer number = the_number;
				get_token();
				return number;

			case T_PC:
				get_token();
				return (int) (access_1541 ? R1541.pc : R64.pc);
				
			case T_SP:
				get_token();
				return (int) (access_1541 ? R1541.sp : R64.sp);
			
			case T_LPAREN:
				get_token();
				if ((number = expression()) != null)
					if (the_token == T_RPAREN) {
						get_token();
						return number;
					} else {
						error("Missing ')'");
						return null;
					}
				else {
					error("Error in expression");
					return null;
				}

			case T_END:
				error("Required argument missing");
				return null;

			default:
				error("'pc', 'sp', '(' or number expected");
				return null;
		}
	}


	/*
	 *  address_args = [expression] END
	 *
	 *  Read start to "address"
	 *
	 *  true: OK, false: Error
	 */

	static boolean address_args() {
		if (the_token == T_END)
			return true;
		else {
			Integer adr;
			if ((adr = expression()) == null)
				return false;
			address = adr;
			return the_token == T_END;
		}
	}


	/*
	 *  range_args = [expression] [[COMMA] expression] END
	 *
	 *  Read start address to "address", end address to "end_address"
	 *
	 *  true: OK, false: Error
	 */

	static boolean range_args(int def_range)
	{
		end_address = address + def_range;

		if (the_token == T_END)
			return true;
		else {
			Integer adr;
			if ((adr = expression()) == null)
				return false;
			address = adr;
			end_address = address + def_range;
			if (the_token == T_END)
				return true;
			else {
				if (the_token == T_COMMA) get_token();
				Integer end_adr;
				if ((end_adr = expression()) == null)
					return false;
				end_address = end_adr;
				return the_token == T_END;
			}
		}
	}


	/*
	 *  instr_args = END
	 *             | IMMED NUMBER END
	 *             | NUMBER [COMMA (X | Y)] END
	 *             | LPAREN NUMBER (RPAREN [COMMA Y] | COMMA X RPAREN) END
	 *
	 *  Read arguments of a 6510 instruction, determine address and addressing mode
	 *
	 *  true: OK, false: Error
	 */

	static class Arg6510 {
		int number;
		AddrMode mode;
	}
	static Arg6510 instr_args() {
		Arg6510 result = new Arg6510();
		switch (the_token) {

			case T_END:
				result.mode = A_IMPL;
				return result;

			case T_IMMED:
				get_token();
				if (the_token == T_NUMBER) {
					result.number = the_number;
					result.mode = A_IMM;
					get_token();
					return the_token == T_END? result:null;
				} else {
					error("Number expected");
					return null;
				}

			case T_NUMBER:
				result.number = the_number;
				get_token();
				switch (the_token) {

					case T_END:
						if (result.number < 0x100)
							result.mode = A_ZERO;
						else
							result.mode = A_ABS;
						return result;

					case T_COMMA:
						get_token();
						switch (the_token) {

							case T_X:
								get_token();
								if (result.number < 0x100)
									result.mode = A_ZEROX;
								else
									result.mode = A_ABSX;
								return the_token == T_END? result:null;

							case T_Y:
								get_token();
								if (result.number < 0x100)
									result.mode = A_ZEROY;
								else
									result.mode = A_ABSY;
								return the_token == T_END? result:null;

							default:
								error("Illegal index register");
								return null;
						}

					default:
						return null;
				}

			case T_LPAREN:
				get_token();
				if (the_token == T_NUMBER) {
					result.number = the_number;
					get_token();
					switch (the_token) {

						case T_RPAREN:
							get_token();
							switch (the_token) {

								case T_END:
									result.mode = A_IND;
									return result;

								case T_COMMA:
									get_token();
									if (the_token == T_Y) {
										result.mode = A_INDY;
										get_token();
										return the_token == T_END? result:null;
									} else {
										error("Only 'y' index register allowed");
										return null;
									}

								default:
									error("Illegal characters after ')'");
									return null;
							}

						case T_COMMA:
							get_token();
							if (the_token == T_X) {
								get_token();
								if (the_token == T_RPAREN) {
									result.mode = A_INDX;
									get_token();
									return the_token == T_END? result:null;
								} else {
									error("')' expected");
									return null;
								}
							} else {
								error("Only 'x' index register allowed");
								return null;
							}

						default:
							error("')' or ',' expected");
							return null;
					}
				} else {
					error("Number expected");
					return null;
				}

			default:
				error("'(', '#' or number expected");
				return null;
		}
	}


	/*
	 *  Display help
	 *  h
	 */

	static void help() {
		fprintf(fout,
					"a [start]           Assemble\n" +
					"b [start] [end]     Binary dump\n" +
					"c start end dest    Compare memory\n" +
					"d [start] [end]     Disassemble\n" +
					"e                   Show interrupt vectors\n" +
					"f start end byte    Fill memory\n" +
					"i [start] [end]     ASCII/PETSCII dump\n" +
					"k [config]          Show/set C64 memory configuration\n" +
					"l start \"file\"      Load data\n" +
					"m [start] [end]     Memory dump\n" +
					"n [start] [end]     Screen code dump\n" +
					"o [\"file\"]          Redirect output\n" +
					"p [start] [end]     Sprite dump\n" +
					"r [reg value]       Show/set CPU registers\n" +
					"s start end \"file\"  Save data\n" +
					"t start end dest    Transfer memory\n" +
					"vc1                 View CIA 1 state\n" +
					"vc2                 View CIA 2 state\n" +
					"vf                  View 1541 state\n" +
					"vs                  View SID state\n" +
					"vv                  View VIC state\n" +
					"x                   Return to Frodo\n" +
					": addr {byte}       Modify memory\n" +
					"1541                Switch to 1541\n" +
					"64                  Switch to C64\n" +
					"? expression        Calculate expression\n");
	}


	/*
	 *  Display/change 6510 registers
	 *  r [reg value]
	 */

	static void registers() {
		Token the_reg;
		Integer value;

		if (the_token != T_END)
			switch (the_reg = the_token) {
				case T_A:
				case T_X:
				case T_Y:
				case T_PC:
				case T_SP:
				case T_DR:
				case T_PR:
					get_token();
					if ((value = expression()) == null)
						return;
					
					switch (the_reg) {
						case T_A:
							if (access_1541)
								R1541.a = value.byteValue();
							else
								R64.a = value.byteValue();
							break;
						case T_X:
							if (access_1541)
								R1541.x = value.byteValue();
							else
								R64.x = value.byteValue();
							break;
						case T_Y:
							if (access_1541)
								R1541.y = value.byteValue();
							else
								R64.y = value.byteValue();
							break;
						case T_PC:
							if (access_1541)
								R1541.pc = value.byteValue();
							else
								R64.pc = value.byteValue();
							break;
						case T_SP:
							if (access_1541)
								R1541.sp = (short) ((value.byteValue() & 0xff) | 0x0100);
							else
								R64.sp = (short) ((value.byteValue() & 0xff) | 0x0100);
							break;
						case T_DR:
							if (!access_1541)
								R64.ddr = value.byteValue();
							break;
						case T_PR:
							if (!access_1541)
								R64.pr = value.byteValue();
							break;
						default:
							break;
					}
					break;

				default:
					return;
			}

		display_registers();
	}

	static void display_registers() {
		if (access_1541) {
			fprintf(fout, " PC  A  X  Y   SP  NVDIZC  Instruction\n");
			fprintf(fout, "%04lx %02lx %02lx %02lx %04lx %c%c%c%c%c%c ",
				R1541.pc & 0xffff, R1541.a & 0xff, R1541.x % 0xff, R1541.y & 0xff, R1541.sp & 0xffff,
				((R1541.p & 0x80) != 0) ? '1' : '0', ((R1541.p & 0x40) != 0) ? '1' : '0', ((R1541.p & 0x08) != 0) ? '1' : '0',
				((R1541.p & 0x04) != 0) ? '1' : '0', ((R1541.p & 0x02) != 0) ? '1' : '0', ((R1541.p & 0x01) != 0) ? '1' : '0');
			disass_line(R1541.pc, SAMReadByte(R1541.pc), SAMReadByte(R1541.pc+1), SAMReadByte(R1541.pc+2));
		} else {
			fprintf(fout, " PC  A  X  Y   SP  DR PR NVDIZC  Instruction\n");
			fprintf(fout, "%04lx %02lx %02lx %02lx %04lx %02lx %02lx %c%c%c%c%c%c ",
				R64.pc & 0xffff, R64.a & 0xff, R64.x & 0xff, R64.y & 0xff, R64.sp & 0xffff, R64.ddr & 0xff, R64.pr & 0xff,
				((R64.p & 0x80) != 0) ? '1' : '0', ((R64.p & 0x40) != 0) ? '1' : '0', ((R64.p & 0x08) != 0) ? '1' : '0',
				((R64.p & 0x04) != 0) ? '1' : '0', ((R64.p & 0x02) != 0) ? '1' : '0', ((R64.p & 0x01) != 0) ? '1' : '0');
			disass_line(R64.pc, SAMReadByte(R64.pc), SAMReadByte(R64.pc+1), SAMReadByte(R64.pc+2));
		}
	}


	/*
	 *  Memory dump
	 *  m [start] [end]
	 */

	private final static int MEMDUMP_BPL = 16;  // Bytes per line

	static void memory_dump() {
		boolean done = false;
		short i;
		byte[] mem = new byte[MEMDUMP_BPL + 2];
		byte b;

		mem[MEMDUMP_BPL] = 0;

		if (!range_args(16 * MEMDUMP_BPL - 1))  // 16 lines unless end address specified
			return;

		do {
			fprintf(fout, "%04lx:", address);
			for (i=0; i<MEMDUMP_BPL; i++, address++) {
				if (address == end_address) done = true;

				fprintf(fout, " %02lx", b = SAMReadByte(address));
				if ((b >= ' ') && (b <= '~'))
					mem[i] = (byte) conv_from_64((char) b);
				else
					mem[i] = '.';
			}
			fprintf(fout, "  '%s'\n", mem);
		} while (!done && !aborted());
	}


	/*
	 *  ASCII dump
	 *  i [start] [end]
	 */

	private final static int ASCIIDUMP_BPL = 64;  // Bytes per line

	static void ascii_dump() {
		boolean done = false;
		short i;
		byte[] mem = new byte[ASCIIDUMP_BPL + 2];
		byte b;

		mem[ASCIIDUMP_BPL] = 0;

		if (!range_args(16 * ASCIIDUMP_BPL - 1))  // 16 lines unless end address specified
			return;

		do {
			fprintf(fout, "%04lx:", address);
			for (i=0; i<ASCIIDUMP_BPL; i++, address++) {
				if (address == end_address) done = true;

				b = SAMReadByte(address);
				if ((b >= ' ') && (b <= '~'))
					mem[i] = (byte) conv_from_64((char) b);
				else
					mem[i] = '.';
			}
			fprintf(fout, " '%s'\n", toString(mem));
		} while (!done && !aborted());
	}


	/*
	 *  Convert PETSCII->ASCII
	 */

	static char conv_from_64(char c)
	{
		if ((c >= 'A') && (c <= 'Z') || (c >= 'a') && (c <= 'z'))
			return (char) (c ^ 0x20);
		else
			return c;
	}


	/*
	 *  Screen code dump
	 *  n [start] [end]
	 */

	private final static int SCRDUMP_BPL = 64;  // Bytes per line

	static void screen_dump() {
		boolean done = false;
		short i;
		byte[] mem = new byte[SCRDUMP_BPL + 2];
		byte b;

		mem[SCRDUMP_BPL] = 0;

		if (!range_args(16 * SCRDUMP_BPL - 1))  // 16 Zeilen unless end address specified
			return;

		do {
			fprintf(fout, "%04lx:", address & 0xffff);
			for (i=0; i<SCRDUMP_BPL; i++, address++) {
				if (address == end_address) done = true;

				b = SAMReadByte(address);
				if (b < 90)
					mem[i] = (byte) conv_from_scode((char) b);
				else
					mem[i] = '.';
			}
			fprintf(fout, " '%s'\n", toString(mem));
		} while (!done && !aborted());
	}


	private static String toString(byte[] mem) {
		StringBuilder sb = new StringBuilder();
		for (byte b: mem) {
			sb.append((char) (b & 0xff));
		}
		return sb.toString();
	}

	/*
	 *  Convert screen code->ASCII
	 */

	static char conv_from_scode(char c) {
		c &= 0x7f;

		if (c <= 31)
			return (char) (c + 64);
		else
			if (c >= 64)
				return (char) (c + 32);
			else
				return c;
	}


	/*
	 *  Binary dump
	 *  b [start] [end]
	 */

	static void binary_dump() {
		boolean done = false;
		char[] bin = new char[10];

		bin[8] = 0;

		if (!range_args(7))  // 8 lines unless end address specified
			return;

		do {
			if (address == end_address) done = true;

			byte_to_bin(SAMReadByte(address), bin);
			fprintf(fout, "%04lx: %s\n", address++, bin);
		} while (!done && !aborted());
	}


	/*
	 *  Sprite data dump
	 *  p [start] [end]
	 */

	static void sprite_dump() {
		boolean done = false;
		short i;
		char[] bin = new char[10];

		bin[8] = 0;

		if (!range_args(21 * 3 - 1))  // 21 lines unless end address specified
			return;

		do {
			fprintf(fout, "%04lx: ", address);
			for (i=0; i<3; i++, address++) {
				if (address == end_address) done = true;

				byte_to_bin(SAMReadByte(address), bin);
				fprintf(fout, "%s", bin);
			}
			fputc('\n', fout);
		} while (!done && !aborted());
	}


	/*
	 *  Convert byte to binary representation
	 */

	static void byte_to_bin(byte b, char[] str) {
		short i;

		for (i=0; i<8; i++, b <<= 1)
			if ((b & 0x80) != 0)
				str[i] = '#';
			else
				str[i] = '.';
	}


	/*
	 *  Disassemble
	 *  d [start] [end]
	 */

	static void disassemble() {
		boolean done = false;
		short i;
		byte[] op = new byte[3];
		int adr;

		if (!range_args(31))  // 32 bytes unless end address specified
			return;

		do {
			fprintf(fout, "%04lx:", adr = address);
			for (i=0; i<3; i++, adr++) {
				if (adr == end_address) done = true;
				op[i] = SAMReadByte(adr);
			}
			address += disass_line(address, op[0], op[1], op[2]);
		} while (!done && !aborted());
	}


	/*
	 *  Disassemble one instruction, return length
	 */

	static int disass_line(int adr, byte op, byte lo, byte hi) {
		AddrMode mode = adr_mode[op & 0xff];
		Mnemonic mnem = mnemonic[op & 0xff];

		// Display instruction bytes in hex
		switch (adr_length[mode.ordinal()]) {
			case 1:
				fprintf(fout, " %02lx       ", op);
				break;

			case 2:
				fprintf(fout, " %02lx %02lx    ", op, lo);
				break;

			case 3:
				fprintf(fout, " %02lx %02lx %02lx ", op, lo, hi);
				break;
		}

		// Tag undocumented opcodes with an asterisk
		if (mnem.ordinal() > M_ILLEGAL.ordinal())
			fputc('*', fout);
		else
			fputc(' ', fout);

		// Print mnemonic
		fprintf(fout, "%c%c%c ", mnem_1[mnem.ordinal()], mnem_2[mnem.ordinal()], mnem_3[mnem.ordinal()]);

		// Pring argument
		switch (mode) {
			case A_IMPL:
				break;

			case A_ACCU:
				fprintf(fout, "a");
				break;

			case A_IMM:
				fprintf(fout, "#%02lx", lo);
				break;

			case A_REL:
				fprintf(fout, "%04lx", ((adr + 2) + (lo & 0xff)) & 0xffff);
				break;

			case A_ZERO:
				fprintf(fout, "%02lx", lo);
				break;

			case A_ZEROX:
				fprintf(fout, "%02lx,x", lo);
				break;

			case A_ZEROY:
				fprintf(fout, "%02lx,y", lo);
				break;

			case A_ABS:
				fprintf(fout, "%04lx", (hi << 8) | lo);
				break;

			case A_ABSX:
				fprintf(fout, "%04lx,x", (hi << 8) | lo);
				break;

			case A_ABSY:
				fprintf(fout, "%04lx,y", (hi << 8) | lo);
				break;

			case A_IND:
				fprintf(fout, "(%04lx)", (hi << 8) | lo);
				break;

			case A_INDX:
				fprintf(fout, "(%02lx,x)", lo);
				break;

			case A_INDY:
				fprintf(fout, "(%02lx),y", lo);
				break;
		}

		fputc('\n', fout);
		return adr_length[mode.ordinal()];
	}


	/*
	 *  Assemble
	 *  a [start]
	 */

	static void assemble() {
		boolean done = false;
		char c1, c2, c3;
		Mnemonic mnem;
		AddrMode mode;
		byte opcode;
		int arg;
		int rel;
	
		// Read parameters
		if (!address_args())
			return;
	
		do {
			fprintf(fout, "%04lx> ", address);
			fflush(ferr);
			read_line();
	
			c1 = get_char();
			c2 = get_char();
			c3 = get_char();
	
			if (c1 != '\n') {
	
				if ((mnem = find_mnemonic(c1, c2, c3)) != M_ILLEGAL) {
	
					get_token();
					Arg6510 args;
					if ((args = instr_args()) != null) {
						arg = args.number;
						mode = args.mode;
	
						// Convert A_IMPL -> A_ACCU if necessary
						Integer op;
						if ((mode == A_IMPL) && (op = find_opcode(mnem, A_ACCU)) != null)
							mode = A_ACCU;
	
						// Handle relative addressing seperately
						if (((mode == A_ABS) || (mode == A_ZERO)) && (op = find_opcode(mnem, A_REL)) != null) {
							mode = A_REL;
							rel = arg - (address + 2) & 0xffff;
							if ((rel < -128) || (rel > 127)) {
								error("Branch too long");
								continue;
							} else
								arg = rel & 0xff;
						}
	
						if ((op = find_opcode(mnem, mode)) != null) {
							opcode = op.byteValue();
							// Print disassembled line
							fprintf(fout, "\013%04lx:", address);
							disass_line(address, opcode, (byte) (arg & 0xff), (byte) (arg >> 8));
	
							switch (adr_length[mode.ordinal()]) {
								case 1:
									SAMWriteByte(address++, opcode);
									break;
	
								case 2:
									SAMWriteByte(address++, opcode);
									SAMWriteByte(address++, (byte) arg);
									break;
	
								case 3:
									SAMWriteByte(address++, opcode);
									SAMWriteByte(address++, (byte) (arg & 0xff));
									SAMWriteByte(address++, (byte) (arg >> 8));
									break;
	
								default:
									error("Internal error");
									break;
							}
	
						} else
							error("Addressing mode not supported by instruction");
	
					} else
						error("Unrecognized addressing mode");
	
				} else
					error("Unknown instruction");
	
			} else			// Input is terminated with a blank line
				done = true;
		} while (!done);
	}


	/*
	 *  Find mnemonic code to three letters
	 *  M_ILLEGAL: No matching mnemonic found
	 */

	static Mnemonic find_mnemonic(char op1, char op2, char op3) {
		int i;

		for (i=0; i<M_MAXIMUM.ordinal(); i++)
			if ((mnem_1[i] == op1) && (mnem_2[i] == op2) && (mnem_3[i] == op3))
				return Mnemonic.values()[i];

		return M_ILLEGAL;
	}


	/*
	 *  Determine opcode of an instruction given mnemonic and addressing mode
	 *  true: OK, false: Mnemonic can't have specified addressing mode
	 */

	static Integer find_opcode(Mnemonic mnem, AddrMode mode) {
		int i;

		for (i=0; i<256; i++)
			if ((mnemonic[i] == mnem) && (adr_mode[i] == mode)) {
				return i;
			}

		return null;
	}


	/*
	 *  Show/set memory configuration
	 *  k [config]
	 */

	static void mem_config() {
		Integer con;

		if (the_token != T_END)
			if ((con = expression()) == null)
				return;
			else
				TheCPU.ExtConfig = con;
		else
			con = TheCPU.ExtConfig;

		fprintf(fout, "Configuration: %ld\n", con & 7);
		fprintf(fout, "A000-BFFF: %s\n", (con & 3) == 3 ? "Basic" : "RAM");
		fprintf(fout, "D000-DFFF: %s\n", ((con & 3) != 0) ? (((con & 4) != 0) ? "I/O" : "Char") : "RAM");
		fprintf(fout, "E000-FFFF: %s\n", ((con & 2) != 0) ? "Kernal" : "RAM");
	}


	/*
	 *  Fill
	 *  f start end byte
	 */

	static void fill() {
		boolean done = false;
		Integer adr, end_adr, value;

		if ((adr = expression()) == null)
			return;
		if ((end_adr = expression()) == null)
			return;
		if ((value = expression()) == null)
			return;

		do {
			if (adr == end_adr) done = true;

			SAMWriteByte(adr++, value.byteValue());
		} while (!done);
	}


	/*
	 *  Compare
	 *  c start end dest
	 */

	static void compare() {
		boolean done = false;
		Integer adr, end_adr, dest;
		int num = 0;

		if ((adr = expression()) == null)
			return;
		if ((end_adr = expression()) == null)
			return;
		if ((dest = expression()) == null)
			return;

		do {
			if (adr == end_adr) done = true;

			if (SAMReadByte(adr) != SAMReadByte(dest)) {
				fprintf(fout, "%04lx ", adr);
				num++;
				if ((num & 7) == 0)
					fputc('\n', fout);
			}
			adr++; dest++;
		} while (!done && !aborted());

		if ((num & 7) != 0)
			fputc('\n', fout);
		fprintf(fout, "%ld byte(s) different\n", num);
	}


	/*
	 *  Transfer memory
	 *  t start end dest
	 */

	static void transfer() {
		boolean done = false;
		Integer adr, end_adr, dest;

		if ((adr = expression()) == null)
			return;
		if ((end_adr = expression()) == null)
			return;
		if ((dest = expression()) == null)
			return;

		if (dest < adr)
			do {
				if (adr == end_adr) done = true;
				SAMWriteByte(dest++, SAMReadByte(adr++));
			} while (!done);
		else {
			dest += end_adr - adr;
			do {
				if (adr == end_adr) done = true;
				SAMWriteByte(dest--, SAMReadByte(end_adr--));
			} while (!done);
		}
	}


	/*
	 *  Change memory
	 *  : addr {byte}
	 */

	static void modify() {
		Integer adr, val;

		if ((adr = expression()) == null)
			return;

		while (the_token != T_END)
			if ((val = expression()) != null)
				SAMWriteByte(adr++, val.byteValue());
			else
				return;
	}


	/*
	 *  Compute and display expression
	 *  ? expression
	 */

	static void print_expr() {
		Integer val;

		if ((val = expression()) == null)
			return;

		fprintf(fout, "Hex: %04lx\nDec: %lu\n", val, val);
	}


	/*
	 *  Redirect output
	 *  o [file]
	 */

	static void redir_output() {
		// Close old file
		if (fout != ferr) {
			fclose(fout);
			fout = ferr;
			return;
		}

		// No argument given?
		if (the_token == T_END)
			return;

		// Otherwise open file
		if (the_token == T_STRING) {
			if ((fout = fopen(new File(new String(the_string)), "w")) == null)
				error("Unable to open file");
		} else
			error("'\"' around file name expected");
	}


	/*
	 *  Display interrupt vectors
	 */

	static void int_vectors() {
		fprintf(fout, "        IRQ  BRK  NMI\n");
		fprintf(fout, "%d  : %04lx %04lx %04lx\n",
			access_1541 ? 6502 : 6510,
			SAMReadByte(0xffff) << 8 | SAMReadByte(0xfffe),
			SAMReadByte(0xffff) << 8 | SAMReadByte(0xfffe),
			SAMReadByte(0xfffb) << 8 | SAMReadByte(0xfffa));

		if (!access_1541 && (TheCPU.ExtConfig & 2) != 0)
			fprintf(fout, "Kernal: %04lx %04lx %04lx\n",
				SAMReadByte(0x0315) << 8 | SAMReadByte(0x0314),
				SAMReadByte(0x0317) << 8 | SAMReadByte(0x0316),
				SAMReadByte(0x0319) << 8 | SAMReadByte(0x0318));
	}


	/*
	 *  Display state of custom chips
	 */

	static void view_state() {
		switch (get_char()) {
			case 'c':		// CIA
				view_cia_state();
				break;

			case 's':		// SID
				view_sid_state();
				break;

			case 'v':		// VIC
				view_vic_state();
				break;

			case 'f':		// Floppy
				view_1541_state();
				break;

			default:
				error("Unknown command");
				break;
		}
	}

	static void view_cia_state() {
		MOS6526State cs;

		switch (get_char()) {
			case '1':
				cs = TheCIA1.GetState();
				break;
			case '2':
				cs = TheCIA2.GetState();
				break;
			default:
				error("Unknown command");
				return;
		}

		fprintf(fout, "Timer A  : %s\n", (cs.cra & 1) != 0 ? "On" : "Off");
		fprintf(fout, " Counter : %04lx  Latch: %04lx\n", (cs.ta_hi << 8) | cs.ta_lo, cs.latcha);
		fprintf(fout, " Run mode: %s\n", (cs.cra & 8) != 0 ? "One-shot" : "Continuous");
		fprintf(fout, " Input   : %s\n", (cs.cra & 0x20) != 0 ? "CNT" : "Phi2");
		fprintf(fout, " Output  : ");
		if ((cs.cra & 2) != 0)
			if ((cs.cra & 4) != 0)
				fprintf(fout, "PB6 Toggle\n\n");
			else
				fprintf(fout, "PB6 Pulse\n\n");
		else
			fprintf(fout, "None\n\n");

		fprintf(fout, "Timer B  : %s\n", (cs.crb & 1) != 0 ? "On" : "Off");
		fprintf(fout, " Counter : %04lx  Latch: %04lx\n", (cs.tb_hi << 8) | cs.tb_lo, cs.latchb);
		fprintf(fout, " Run mode: %s\n", (cs.crb & 8) != 0 ? "One-shot" : "Continuous");
		fprintf(fout, " Input   : ");
		if ((cs.crb & 0x40) != 0)
			if ((cs.crb & 0x20) != 0)
				fprintf(fout, "Timer A underflow (CNT high)\n");
			else
				fprintf(fout, "Timer A underflow\n");
		else
			if ((cs.crb & 0x20) != 0)
				fprintf(fout, "CNT\n");
			else
				fprintf(fout, "Phi2\n");
		fprintf(fout, " Output  : ");
		if ((cs.crb & 2) != 0)
			if ((cs.crb & 4) != 0)
				fprintf(fout, "PB7 Toggle\n\n");
			else
				fprintf(fout, "PB7 Pulse\n\n");
		else
			fprintf(fout, "None\n\n");

		fprintf(fout, "TOD         : %lx%lx:%lx%lx:%lx%lx.%lx %s\n",
			(cs.tod_hr >> 4) & 1, cs.tod_hr & 0x0f,
			(cs.tod_min >> 4) & 7, cs.tod_min & 0x0f,
			(cs.tod_sec >> 4) & 7, cs.tod_sec & 0x0f,
			cs.tod_10ths & 0x0f, (cs.tod_hr & 0x80) != 0 ? "PM" : "AM");
		fprintf(fout, "Alarm       : %lx%lx:%lx%lx:%lx%lx.%lx %s\n",
			(cs.alm_hr >> 4) & 1, cs.alm_hr & 0x0f,
			(cs.alm_min >> 4) & 7, cs.alm_min & 0x0f,
			(cs.alm_sec >> 4) & 7, cs.alm_sec & 0x0f,
			cs.alm_10ths & 0x0f, (cs.alm_hr & 0x80) != 0 ? "PM" : "AM");
		fprintf(fout, "TOD input   : %s\n", (cs.cra & 0x80) != 0 ? "50Hz" : "60Hz");
		fprintf(fout, "Write to    : %s registers\n\n", (cs.crb & 0x80) != 0 ? "Alarm" : "TOD");

		fprintf(fout, "Serial data : %02lx\n", cs.sdr);
		fprintf(fout, "Serial mode : %s\n\n", (cs.cra & 0x40) != 0 ? "Output" : "Input");

		fprintf(fout, "Pending int.: ");
		dump_cia_ints(cs.int_data);
		fprintf(fout, "Enabled int.: ");
		dump_cia_ints(cs.int_mask);
	}

	static void dump_cia_ints(byte i) {
		if ((i & 0x1f) != 0) {
			if ((i & 1) != 0) fprintf(fout, "TA ");
			if ((i & 2) != 0) fprintf(fout, "TB ");
			if ((i & 4) != 0) fprintf(fout, "Alarm ");
			if ((i & 8) != 0) fprintf(fout, "Serial ");
			if ((i & 0x10) != 0) fprintf(fout, "Flag");
		} else
			fprintf(fout, "None");
		fputc('\n', fout);
	}

	static void view_sid_state() {
		MOS6581State ss;

		ss = TheSID.GetState();

		fprintf(fout, "Voice 1\n");
		fprintf(fout, " Frequency  : %04lx\n", (ss.freq_hi_1 << 8) | ss.freq_lo_1);
		fprintf(fout, " Pulse Width: %04lx\n", ((ss.pw_hi_1 & 0x0f) << 8) | ss.pw_lo_1);
		fprintf(fout, " Env. (ADSR): %lx %lx %lx %lx\n", ss.AD_1 >> 4, ss.AD_1 & 0x0f, ss.SR_1 >> 4, ss.SR_1 & 0x0f);
		fprintf(fout, " Waveform   : ");
		dump_sid_waveform(ss.ctrl_1);
		fprintf(fout, " Gate       : %s  Ring mod.: %s\n", (ss.ctrl_1 & 0x01) != 0 ? "On " : "Off", (ss.ctrl_1 & 0x04) != 0 ? "On" : "Off");
		fprintf(fout, " Test bit   : %s  Synchron.: %s\n", (ss.ctrl_1 & 0x08) != 0 ? "On " : "Off", (ss.ctrl_1 & 0x02) != 0 ? "On" : "Off");
		fprintf(fout, " Filter     : %s\n", (ss.res_filt & 0x01) != 0 ? "On" : "Off");

		fprintf(fout, "\nVoice 2\n");
		fprintf(fout, " Frequency  : %04lx\n", (ss.freq_hi_2 << 8) | ss.freq_lo_2);
		fprintf(fout, " Pulse Width: %04lx\n", ((ss.pw_hi_2 & 0x0f) << 8) | ss.pw_lo_2);
		fprintf(fout, " Env. (ADSR): %lx %lx %lx %lx\n", ss.AD_2 >> 4, ss.AD_2 & 0x0f, ss.SR_2 >> 4, ss.SR_2 & 0x0f);
		fprintf(fout, " Waveform   : ");
		dump_sid_waveform(ss.ctrl_2);
		fprintf(fout, " Gate       : %s  Ring mod.: %s\n", (ss.ctrl_2 & 0x01) != 0 ? "On " : "Off", (ss.ctrl_2 & 0x04) != 0 ? "On" : "Off");
		fprintf(fout, " Test bit   : %s  Synchron.: %s\n", (ss.ctrl_2 & 0x08) != 0 ? "On " : "Off", (ss.ctrl_2 & 0x02) != 0 ? "On" : "Off");
		fprintf(fout, " Filter     : %s\n", (ss.res_filt & 0x02) != 0 ? "On" : "Off");

		fprintf(fout, "\nVoice 3\n");
		fprintf(fout, " Frequency  : %04lx\n", (ss.freq_hi_3 << 8) | ss.freq_lo_3);
		fprintf(fout, " Pulse Width: %04lx\n", ((ss.pw_hi_3 & 0x0f) << 8) | ss.pw_lo_3);
		fprintf(fout, " Env. (ADSR): %lx %lx %lx %lx\n", ss.AD_3 >> 4, ss.AD_3 & 0x0f, ss.SR_3 >> 4, ss.SR_3 & 0x0f);
		fprintf(fout, " Waveform   : ");
		dump_sid_waveform(ss.ctrl_3);
		fprintf(fout, " Gate       : %s  Ring mod.: %s\n", (ss.ctrl_3 & 0x01) != 0 ? "On " : "Off", (ss.ctrl_3 & 0x04) != 0 ? "On" : "Off");
		fprintf(fout, " Test bit   : %s  Synchron.: %s\n", (ss.ctrl_3 & 0x08) != 0 ? "On " : "Off", (ss.ctrl_3 & 0x02) != 0 ? "On" : "Off");
		fprintf(fout, " Filter     : %s  Mute     : %s\n", (ss.res_filt & 0x04) != 0 ? "On" : "Off", (ss.mode_vol & 0x80) != 0 ? "Yes" : "No");

		fprintf(fout, "\nFilters/Volume\n");
		fprintf(fout, " Frequency: %04lx\n", (ss.fc_hi << 3) | (ss.fc_lo & 0x07));
		fprintf(fout, " Resonance: %lx\n", ss.res_filt >> 4);
		fprintf(fout, " Mode     : ");
		if ((ss.mode_vol & 0x70) != 0) {
			if ((ss.mode_vol & 0x10) != 0) fprintf(fout, "Low-pass ");
			if ((ss.mode_vol & 0x20) != 0) fprintf(fout, "Band-pass ");
			if ((ss.mode_vol & 0x40) != 0) fprintf(fout, "High-pass");
		} else
			fprintf(fout, "None");
		fprintf(fout, "\n Volume   : %lx\n", ss.mode_vol & 0x0f);
	}

	static void dump_sid_waveform(byte wave) {
		if ((wave & 0xf0) != 0) {
			if ((wave & 0x10) != 0) fprintf(fout, "Triangle ");
			if ((wave & 0x20) != 0) fprintf(fout, "Sawtooth ");
			if ((wave & 0x40) != 0) fprintf(fout, "Rectangle ");
			if ((wave & 0x80) != 0) fprintf(fout, "Noise");
		} else
			fprintf(fout, "None");
		fputc('\n', fout);
	}

	static void view_vic_state() {
		MOS6569State vs;
		short i;

		vs = TheVIC.GetState();

		fprintf(fout, "Raster line       : %04lx\n", vs.raster | ((vs.ctrl1 & 0x80) << 1));
		fprintf(fout, "IRQ raster line   : %04lx\n\n", vs.irq_raster);

		fprintf(fout, "X scroll          : %ld\n", vs.ctrl2 & 7);
		fprintf(fout, "Y scroll          : %ld\n", vs.ctrl1 & 7);
		fprintf(fout, "Horizontal border : %ld columns\n", (vs.ctrl2 & 8) != 0 ? 40 : 38);
		fprintf(fout, "Vertical border   : %ld rows\n\n", (vs.ctrl1 & 8) != 0 ? 25 : 24);

		fprintf(fout, "Display mode      : ");
		switch (((vs.ctrl1 >> 4) & 6) | ((vs.ctrl2 >> 4) & 1)) {
			case 0:
				fprintf(fout, "Standard text\n");
				break;
			case 1:
				fprintf(fout, "Multicolor text\n");
				break;
			case 2:
				fprintf(fout, "Standard bitmap\n");
				break;
			case 3:
				fprintf(fout, "Multicolor bitmap\n");
				break;
			case 4:
				fprintf(fout, "ECM text\n");
				break;
			case 5:
				fprintf(fout, "Invalid text (ECM+MCM)\n");
				break;
			case 6:
				fprintf(fout, "Invalid bitmap (ECM+BMM)\n");
				break;
			case 7:
				fprintf(fout, "Invalid bitmap (ECM+BMM+ECM)\n");
				break;
		}
		fprintf(fout, "Sequencer state   : %s\n", vs.display_state ? "Display" : "Idle");
		fprintf(fout, "Bad line state    : %s\n", vs.bad_line ? "Yes" : "No");
		fprintf(fout, "Bad lines enabled : %s\n", vs.bad_line_enable ? "Yes" : "No");
		fprintf(fout, "Video counter     : %04lx\n", vs.vc);
		fprintf(fout, "Video counter base: %04lx\n", vs.vc_base);
		fprintf(fout, "Row counter       : %ld\n\n", vs.rc);

		fprintf(fout, "VIC bank          : %04lx-%04lx\n", vs.bank_base, vs.bank_base + 0x3fff);
		fprintf(fout, "Video matrix base : %04lx\n", vs.matrix_base);
		fprintf(fout, "Character base    : %04lx\n", vs.char_base);
		fprintf(fout, "Bitmap base       : %04lx\n\n", vs.bitmap_base);

		fprintf(fout, "         Spr.0  Spr.1  Spr.2  Spr.3  Spr.4  Spr.5  Spr.6  Spr.7\n");
		fprintf(fout, "Enabled: "); dump_spr_flags(vs.me);
		fprintf(fout, "Data   : %04lx   %04lx   %04lx   %04lx   %04lx   %04lx   %04lx   %04lx\n",
			vs.sprite_base[0], vs.sprite_base[1], vs.sprite_base[2], vs.sprite_base[3],
			vs.sprite_base[4], vs.sprite_base[5], vs.sprite_base[6], vs.sprite_base[7]);
		fprintf(fout, "MC     : %02lx     %02lx     %02lx     %02lx     %02lx     %02lx     %02lx     %02lx\n",
			vs.mc[0], vs.mc[1], vs.mc[2], vs.mc[3], vs.mc[4], vs.mc[5], vs.mc[6], vs.mc[7]);

		fprintf(fout, "Mode   : ");
		for (i=0; i<8; i++)
			if ((vs.mmc & (1<<i)) != 0)
				fprintf(fout, "Multi  ");
			else
				fprintf(fout, "Std.   ");

		fprintf(fout, "\nX-Exp. : "); dump_spr_flags(vs.mxe);
		fprintf(fout, "Y-Exp. : "); dump_spr_flags(vs.mye);

		fprintf(fout, "Prio.  : ");
		for (i=0; i<8; i++)
			if ((vs.mdp & (1<<i)) != 0)
				fprintf(fout, "Back   ");
			else
				fprintf(fout, "Fore   ");

		fprintf(fout, "\n\nPending interrupts: ");
		dump_vic_ints(vs.irq_flag);
		fprintf(fout, "Enabled interrupts: ");
		dump_vic_ints(vs.irq_mask);
	}

	static void dump_spr_flags(byte f) {
		short i;

		for (i=0; i<8; i++)
			if ((f & (1<<i)) != 0)
				fprintf(fout, "Yes    ");
			else
				fprintf(fout, "No     ");

		fputc('\n', fout);
	}

	static void dump_vic_ints(byte i) {
		if ((i & 0x1f) != 0) {
			if ((i & 1) != 0) fprintf(fout, "Raster ");
			if ((i & 2) != 0) fprintf(fout, "Spr-Data ");
			if ((i & 4) != 0) fprintf(fout, "Spr-Spr ");
			if ((i & 8) != 0) fprintf(fout, "Lightpen");
		} else
			fprintf(fout, "None");
		fputc('\n', fout);
	}

	static void view_1541_state() {
		fprintf(fout, "VIA 1:\n");
		fprintf(fout, " Timer 1 Counter: %04x  Latch: %04x\n", R1541.via1_t1c, R1541.via1_t1l);
		fprintf(fout, " Timer 2 Counter: %04x  Latch: %04x\n", R1541.via1_t2c, R1541.via1_t2l);
		fprintf(fout, " ACR: %02x\n", R1541.via1_acr);
		fprintf(fout, " PCR: %02x\n", R1541.via1_pcr);
		fprintf(fout, " Pending interrupts: ");
		dump_via_ints(R1541.via1_ifr);
		fprintf(fout, " Enabled interrupts: ");
		dump_via_ints(R1541.via1_ier);

		fprintf(fout, "\nVIA 2:\n");
		fprintf(fout, " Timer 1 Counter: %04x  Latch: %04x\n", R1541.via2_t1c, R1541.via2_t1l);
		fprintf(fout, " Timer 2 Counter: %04x  Latch: %04x\n", R1541.via2_t2c, R1541.via2_t2l);
		fprintf(fout, " ACR: %02x\n", R1541.via2_acr);
		fprintf(fout, " PCR: %02x\n", R1541.via2_pcr);
		fprintf(fout, " Pending interrupts: ");
		dump_via_ints(R1541.via2_ifr);
		fprintf(fout, " Enabled interrupts: ");
		dump_via_ints(R1541.via2_ier);
	}

	static void dump_via_ints(byte i) {
		if ((i & 0x7f) != 0) {
			if ((i & 0x40) != 0) fprintf(fout, "T1 ");
			if ((i & 0x20) != 0) fprintf(fout, "T2 ");
			if ((i & 2) != 0) fprintf(fout, "CA1 ");
			if ((i & 1) != 0) fprintf(fout, "CA2 ");
			if ((i & 0x10) != 0) fprintf(fout, "CB1 ");
			if ((i & 8) != 0) fprintf(fout, "CB2 ");
			if ((i & 4) != 0) fprintf(fout, "Serial ");
		} else
			fprintf(fout, "None");
		fputc('\n', fout);
	}


	/*
	 *  Load data
	 *  l start "file"
	 */

	static void load_data() {
		Integer adr;
		StdioFile file;
		int fc;

		if ((adr = expression()) == null)
			return;
		if (the_token == T_END) {
			error("Missing file name");
			return;
		}
		if (the_token != T_STRING) {
			error("'\"' around file name expected");
			return;
		}

		if ((file = fopen(new File(new String(the_string)), "rb")) == null)
			error("Unable to open file");
		else {
			while ((fc = fgetc(file)) != EOF)
				SAMWriteByte(adr++, (byte) fc);
			fclose(file);
		}
	}


	/*
	 *  Save data
	 *  s start end "file"
	 */

	static void save_data() {
		boolean done = false;
		int adr, end_adr;
		StdioFile file;

		Integer i;
		if ((i = expression()) == null)
			return;
		adr = i;
		if ((i = expression()) == null)
			return;
		end_adr = i;
		if (the_token == T_END) {
			error("Missing file name");
			return;
		}
		if (the_token != T_STRING) {
			error("'\"' around file name expected");
			return;
		}

		if ((file = fopen(new File(new String(the_string)), "wb")) == null)
			error("Unable to create file");
		else {
			do {
				if (adr == end_adr) done = true;

				fputc(SAMReadByte(adr++), file);
			} while (!done);
			fclose(file);
		}
	}

}
