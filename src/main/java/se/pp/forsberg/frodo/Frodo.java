package se.pp.forsberg.frodo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import se.pp.forsberg.frodo.prefs.Prefs;

public class Frodo extends Application {

	public static void main(String[] args) {
		launch(args);
	}
	
	private Stage stage;
	
	private C64 theC64;

	@Override
	public void start(Stage stage) throws Exception {
		this.stage = stage;
		stage.setOnCloseRequest(this::stop);
		
		new Thread(() -> {
			try {
				readyToRun();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}, "Frodo emulator thread").start();
	}
	
	private void c64Ready() {
		theC64.TheDisplay.JavaFXInit(stage);
	}
	
	private static File homeDir = new File(System.getProperty("user.home"));
	static File frodoDir = new File(homeDir, ".Frodo");
	public static File prefsFile = new File(frodoDir, "prefs.txt");
	public static File autoSnapFile = new File(frodoDir, "auto.snp");
	
	private void readyToRun() throws IOException {
		Files.createDirectories(frodoDir.toPath());
		Prefs.ThePrefs.Load(prefsFile);
		
//		if (Prefs.ThePrefs.ShowEditor(true, prefsFile)) {

			// Create and start C64
			theC64 = new C64();
			if (loadRomFiles()) {
				Platform.runLater(this::c64Ready);
				theC64.Run();
			} else {
				System.err.println("No rom files found");
				System.exit(1);
			}
//		}
	}
	private final static String BASIC_ROM_FILE = "Basic ROM"; // "Basic ROM"
	private final static String KERNAL_ROM_FILE = "Kernal ROM"; // "Kernal ROM"
	private final static String CHAR_ROM_FILE = "Char ROM"; // "Char ROM"
	private final static String FLOPPY_ROM_FILE = "1541 ROM"; // "1541 ROM"

	private boolean loadRomFiles() {
		try {
			return loadBasic(theC64.Basic) &&
			       loadKernal(theC64.Kernal) &&
			       loadCharRom(theC64.Char) &&
			       loadFloppyRom(theC64.ROM1541);
		} catch (Exception x) {
			theC64.TheDisplay.ShowRequester(
					x.getMessage(),
					"Quit",
					() -> System.exit(1));
			return false;
		}
	}
	static boolean loadBasic(byte[] rom) { return loadRom(BASIC_ROM_FILE, rom); }
	static boolean loadKernal(byte[] rom) { return loadRom(KERNAL_ROM_FILE, rom); }
	static boolean loadCharRom(byte[] rom) { return loadRom(CHAR_ROM_FILE, rom); }
	static boolean loadFloppyRom(byte[] rom) { return loadRom(FLOPPY_ROM_FILE, rom); }
	static boolean loadRom(String file, byte[] rom) {
		try (InputStream is = Frodo.class.getResourceAsStream(file)) {
			int totRead = 0;
			do {
				int read = is.read(rom, totRead, rom.length - totRead);
				if (read < 0) {
					throw new IOException("ROM file truncated");
				}
				totRead += read;
			} while (totRead < rom.length);
		} catch (Exception x) {
			throw new RuntimeException("Can't read '"+ file + "'.", x);
		}
		return true;
	}
	

	public void stop(WindowEvent e) {
		stop();
	}
	@Override
	public void stop() {
		theC64.TheDisplay.quit();
	}
}
