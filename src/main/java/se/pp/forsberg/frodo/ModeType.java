package se.pp.forsberg.frodo;

import se.pp.forsberg.frodo.enums.AccessMode;
import se.pp.forsberg.frodo.enums.FileType;

class ModeType {
	AccessMode filemode =  AccessMode.FMODE_READ;
	FileType filetype = FileType.FTYPE_PRG;
}