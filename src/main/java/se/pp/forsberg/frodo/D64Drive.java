package se.pp.forsberg.frodo;

import java.io.File;

import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.libc.BPtr;
import se.pp.forsberg.libc.ByteC;
import se.pp.forsberg.libc.StdioFile;

import static se.pp.forsberg.libc.LibC.*;
import static se.pp.forsberg.libc.SeekWhence.*;
import static se.pp.forsberg.frodo.D64Drive.ChannelMode.*;
import static se.pp.forsberg.frodo.enums.ErrorCode1541.*;
import static se.pp.forsberg.frodo.enums.AccessMode.*;
import static se.pp.forsberg.frodo.enums.FileType.*;

public class D64Drive extends Drive {
	
	private final static int NUM_TRACKS = 35;
	private final static int NUM_SECTORS = 683;
	
	private class DiskLocation {
		int track, sector;
		public DiskLocation() {}
		public DiskLocation(int track, int sector) {
			this.track = track;
			this.sector = sector;
		}
	}
	// BAM structure
	private static class BAM extends BPtr {
		public final static int SIZE = 256;
		public BPtr dir_track = new BPtr(this, 0); 		// Track...
		public BPtr dir_sector = new BPtr(this, 1);		// ...and sector of first directory block
		public BPtr fmt_type = new BPtr(this, 2);		// Format type
		public BPtr pad0 = new BPtr(this, 3);
		public BPtr	bitmap = new BPtr(buf, 4); 			// Sector allocation
		public BPtr	disk_name = new BPtr(buf, 144); 	// Disk name 18
		public BPtr	id = new BPtr(buf, 162);			// Disk ID
		public BPtr	pad1 = new BPtr(buf, 164);
		public BPtr	fmt_char = new BPtr(buf, 165);		// Format characters
		public BPtr	pad2 = new BPtr(buf, 167);
		public BPtr	pad3 = new BPtr(buf, 171);
		// 256
		public BAM(BPtr bPtr) {
			super(bPtr);
		}
	}

	// Directory entry structure
	private static class DirEntry extends BPtr {
		public final static int SIZE = 32;
		
		BPtr type = new BPtr(this, 0);			// File type
		BPtr track = new BPtr(this, 1);			// Track...
		BPtr sector = new BPtr(this, 2);		// ...and sector of first data block
		BPtr name = new BPtr(this, 3);			// File name
		BPtr side_track = new BPtr(this, 19);	// Track...
		BPtr side_sector = new BPtr(this, 20);	// ...and sector of first side sector
		BPtr rec_len = new BPtr(this, 21);		// Record length
		BPtr pad0 = new BPtr(this, 22);
		BPtr ovr_track = new BPtr(this, 26);	// Track...
		BPtr ovr_sector = new BPtr(this, 27);	// ...and sector on overwrite
		BPtr num_blocks_l = new BPtr(this, 28);	// Number of blocks, LSB
		BPtr num_blocks_h = new BPtr(this, 29);	// Number of blocks, MSB
		BPtr pad1 = new BPtr(this, 30);
		// 32
		public DirEntry(BPtr p) {
			super(p);
		}
		public DirEntry(BPtr p, int offs) {
			super(p, offs);
		}
	}

	// Directory block structure
	private static class Directory extends BPtr {
		public final static int SIZE = 131;
		
		BPtr	padding = new BPtr(this, 0);
		BPtr	next_track = new BPtr(this, 2);
		BPtr	next_sector = new BPtr(this, 3);
		DirEntry[]	entry = {
			new DirEntry(this, 4 + DirEntry.SIZE * 0),
			new DirEntry(this, 4 + DirEntry.SIZE * 1),
			new DirEntry(this, 4 + DirEntry.SIZE * 2),
			new DirEntry(this, 4 + DirEntry.SIZE * 3),
			new DirEntry(this, 4 + DirEntry.SIZE * 4),
			new DirEntry(this, 4 + DirEntry.SIZE * 5),
			new DirEntry(this, 4 + DirEntry.SIZE * 6),
			new DirEntry(this, 4 + DirEntry.SIZE * 7)
		};
		public Directory() {
			super(new byte[SIZE]);
		}
		public Directory(BPtr p) {
			super(p);
		}
	}
	// Channel modes (IRC users listen up :-)
	public enum ChannelMode {
		CHMOD_FREE,			// Channel free
		CHMOD_COMMAND,		// Command/error channel
		CHMOD_DIRECTORY,	// Reading directory
		CHMOD_FILE,			// Sequential file open
		CHMOD_DIRECT		// Direct buffer access ('#')
	}

	File orig_d64_name; // Original path of .d64 file

	StdioFile the_file;			// File pointer for .d64 file

	byte[] ram;				// 2KB 1541 RAM
	BAM bam;				// Pointer to BAM
	Directory dir = new Directory();			// Buffer for directory blocks

	ChannelMode[] chan_mode = new ChannelMode[16];		// Channel mode
	int[] chan_buf_num = new int[16];	// Buffer number of channel (for direct access channels)
	BPtr[] chan_buf = new BPtr[16];	// Pointer to buffer
	BPtr[] buf_ptr = new BPtr[16];		// Pointer in buffer
	int[] buf_len = new int[16];		// Remaining bytes in buffer

	boolean[] buf_free = new boolean[4];		// Buffer 0..3 free?

	byte[] cmd_buffer = new byte[44];	// Buffer for incoming command strings
	int cmd_len;			// Length of received command

	int image_header;		// Length of .d64 file header

	byte[] error_info = new byte[683];	// Sector error information (1 byte/sector)

	public D64Drive(IEC iec, File filepath) {
		super(iec);
		the_file = null;
		ram = null;

		Ready = false;
		orig_d64_name = filepath;
		for (int i=0; i<=14; i++) {
			chan_mode[i] = CHMOD_FREE;
			chan_buf[i] = null;
		}
		chan_mode[15] = CHMOD_COMMAND;

		// Open .d64 file
		open_close_d64_file(filepath);
		if (the_file != null) {

			// Allocate 1541 RAM
			ram = new byte[0x800];
			bam = new BAM(new BPtr(ram, 0x700));

			Reset();
			Ready = true;
		}
	}
//	D64Drive::~D64Drive()
//	{
//		// Close .d64 file
//		open_close_d64_file("");
//
//		delete[] ram;
//		Ready = false;
//	}
	@Override
	public byte Open(int channel, byte[] filename) {
		set_error(ERR_OK);

		// Channel 15: execute file name as command
		if (channel == 15) {
			execute_command(filename);
			return ST_OK;
		}

		if (chan_mode[channel] != CHMOD_FREE) {
			set_error(ERR_NOCHANNEL);
			return ST_OK;
		}

		if (filename[0] == '$')
			if (channel != 0)
				return open_file_ts(channel, new DiskLocation(18, 0));
			else
				return open_directory(new BPtr(filename, 1));

		if (filename[0] == '#')
			return open_direct(channel, filename);

		return open_file(channel, filename);
	}
	@Override
	public byte Close(int channel) {
		if (channel == 15) {
			close_all_channels();
			return ST_OK;
		}

		switch (chan_mode[channel]) {
			case CHMOD_FREE:
				break;
	 
			case CHMOD_DIRECT:
				free_buffer(chan_buf_num[channel]);
				chan_buf[channel] = null;
				chan_mode[channel] = CHMOD_FREE;
				break;

			default:
				chan_buf[channel] = null;
				chan_mode[channel] = CHMOD_FREE;
				break;
		}

		return ST_OK;
	}
	@Override
	public byte Read(int channel, ByteC b) {
	switch (chan_mode[channel]) {
		case CHMOD_COMMAND:
			b.b = error_ptr.getInc();
			if (--error_len != 0)
				return ST_OK;
			else {
				set_error(ERR_OK);
				return ST_EOF;
			}

		case CHMOD_FILE:
			// Read next block if necessary
			if (chan_buf[channel].get() != 0 && buf_len[channel] == 0) {
				if (!read_sector(chan_buf[channel].get(), chan_buf[channel].get(1), chan_buf[channel]))
					return ST_READ_TIMEOUT;
				buf_ptr[channel] = new BPtr(chan_buf[channel], 2);

				// Determine block length
				buf_len[channel] = chan_buf[channel].get() != 0 ? 254 : (byte) (chan_buf[channel].get(1)-1);
			}

			if (buf_len[channel] > 0) {
				b.b = buf_ptr[channel].getInc();
				if (--buf_len[channel] == 0 && chan_buf[channel].get() == 0)
					return ST_EOF;
				else
					return ST_OK;
			} else
				return ST_READ_TIMEOUT;
		
		case CHMOD_DIRECTORY:
		case CHMOD_DIRECT:
			if (buf_len[channel] > 0) {
				b.b = buf_ptr[channel].getInc();
				if (--buf_len[channel] != 0)
					return ST_OK;
				else
					return ST_EOF;
			} else
				return ST_READ_TIMEOUT;
		default:
			break;
		}
		return ST_READ_TIMEOUT;
	}
	@Override
	public byte Write(int channel, byte b, boolean eoi) {

		switch (chan_mode[channel]) {
			case CHMOD_FREE:
				set_error(ERR_FILENOTOPEN);
				break;

			case CHMOD_COMMAND:
				// Collect characters and execute command on EOI
				if (cmd_len >= 40)
					return ST_TIMEOUT;

				cmd_buffer[cmd_len++] = b;

				if (eoi) {
					cmd_buffer[cmd_len++] = 0;
					cmd_len = 0;
					execute_command(cmd_buffer);
				}
				return ST_OK;

			case CHMOD_DIRECTORY:
				set_error(ERR_WRITEFILEOPEN);
				break;
		default:
			break;
		}
		return ST_TIMEOUT;
	}
	@Override
	public void Reset() {
		close_all_channels();

		read_sector(18, 0, bam);

		cmd_len = 0;
		for (int i=0; i<4; i++)
			buf_free[i] = true;

		set_error(ERR_STARTUP);
	}

	private void open_close_d64_file(File d64name) {
		long size;
		byte[] magic = new byte[4];

		// Close old .d64, if open
		if (the_file != null) {
			close_all_channels();
			fclose(the_file);
			the_file = null;
		}

		// Open new .d64 file
		if (d64name != null) {
			if ((the_file = fopen(d64name, "rb")) != null) {

				// Check length
				fseek(the_file, 0, SEEK_END);
				if ((size = ftell(the_file)) < NUM_SECTORS * 256) {
					fclose(the_file);
					the_file = null;
					return;
				}

				// x64 image?
				rewind(the_file);
				fread(magic, 4, 1, the_file);
				if (magic[0] == 0x43 && magic[1] == 0x15 && magic[2] == 0x41 && magic[3] == 0x64)
					image_header = 64;
				else
					image_header = 0;

				// Preset error info (all sectors no error)
				memset(error_info, 1, NUM_SECTORS);

				// Load sector error info from .d64 file, if present
				if (image_header == 0 && size == NUM_SECTORS * 257) {
					fseek(the_file, NUM_SECTORS * 256, SEEK_SET);
					fread(error_info, NUM_SECTORS, 1, the_file);
				}
			}
		}
	}
	private byte open_file(int channel, byte[] filename) {
		byte[] plainname = new byte[256];
		ModeType mt = new ModeType();
		mt.filemode = FMODE_READ;
		mt.filetype = FTYPE_PRG;
		DiskLocation dl = new DiskLocation();

		convert_filename(filename, plainname, mt);

		// Channel 0 is READ PRG, channel 1 is WRITE PRG
		if (channel == 0) {
			mt.filemode = FMODE_READ;
			mt.filetype = FTYPE_PRG;
		}
		if (channel == 1) {
			mt.filemode = FMODE_WRITE;
			mt.filetype = FTYPE_PRG;
		}

		// Allow only read accesses
		if (mt.filemode != FMODE_READ) {
			set_error(ERR_WRITEPROTECT);
			return ST_OK;
		}

		// Find file in directory and open it
		if (find_file(plainname, dl))
			return open_file_ts(channel, dl);
		else
			set_error(ERR_FILENOTFOUND);

		return ST_OK;
	}
	private ModeType convert_filename(byte[] srcname, byte[] destname, ModeType mt) {
		BPtr p;

		// Search for ':', p points to first character after ':'
		if ((p = strchr(srcname, ':')) != null)
			p.inc();
		else
			p = new BPtr(srcname);

		// Remaining string -> destname
		strncpy(destname, p, NAMEBUF_LENGTH);

		// Search for ','
		p = new BPtr(destname);
		while (p.get() != 0 && (p.get() != ',')) p.inc();

		// Look for mode parameters seperated by ','
		p = new BPtr(destname);
		while ((p = strchr(p, ',')) != null) {

			// Cut string after the first ','
			p.setInc((byte) 0);

			switch ((char) p.get()) {
				case 'P':
				mt.filetype = FTYPE_PRG;
					break;
				case 'S':
					mt.filetype = FTYPE_SEQ;
					break;
				case 'U':
					mt.filetype = FTYPE_USR;
					break;
				case 'L':
					mt.filetype = FTYPE_REL;
					break;
				case 'R':
					mt.filemode = FMODE_READ;
					break;
				case 'W':
					mt.filemode = FMODE_WRITE;
					break;
				case 'A':
					mt.filemode = FMODE_APPEND;
					break;
			}
		}
		return mt;
	}
	private boolean find_file(byte[] filename, DiskLocation dl) {
		int i, j;
		BPtr p, q;
		DirEntry de;

		// Scan all directory blocks
		dir.next_track.set((byte) bam.dir_track.get());
		dir.next_sector.set((byte) bam.dir_sector.get());

		while (dir.next_track.get() != 0) {
			if (!read_sector(dir.next_track.get(), dir.next_sector.get(), dir.next_track))
				return false;

			// Scan all 8 entries of a block
next_entry:	for (j=0; j<8; j++) {
				de = dir.entry[j];
				dl.track = de.track.get();
				dl.sector = de.sector.get();

				if (de.type.get() != 0) {
					p = new BPtr(filename);
					q = de.name;
					for (i=0; i<16 && p.get() != 0; i++, p.inc(), q.inc()) {
						if (p.get() == '*')	// Wildcard '*' matches all following characters
							return true;
						if (p.get() != q.get()) {
							if (p.get() != '?') continue next_entry;	// Wildcard '?' matches single character
							if (q.get() == 0xa0) continue next_entry;
						}
					}

					if (i == 16 || q.get() == 0xa0)
						return true;
				}
			}
		}

		return false;
	}
	private byte open_file_ts(int channel, DiskLocation dl) {
		chan_buf[channel] = new BPtr(new byte[256]);
		chan_mode[channel] = CHMOD_FILE;

		// On the next call to Read, the first block will be read
		chan_buf[channel].set(0, (byte) dl.track);
		chan_buf[channel].set(1, (byte) dl.sector);
		buf_len[channel] = 0;

		return ST_OK;
	}
	private static boolean match(BPtr p, BPtr n) {
		p = new BPtr(p);
		n = new BPtr(n);
		if (p.get() == 0)		// Null pattern matches everything
			return true;

		do {
			if (p.get() == '*')	// Wildcard '*' matches all following characters
				return true;
			if ((p.get() != n.get()) && (p.get() != '?'))	// Wildcard '?' matches single character
				return false;
			p.inc(); n.inc();
		} while (p.get() != 0);

		return n.get() == 0xa0;
	}
	
	private static char[] type_char_1 = "DSPUREERSELQGRL?".toCharArray();
	private static char[] type_char_2 = "EERSELQGRL??????".toCharArray();
	private static char[] type_char_3 = "LQGRL???????????".toCharArray();
	private byte open_directory(BPtr pattern) {
		pattern = new BPtr(pattern);
		int i, j, n, m;
		BPtr p, q;
		DirEntry de;
		byte c;
		BPtr tmppat;

		// Special treatment for "$0"
		if (pattern.get() == '0' && pattern.get(1) == 0)
			pattern.inc();

		// Skip everything before the ':' in the pattern
		if ((tmppat = strchr(pattern, ':')) != null) {
			pattern = tmppat;
			pattern.inc();
		}

		byte[] buf = new byte[8192];
		p = new BPtr(buf);
		buf_ptr[0] = new BPtr(buf);
		chan_buf[0] = new BPtr(buf);
//		p = buf_ptr[0] = chan_buf[0] = new uint8[8192];
		chan_mode[0] = CHMOD_DIRECTORY;

		// Create directory title
		p.setInc((byte) 0x01);	// Load address $0401 (from PET days :-)
		p.setInc((byte) 0x04);
		p.setInc((byte) 0x01);	// Dummy line link
		p.setInc((byte) 0x01);
		p.setInc((byte) 0);		// Drive number (0) as line number
		p.setInc((byte) 0);
		p.setInc((byte) 0x12);	// RVS ON
		p.setInc((byte) '\"');

		q = new BPtr(bam.disk_name);
		for (i=0; i<23; i++) {
			if ((c = q.getInc()) == 0xa0)
				p.setInc((byte) ' ');		// Replace 0xa0 by space
			else
				p.setInc((byte) c);
		}
		p.set(-7, (byte) '\"');
//		*(p-7) = '\"';
		p.setInc((byte) 0);

		// Scan all directory blocks
		dir.next_track = bam.dir_track;
		dir.next_sector = bam.dir_sector;

		while (dir.next_track.get() != 0) {
			if (!read_sector(dir.next_track.get(), dir.next_sector.get(), dir.next_track))
				return ST_OK;

			// Scan all 8 entries of a block
			for (j=0; j<8; j++) {
				de = dir.entry[j];

				if (de.type.get() != 0 && match(pattern, de.name)) {
					p.setInc((byte) 0x01); // Dummy line link
					p.setInc((byte) 0x01);

					p.setInc(de.num_blocks_l.get()); // Line number
					p.setInc(de.num_blocks_h.get());

					p.setInc((byte) ' ');
					n = (de.num_blocks_h.get() << 8) + de.num_blocks_l.get();
					if (n<10) p.setInc((byte) ' ');
					if (n<100) p.setInc((byte) ' ');

					p.setInc((byte) '\"');
					q = new BPtr(de.name);
					m = 0;
					for (i=0; i<16; i++) {
						if ((c = q.getInc()) == 0xa0) {
							if (m != 0)
								p.setInc((byte) ' ');		// Replace all 0xa0 by spaces
							else
								m = p.setInc((byte) '\"');	// But the first by a '"'
						} else
							p.setInc((byte) c);
					}
					if (m != 0)
						p.setInc((byte) ' ');
					else
						p.setInc((byte) '\"');			// No 0xa0, then append a space

					if ((de.type.get() & 0x80) != 0)
						p.setInc((byte) ' ');
					else
						p.setInc((byte) '*');

					p.setInc((byte) type_char_1[de.type.get() & 0x0f]);
					p.setInc((byte) type_char_2[de.type.get() & 0x0f]);
					p.setInc((byte) type_char_3[de.type.get() & 0x0f]);

					if ((de.type.get() & 0x40) != 0)
						p.setInc((byte) '<');
					else
						p.setInc((byte) ' ');

					p.setInc((byte) ' ');
					if (n >= 10) p.setInc((byte) ' ');
					if (n >= 100) p.setInc((byte) ' ');
					p.setInc((byte) 0);
				}
			}
		}

		// Final line
		q = p;
		for (i=0; i<29; i++)
			q.setInc((byte) ' ');

		n = 0;
		for (i=0; i<35; i++)
			n += bam.bitmap.get(i*4);

		p.setInc((byte) 0x01);		// Dummy line link
		p.setInc((byte) 0x01);
		p.setInc((byte) n & 0xff);	// Number of free blocks as line number
		p.setInc((byte) (n >> 8) & 0xff);

		p.setInc("BLOCKS FREE.");

		p = q;
		p.setInc((byte) 0);
		p.setInc((byte) 0);
		p.setInc((byte) 0);

		buf_len[0] = p.offs - chan_buf[0].offs;

		return ST_OK;
	}
	private byte open_direct(int channel, byte[] filename) {
		int buf = -1;

		if (filename[1] == 0)
			buf = alloc_buffer(-1);
		else
			if ((filename[1] >= '0') && (filename[1] <= '3') && (filename[2] == 0))
				buf = alloc_buffer(filename[1] - '0');

		if (buf == -1) {
			set_error(ERR_NOCHANNEL);
			return ST_OK;
		}

		// The buffers are in the 1541 RAM at $300 and are 256 bytes each
		chan_buf[channel] = new BPtr(ram, 0x300 + (buf << 8));
		buf_ptr[channel] = new BPtr(ram, 0x300 + (buf << 8));
//		chan_buf[channel] = buf_ptr[channel] = ram + 0x300 + (buf << 8);
		chan_mode[channel] = CHMOD_DIRECT;
		chan_buf_num[channel] = buf;

		// Store actual buffer number in buffer
		chan_buf[channel].set((byte) (buf + '0'));
		buf_len[channel] = 1;

		return ST_OK;
	}
	private void close_all_channels() {
		for (int i=0; i<15; i++)
			Close(i);

		cmd_len = 0;
	}
	private void execute_command(byte[] command) {
		int adr;
		int len;

		switch (command[0]) {
			case 'B':
				if (command[1] != '-')
					set_error(ERR_SYNTAX30);
				else
					switch (command[2]) {
						case 'R':
							block_read_cmd(new BPtr(command, 3));
							break;

						case 'P':
							buffer_ptr_cmd(new BPtr(command, 3));
							break;

						case 'A':
						case 'F':
						case 'W':
							set_error(ERR_WRITEPROTECT);
							break;

						default:
							set_error(ERR_SYNTAX30);
							break;
					}
				break;

			case 'M':
				if (command[1] != '-')
					set_error(ERR_SYNTAX30);
				else
					switch (command[2]) {
						case 'R':
							adr = ((command[4] << 8) & 0xff00) | (command[3] & 0xff);
							error_ptr = new BPtr(ram, adr & 0x07ff);
							if ((error_len = (command[5] & 0xff)) == 0)
								error_len = 1;
							break;

						case 'W':
							adr = ((command[4] << 8) & 0xff00) | (command[3] & 0xff);
							len = command[5] & 0xff;
							for (int i=0; i<len; i++)
								ram[adr+i] = command[i+6];
							break;

						default:
							set_error(ERR_SYNTAX30);
					}
				break;

			case 'I':
				close_all_channels();
				read_sector(18, 0, bam);
				set_error(ERR_OK);
				break;

			case 'U':
				switch (command[1] & 0x0f) {
					case 1:		// U1/UA: Block-Read
						block_read_cmd(new BPtr(command, 2));
						break;

					case 2:		// U2/UB: Block-Write
						set_error(ERR_WRITEPROTECT);
						break;

					case 10:	// U:/UJ: Reset
						Reset();
						break;

					default:
						set_error(ERR_SYNTAX30);
						break;
				}
				break;

			case 'G':
				if (command[1] != ':')
					set_error(ERR_SYNTAX30);
				else
					chd64_cmd(new BPtr(command, 2));
				break;

			case 'C':
			case 'N':
			case 'R':
			case 'S':
			case 'V':
				set_error(ERR_WRITEPROTECT);
				break;

			default:
				set_error(ERR_SYNTAX30);
				break;
		}
	}
	class BCmd {
		int arg1, arg2, arg3, arg4;
	}
	private void block_read_cmd(BPtr command) {
		command = new BPtr(command);
		BCmd bc = new BCmd();
		if (parse_bcmd(command, bc)) {
			if (chan_mode[bc.arg1] == CHMOD_DIRECT) {
				read_sector(bc.arg3, bc.arg4, buf_ptr[bc.arg1] = chan_buf[bc.arg1]);
				buf_len[bc.arg1] = 256;
				set_error(ERR_OK);
			} else
				set_error(ERR_NOCHANNEL);
		} else
			set_error(ERR_SYNTAX30);
	}
	private void buffer_ptr_cmd(BPtr command) {
		command = new BPtr(command);
		BCmd bc = new BCmd();

		if (parse_bcmd(command, bc)) {
			if (chan_mode[bc.arg1] == CHMOD_DIRECT) {
				buf_ptr[bc.arg1] = new BPtr(chan_buf[bc.arg1], bc.arg2);
				buf_len[bc.arg1] = 256 - bc.arg2;
				set_error(ERR_OK);
			} else
				set_error(ERR_NOCHANNEL);
		} else
			set_error(ERR_SYNTAX30);
	}
	private boolean parse_bcmd(BPtr cmd, BCmd bc) {
		int i;
		cmd = new BPtr(cmd);

		if (cmd.get() == ':') cmd.inc();

		// Read four parameters separated by space, cursor right or comma
		while (cmd.get() == ' ' || cmd.get() == 0x1d || cmd.get() == 0x2c) cmd.inc();
		if (cmd.get() == 0) return false;

		i = 0;
		while (cmd.get() >= 0x30 && cmd.get() < 0x40) {
			i *= 10;
			i += cmd.getInc() & 0x0f;
		}
		bc.arg1 = i & 0xff;

		while (cmd.get() == ' ' || cmd.get() == 0x1d || cmd.get() == 0x2c) cmd.inc();;
		if (cmd.get() == 0) return false;

		i = 0;
		while (cmd.get() >= 0x30 && cmd.get() < 0x40) {
			i *= 10;
			i += cmd.getInc() & 0x0f;
		}
		bc.arg2 = i & 0xff;

		while (cmd.get() == ' ' || cmd.get() == 0x1d || cmd.get() == 0x2c) cmd.inc();
		if (cmd.get() == 0) return false;

		i = 0;
		while (cmd.get() >= 0x30 && cmd.get() < 0x40) {
			i *= 10;
			i += cmd.getInc() & 0x0f;
		}
		bc.arg3 = i & 0xff;

		while (cmd.get() == ' ' || cmd.get() == 0x1d || cmd.get() == 0x2c) cmd.inc();
		if (cmd.get() == 0) return false;

		i = 0;
		while (cmd.get() >= 0x30 && cmd.get() < 0x40) {
			i *= 10;
			i += cmd.getInc() & 0x0f;
		}
		bc.arg4 = i & 0xff;

		return true;
	}
	private void chd64_cmd(BPtr d64name) {
		d64name = new BPtr(d64name);
		byte[] str = new byte[NAMEBUF_LENGTH];
		BPtr p = new BPtr(str);

		// Convert .d64 file name
		for (int i=0; i<NAMEBUF_LENGTH && (p.setInc(conv_from_64(d64name.getInc(), false)) != 0); i++) ;

		close_all_channels();

		// G:. resets the .d64 file name to its original setting
		if (str[0] == '.' && str[1] == 0)
			open_close_d64_file(orig_d64_name);
		else
			open_close_d64_file(new File(orig_d64_name.getParentFile(), toString(str)));

		// Read BAM
		read_sector(18, 0, bam);
	}
	private int alloc_buffer(int want) {
		if (want == -1) {
			for (want=3; want>=0; want--)
				if (buf_free[want]) {
					buf_free[want] = false;
					return want;
				}
			return -1;
		}

		if (want < 4)
			if (buf_free[want]) {
				buf_free[want] = false;
				return want;
			} else
				return -1;
		else
			return -1;
	}
	private void free_buffer(int buf) {
		buf_free[buf] = true;
	}
	private boolean read_sector(int track, int sector, byte[] buffer) {
		return read_sector(track, sector, new BPtr(buffer));
	}
	private boolean read_sector(int track, int sector, BPtr buffer) {
		buffer = new BPtr(buffer);
		int offset;

		// Convert track/sector to byte offset in file
		if ((offset = offset_from_ts(track, sector)) < 0) {
			set_error(ERR_ILLEGALTS);
			return false;
		}

		if (the_file == null) {
			set_error(ERR_NOTREADY);
			return false;
		}

//	#ifdef AMIGA
//		if (offset != ftell(the_file))
//			fseek(the_file, offset + image_header, SEEK_SET);
//	#else
		fseek(the_file, offset + image_header, SEEK_SET);
//	#endif
		fread(buffer, 256, 1, the_file);
		return true;
	}

	private static int[] num_sectors = {
		0,
		21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,21,
		19,19,19,19,19,19,19,
		18,18,18,18,18,18,
		17,17,17,17,17,
		17,17,17,17,17		// Tracks 36..40
	};

	private static int[] sector_offset = {
		0,
		0,21,42,63,84,105,126,147,168,189,210,231,252,273,294,315,336,
		357,376,395,414,433,452,471,
		490,508,526,544,562,580,
		598,615,632,649,666,
		683,700,717,734,751	// Tracks 36..40
	};
	private int offset_from_ts(int track, int sector) {
		if ((track < 1) || (track > NUM_TRACKS) ||
				(sector < 0) || (sector >= num_sectors[track]))
				return -1;

		return (sector_offset[track] + sector) << 8;
	}
	private byte conv_from_64(byte c, boolean map_slash) {
		if ((c >= 'A') && (c <= 'Z') || (c >= 'a') && (c <= 'z'))
			return (byte) (c ^ 0x20);
		if ((c >= 0xc1) && (c <= 0xda))
			return (byte) (c ^ 0x80);
		if ((c == '/') && map_slash && Prefs.ThePrefs.MapSlash)
			return '\\';
		return c;
	}
}
