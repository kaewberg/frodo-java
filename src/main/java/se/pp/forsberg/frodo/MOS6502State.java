package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.BPtr;

class MOS6502State {
	public static final int STATE_SIZE = 48;
	public byte a, x, y;
	public byte p;			// Processor flags
	public short pc, sp;

	public byte[] intr = new byte[4];		// Interrupt state
	public boolean instruction_complete;
	public boolean idle;

	public byte via1_pra;		// VIA 1
	public byte via1_ddra;
	public byte via1_prb;
	public byte via1_ddrb;
	public short via1_t1c;
	public short via1_t1l;
	public short via1_t2c;
	public short via1_t2l;
	public byte via1_sr;
	public byte via1_acr;
	public byte via1_pcr;
	public byte via1_ifr;
	public byte via1_ier;

	public byte via2_pra;		// VIA 2
	public byte via2_ddra;
	public byte via2_prb;
	public byte via2_ddrb;
	public short via2_t1c;
	public short via2_t1l;
	public short via2_t2c;
	public short via2_t2l;
	public byte via2_sr;
	public byte via2_acr;
	public byte via2_pcr;
	public byte via2_ifr;
	public byte via2_ier;
	
	public MOS6502State(byte[] buf) {
		BPtr bp = new BPtr(buf);
		a = bp.getInc(); x = bp.getInc(); y = bp.getInc(); pc = bp.getIncShort(); sp = bp.getIncShort();
		for (int i = 0; i < 4; i++) {
			intr[i] = bp.getInc();
		}
		instruction_complete = bp.getIncBool(); idle = bp.getIncBool();
		
		via1_pra = bp.getInc(); via1_ddra = bp.getInc(); via1_prb = bp.getInc(); via1_ddrb = bp.getInc();
		via1_t1c = bp.getIncShort(); via1_t1l = bp.getIncShort(); via1_t2c = bp.getIncShort(); via1_t2l = bp.getIncShort();
		via1_sr = bp.getInc(); via1_acr = bp.getInc(); via1_pcr = bp.getInc(); via1_ifr = bp.getInc(); via1_ier = bp.getInc();
		
		via2_pra = bp.getInc(); via2_ddra = bp.getInc(); via2_prb = bp.getInc(); via2_ddrb = bp.getInc();
		via2_t1c = bp.getIncShort(); via2_t1l = bp.getIncShort(); via2_t2c = bp.getIncShort(); via2_t2l = bp.getIncShort();
		via2_sr = bp.getInc(); via2_acr = bp.getInc(); via2_pcr = bp.getInc(); via2_ifr = bp.getInc(); via2_ier = bp.getInc();
	}

	public MOS6502State() {
	}

	public byte[] toBytes() {
		byte [] buf = new byte[STATE_SIZE];
		BPtr bp = new BPtr(buf);

		bp.setInc(a); bp.setInc(x); bp.setInc(y); bp.setInc(pc); bp.setInc(sp);
		// 8
		for (int i = 0; i < 4; i++) {
			bp.setInc(intr[i]);
		}
		bp.setInc(instruction_complete); bp.setInc(idle);
		
		bp.setInc(via1_pra);  bp.setInc(via1_ddra);
		// 16
		bp.setInc(via1_prb); bp.setInc(via1_ddrb);
		bp.setInc(via1_t1c); bp.setInc(via1_t1l); bp.setInc(via1_t2c); /* 24 */ bp.setInc(via1_t2l);
		bp.setInc(via1_sr); bp.setInc(via1_acr); bp.setInc(via1_pcr); bp.setInc(via1_ifr); bp.setInc(via1_ier);

		bp.setInc(via2_pra);/*32*/ bp.setInc(via2_ddra);
		bp.setInc(via2_prb); bp.setInc(via2_ddrb);
		bp.setInc(via2_t1c); bp.setInc(via2_t1l); bp.setInc(via2_t2c); bp.setInc(via2_t2l);
		bp.setInc(via2_sr); bp.setInc(via2_acr); bp.setInc(via2_pcr); bp.setInc(via2_ifr); bp.setInc(via2_ier);
		// 48
		return buf;
	}
}
