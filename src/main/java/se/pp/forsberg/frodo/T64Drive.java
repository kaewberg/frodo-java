package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.ErrorCode1541.*;
import static se.pp.forsberg.frodo.enums.FileType.*;
import static se.pp.forsberg.frodo.IEC.NAMEBUF_LENGTH;
import static se.pp.forsberg.frodo.enums.AccessMode.*;
import static se.pp.forsberg.libc.LibC.*;
import static se.pp.forsberg.libc.SeekWhence.SEEK_SET;

import java.io.File;

import se.pp.forsberg.libc.BPtr;
import se.pp.forsberg.libc.ByteC;
import se.pp.forsberg.libc.IntC;
import se.pp.forsberg.libc.StdioFile;
import se.pp.forsberg.frodo.enums.AccessMode;
import se.pp.forsberg.frodo.enums.FileType;
import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.frodo.enums.ErrorCode1541;

class T64Drive extends Drive {

	
	// Information for file inside a .t64 file
	private static class FileInfo {
		byte[] name = new byte[17];		// File name, PETSCII
		FileType type;			// File type
		byte sa_lo, sa_hi;	// Start address
		int offset;			// Offset of first byte in .t64 file
		int length;			// Length of file
	}

	public T64Drive(IEC iec, File filepath) {
		super(iec);
		the_file = null;
		file_info = null;

		Ready = false;
		orig_t64_name = filepath;
		for (int i=0; i<16; i++)
			file[i] = null;

		// Open .t64 file
		open_close_t64_file(filepath);
		if (the_file != null) {
			Reset();
			Ready = true;
		}
	}

//T64Drive::~T64Drive()
//{
//	// Close .t64 file
//	open_close_t64_file("");
//
//	Ready = false;
//}
	@Override
	public byte Open(int channel, byte[] filename) {
		set_error(ERR_OK);

		// Channel 15: Execute file name as command
		if (channel == 15) {
			execute_command(filename);
			return ST_OK;
		}

		// Close previous file if still open
		if (file[channel] != null) {
			fclose(file[channel]);
			file[channel] = null;
		}

		if (filename[0] == '#') {
			set_error(ERR_NOCHANNEL);
			return ST_OK;
		}

		if (the_file == null) {
			set_error(ERR_NOTREADY);
			return ST_OK;
		}

		if (filename[0] == '$')
			return open_directory(channel, new BPtr(filename, 1));

		return open_file(channel, filename);
	}
	
	@Override
	public byte Close(int channel) {
		if (channel == 15) {
			close_all_channels();
			return ST_OK;
		}

		if (file[channel] != null) {
			fclose(file[channel]);
			file[channel] = null;
		}

		return ST_OK;
	}
	
	@Override
	public byte Read(int channel, ByteC b) {
		int c;

		// Channel 15: Error channel
		if (channel == 15) {
			b.b = error_ptr.getInc();

			if (b.b != '\r')
				return ST_OK;
			else {	// End of message
				set_error(ERR_OK);
				return ST_EOF;
			}
		}

		if (file[channel] == null) return ST_READ_TIMEOUT;

		// Get char from buffer and read next
		b.b = read_char[channel];
		c = fgetc(file[channel]);
		if (c == -1)
			return ST_EOF;
		else {
			read_char[channel] = (byte) c;
			return ST_OK;
		}
	}
	
	@Override
	public void Reset() {
		close_all_channels();
		cmd_len = 0;	
		set_error(ERR_STARTUP);
	}

	@Override
	public byte Write(int channel, byte b, boolean eoi) {
		// Channel 15: Collect chars and execute command on EOI
		if (channel == 15) {
			if (cmd_len >= 40)
				return ST_TIMEOUT;
			
			cmd_buffer[cmd_len++] = b;

			if (eoi) {
				cmd_buffer[cmd_len] = 0;
				cmd_len = 0;
				execute_command(cmd_buffer);
			}
			return ST_OK;
		}

		if (file[channel] == null)
			set_error(ERR_FILENOTOPEN);
		else
			set_error(ERR_WRITEPROTECT);

		return ST_TIMEOUT;
	}
	private void open_close_t64_file(File t64name) {
		byte[] buf = new byte[64];
		boolean parsed_ok = false;

		// Close old .t64, if open
		if (the_file != null) {
			close_all_channels();
			fclose(the_file);
			the_file = null;
			file_info = null;
		}

		// Open new .t64 file
		if (t64name != null) {
			if ((the_file = fopen(t64name, "rb")) != null) {

				// Check file ID
				fread(buf, 64, 1, the_file);
				if (buf[0] == 0x43 && buf[1] == 0x36 && buf[2] == 0x34) {
					is_lynx = false;
					parsed_ok = parse_t64_file();
				} else if (buf[0x3c] == 0x4c && buf[0x3d] == 0x59 && buf[0x3e] == 0x4e && buf[0x3f] == 0x58) {
					is_lynx = true;
					parsed_ok = parse_lynx_file();
				}

				if (!parsed_ok) {
					fclose(the_file);
					the_file = null;
					file_info = null;
					return;
				}
			}
		}
	}
	private boolean parse_t64_file() {
		byte[] buf = new byte[32];
		byte[] buf2;
		BPtr bp;
		BPtr p;
		int max, i, j;

		// Read header and get maximum number of files contained
		fseek(the_file, 32, SEEK_SET);
		fread(buf, 32, 1, the_file);
		max = (buf[3] << 8) | buf[2];

		bp = new BPtr(buf, 8);
		memcpy(dir_title, buf, 16);

		// Allocate buffer for file records and read them
		buf2 = new byte[max*32];
		fread(buf2, 32, max, the_file);

		// Determine number of files contained
		for (i=0, num_files=0; i<max; i++)
			if (buf2[i*32] == 1)
				num_files++;

		if (num_files == 0)
			return false;

		// Construct file information array
		file_info = new FileInfo[num_files];
		for (i=0, j=0; i<max; i++)
			if (buf2[i*32] == 1) {
				bp = new BPtr(buf, i*32 + 16);
				memcpy(file_info[j].name, bp, 16);
//				memcpy(file_info[j].name, buf2+i*32+16, 16);

				// Strip trailing spaces
				file_info[j].name[16] = 0x20;
				p = new BPtr(file_info[j].name, 16);
//				p = file_info[j].name + 16;
				while (p.getDec() == 0x20);
				p.set(2, (byte) 0);

				file_info[j].type = FTYPE_PRG;
				file_info[j].sa_lo = buf2[i*32+2];
				file_info[j].sa_hi = buf2[i*32+3];
				file_info[j].offset = (buf2[i*32+11] << 24) | (buf2[i*32+10] << 16) | (buf2[i*32+9] << 8) | buf2[i*32+8];
				file_info[j].length = ((buf2[i*32+5] << 8) | buf2[i*32+4]) - ((buf2[i*32+3] << 8) | buf2[i*32+2]);
				j++;
			}

		return true;
	}
	private boolean parse_lynx_file() {

		BPtr p;
		int dir_blocks, cur_offset, num_blocks, last_block, i;
		IntC dir_block = new IntC(), num_file = new IntC(), num_block = new IntC(), last_bloc = new IntC();
		char type_char;
		ByteC type_cha = new ByteC();

		// Dummy directory title
		strcpy(dir_title, "LYNX ARCHIVE    ");

		// Read header and get number of directory blocks and files contained
		fseek(the_file, 0x60, SEEK_SET);
		fscanf(the_file, "%d", dir_block);
		dir_blocks = dir_block.i;
		while (fgetc(the_file) != 0x0d)
			if (feof(the_file))
				return false;
		fscanf(the_file, "%d", num_file);
		num_files = num_file.i;
		fgetc(the_file);
//		fscanf(the_file, "%d\015", &num_files);

		// Construct file information array
		file_info = new FileInfo[num_files];
		cur_offset = dir_blocks * 254;
		for (i=0; i<num_files; i++) {

			// Read file name
			fread(file_info[i].name, 16, 1, the_file);

			// Strip trailing shift-spaces
			file_info[i].name[16] = (byte) 0xa0;
			p = new BPtr(file_info[i].name, 16);
//			p = (uint8 *)file_info[i].name + 16;
			while (p.getDec() == 0xa0) ;
			p.set(2, (byte) 0);

			// Read file length and type
			fgetc(the_file);
			fscanf(the_file, "%d", num_block);
			num_blocks = num_block.i;
			fgetc(the_file);
			fscanf(the_file, "%c", type_cha);
			type_char = (char) type_cha.b;
			fgetc(the_file);
			fscanf(the_file, "%d", last_bloc);
			last_block = last_bloc.i;
			fgetc(the_file);
//			fscanf(the_file, "\015%d\015%c\015%d\015", &num_blocks, &type_char, &last_block);

			switch (type_char) {
				case 'S':
					file_info[i].type = FTYPE_SEQ;
					break;
				case 'U':
					file_info[i].type = FTYPE_USR;
					break;
				case 'R':
					file_info[i].type = FTYPE_REL;
					break;
				default:
					file_info[i].type = FTYPE_PRG;
					break;
			}
			file_info[i].sa_lo = 0;	// Only used for .t64 files
			file_info[i].sa_hi = 0;
			file_info[i].offset = cur_offset;
			file_info[i].length = (num_blocks-1) * 254 + last_block;

			cur_offset += num_blocks * 254;
		}
		
		return true;
	}

	private byte open_file(int channel, byte[] filename) {
		byte[] plainname = new byte[NAMEBUF_LENGTH];
		ModeType cr = new ModeType();
		cr.filemode = FMODE_READ;
		cr.filetype = FTYPE_PRG;
		int num;

		cr = convert_filename(filename, plainname, cr.filemode, cr.filetype);

		// Channel 0 is READ PRG, channel 1 is WRITE PRG
		if (channel != 0) {
			cr.filemode = FMODE_READ;
			cr.filetype = FTYPE_PRG;
		}
		if (channel == 1) {
			cr.filemode = FMODE_WRITE;
			cr.filetype = FTYPE_PRG;
		}

		// Allow only read accesses
		if (cr.filemode != FMODE_READ) {
			set_error(ERR_WRITEPROTECT);
			return ST_OK;
		}

		// Find file
		IntC numC = new IntC();
		if (find_first_file(plainname, cr.filetype, numC)) {
			num = numC.i;
			// Open temporary file
			if ((file[channel] = tmpfile()) != null) {

				// Write load address (.t64 only)
				if (!is_lynx) {
					fputc(file_info[num].sa_lo, file [channel]);
					fputc(file_info[num].sa_hi, file [channel]);
//					fwrite(&file_info[num].sa_lo, 1, 1, file[channel]);
//					fwrite(&file_info[num].sa_hi, 1, 1, file[channel]);
				}

				// Copy file contents from .t64 file to temp file
				byte[] buf = new byte[file_info[num].length];
				fseek(the_file, file_info[num].offset, SEEK_SET);
				fread(buf, file_info[num].length, 1, the_file);
				fwrite(buf, file_info[num].length, 1, file[channel]);
				rewind(file[channel]);
				
				if (cr.filemode == FMODE_READ)	// Read and buffer first byte
					read_char[channel] = (byte) fgetc(file[channel]);
			}
		} else
			set_error(ERR_FILENOTFOUND);

		return ST_OK;
	}
	private byte open_directory(int channel, byte[] filename) {
		return open_directory(channel, new BPtr(filename));
	}
	private byte open_directory(int channel, BPtr filename) {
		filename = new BPtr(filename);
		byte[] buf = "\001\004\001\001\0\0\022\042                \042 00 2A\0".getBytes();
		byte[] str = new byte[NAMEBUF_LENGTH];
		byte[] pattern = new byte[NAMEBUF_LENGTH];
		BPtr p, q;
		int i, num;
		ModeType mt = new ModeType();

		// Special treatment for "$0"
		if (strlen(filename) == 1 && filename.get() == '0')
			filename.inc();

		// Convert filename ('$' already stripped), filemode/type are ignored
		mt = convert_filename(filename, pattern, mt.filemode, mt.filetype);

		// Create temporary file
		if ((file[channel] = tmpfile()) == null)
			return ST_OK;

		// Create directory title
		p = new BPtr(buf, 8);
		for (i=0; i<16 && dir_title[i] != 0; i++)
			p.setInc(dir_title[i]);
		fwrite(buf, 1, 32, file[channel]);

		// Create and write one line for every directory entry
		for (num=0; num<num_files; num++) {

			// Include only files matching the pattern
			if (match(pattern, file_info[num].name)) {

				// Clear line with spaces and terminate with null byte
				memset(buf, ' ', 31);
				buf[31] = 0;

				p = new BPtr(buf);
				p.setInc((byte) 0x01);	// Dummy line link
				p.setInc((byte) 0x01);

				// Calculate size in blocks (254 bytes each)
				i = (file_info[num].length + 254) / 254;
				p.setInc((byte) i & 0xff);
				p.setInc((byte) (i >> 8) & 0xff);

				p.inc();
				if (i < 10) p.inc();	// Less than 10: add one space
				if (i < 100) p.inc();	// Less than 100: add another space

				// Convert and insert file name
				strcpy(str, file_info[num].name);
				p.setInc('\"');
				q = new BPtr(p);
				for (i=0; i<16 && str[i] != 0; i++)
					q.setInc(str[i]);
				q.setInc('\"');
				p.inc(18);

				// File type
				switch (file_info[num].type) {
					case FTYPE_PRG:
						p.setInc("PRG");
						break;
					case FTYPE_SEQ:
						p.setInc("SEQ");
						break;
					case FTYPE_USR:
						p.setInc("USR");
						break;
					case FTYPE_REL:
						p.setInc("REL");
						break;
					default:
						p.setInc("???");
						break;
				}

				// Write line
				fwrite(buf, 1, 32, file[channel]);
			}
		}

		// Final line
		fwrite("\001\001\0\0BLOCKS FREE.             \0\0", 1, 32, file[channel]);

		// Rewind file for reading and read first byte
		rewind(file[channel]);
		read_char[channel] = (byte) fgetc(file[channel]);

		return ST_OK;
	}
	private ModeType convert_filename(byte[] srcname, byte[] destname, AccessMode filemode, FileType filetype) {
		return convert_filename(new BPtr(srcname), new BPtr(destname), filemode, filetype);
	}
	private ModeType convert_filename(BPtr srcname, byte[] destname, AccessMode filemode, FileType filetype) {
		return convert_filename(srcname, new BPtr(destname), filemode, filetype);
	}
	private ModeType convert_filename(BPtr srcname, BPtr destname, AccessMode filemode, FileType filetype) {
		srcname = new BPtr(srcname);
		destname = new BPtr(destname);
		BPtr p;
		ModeType r = new ModeType();
		r.filemode = filemode;
		r.filetype = filetype;

		// Search for ':', p points to first character after ':'
		if ((p = strchr(srcname, ':')) != null)
			p.inc();
		else
			p = new BPtr(srcname);

		// Remaining string -> destname
		strncpy(destname, p, NAMEBUF_LENGTH);

		// Search for ','
		p = new BPtr(destname);
		while (p.get() != 0 && (p.get() != ',')) p.inc();

		// Look for mode parameters seperated by ','
		p = new BPtr(destname);
		while ((p = strchr(p, ',')) != null) {

			// Cut string after the first ','
			p.setInc((byte) 0);

			switch ((char) p.get()) {
				case 'P':
					r.filetype = FTYPE_PRG;
					break;
				case 'S':
					r.filetype = FTYPE_SEQ;
					break;
				case 'U':
					r.filetype = FTYPE_USR;
					break;
				case 'L':
					r.filetype = FTYPE_REL;
					break;
				case 'R':
					r.filemode = FMODE_READ;
					break;
				case 'W':
					r.filemode = FMODE_WRITE;
					break;
				case 'A':
					r.filemode = FMODE_APPEND;
					break;
			}
		}
		return r;
	}
	private static boolean match(byte[] p, byte[] n) {
		return match(new BPtr(p), new BPtr(n));
	}
	private static boolean match(BPtr p, BPtr n) {
		p = new BPtr(p);
		n = new BPtr(n);
		if (p.get() == 0)		// Null pattern matches everything
			return true;

		do {
			if (p.get() == '*')	// Wildcard '*' matches all following characters
				return true;
			if ((p.get() != n.get()) && (p.get() != '?'))	// Wildcard '?' matches single character
				return false;
			p.inc(); n.inc();
		} while (p.get() != 0);

		return n.get() == 0;
	}
	private boolean find_first_file(byte[] name, FileType filetype, IntC num) {
		for (int i=0; i<num_files; i++)
			if (match(name, file_info[i].name) && filetype == file_info[i].type) {
				num.i = i;
				return true;
			}

		return false;
	}
	private void close_all_channels() {
		for (int i=0; i<15; i++)
			Close(i);

		cmd_len = 0;
	}
	private void execute_command(byte[] command) {
	switch (command[0]) {
		case 'I':
			close_all_channels();
			set_error(ERR_OK);
			break;

		case 'U':
			if ((command[1] & 0x0f) == 0x0a) {
				Reset();
			} else
				set_error(ERR_SYNTAX30);
			break;

		case 'G':
			if (command[1] != ':')
				set_error(ERR_SYNTAX30);
			else
				cht64_cmd(new BPtr(command, 2));
			break;

		default:
			set_error(ERR_SYNTAX30);
	}
}
	private void cht64_cmd(BPtr t64path) {
		BPtr t64name = new BPtr(t64path);
		byte[] str = new byte[NAMEBUF_LENGTH];
		BPtr p = new BPtr(str);

		// Convert .t64 file name
		for (int i=0; i<NAMEBUF_LENGTH && (p.setInc(conv_from_64(t64name.getInc(), false))) != 0; i++) ;

		close_all_channels();

		// G:. resets the .t64 file name to its original setting
		if (str[0] == '.' && str[1] == 0)
			open_close_t64_file(orig_t64_name);
		else
			open_close_t64_file(new File(orig_t64_name.getParentFile(), toString(str)));

		if (the_file == null)
			set_error(ERR_NOTREADY);
	}

	private byte conv_from_64(byte c, boolean map_slash) {
		if ((c >= 'A') && (c <= 'Z') || (c >= 'a') && (c <= 'z'))
			return (byte) (c ^ 0x20);
		if ((c >= 0xc1) && (c <= 0xda))
			return (byte) (c ^ 0x80);
		if ((c == '/') && map_slash && Prefs.ThePrefs.MapSlash)
			return '\\';
		return c;
	}

	private StdioFile the_file;			// File pointer for .t64 file
	private boolean is_lynx;			// Flag: .t64 file is really a LYNX archive

	File orig_t64_name; // Original path of .t64 file
	byte[] dir_title = new byte[16];		// Directory title
	StdioFile[] file = new StdioFile[16];			// File pointers for each of the 16 channels (all temporary files)

	private int num_files;			// Number of files in .t64 file and in file_info array
	FileInfo[] file_info;	// Pointer to array of file information structs for each file

	byte[] cmd_buffer = new byte[44];	// Buffer for incoming command strings
	private int cmd_len;			// Length of received command

	byte[] read_char = new byte[16];	// Buffers for one-byte read-ahead

}
