
package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.InterruptType6502.*;
import static se.pp.forsberg.frodo.enums.DriveLEDState.*;

class MOS6502_1541 extends CpuBase {
	public MOS6502_1541(C64 c64, Job1541 job, C64Display disp, byte[] Ram, byte[] Rom) {
		ram = Ram; rom = Rom; the_c64 = c64; the_display = disp; the_job = job;
		a = x = y = 0;
		sp = (byte) 0xff;
		n_flag = z_flag = 0;
		v_flag = d_flag = c_flag = false;
		i_flag = true;

		via1_t1c = via1_t1l = via1_t2c = via1_t2l = 0;
		via1_sr = 0;
		via2_t1c = via2_t1l = via2_t2c = via2_t2l = 0;
		via2_sr = 0;

		first_irq_cycle = 0;
		Idle = false;
	}

//	#define read_to(adr, to) \
//		to = read_byte(adr);
//
//	// Read byte from memory, throw away result
//	#define read_idle(adr) \
//		read_byte(adr);


//	#ifdef FRODO_SC
	
//	#define Last state = 0; break; 
	public void EmulateCycle() {			// Emulate one clock cycle
		// Any pending interrupts in state 0 (opcode fetch)?
		if (state == 0 && interrupt.intr_any() != 0) {
			if (interrupt.intr[INT_RESET.ordinal()] != 0)
				Reset();
			else if ((interrupt.intr[INT_VIA1IRQ.ordinal()] != 0 || interrupt.intr[INT_VIA2IRQ.ordinal()] != 0 || interrupt.intr[INT_IECIRQ.ordinal()] != 0) && (the_c64.CycleCounter-first_irq_cycle >= 2) && !i_flag)
				state = 0x0008;
		}

		CommonResult r = commonOps(true);
		if (r.baLowEscape) {
			return;
		}
		if (!r.opHandled) {
		switch (OpCodes.values()[state]) {
//	#define IS_CPU_1541
//	#include "CPU_emulcycle.i"

			// Extension opcode
			case E_O_EXT:
				if (pc < 0xc000) {
					illegal_op((byte) 0xf2, (short) (pc-1));
					break;
				}
				switch (read_byte(pc++)) {
				case 0x00:	// Go to sleep in DOS idle loop if error flag is clear and no command received
					Idle = (ram[0x26c] | ram[0x7c]) == 0;
					pc = 0xebff;
					state = 0; break;
					case 0x01:	// Write sector
						the_job.WriteSector();
						pc = 0xf5dc;
						state = 0; break;
					case 0x02:	// Format track
						the_job.FormatTrack();
						pc = 0xfd8b;
						state = 0; break;
					default:
						illegal_op((byte) 0xf2, (short) (pc-1));
						break;
				}
				break;

			default:
				illegal_op(op, (short) (pc-1));
				break;
			}
		}
	}
//	#else
//		int EmulateLine(int cycles_left);	// Emulate until cycles_left underflows
//	#endif

	public void Reset() {
		// IEC lines and VIA registers
		IECLines = (byte) 0xc0;

		via1_pra = via1_ddra = via1_prb = via1_ddrb = 0;
		via1_acr = via1_pcr = 0;
		via1_ifr = via1_ier = 0;
		via2_pra = via2_ddra = via2_prb = via2_ddrb = 0;
		via2_acr = via2_pcr = 0;
		via2_ifr = via2_ier = 0;

		// Clear all interrupt lines
		interrupt.setIntr_any(0);

		// Read reset vector
		jump(read_word(0xfffc));

		// Wake up 1541
		Idle = false;
	}
	public void AsyncReset() {				// Reset the CPU asynchronously
		interrupt.intr[INT_RESET.ordinal()] = 1;
		Idle = false;
	}
	public MOS6502State GetState() {
		MOS6502State s = new MOS6502State();
		
		s.a = a;
		s.x = x;
		s.y = y;

		s.p = (byte) (0x20 | (n_flag & 0x80));
		if (v_flag) s.p |= 0x40;
		if (d_flag) s.p |= 0x08;
		if (i_flag) s.p |= 0x04;
		if (z_flag == 0) s.p |= 0x02;
		if (c_flag) s.p |= 0x01;
		
		s.pc = (short) pc;
		s.sp = (short) (sp | 0x0100);

		s.intr[INT_VIA1IRQ.ordinal()] = interrupt.intr[INT_VIA1IRQ.ordinal()];
		s.intr[INT_VIA2IRQ.ordinal()] = interrupt.intr[INT_VIA2IRQ.ordinal()];
		s.intr[INT_IECIRQ.ordinal()] = interrupt.intr[INT_IECIRQ.ordinal()];
		s.intr[INT_RESET.ordinal()] = interrupt.intr[INT_RESET.ordinal()];
		s.idle = Idle;

		s.via1_pra = via1_pra; s.via1_ddra = via1_ddra;
		s.via1_prb = via1_prb; s.via1_ddrb = via1_ddrb;
		s.via1_t1c = via1_t1c; s.via1_t1l = via1_t1l;
		s.via1_t2c = via1_t2c; s.via1_t2l = via1_t2l;
		s.via1_sr = via1_sr;
		s.via1_acr = via1_acr; s.via1_pcr = via1_pcr;
		s.via1_ifr = via1_ifr; s.via1_ier = via1_ier;

		s.via2_pra = via2_pra; s.via2_ddra = via2_ddra;
		s.via2_prb = via2_prb; s.via2_ddrb = via2_ddrb;
		s.via2_t1c = via2_t1c; s.via2_t1l = via2_t1l;
		s.via2_t2c = via2_t2c; s.via2_t2l = via2_t2l;
		s.via2_sr = via2_sr;
		s.via2_acr = via2_acr; s.via2_pcr = via2_pcr;
		s.via2_ifr = via2_ifr; s.via2_ier = via2_ier;

		return s;
	}
	public void SetState(MOS6502State s) {
		a = s.a;
		x = s.x;
		y = s.y;

		n_flag = s.p;
		v_flag = (s.p & 0x40) != 0;
		d_flag = (s.p & 0x08) != 0;
		i_flag = (s.p & 0x04) != 0;
		z_flag = (byte) ((s.p & 0x02) == 0? 255:0);
		c_flag = (s.p & 0x01) != 0;

		pc = s.pc;
		sp = (byte) (s.sp & 0xff);

		interrupt.intr[INT_VIA1IRQ.ordinal()] = s.intr[INT_VIA1IRQ.ordinal()];
		interrupt.intr[INT_VIA2IRQ.ordinal()] = s.intr[INT_VIA2IRQ.ordinal()];
		interrupt.intr[INT_IECIRQ.ordinal()] = s.intr[INT_IECIRQ.ordinal()];
		interrupt.intr[INT_RESET.ordinal()] = s.intr[INT_RESET.ordinal()];
		Idle = s.idle;

		via1_pra = s.via1_pra; via1_ddra = s.via1_ddra;
		via1_prb = s.via1_prb; via1_ddrb = s.via1_ddrb;
		via1_t1c = s.via1_t1c; via1_t1l = s.via1_t1l;
		via1_t2c = s.via1_t2c; via1_t2l = s.via1_t2l;
		via1_sr = s.via1_sr;
		via1_acr = s.via1_acr; via1_pcr = s.via1_pcr;
		via1_ifr = s.via1_ifr; via1_ier = s.via1_ier;

		via2_pra = s.via2_pra; via2_ddra = s.via2_ddra;
		via2_prb = s.via2_prb; via2_ddrb = s.via2_ddrb;
		via2_t1c = s.via2_t1c; via2_t1l = s.via2_t1l;
		via2_t2c = s.via2_t2c; via2_t2l = s.via2_t2l;
		via2_sr = s.via2_sr;
		via2_acr = s.via2_acr; via2_pcr = s.via2_pcr;
		via2_ifr = s.via2_ifr; via2_ier = s.via2_ier;

	}
	public byte ExtReadByte(int adr) {
		return read_byte(adr);
	}
	public void ExtWriteByte(int adr, byte b) {
		write_byte(adr, b);
	}
	public void CountVIATimers(int cycles) {
		long tmp;

		via1_t1c = (short) (tmp = via1_t1c - cycles);
		if (tmp > 0xffff) {
			if ((via1_acr & 0x40) != 0)	// Reload from latch in free-run mode
				via1_t1c = via1_t1l;
			via1_ifr |= 0x40;
		}

		if ((via1_acr & 0x20) == 0) {	// Only count in one-shot mode
			via1_t2c = (short) (tmp = via1_t2c - cycles);
			if (tmp > 0xffff)
				via1_ifr |= 0x20;
		}

		via2_t1c = (short) (tmp = via2_t1c - cycles);
		if (tmp > 0xffff) {
			if ((via2_acr & 0x40) != 0)	// Reload from latch in free-run mode
				via2_t1c = via2_t1l;
			via2_ifr |= 0x40;
			if ((via2_ier & 0x40) != 0)
				TriggerJobIRQ();
		}

		if ((via2_acr & 0x20) == 0) {	// Only count in one-shot mode
			via2_t2c = (short) (tmp = via2_t2c - cycles);
			if (tmp > 0xffff)
				via2_ifr |= 0x20;
		}
	}
	public void NewATNState() {
		byte b = (byte) (~via1_prb & via1_ddrb);
		IECLines = (byte) ((b << 6) & ((~b ^ TheCIA2.IECLines) << 3) & 0x80	// DATA (incl. ATN acknowledge)
			| (b << 3) & 0x40);											// CLK
	}
	public void IECInterrupt() {
		ram[0x7c] = 1;

		// Wake up 1541
		Idle = false;
	}

	public void TriggerJobIRQ() {
		if ((interrupt.intr[INT_VIA2IRQ.ordinal()]) == 0)
			first_irq_cycle = (int) the_c64.CycleCounter;
		interrupt.intr[INT_VIA2IRQ.ordinal()] = 1;
		Idle = false;
	}
	public boolean InterruptEnabled() {
		return !i_flag;
	}

	public MOS6526_2 TheCIA2;		// Pointer to C64 CIA 2

	public byte IECLines;			// State of IEC lines (bit 7 - DATA, bit 6 - CLK)
	public boolean Idle;				// true: 1541 is idle

	@Override
	protected byte read_byte(int adr) {
		if (adr >= 0xc000)
			return rom[adr & 0x3fff];
		else if (adr < 0x1000)
			return ram[adr & 0x07ff];
		else
			return read_byte_io(adr);
	}
	private byte read_byte_io(int adr) {
		if ((adr & 0xfc00) == 0x1800)	// VIA 1
			switch (adr & 0xf) {
				case 0:
					return (byte) ((via1_prb & 0x1a
						| ((IECLines & TheCIA2.IECLines) >> 7)		// DATA
						| ((IECLines & TheCIA2.IECLines) >> 4) & 0x04	// CLK
						| (TheCIA2.IECLines << 3) & 0x80) ^ 0x85);		// ATN
				case 1:
				case 15:
					return (byte) 0xff;	// Keep 1541C ROMs happy (track 0 sensor)
				case 2:
					return via1_ddrb;
				case 3:
					return via1_ddra;
				case 4:
					via1_ifr &= 0xbf;
					return (byte) via1_t1c;
				case 5:
					return (byte) (via1_t1c >> 8);
				case 6:
					return (byte) via1_t1l;
				case 7:
					return (byte) (via1_t1l >> 8);
				case 8:
					via1_ifr &= 0xdf;
					return (byte) via1_t2c;
				case 9:
					return (byte) (via1_t2c >> 8);
				case 10:
					return via1_sr;
				case 11:
					return via1_acr;
				case 12:
					return via1_pcr;
				case 13:
					return (byte) (via1_ifr | (((via1_ifr & via1_ier) != 0) ? 0x80 : 0));
				case 14:
					return (byte) (via1_ier | 0x80);
				default:	// Can't happen
					return 0;
			}

		else if ((adr & 0xfc00) == 0x1c00)	// VIA 2
			switch (adr & 0xf) {
				case 0:
					if (the_job.SyncFound())
						return (byte) (via2_prb & 0x7f | the_job.WPState());
					else
						return (byte) (via2_prb | 0x80 | the_job.WPState());
				case 1:
				case 15:
					return the_job.ReadGCRByte();
				case 2:
					return via2_ddrb;
				case 3:
					return via2_ddra;
				case 4:
					via2_ifr &= 0xbf;
					interrupt.intr[INT_VIA2IRQ.ordinal()] = 0;	// Clear job IRQ
					return (byte) via2_t1c;
				case 5:
					return (byte) (via2_t1c >> 8);
				case 6:
					return (byte) via2_t1l;
				case 7:
					return (byte) (via2_t1l >> 8);
				case 8:
					via2_ifr &= 0xdf;
					return (byte) via2_t2c;
				case 9:
					return (byte) (via2_t2c >> 8);
				case 10:
					return via2_sr;
				case 11:
					return via2_acr;
				case 12:
					return via2_pcr;
				case 13:
					return (byte) (via2_ifr | (((via2_ifr & via2_ier) != 0) ? 0x80 : 0));
				case 14:
					return (byte) (via2_ier | 0x80);
				default:	// Can't happen
					return 0;
			}

		else
			return (byte) (adr >> 8);
	}
	private short read_word(int adr) {
		return (short) (read_byte(adr) | (read_byte(adr+1) << 8));
	}
	@Override
	protected void write_byte(int adr, byte b) {
		if (adr < 0x1000)
			ram[adr & 0x7ff] = b;
		else
			write_byte_io(adr, b);
	}
	private void write_byte_io(int adr, byte b) {
		if ((adr & 0xfc00) == 0x1800)	// VIA 1
			switch (adr & 0xf) {
				case 0:
					via1_prb = b;
					b = (byte) (~via1_prb & via1_ddrb);
					IECLines = (byte) ((b << 6) & ((~b ^ TheCIA2.IECLines) << 3) & 0x80
						| (b << 3) & 0x40);
					break;
				case 1:
				case 15:
					via1_pra = b;
					break;
				case 2:
					via1_ddrb = b;
					b &= ~via1_prb;
					IECLines = (byte) ((b << 6) & ((~b ^ TheCIA2.IECLines) << 3) & 0x80
						| (b << 3) & 0x40);
					break;
				case 3:
					via1_ddra = b;
					break;
				case 4:
				case 6:
					via1_t1l = (short) (via1_t1l & 0xff00 | b);
					break;
				case 5:
					via1_t1l = (short) (via1_t1l & 0xff | (b << 8));
					via1_ifr &= 0xbf;
					via1_t1c = via1_t1l;
					break;
				case 7:
					via1_t1l = (short) (via1_t1l & 0xff | (b << 8));
					break;
				case 8:
					via1_t2l = (short) (via1_t2l & 0xff00 | b);
					break;
				case 9:
					via1_t2l = (short) (via1_t2l & 0xff | (b << 8));
					via1_ifr &= 0xdf;
					via1_t2c = via1_t2l;
					break;
				case 10:
					via1_sr = b;
					break;
				case 11:
					via1_acr = b;
					break;
				case 12:
					via1_pcr = b;
					break;
				case 13:
					via1_ifr &= ~b;
					break;
				case 14:
					if ((b & 0x80) != 0)
						via1_ier |= b & 0x7f;
					else
						via1_ier &= ~b;
					break;
			}

		else if ((adr & 0xfc00) == 0x1c00)	// VIA 2
			switch (adr & 0xf) {
				case 0:
					if (((via2_prb ^ b) & 8) != 0)	// Bit 3: Drive LED
						the_display.UpdateLEDs((b & 8) != 0 ? DRVLED_ON : DRVLED_OFF, DRVLED_OFF, DRVLED_OFF, DRVLED_OFF);
					if (((via2_prb ^ b) & 3) != 0)	// Bits 0/1: Stepper motor
						if ((via2_prb & 3) == ((b+1) & 3))
							the_job.MoveHeadOut();
						else if ((via2_prb & 3) == ((b-1) & 3))
							the_job.MoveHeadIn();
					via2_prb = (byte) (b & 0xef);
					break;
				case 1:
				case 15:
					via2_pra = b;
					break;
				case 2:
					via2_ddrb = b;
					break;
				case 3:
					via2_ddra = b;
					break;
				case 4:
				case 6:
					via2_t1l = (short) (via2_t1l & 0xff00 | b);
					break;
				case 5:
					via2_t1l = (short) (via2_t1l & 0xff | (b << 8));
					via2_ifr &= 0xbf;
					via2_t1c = via2_t1l;
					break;
				case 7:
					via2_t1l = (short) (via2_t1l & 0xff | (b << 8));
					break;
				case 8:
					via2_t2l = (short) (via2_t2l & 0xff00 | b);
					break;
				case 9:
					via2_t2l = (short) (via2_t2l & 0xff | (b << 8));
					via2_ifr &= 0xdf;
					via2_t2c = via2_t2l;
					break;
				case 10:
					via2_sr = b;
					break;
				case 11:
					via2_acr = b;
					break;
				case 12:
					via2_pcr = b;
					break;
				case 13:
					via2_ifr &= ~b;
					break;
				case 14:
					if ((b & 0x80) != 0)
						via2_ier |= b & 0x7f;
					else
						via2_ier &= ~b;
					break;
			}
	}

	private byte read_zp(int adr) {
		return ram[adr];
	}
	private short read_zp_word(int adr) {
		return (short) (ram[adr & 0xff] | (ram[(adr+1) & 0xff] << 8));
	}
	private void write_zp(int adr, byte b) {
		ram[adr] = b;
	}

	private void jump(int adr) {
//		#if PC_IS_POINTER
//		void MOS6502_1541::jump(uint16 adr)
//		{
//			if (adr >= 0xc000) {
//				pc = rom + (adr & 0x3fff);
//				pc_base = rom - 0xc000;
//			} else if (adr < 0x800) {
//				pc = ram + adr;
//				pc_base = ram;
//			} else
//				illegal_jump(pc-pc_base, adr);
//		}
//		#else
//		inline void MOS6502_1541::jump(uint16 adr)
//		{
			pc = adr;
//		}
//		#endif
	}
	private void illegal_op(byte op, short at) {
		Reset();
		the_display.ShowRequester(
			String.format("1541: Illegal opcode %02x at %04x.",  op, at),
			"Reset 1541",
			"Reset C64",
			() -> {},
			() -> the_c64.Reset());
	}
	private void illegal_jump(short at, short to) {
		Reset();
		the_display.ShowRequester(
				String.format("1541: Jump to I/O space at %04x to %04x.", at, to),
				"Reset 1541",
				"Reset C64",
				() -> {},
				() -> the_c64.Reset());
	}
	
	private byte[] ram;				// Pointer to main RAM
	private byte[] rom;				// Pointer to ROM
	private C64 the_c64;			// Pointer to C64 object
	private C64Display the_display; // Pointer to C64 display object
	private Job1541 the_job;		// Pointer to 1541 job object

	private static class Interrupt {
		byte[] intr = new byte[4];
		int intr_any() {
			// Byte order!!!
			return (intr[0] & 0xff) | ((intr[1] << 8) & 0xff00) | ((intr[2] << 16) & 0xff0000) | ((intr[3] << 24) & 0xff000000);
		}
		void setIntr_any(int i) {
			intr[0] = (byte) i;
			intr[1] = (byte) (i >> 8);
			intr[2] = (byte) (i >> 16);
			intr[3] = (byte) (i >> 24);
		}
	}
	private Interrupt interrupt = new Interrupt();
//		union {					// Pending interrupts
//			byte intr[4];		// Index: See definitions above
//			unsigned long intr_any;
//		} interrupt;

	private byte n_flag, z_flag;
	private boolean v_flag, d_flag, i_flag, c_flag;
	private byte a, x, y, sp;
//	#if PC_IS_POINTER
//		byte *pc, *pc_base;
//	#else
		int pc;
//	#endif

//	#ifdef FRODO_SC
	private int first_irq_cycle;

	private byte state, op;		// Current state and opcode
	private short ar, ar2;			// Address registers
	private byte rdbuf;			// Data buffer for RMW instructions
	private byte ddr, pr;			// Processor port
//	#else
//		int borrowed_cycles;	// Borrowed cycles from next line
//	#endif

	private byte via1_pra;		// PRA of VIA 1
	private byte via1_ddra;	// DDRA of VIA 1
	private byte via1_prb;		// PRB of VIA 1
	private byte via1_ddrb;	// DDRB of VIA 1
	private short via1_t1c;		// T1 Counter of VIA 1
	private short via1_t1l;		// T1 Latch of VIA 1
	private short via1_t2c;		// T2 Counter of VIA 1
	private short via1_t2l;		// T2 Latch of VIA 1
	private byte via1_sr;		// SR of VIA 1
	private byte via1_acr;		// ACR of VIA 1
	private byte via1_pcr;		// PCR of VIA 1
	private byte via1_ifr;		// IFR of VIA 1
	private byte via1_ier;		// IER of VIA 1

	private byte via2_pra;		// PRA of VIA 2
	private byte via2_ddra;	// DDRA of VIA 2
	private byte via2_prb;		// PRB of VIA 2
	private byte via2_ddrb;	// DDRB of VIA 2
	private short via2_t1c;		// T1 Counter of VIA 2
	private short via2_t1l;		// T1 Latch of VIA 2
	private short via2_t2c;		// T2 Counter of VIA 2
	private short via2_t2l;		// T2 Latch of VIA 2
	private byte via2_sr;		// SR of VIA 2
	private byte via2_acr;		// ACR of VIA 2
	private byte via2_pcr;		// PCR of VIA 2
	private byte via2_ifr;		// IFR of VIA 2
	private byte via2_ier;		// IER of VIA 2
}
