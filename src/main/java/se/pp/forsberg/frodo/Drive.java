package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.DriveLEDState.*;
import static se.pp.forsberg.frodo.enums.ErrorCode1541.*;
import se.pp.forsberg.frodo.enums.DriveLEDState;
import se.pp.forsberg.frodo.enums.ErrorCode1541;

import se.pp.forsberg.libc.BPtr;
import se.pp.forsberg.libc.ByteC;

abstract class Drive {
	
	// C64 status codes
	final static int ST_OK = 0;				// No error
	final static int ST_READ_TIMEOUT	= 0x02;	// Timeout on reading
	final static int ST_TIMEOUT = 0x03;		// Timeout
	final static int ST_EOF = 0x40;			// End of file
	final static int ST_NOTPRESENT = 0x80;	// Device not present
	
	static int NAMEBUF_LENGTH = 256;
	
	// 1541 error messages
	private final static byte[][] Errors_1541 = {
			"00, OK,00,00\r".getBytes(),
			"25,WRITE ERROR,00,00\r".getBytes(),
			"26,WRITE PROTECT ON,00,00\r".getBytes(),
			"30,SYNTAX ERROR,00,00\r".getBytes(),
			"33,SYNTAX ERROR,00,00\r".getBytes(),
			"60,WRITE FILE OPEN,00,00\r".getBytes(),
			"61,FILE NOT OPEN,00,00\r".getBytes(),
			"62,FILE NOT FOUND,00,00\r".getBytes(),
			"67,ILLEGAL TRACK OR SECTOR,00,00\r".getBytes(),
			"70,NO CHANNEL,00,00\r".getBytes(),
			"73,CBM DOS V2.6 1541,00,00\r".getBytes(),
			"74,DRIVE NOT READY,00,00\r".getBytes()
		};
	public Drive(IEC iec) {
		the_iec = iec;
		LED = DRVLED_OFF;
		Ready = false;
		set_error(ERR_STARTUP);
	}
//	virtual ~Drive() {}

	public abstract byte Open(int channel, byte[] filename);
	public abstract byte Close(int channel);
	public abstract byte Read(int channel, ByteC b);
	public abstract byte Write(int channel, byte b, boolean eoi);
	public abstract void Reset();

	public DriveLEDState LED;			// Drive LED state
	public boolean Ready;			// Drive is ready for operation

	void set_error(ErrorCode1541 error) {
		error_ptr = new BPtr(Errors_1541[error.ordinal()]);
		error_len = Errors_1541[error.ordinal()].length;

		// Set drive condition
		if (error != ERR_OK)
			if (error == ERR_STARTUP)
				LED = DRVLED_OFF;
			else
				LED = DRVLED_ERROR;
		else if (LED == DRVLED_ERROR)
			LED = DRVLED_OFF;
		the_iec.UpdateLEDs();
	}
	String toString(byte[] str) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < str.length; i++) {
			sb.append((char) str[i]);
		}
		return sb.toString();
	}

	BPtr error_ptr;	// Pointer within error message	
	int error_len;		// Remaining length of error message

	private IEC the_iec;		// Pointer to IEC object
}
