package se.pp.forsberg.frodo;


import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static se.pp.forsberg.frodo.DigitalRenderer.EGState.EG_ATTACK;
import static se.pp.forsberg.frodo.DigitalRenderer.EGState.EG_DECAY;
import static se.pp.forsberg.frodo.DigitalRenderer.EGState.EG_IDLE;
import static se.pp.forsberg.frodo.DigitalRenderer.EGState.EG_RELEASE;
import static se.pp.forsberg.frodo.DigitalRenderer.FilterType.FILT_ALL;
import static se.pp.forsberg.frodo.DigitalRenderer.FilterType.FILT_HPBP;
import static se.pp.forsberg.frodo.DigitalRenderer.FilterType.FILT_LP;
import static se.pp.forsberg.frodo.DigitalRenderer.FilterType.FILT_LPBP;
import static se.pp.forsberg.frodo.DigitalRenderer.FilterType.FILT_NONE;
import static se.pp.forsberg.frodo.DigitalRenderer.WaveformType.WAVE_NONE;
import static se.pp.forsberg.libc.LibC.memset;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.libc.SPtr;


/**
 **  Renderer for digital SID emulation (SIDTYPE_DIGITAL)
 **/


class DigitalRenderer implements SIDRenderer {

	// SID waveforms (some of them :-)
	public static enum WaveformType {
		WAVE_NONE,
		WAVE_TRI,
		WAVE_SAW,
		WAVE_TRISAW,
		WAVE_RECT,
		WAVE_TRIRECT,
		WAVE_SAWRECT,
		WAVE_TRISAWRECT,
		WAVE_NOISE
	};

	// EG states
	public static enum EGState {
		EG_IDLE,
		EG_ATTACK,
		EG_DECAY,
		EG_RELEASE
	};

	// Filter types
	public static enum FilterType {
		FILT_NONE,
		FILT_LP,
		FILT_BP,
		FILT_LPBP,
		FILT_HP,
		FILT_NOTCH,
		FILT_HPBP,
		FILT_ALL
	};
		
//	#if defined(AMIGA) || defined(__riscos__)
//	const uint32 SAMPLE_FREQ = 22050;	// Sample output frequency in Hz
//	#else
	private final static int SAMPLE_FREQ = 44100;	// Sample output frequency in Hz
//	#endif
	private final static int SID_FREQ = 985248;		// SID frequency in Hz
	private final static int CALC_FREQ = 50;			// Frequency at which calc_buffer is called in Hz (should be 50Hz)
	private final static int SID_CYCLES = SID_FREQ/SAMPLE_FREQ;	// # of SID clocks per sample frame
	private final static int SAMPLE_BUF_SIZE = 0x138*2;// Size of buffer for sampled voice (double buffered)

	// Structure for one voice
	private static class DRVoice {
		WaveformType wave;		// Selected waveform
		EGState eg_state;	// Current state of EG
		DRVoice mod_by;	// Voice that modulates this one
		DRVoice mod_to;	// Voice that is modulated by this one

		int count;	// Counter for waveform generator, 8.16 fixed
		int add;		// Added to counter in every frame

		short freq;		// SID frequency value
		short pw;		// SID pulse-width value

		int a_add;	// EG parameters
		int d_sub;
		int s_level;
		int r_sub;
		int eg_level;	// Current EG level, 8.16 fixed

		int noise;	// Last noise generator output value

		boolean gate;		// EG gate bit
		boolean ring;		// Ring modulation bit
		boolean test;		// Test bit
		boolean filter;	// Flag: Voice filtered

						// The following bit is set for the modulating
						// voice, not for the modulated one (as the SID bits)
		boolean sync;		// Sync modulation bit
	}
	
	public DigitalRenderer() {
		// Link voices together
		voice[0].mod_by = voice[2];
		voice[1].mod_by = voice[0];
		voice[2].mod_by = voice[1];
		voice[0].mod_to = voice[1];
		voice[1].mod_to = voice[2];
		voice[2].mod_to = voice[0];

		// Calculate triangle table
		for (int i=0; i<0x1000; i++) {
			TriTable[i] = (short) ((i << 4) | (i >> 8));
			TriTable[0x1fff-i] = (short) ((i << 4) | (i >> 8));
		}

//	#ifdef PRECOMPUTE_RESONANCE
//	#ifdef USE_FIXPOINT_MATHS
//		// slow floating point doesn't matter much on startup!
//		for (int i=0; i<256; i++) {
//		  resonanceLP[i] = FixNo(CALC_RESONANCE_LP(i));
//		  resonanceHP[i] = FixNo(CALC_RESONANCE_HP(i));
//		}
//		// Pre-compute the quotient. No problem since int-part is small enough
//		sidquot = (int32)((((double)SID_FREQ)*65536) / SAMPLE_FREQ);
//		// compute lookup table for sin and cos
//		InitFixSinTab();
//	#else
		for (int i=0; i<256; i++) {
		  resonanceLP[i] = CALC_RESONANCE_LP(i);
		  resonanceHP[i] = CALC_RESONANCE_HP(i);
		}
//	#endif
//	#endif

		Reset();

		// System specific initialization
		init_sound();
	}
	
	@Override public void Reset() {
		volume = 0;
		v3_mute = false;

		for (int v=0; v<3; v++) {
			voice[v].wave = WAVE_NONE;
			voice[v].eg_state = EG_IDLE;
			voice[v].count = voice[v].add = 0;
			voice[v].freq = voice[v].pw = 0;
			voice[v].eg_level = voice[v].s_level = 0;
			voice[v].a_add = voice[v].d_sub = voice[v].r_sub = EGTable[0];
			voice[v].gate = voice[v].ring = voice[v].test = false;
			voice[v].filter = voice[v].sync = false;
		}

		f_type = FILT_NONE;
		f_freq = f_res = 0;
//	#ifdef USE_FIXPOINT_MATHS
//		f_ampl = FixNo(1);
//		d1 = d2 = g1 = g2 = 0;
//		xn1 = xn2 = yn1 = yn2 = 0;
//	#else
		f_ampl = 1.0;
		d1 = d2 = g1 = g2 = 0.0;
		xn1 = xn2 = yn1 = yn2 = 0.0;
//	#endif

		sample_in_ptr = 0;
		memset(sample_buf, 0, SAMPLE_BUF_SIZE);
	}

	@Override public void EmulateLine() {
		// TODO Auto-generated method stub
		
	}

	@Override public void WriteRegister(int adr, byte b) {
		if (!ready)
			return;
		int v = adr/7;	// Voice number

		switch (adr) {
			case 0:
			case 7:
			case 14:
				voice[v].freq = (short) ((voice[v].freq & 0xff00) | b);
//	#ifdef USE_FIXPOINT_MATHS
//				voice[v].add = sidquot.imul((int)voice[v].freq);
//	#else
				voice[v].add = (int)((double)voice[v].freq * SID_FREQ / SAMPLE_FREQ);
//	#endif
				break;

			case 1:
			case 8:
			case 15:
				voice[v].freq = (short) ((voice[v].freq & 0xff) | (b << 8));
//	#ifdef USE_FIXPOINT_MATHS
//				voice[v].add = sidquot.imul((int)voice[v].freq);
//	#else
				voice[v].add = (int)((double)voice[v].freq * SID_FREQ / SAMPLE_FREQ);
//	#endif
				break;

			case 2:
			case 9:
			case 16:
				voice[v].pw = (short) ((voice[v].pw & 0x0f00) | b);
				break;

			case 3:
			case 10:
			case 17:
				voice[v].pw = (short) ((voice[v].pw & 0xff) | ((b & 0xf) << 8));
				break;

			case 4:
			case 11:
			case 18:
				voice[v].wave = WaveformType.values()[(b >> 4) & 0xf];
				if (((b & 1) != 0) != voice[v].gate)
					if ((b & 1) != 0)	// Gate turned on
						voice[v].eg_state = EG_ATTACK;
					else			// Gate turned off
						if (voice[v].eg_state != EG_IDLE)
							voice[v].eg_state = EG_RELEASE;
				voice[v].gate = (b & 1) != 0;
				voice[v].mod_by.sync = (b & 2) != 0;
				voice[v].ring = (b & 4) != 0;
				if ((voice[v].test = (b & 8) != 0))
					voice[v].count = 0;
				break;

			case 5:
			case 12:
			case 19:
				voice[v].a_add = EGTable[b >> 4];
				voice[v].d_sub = EGTable[b & 0xf];
				break;

			case 6:
			case 13:
			case 20:
				voice[v].s_level = (b >> 4) * 0x111111;
				voice[v].r_sub = EGTable[b & 0xf];
				break;

			case 22:
				if (b != f_freq) {
					f_freq = b;
					if (Prefs.ThePrefs.SIDFilters)
						calc_filter();
				}
				break;

			case 23:
				voice[0].filter = (b & 1) != 0;
				voice[1].filter = (b & 2) != 0;
				voice[2].filter = (b & 4) != 0;
				if ((b >> 4) != f_res) {
					f_res = (byte) (b >> 4);
					if (Prefs.ThePrefs.SIDFilters)
						calc_filter();
				}
				break;

			case 24:
				volume = (byte) (b & 0xf);
				v3_mute = (b & 0x80) != 0;
				FilterType ft =  FilterType.values()[((b >> 4) & 7)];
				if (ft != f_type) {
					f_type = ft;
//	#ifdef USE_FIXPOINT_MATHS
//					xn1 = xn2 = yn1 = yn2 = 0;
//	#else
					xn1 = xn2 = yn1 = yn2 = 0.0;
//	#endif
					if (Prefs.ThePrefs.SIDFilters)
						calc_filter();
				}
				break;
		}
	}

	@Override public void NewPrefs(Prefs prefs) {

		calc_filter();
	}

	@Override public void Pause() {
		clip.stop();
	}

	@Override public void Resume() {
		clip.start();
	}
	
	private Clip clip;
	AudioInputStream ais;
	private static int SND_BUF_SIZE = 44100;
	private RingBufferShort sound_buffer = new RingBufferShort(SND_BUF_SIZE);
	
	private static class RingBufferShort {
		int capacity;
		short[] buf;
		int written, read;
		Object sem = new Object();
		
		public RingBufferShort() {
			this(65536);
		}
		public RingBufferShort(int i) {
			capacity = i;
			buf = new short[capacity];
		}
		public void write(short[] src, int offs, int len) {
			synchronized(sem) {
				if (len > capacity) {
					throw new IllegalArgumentException(String.format("Buffer too small, %d < %d", capacity, len));
				}
				int avail = capacity - (written - read);
				while (avail < len) {
					try {
						sem.wait();
					} catch (InterruptedException e) {
						System.err.println("Failed to write sound data");
						e.printStackTrace();
						written += len;
						return;
					}
					avail = capacity - (written - read);
				}
				int writeOffset = written % capacity;
				int space = capacity - writeOffset;
				if (space <= len) {
					System.arraycopy(src, offs, buf, writeOffset, len);
				} else {
					System.arraycopy(src, offs, buf, writeOffset, space);
					System.arraycopy(src, offs + space, buf, 0, len - space);
				}
				written += len;
				sem.notify();
			}
		}
		public short read() {
			synchronized (sem) {
				while (read >= written) {
					try {
						sem.wait();
					} catch (InterruptedException e) {
						System.err.println("Failed to read sound data");
					}
				}
				sem.notify();
				return buf[++read % capacity];
			}
		}
		public void read(short[] dst, int offs, int len) {
			synchronized(sem) {
				if (len > capacity) {
					throw new IllegalArgumentException(String.format("Buffer too small, %d < %d", capacity, len));
				}
				int avail = written - read;
				while (avail < len) {
					try {
						sem.wait();
					} catch (InterruptedException e) {
						System.err.println("Failed to read sound data");
						e.printStackTrace();
						read += len;
						return;
					}
					avail = written - read;
				}
				int readOffset = read % capacity;
				int space = capacity - readOffset;
				if (space <= len) {
					System.arraycopy(buf, readOffset, dst, offs, len);
				} else {
					System.arraycopy(buf, readOffset, dst, offs, space);
					System.arraycopy(buf, 0, dst, offs + space, len - space);
				}
				read += len;
				sem.notify();
			}
		}
	}
	
	private class SidInputStream extends InputStream {		
		@Override
		public int read() throws IOException {
			throw new IllegalArgumentException("can only read even number of bytes");
		}
		@Override
		public int read(byte[] dst) throws IOException {
			return read(dst, 0, dst.length);
		}
		@Override
		public int read(byte[] dst, int off, int len) throws IOException {
			if (len % 2 == 1) {
				throw new IllegalArgumentException("can only read even number of bytes");
			}
			int lenS = Integer.min(len/2, sound_buffer.capacity);
			for (int i = 0; i < lenS; i++) {
				short s = sound_buffer.read();
				dst[off + i*2] = (byte) (s >> 8);
				dst[off + i*2 + 1] = (byte) s;
			}
			return lenS * 2;
		}
	}
	
	private void init_sound() {
		try {
			clip = AudioSystem.getClip();
			ais = new AudioInputStream(
					new SidInputStream(),
					new AudioFormat(
						SAMPLE_FREQ,
						8,	// sample size in bits
						1,		// channels
						true,	// signed
						false  // bigendian
						),
					Integer.MAX_VALUE);
			clip.open(ais);
			clip.start();
			ready = true;
		} catch (Exception e) {
			System.err.println("Failed to open audio");
			e.printStackTrace();
		}
	}
	private void calc_filter() {

//#ifdef USE_FIXPOINT_MATHS
//	FixPoint fr, arg;
//
//	if (f_type == FILT_ALL)
//	{
//		d1 = 0; d2 = 0; g1 = 0; g2 = 0; f_ampl = FixNo(1); return;
//	}
//	else if (f_type == FILT_NONE)
//	{
//		d1 = 0; d2 = 0; g1 = 0; g2 = 0; f_ampl = 0; return;
//        }
//#else
	double fr, arg;

	// Check for some trivial cases
	if (f_type == FILT_ALL) {
		d1 = 0.0; d2 = 0.0;
		g1 = 0.0; g2 = 0.0;
		f_ampl = 1.0;
		return;
	} else if (f_type == FILT_NONE) {
		d1 = 0.0; d2 = 0.0;
		g1 = 0.0; g2 = 0.0;
		f_ampl = 0.0;
		return;
	}
//#endif

	// Calculate resonance frequency
	if (f_type == FILT_LP || f_type == FILT_LPBP)
//#ifdef PRECOMPUTE_RESONANCE
		fr = resonanceLP[f_freq];
//#else
//		fr = CALC_RESONANCE_LP(f_freq);
//#endif
	else
//#ifdef PRECOMPUTE_RESONANCE
		fr = resonanceHP[f_freq];
//#else
//		fr = CALC_RESONANCE_HP(f_freq);
//#endif

//#ifdef USE_FIXPOINT_MATHS
//	// explanations see below.
//	arg = fr / (SAMPLE_FREQ >> 1);
//	if (arg > FixNo(0.99)) {arg = FixNo(0.99);}
//	if (arg < FixNo(0.01)) {arg = FixNo(0.01);}
//
//	g2 = FixNo(0.55) + FixNo(1.2) * arg * (arg - 1) + FixNo(0.0133333333) * f_res;
//	g1 = FixNo(-2) * g2.sqrt() * fixcos(arg);
//
//	if (f_type == FILT_LPBP || f_type == FILT_HPBP) {g2 += FixNo(0.1);}
//
//	if (g1.abs() >= g2 + 1)
//	{
//	  if (g1 > 0) {g1 = g2 + FixNo(0.99);}
//	  else {g1 = -(g2 + FixNo(0.99));}
//	}
//
//	switch (f_type)
//	{
//	  case FILT_LPBP:
//	  case FILT_LP:
//		d1 = FixNo(2); d2 = FixNo(1); f_ampl = FixNo(0.25) * (1 + g1 + g2); break;
//	  case FILT_HPBP:
//	  case FILT_HP:
//		d1 = FixNo(-2); d2 = FixNo(1); f_ampl = FixNo(0.25) * (1 - g1 + g2); break;
//	  case FILT_BP:
//		d1 = 0; d2 = FixNo(-1);
//		f_ampl = FixNo(0.25) * (1 + g1 + g2) * (1 + fixcos(arg)) / fixsin(arg);
//		break;
//	  case FILT_NOTCH:
//		d1 = FixNo(-2) * fixcos(arg); d2 = FixNo(1);
//		f_ampl = FixNo(0.25) * (1 + g1 + g2) * (1 + fixcos(arg)) / fixsin(arg);
//		break;
//	  default: break;
//	}
//
//#else

	// Limit to <1/2 sample frequency, avoid div by 0 in case FILT_BP below
	arg = fr / (double)(SAMPLE_FREQ >> 1);
	if (arg > 0.99)
		arg = 0.99;
	if (arg < 0.01)
		arg = 0.01;

	// Calculate poles (resonance frequency and resonance)
	g2 = 0.55 + 1.2 * arg * arg - 1.2 * arg + (float)f_res * 0.0133333333;
	g1 = -2.0 * sqrt(g2) * cos(PI * arg);

	// Increase resonance if LP/HP combined with BP
	if (f_type == FILT_LPBP || f_type == FILT_HPBP)
		g2 += 0.1;

	// Stabilize filter
	if (abs(g1) >= g2 + 1.0)
		if (g1 > 0.0)
			g1 = g2 + 0.99;
		else
			g1 = -(g2 + 0.99);

	// Calculate roots (filter characteristic) and input attenuation
	switch (f_type) {

		case FILT_LPBP:
		case FILT_LP:
			d1 = 2.0; d2 = 1.0;
			f_ampl = 0.25 * (1.0 + g1 + g2);
			break;

		case FILT_HPBP:
		case FILT_HP:
			d1 = -2.0; d2 = 1.0;
			f_ampl = 0.25 * (1.0 - g1 + g2);
			break;

		case FILT_BP:
			d1 = 0.0; d2 = -1.0;
			f_ampl = 0.25 * (1.0 + g1 + g2) * (1 + cos(PI * arg)) / sin(PI * arg);
			break;

		case FILT_NOTCH:
			d1 = -2.0 * cos(PI * arg); d2 = 1.0;
			f_ampl = 0.25 * (1.0 + g1 + g2) * (1 + cos(PI * arg)) / (sin(PI * arg));
			break;

		default:
			break;
	}
//#endif
	}
//#ifdef __riscos__
//	void calc_buffer(uint8 *buf, long count);
//#else
	private void calc_buffer(SPtr buf, int count) {

		// Get filter coefficients, so the emulator won't change
		// them in the middle of our calculations
//	#ifdef USE_FIXPOINT_MATHS
//		FixPoint cf_ampl = f_ampl;
//		FixPoint cd1 = d1, cd2 = d2, cg1 = g1, cg2 = g2;
//	#else
		double cf_ampl = f_ampl;
		double cd1 = d1, cd2 = d2, cg1 = g1, cg2 = g2;
//	#endif

//	#ifdef __riscos__
//		uint8 *LinToLog, *LogScale;
//	#endif

		// Index in sample_buf for reading, 16.16 fixed
		int sample_count = (sample_in_ptr + SAMPLE_BUF_SIZE/2) << 16;

//	#ifdef __riscos__	// on RISC OS we have 8 bit logarithmic sound
//		DigitalRenderer_GetTables(&LinToLog, &LogScale);	// get translation tables
//	#else
//	#ifdef __BEOS__
//		count >>= 2;	// 16 bit stereo output, count is in bytes
//	#else
		count >>= 1;	// 16 bit mono output, count is in bytes
//	#endif
//	#endif
		while ((count--) != 0) {
			int sum_output;
			int sum_output_filter = 0;

			// Get current master volume from sample buffer,
			// calculate sampled voice
			byte master_volume = sample_buf[(sample_count >> 16) % SAMPLE_BUF_SIZE];
			sample_count += ((0x138 * 50) << 16) / SAMPLE_FREQ;
			sum_output = SampleTab[master_volume] << 8;

			// Loop for all three voices
			for (int j=0; j<3; j++) {
				DRVoice v = voice[j];

				// Envelope generators
				short envelope;

				switch (v.eg_state) {
					case EG_ATTACK:
						v.eg_level += v.a_add;
						if (v.eg_level > 0xffffff) {
							v.eg_level = 0xffffff;
							v.eg_state = EG_DECAY;
						}
						break;
					case EG_DECAY:
						if (v.eg_level <= v.s_level || v.eg_level > 0xffffff)
							v.eg_level = v.s_level;
						else {
							v.eg_level -= v.d_sub >> EGDRShift[v.eg_level >> 16];
							if (v.eg_level <= v.s_level || v.eg_level > 0xffffff)
								v.eg_level = v.s_level;
						}
						break;
					case EG_RELEASE:
						v.eg_level -= v.r_sub >> EGDRShift[v.eg_level >> 16];
						if (v.eg_level > 0xffffff) {
							v.eg_level = 0;
							v.eg_state = EG_IDLE;
						}
						break;
					case EG_IDLE:
						v.eg_level = 0;
						break;
				}
				envelope = (short) ((v.eg_level * master_volume) >> 20);

				// Waveform generator
				short output;

				if (!v.test)
					v.count += v.add;

				if (v.sync && (v.count > 0x1000000))
					v.mod_to.count = 0;

				v.count &= 0xffffff;

				switch (v.wave) {
					case WAVE_TRI:
						if (v.ring)
							output = TriTable[(v.count ^ (v.mod_by.count & 0x800000)) >> 11];
						else
							output = TriTable[v.count >> 11];
						break;
					case WAVE_SAW:
						output = (short) (v.count >> 8);
						break;
					case WAVE_RECT:
						if (v.count > (int)(v.pw << 12))
							output = (short) 0xffff;
						else
							output = 0;
						break;
					case WAVE_TRISAW:
						output = TriSawTable[v.count >> 16];
						break;
					case WAVE_TRIRECT:
						if (v.count > (int)(v.pw << 12))
							output = TriRectTable[v.count >> 16];
						else
							output = 0;
						break;
					case WAVE_SAWRECT:
						if (v.count > (int)(v.pw << 12))
							output = SawRectTable[v.count >> 16];
						else
							output = 0;
						break;
					case WAVE_TRISAWRECT:
						if (v.count > (int)(v.pw << 12))
							output = TriSawRectTable[v.count >> 16];
						else
							output = 0;
						break;
					case WAVE_NOISE:
						if (v.count > 0x100000) {
							output = (short) (v.noise = sid_random() << 8);
							v.count &= 0xfffff;
						} else
							output = (short) v.noise;
						break;
					default:
						output = (short) 0x8000;
						break;
				}
				if (v.filter)
					sum_output_filter += (short)(output ^ 0x8000) * envelope;
				else
					sum_output += (short)(output ^ 0x8000) * envelope;
			}

			// Filter
			if (Prefs.ThePrefs.SIDFilters) {
//	#ifdef USE_FIXPOINT_MATHS
//				int32 xn = cf_ampl.imul(sum_output_filter);
//				int32 yn = xn+cd1.imul(xn1)+cd2.imul(xn2)-cg1.imul(yn1)-cg2.imul(yn2);
//				yn2 = yn1; yn1 = yn; xn2 = xn1; xn1 = xn;
//				sum_output_filter = yn;
//	#else
				double xn = sum_output_filter * cf_ampl;
				double yn = xn + cd1 * xn1 + cd2 * xn2 - cg1 * yn1 - cg2 * yn2;
				yn2 = yn1; yn1 = yn; xn2 = xn1; xn1 = xn;
				sum_output_filter = (int)yn;
//	#endif
			}

			// Write to buffer
//	#ifdef __BEOS__
//			int16 audio_data = (sum_output + sum_output_filter) >> 10;
//			int val = *buf + audio_data;
//			if (val > 32767)
//				val = 32767;
//			if (val < -32768)
//				val = -32768;
//			*buf++ = val;
//			val = *buf + audio_data;
//			if (val > 32767)
//				val = 32767;
//			if (val < -32768)
//				val = -32768;
//			*buf++ = val;
//	#elif defined(__riscos__)	// lookup in 8k (13bit) translation table
//			*buf++ = LinToLog[((sum_output + sum_output_filter) >> 13) & 0x1fff];
//	#else
			buf.setInc((short) ((sum_output + sum_output_filter) >> 10));
//	#endif
		}
	}
//#endif

	private boolean ready;						// Flag: Renderer has initialized and is ready
	private byte volume;					// Master volume
	private boolean v3_mute;					// Voice 3 muted

	private DRVoice[] voice = {
			new DRVoice(),				// Data for 3 voices
			new DRVoice(),
			new DRVoice()
	};

	private FilterType f_type;					// Filter type
	private byte f_freq;					// SID filter frequency (upper 8 bits)
	private byte f_res;					// Filter resonance (0..15)
	
//	#ifdef USE_FIXPOINT_MATHS
//		FixPoint f_ampl;
//		FixPoint d1, d2, g1, g2;
//		int32 xn1, xn2, yn1, yn2;		// can become very large
//		FixPoint sidquot;
//	#ifdef PRECOMPUTE_RESONANCE
//		FixPoint resonanceLP[256];
//		FixPoint resonanceHP[256];
//	#endif
//	#else
	double f_ampl;					// IIR filter input attenuation
	double d1, d2, g1, g2;			// IIR filter coefficients
	double xn1, xn2, yn1, yn2;		// IIR filter previous input/output signal
//	#ifdef PRECOMPUTE_RESONANCE
	double[] resonanceLP = new double[256];			// shortcut for calc_filter
	double[] resonanceHP = new double[256];
//	#endif
//	#endif

	byte[] sample_buf = new byte[SAMPLE_BUF_SIZE]; // Buffer for sampled voice
	int sample_in_ptr;				// Index in sample_buf for writing

	// "New" sid chip
	private static boolean EMUL_MOS8580 = false;
	
	private short[] TriTable;
	private short[] TriSawTable, TriRectTable, SawRectTable, TriSawRectTable;
	
	{
		if (EMUL_MOS8580) {
			TriSawTable = toShort(new int[] {
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0808,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x1010, 0x3C3C,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0808,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x1010, 0x3C3C
			});
			TriRectTable = toShort(new int[] {
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080, 0xC0C0,
				0x0000, 0x8080, 0x8080, 0xE0E0, 0x8080, 0xE0E0, 0xF0F0, 0xFCFC,
				0xFFFF, 0xFCFC, 0xFAFA, 0xF0F0, 0xF6F6, 0xE0E0, 0xE0E0, 0x8080,
				0xEEEE, 0xE0E0, 0xE0E0, 0x8080, 0xC0C0, 0x0000, 0x0000, 0x0000,
				0xDEDE, 0xC0C0, 0xC0C0, 0x0000, 0x8080, 0x0000, 0x0000, 0x0000,
				0x8080, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0xBEBE, 0x8080, 0x8080, 0x0000, 0x8080, 0x0000, 0x0000, 0x0000,
				0x8080, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x7E7E, 0x4040, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
			});
			SawRectTable = toShort(new int[] {
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x7878,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x7878
			});
			TriSawRectTable = toShort(new int[] {
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
				0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
			});
		} else {
			// Sampled from an 8580R5
			TriSawTable = toShort(new int[] {
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0808,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x1818, 0x3C3C,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x1C1C,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x8080, 0x0000, 0x8080, 0x8080,
					0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xE0E0,
					0xF0F0, 0xF0F0, 0xF0F0, 0xF0F0, 0xF8F8, 0xF8F8, 0xFCFC, 0xFEFE
				});

				TriRectTable = toShort(new int[] {
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0xFFFF, 0xFCFC, 0xF8F8, 0xF0F0, 0xF4F4, 0xF0F0, 0xF0F0, 0xE0E0,
					0xECEC, 0xE0E0, 0xE0E0, 0xC0C0, 0xE0E0, 0xC0C0, 0xC0C0, 0xC0C0,
					0xDCDC, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0x8080, 0x8080,
					0xC0C0, 0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0x0000, 0x0000,
					0xBEBE, 0xA0A0, 0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0x0000,
					0x8080, 0x8080, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x8080, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x7E7E, 0x7070, 0x6060, 0x0000, 0x4040, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
				});

				SawRectTable = toShort(new int[] {
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080, 0x8080,
					0x0000, 0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0xB0B0, 0xBEBE,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080,
					0x0000, 0x0000, 0x0000, 0x8080, 0x8080, 0x8080, 0x8080, 0xC0C0,
					0x0000, 0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0xC0C0,
					0x8080, 0x8080, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xDCDC,
					0x8080, 0x8080, 0x8080, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0, 0xC0C0,
					0xC0C0, 0xC0C0, 0xC0C0, 0xE0E0, 0xE0E0, 0xE0E0, 0xE0E0, 0xECEC,
					0xC0C0, 0xE0E0, 0xE0E0, 0xE0E0, 0xE0E0, 0xF0F0, 0xF0F0, 0xF4F4,
					0xF0F0, 0xF0F0, 0xF8F8, 0xF8F8, 0xF8F8, 0xFCFC, 0xFEFE, 0xFFFF
				});

				TriSawRectTable = toShort(new int[] {
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000,
					0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x8080, 0x8080,
					0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0x8080, 0xC0C0, 0xC0C0,
					0xC0C0, 0xC0C0, 0xE0E0, 0xE0E0, 0xE0E0, 0xF0F0, 0xF8F8, 0xFCFC
				});
		}
	}
	
	private static int[] EGTable = {
			(SID_CYCLES << 16) / 9, (SID_CYCLES << 16) / 32,
			(SID_CYCLES << 16) / 63, (SID_CYCLES << 16) / 95,
			(SID_CYCLES << 16) / 149, (SID_CYCLES << 16) / 220,
			(SID_CYCLES << 16) / 267, (SID_CYCLES << 16) / 313,
			(SID_CYCLES << 16) / 392, (SID_CYCLES << 16) / 977,
			(SID_CYCLES << 16) / 1954, (SID_CYCLES << 16) / 3126,
			(SID_CYCLES << 16) / 3906, (SID_CYCLES << 16) / 11720,
			(SID_CYCLES << 16) / 19531, (SID_CYCLES << 16) / 31251
		};
	 
	private static byte[] EGDRShift = {
			5,5,5,5,5,5,5,5,4,4,4,4,4,4,4,4,
			3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,
			2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
			2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,
			1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
			1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
			0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		};

	private static short[] SampleTab = toShort(new int[] {
			0x8000, 0x9111, 0xa222, 0xb333, 0xc444, 0xd555, 0xe666, 0xf777,
			0x0888, 0x1999, 0x2aaa, 0x3bbb, 0x4ccc, 0x5ddd, 0x6eee, 0x7fff,
		});


	private static short[] toShort(int[] a) {
		short[] result = new short[a.length];
		for (int i = 0; i < a.length; i++) {
			result[i] = (short) a[i];
		}
		return result;
	}

	private static double CALC_RESONANCE_LP(double f) {
		return 227.755
			- 1.7635 * f
			- 0.0176385 * f * f
			+ 0.00333484 * f * f * f
			- 9.05683E-6 * f * f * f * f;
	}

	private static double CALC_RESONANCE_HP(double f) {
		return 366.374
			- 14.0052 * f
			+ 0.603212 * f * f
			- 0.000880196 * f * f * f;
	}
	static int seed = 1;
	private static byte sid_random()
	{
		seed = seed * 1103515245 + 12345;
		return (byte) (seed >> 16);
	}
	
//#ifdef __BEOS__
//	static bool stream_func(void *arg, char *buf, size_t count, void *header);
//	C64 *the_c64;					// Pointer to C64 object
//	BDACStream *the_stream;			// Pointer to stream
//	BSubscriber *the_sub;			// Pointer to subscriber
//	bool in_stream;					// Flag: Subscriber has entered stream
//#endif
//
//#ifdef AMIGA
//	static void sub_invoc(void);	// Sound sub-process
//	void sub_func(void);
//	struct Process *sound_process;
//	int quit_sig, pause_sig,
//		resume_sig, ahi_sig;		// Sub-process signals
//	struct Task *main_task;			// Main task
//	int main_sig;					// Main task signals
//	static ULONG sound_func(void);	// AHI callback
//	struct MsgPort *ahi_port;		// Port and IORequest for AHI
//	struct AHIRequest *ahi_io;
//	struct AHIAudioCtrl *ahi_ctrl;	// AHI control structure
//	struct AHISampleInfo sample[2];	// SampleInfos for double buffering
//	struct Hook sf_hook;			// Hook for callback function
//	int play_buf;					// Number of buffer currently playing
//#endif
//
//#ifdef __linux__
//	int devfd, sndbufsize, buffer_rate;
//	int16 *sound_buffer;
//#endif
//
//#ifdef SUN
//	int fd;
//	audio_info status;
//	uint_t sent_samples,delta_samples;
//	WORD *sound_calc_buf;
//#endif
//
//#ifdef __hpux
//	int fd;
//	audio_status status;
//	int16 * sound_calc_buf;
//	int linecnt;
//#endif
//
//#ifdef __mac__
//	SndChannelPtr chan1;
//	SndDoubleBufferHeader myDblHeader;
//	SndDoubleBufferPtr sampleBuffer1, sampleBuffer2;
//	SCStatus myStatus;
//	short sndbufsize;
//	OSErr err;
//
//	static void doubleBackProc(SndChannelPtr chan, SndDoubleBufferPtr doubleBuffer);
//#endif
//
//#ifdef WIN32
//public:
//	void VBlank(void);
//
//private:
//	void StartPlayer(void);
//	void StopPlayer(void);
//
//	BOOL direct_sound;
//	DigitalPlayer *ThePlayer;
//	SWORD *sound_buffer;
//	int to_output;
//	int sb_pos;
//	int divisor;
//	int *lead;
//	int lead_pos;
//#endif
//
//#ifdef __riscos__
//	int linecnt, sndbufsize;
//	uint8 *sound_buffer;
//	C64 *the_c64;
//#endif

}
