package se.pp.forsberg.frodo;

import se.pp.forsberg.libc.BPtr;

class MOS6526State {

	public final static int STATE_SIZE = 25;
	
	public byte pra;
	public byte ddra;
	public byte prb;
	public byte ddrb;
	public byte ta_lo;
	public byte ta_hi;
	public byte tb_lo;
	public byte tb_hi;
	public byte tod_10ths;
	public byte tod_sec;
	public byte tod_min;
	public byte tod_hr;
	public byte sdr;
	public byte int_data;		// Pending interrupts
	public byte cra;
	public byte crb;
						// Additional registers
	public short latcha;		// Timer latches
	public short latchb;
	public byte alm_10ths;	// Alarm time
	public byte alm_sec;
	public byte alm_min;
	public byte alm_hr;
	public byte int_mask;		// Enabled interrupts

	public MOS6526State() {}
	
	public MOS6526State(byte[] buf) {
		BPtr bp = new BPtr(buf);
		
		pra = bp.getInc(); ddra = bp.getInc(); prb = bp.getInc(); ddrb = bp.getInc(); ta_lo = bp.getInc(); ta_hi = bp.getInc(); tb_lo = bp.getInc(); tb_hi = bp.getInc();
		tod_10ths = bp.getInc(); tod_sec = bp.getInc(); tod_min = bp.getInc(); tod_hr = bp.getInc(); sdr = bp.getInc(); int_data = bp.getInc(); cra = bp.getInc(); crb = bp.getInc();
		latcha = bp.getIncShort(); latchb = bp.getIncShort(); alm_10ths = bp.getInc(); alm_sec = bp.getInc(); alm_min = bp.getInc(); alm_hr = bp.getInc();
		int_mask = bp.getInc();
	}

	public byte[] toBytes() {
		byte[] buf = new  byte[STATE_SIZE];
		BPtr bp = new BPtr(buf);
		
		bp.setInc(pra); bp.setInc(ddra); bp.setInc(prb); bp.setInc(ddrb); bp.setInc(ta_lo); bp.setInc(ta_hi); bp.setInc(tb_lo); bp.setInc(tb_hi);
		bp.setInc(tod_10ths); bp.setInc(tod_sec); bp.setInc(tod_min); bp.setInc(tod_hr); bp.setInc(sdr); bp.setInc(int_data); bp.setInc(cra); bp.setInc(crb);
		bp.setInc(latcha); bp.setInc(latchb); bp.setInc(alm_10ths); bp.setInc(alm_sec); bp.setInc(alm_min); bp.setInc(alm_hr);
		bp.setInc(int_mask);
		
		return buf;
	}
}
