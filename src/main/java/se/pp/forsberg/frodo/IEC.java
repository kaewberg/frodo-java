package se.pp.forsberg.frodo;

import static se.pp.forsberg.frodo.enums.DriveLEDState.*;
import static se.pp.forsberg.frodo.enums.DriveType.*;

import se.pp.forsberg.frodo.prefs.Prefs;
import se.pp.forsberg.libc.BPtr;
import se.pp.forsberg.libc.ByteC;

class IEC {
	
	public final static int NAMEBUF_LENGTH = 256;
	private final static byte CMD_DATA = 0x60;	// Data transfer
	private final static byte CMD_CLOSE = (byte) 0xe0;	// Close channel
	private final static byte CMD_OPEN = (byte) 0xf0;		// Open channel

	// C64 status codes
	private final static byte ST_OK = 0;				// No error
	@SuppressWarnings("unused")
	private final static byte ST_READ_TIMEOUT	= 0x02;	// Timeout on reading
	private final static byte ST_TIMEOUT = 0x03;		// Timeout
	@SuppressWarnings("unused")
	private final static byte ST_EOF = 0x40;			// End of file
	private final static byte ST_NOTPRESENT = (byte) 0x80;	// Device not present
	
	// IEC ATN codes
	private final static byte ATN_LISTEN = 0x20;
	private final static byte ATN_UNLISTEN = 0x30;
	private final static byte ATN_TALK = 0x40;
	private final static byte ATN_UNTALK = 0x50;
	
	public IEC(C64Display display) {
		int i;

		the_display = display;
		// Create drives 8..11
		for (i=0; i<4; i++)
			drive[i] = null;	// Important because UpdateLEDs is called from the drive constructors (via set_error)

		if (!Prefs.ThePrefs.Emul1541Proc)
			for (i=0; i<4; i++) {
				if (Prefs.ThePrefs.DriveType[i] == DRVTYPE_DIR)
					drive[i] = new FSDrive(this, Prefs.ThePrefs.DrivePath[i]);
				else if (Prefs.ThePrefs.DriveType[i] == DRVTYPE_D64)
					drive[i] = new D64Drive(this, Prefs.ThePrefs.DrivePath[i]);
				else
					drive[i] = new T64Drive(this, Prefs.ThePrefs.DrivePath[i]);
			}

		listener_active = talker_active = false;
		listening = false;
	}
//		~IEC()
//	{
//		for (int i=0; i<4; i++)
//			delete drive[i];
//	}

	public void Reset() {

		for (int i=0; i<4; i++)
			if (drive[i] != null && drive[i].Ready)
				drive[i].Reset();

		UpdateLEDs();
	}
	public void NewPrefs(Prefs prefs) {
		// Delete and recreate all changed drives
		for (int i=0; i<4; i++)
			if ((Prefs.ThePrefs.DriveType[i] != prefs.DriveType[i]) || !Prefs.ThePrefs.DrivePath[i].equals(prefs.DrivePath[i]) || Prefs.ThePrefs.Emul1541Proc != prefs.Emul1541Proc) {
				drive[i] = null;	// Important because UpdateLEDs is called from drive constructors (via set_error())
				if (!prefs.Emul1541Proc) {
					if (prefs.DriveType[i] == DRVTYPE_DIR)
						drive[i] = new FSDrive(this, prefs.DrivePath[i]);
					else if (prefs.DriveType[i] == DRVTYPE_D64)
						drive[i] = new D64Drive(this, prefs.DrivePath[i]);
					else
						drive[i] = new T64Drive(this, prefs.DrivePath[i]);
				}
			}

		UpdateLEDs();
	}
	public void UpdateLEDs() {
		if (drive[0] != null && drive[1] != null && drive[2] != null && drive[3] != null)
			the_display.UpdateLEDs(drive[0].LED, drive[1].LED, drive[2].LED, drive[3].LED);
	}

	public byte Out(byte b, boolean eoi) {
		if (listener_active) {
			if (received_cmd == CMD_OPEN)
				return open_out(b, eoi);
			if (received_cmd == CMD_DATA)
				return data_out(b, eoi);
			return ST_TIMEOUT;
		} else
			return ST_TIMEOUT;
	}
	public byte OutATN(byte b) {
		received_cmd = sec_addr = 0;	// Command is sent with secondary address
		switch (b & 0xf0) {
			case ATN_LISTEN:
				listening = true;
				return listen(b & 0x0f);
			case ATN_UNLISTEN:
				listening = false;
				return unlisten();
			case ATN_TALK:
				listening = false;
				return talk(b & 0x0f);
			case ATN_UNTALK:
				listening = false;
				return untalk();
		}
		return ST_TIMEOUT;
	}
	public byte OutSec(byte b) {
		if (listening) {
			if (listener_active) {
				sec_addr = (byte) (b & 0x0f);
				received_cmd = (byte) (b & 0xf0);
				return sec_listen();
			}
		} else {
			if (talker_active) {
				sec_addr = (byte) (b & 0x0f);
				received_cmd = (byte) (b & 0xf0);
				return sec_talk();
			}
		}
		return ST_TIMEOUT;
	}
	public class Bytes {
		byte retval, b;
	}
	public byte In(ByteC b) {
		if (talker_active && (received_cmd == CMD_DATA))
			return data_in(b);

		b.b = 0;
		return ST_TIMEOUT;
	}
	public void SetATN() {
		// Only needed for real IEC
	}
	public void RelATN() {
		// Only needed for real IEC
	}
	public void Turnaround() {
		// Only needed for real IEC
	}
	public void Release() {
		// Only needed for real IEC
	}

	private byte listen(int device) {
		if ((device >= 8) && (device <= 11)) {
			if ((listener = drive[device-8]) != null && listener.Ready) {
				listener_active = true;
				return ST_OK;
			}
		}

		listener_active = false;
		return ST_NOTPRESENT;
	}
	private byte talk(int device) {
		if ((device >= 8) && (device <= 11)) {
			if ((talker = drive[device-8]) != null && talker.Ready) {
				talker_active = true;
				return ST_OK;
			}
		}

		talker_active = false;
		return ST_NOTPRESENT;
	}
	private byte unlisten() {
		listener_active = false;
		return ST_OK;
	}
	private byte untalk() {
		talker_active = false;
		return ST_OK;
	}
	private byte sec_listen() {
		switch (received_cmd) {

		case CMD_OPEN:	// Prepare for receiving the file name
//			name_ptr = name_buf;
			name_ptr.offs = 0;
			name_len = 0;
			return ST_OK;

		case CMD_CLOSE: // Close channel
			if (listener.LED != DRVLED_ERROR) {
				listener.LED = DRVLED_OFF;		// Turn off drive LED
				UpdateLEDs();
			}
			return listener.Close(sec_addr);
	}
	return ST_OK;
	}
	private byte sec_talk() {
		return ST_OK;
	}
	private byte open_out(byte b, boolean eoi) {
		if (name_len < NAMEBUF_LENGTH) {
			name_ptr.setInc(b);
//			*name_ptr++ = byte;
			name_len++;
		}

		if (eoi) {
			name_ptr.set((byte) 0);
//			*name_ptr = 0;				// End string
			listener.LED = DRVLED_ON;	// Turn on drive LED
			UpdateLEDs();
			return listener.Open(sec_addr, name_buf);
		}

		return ST_OK;
	}
	private byte data_out(byte b, boolean eoi) {
		return listener.Write(sec_addr, b, eoi);
	}
	private byte data_in(ByteC b) {
		return talker.Read(sec_addr, b);
	}

	private C64Display the_display;	// Pointer to display object (for drive LEDs)

	byte[] name_buf = new byte[NAMEBUF_LENGTH];
	BPtr name_ptr = new BPtr(name_buf);
//		char name_buf[NAMEBUF_LENGTH];	// Buffer for file names and command strings
//		char *name_ptr;			// Pointer for reception of file name
		int name_len;			// Received length of file name

		private Drive[] drive = new Drive[4];		// 4 drives (8..11)

		private Drive listener;		// Pointer to active listener
		private Drive talker;			// Pointer to active talker

		private boolean listener_active;	// Listener selected, listener_data is valid
		private boolean talker_active;		// Talker selected, talker_data is valid
		private boolean listening;			// Last ATN was listen (to decide between sec_listen/sec_talk)

		private byte received_cmd;		// Received command code ($x0)
		private byte sec_addr;			// Received secondary address ($0x)
}
