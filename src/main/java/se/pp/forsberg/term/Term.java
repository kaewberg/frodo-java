package se.pp.forsberg.term;

import java.io.InputStream;
import java.io.OutputStream;

import se.pp.forsberg.libc.StdioFile;

public interface Term {
	OutputStream getOut();
	OutputStream getErr();
	InputStream getIn();
	default StdioFile getStdOut() {
		return StdioFile.fdopen(getOut());
	}
	default StdioFile getStdErr() {
		return StdioFile.fdopen(getErr());
	}
	default StdioFile getStdIn() {
		return StdioFile.fdopen(getIn());
	}
}
