﻿Imports System.IO

Public Class Main

    Public Shared Sub Main()
        Dim fi As FileInfo = FindLauncher(New DirectoryInfo("."))

        Dim p As Process = New Process()
        p.StartInfo.UseShellExecute = True
        p.StartInfo.CreateNoWindow = True
        p.StartInfo.FileName = fi.FullName
        ' p.StartInfo.RedirectStandardOutput = True
        ' p.StartInfo.RedirectStandardInput = True
        ' p.StartInfo.RedirectStandardError = True

        p.Start()
    End Sub

    Private Shared Function FindLauncher(di As DirectoryInfo) As FileInfo
        If di Is Nothing Then
            Return Nothing
        End If
        Dim f As FileInfo
        f = New FileInfo(di.FullName + "\target\Frodo\bin\launcher.bat")
        If f.Exists Then
            Return f
        End If
        f = New FileInfo(di.FullName + "\bin\launcher.bat")
        If f.Exists Then
            Return f
        End If
        Return FindLauncher(di.Parent)
    End Function
End Class
